-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 25 juil. 2019 à 14:03
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `manzer_partazer`
--

-- --------------------------------------------------------

--
-- Structure de la table `action_consommateur`
--

DROP TABLE IF EXISTS `action_consommateur`;
CREATE TABLE IF NOT EXISTS `action_consommateur` (
  `id_action_consommateur` int(11) NOT NULL AUTO_INCREMENT,
  `vue_produit` tinyint(2) DEFAULT NULL,
  `favoris` tinyint(2) DEFAULT NULL,
  `consommateur_id` int(11) NOT NULL,
  `donation_id` int(11) NOT NULL,
  PRIMARY KEY (`id_action_consommateur`),
  KEY `fk_action_consommateur_consommateur1_idx` (`consommateur_id`),
  KEY `fk_action_consommateur_donation1_idx` (`donation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `admin_fw`
--

DROP TABLE IF EXISTS `admin_fw`;
CREATE TABLE IF NOT EXISTS `admin_fw` (
  `id_admin_fw` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `pseudo` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `utilisateur_id` int(11) NOT NULL,
  PRIMARY KEY (`id_admin_fw`),
  KEY `fk_admin_fw_utilisateur1_idx` (`utilisateur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

DROP TABLE IF EXISTS `adresse`;
CREATE TABLE IF NOT EXISTS `adresse` (
  `id_adresse` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `latitude` double DEFAULT '0',
  `longitude` double DEFAULT '0',
  `code_postal` varchar(45) DEFAULT NULL,
  `ville` varchar(45) DEFAULT NULL,
  `pays_id` int(11) NOT NULL,
  PRIMARY KEY (`id_adresse`),
  KEY `fk_adresse_pays1_idx` (`pays_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `adresse`
--

INSERT INTO `adresse` (`id_adresse`, `label`, `latitude`, `longitude`, `code_postal`, `ville`, `pays_id`) VALUES
(79, 'Analakely', -18.91396561829579, 47.52424680682046, '101', 'Antananarivo', 1),
(80, 'Ambanidia', -18.9206177799981, 47.5329428259283, '101', 'Antananarivo', 1),
(81, 'Ambondrona', -18.904065055585, 47.52602012828, '101', 'Antananarivo', 1),
(82, 'Anosy', -18.916398141045, 47.518637431785, '101', 'Antananarivo', 1),
(85, 'Itaosy', -18.9178165929242, 47.4724266584963, '101', 'Antananarivo', 1),
(86, 'Ankorondrano', -18.886758078915, 47.5212985184044, '101', 'Antananarivo', 1),
(87, 'Talatamaty', -18.840807381348, 47.461742749438, '101', 'Antananarivo', 1),
(89, 'Behoririka', -18.9021320755859, 47.5246026646346, '101', 'Antananarivo', 1),
(90, 'Ankatso', 0, 0, '101', 'Antananarivo', 1),
(91, 'Antanambao', -18.22030435768, 49.300573468208, '200', 'Toamasina', 1),
(92, 'Ivato', -18.805635592221, 47.474742182296, '101', 'Antananarivo', 1);

-- --------------------------------------------------------

--
-- Structure de la table `adresse_entreprise`
--

DROP TABLE IF EXISTS `adresse_entreprise`;
CREATE TABLE IF NOT EXISTS `adresse_entreprise` (
  `id_adresse_entreprise` int(11) NOT NULL AUTO_INCREMENT,
  `entreprise_id` int(11) NOT NULL,
  `adresse_id` int(11) NOT NULL,
  PRIMARY KEY (`id_adresse_entreprise`),
  KEY `fk_adresse_entreprise_entreprise1_idx` (`entreprise_id`),
  KEY `fk_adresse_entreprise_adresse1_idx` (`adresse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `adresse_entreprise`
--

INSERT INTO `adresse_entreprise` (`id_adresse_entreprise`, `entreprise_id`, `adresse_id`) VALUES
(79, 1, 79),
(80, 1, 80),
(81, 1, 81),
(82, 1, 82),
(85, 2, 85),
(86, 2, 86),
(87, 2, 87),
(89, 3, 89),
(90, 3, 91),
(91, 1, 92);

-- --------------------------------------------------------

--
-- Structure de la table `beneficiaire_organisation`
--

DROP TABLE IF EXISTS `beneficiaire_organisation`;
CREATE TABLE IF NOT EXISTS `beneficiaire_organisation` (
  `id_beneficiaire_organisation` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `proprietaire` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_beneficiaire_organisation`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `beneficiaire_organisation`
--

INSERT INTO `beneficiaire_organisation` (`id_beneficiaire_organisation`, `label`, `proprietaire`) VALUES
(1, 'Orphelins', 0),
(2, 'Enfants handicapés', 0),
(3, 'Enfants défavorisés', 0);

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `id_commande` int(11) NOT NULL AUTO_INCREMENT,
  `consommateur_id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `etat` int(11) NOT NULL,
  PRIMARY KEY (`id_commande`),
  KEY `fk_commande_consommateur1_idx` (`consommateur_id`),
  KEY `fk_commande_etat_commande1_idx` (`etat`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id_commande`, `consommateur_id`, `date`, `etat`) VALUES
(1, 1, '2019-07-11 00:00:00', 2),
(2, 2, '2019-07-17 00:00:00', 1),
(3, 1, '2019-07-17 00:00:00', 2);

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

DROP TABLE IF EXISTS `compte`;
CREATE TABLE IF NOT EXISTS `compte` (
  `id_compte` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) DEFAULT NULL,
  `pays_id` int(11) NOT NULL,
  PRIMARY KEY (`id_compte`),
  KEY `fk_compte_pays1_idx` (`pays_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `compte`
--

INSERT INTO `compte` (`id_compte`, `label`, `pays_id`) VALUES
(1, 'Orange money', 1),
(2, 'MVola', 1),
(3, 'Airtel Money', 1),
(4, 'BOA Madagascar', 1),
(5, 'BFV', 1),
(6, 'BNI', 1),
(7, 'BMOI', 1),
(10, 'SBM Madagascar', 1),
(11, 'MCB Madagascar', 1),
(14, 'AccèsBanque Madagascar', 1),
(15, 'Microcred Madagascar', 1),
(16, 'BFCOI', 2),
(17, 'Banque de la Réunion', 2),
(18, 'Crédit Agricole de la Réunion', 2),
(19, 'BNP Paribas', 2),
(20, 'La Banque Postale', 2),
(21, 'BRED Banque populaire', 2),
(22, 'Mauritius Commercial Bank', 3),
(23, 'Banque de Maurice', 3),
(24, 'State Bank of Mauritius', 3),
(25, 'Barclays', 3),
(26, 'Standard Bank', 3),
(27, 'HSBC', 3),
(28, 'Bank One', 3),
(29, 'Investec', 3),
(30, 'Banque Centrale des Comores', 4),
(31, 'Banque Fédérale de Commerce Comores', 4),
(32, 'Exim Bank Comores', 4),
(33, 'Banque pour l\'industrie et le commerce Comores', 4),
(34, 'Orange Bank', 2);

-- --------------------------------------------------------

--
-- Structure de la table `consommateur`
--

DROP TABLE IF EXISTS `consommateur`;
CREATE TABLE IF NOT EXISTS `consommateur` (
  `id_consommateur` int(11) NOT NULL AUTO_INCREMENT,
  `utilisateur_id` int(11) NOT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `prenom` varchar(100) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `mobile_banking` varchar(100) DEFAULT NULL,
  `adresse_de_livraison` varchar(255) DEFAULT NULL,
  `numero_telephone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_consommateur`),
  KEY `fk_consommateur_utilisateur1_idx` (`utilisateur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `consommateur`
--

INSERT INTO `consommateur` (`id_consommateur`, `utilisateur_id`, `nom`, `prenom`, `mail`, `mobile_banking`, `adresse_de_livraison`, `numero_telephone`) VALUES
(1, 1, 'Lico nom', 'Lico prenom', 'lico@gmail.com', '00000000', 'logement', '033333333333'),
(2, 3, 'korti name', 'korti last name', 'korti@gmail.com', '000000000000000000', 'analakely', '00000000000');

-- --------------------------------------------------------

--
-- Structure de la table `contrat`
--

DROP TABLE IF EXISTS `contrat`;
CREATE TABLE IF NOT EXISTS `contrat` (
  `id_contrat` int(11) NOT NULL AUTO_INCREMENT,
  `fichier` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `entreprise_id` int(11) NOT NULL,
  `validity` varchar(55) DEFAULT NULL,
  `employe_charge_id` int(11) NOT NULL,
  `etat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_contrat`),
  KEY `fk_contrat_entreprise1_idx` (`entreprise_id`),
  KEY `fk_contrat_employe1_idx` (`employe_charge_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `contrat`
--

INSERT INTO `contrat` (`id_contrat`, `fichier`, `type`, `entreprise_id`, `validity`, `employe_charge_id`, `etat`) VALUES
(1, 'uploads/contrats/1/20190409131320_protocole_entente_MG_Lico_MP.docx', 'entente', 1, '24 mois, renouvelable', 2, 1),
(2, 'uploads/contrats/3/20190419112409_protocole_entente_MG_Korti_MP.docx', 'entente', 3, '24 mois, renouvelable', 6, 1);

-- --------------------------------------------------------

--
-- Structure de la table `donation`
--

DROP TABLE IF EXISTS `donation`;
CREATE TABLE IF NOT EXISTS `donation` (
  `id_donation` int(11) NOT NULL AUTO_INCREMENT,
  `entreprise_id` int(11) NOT NULL,
  `organisation_id` int(11) DEFAULT NULL,
  `produit_id_produit` int(11) DEFAULT NULL,
  `moment` datetime DEFAULT NULL,
  `quantite` int(11) DEFAULT NULL,
  `date_peremption_produit` datetime DEFAULT NULL,
  `etat_donation_id` int(11) NOT NULL,
  `action` int(11) NOT NULL DEFAULT '1' COMMENT '1:donation - 2:vente',
  `type_date` int(11) DEFAULT '1' COMMENT '1:peremption - 2:production (description du champ date_peremption)',
  `minimum_qte_vente` int(11) DEFAULT '-1' COMMENT '-1:sans',
  `adresse_ramassage_id` int(11) DEFAULT NULL COMMENT '-1',
  `pourcentage_reduction` int(11) DEFAULT '0' COMMENT '0:sans - max:100',
  PRIMARY KEY (`id_donation`),
  KEY `fk_donation_organisation1_idx` (`organisation_id`),
  KEY `fk_donation_produit1_idx` (`produit_id_produit`),
  KEY `fk_donation_entreprise1_idx` (`entreprise_id`),
  KEY `fk_donation_etat_donation1_idx` (`etat_donation_id`),
  KEY `fk_donation_adresse1_idx` (`adresse_ramassage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `donation`
--

INSERT INTO `donation` (`id_donation`, `entreprise_id`, `organisation_id`, `produit_id_produit`, `moment`, `quantite`, `date_peremption_produit`, `etat_donation_id`, `action`, `type_date`, `minimum_qte_vente`, `adresse_ramassage_id`, `pourcentage_reduction`) VALUES
(1, 2, NULL, 1, '2019-04-18 17:58:24', 1, '2019-04-27 00:00:00', 2, 2, 1, 1, 79, 5),
(2, 1, NULL, 2, '2019-04-18 18:03:26', 10, '2019-04-05 00:00:00', 2, 2, 2, 1, 79, 20),
(3, 1, NULL, 5, '2019-04-18 18:17:41', 10, '2019-04-13 00:00:00', 5, 2, 2, 10, 81, 50),
(4, 1, NULL, 6, '2019-04-18 18:20:52', 10, '2019-04-27 00:00:00', 2, 2, 1, 5, 82, 2),
(5, 2, NULL, 7, '2019-04-19 15:48:59', 50, '2019-04-06 00:00:00', 2, 2, 1, 10, 87, 10),
(6, 3, 1, 7, '2019-01-19 16:01:22', 5, '2019-04-26 00:00:00', 2, 1, 2, -1, NULL, 0),
(7, 3, NULL, 9, '2019-04-19 16:08:16', 69, '2019-04-27 00:00:00', 2, 2, 2, 14, 91, 25),
(8, 1, NULL, 10, '2019-04-27 22:26:30', 20, '2019-04-18 00:00:00', 2, 2, 2, 2, 79, 25),
(9, 3, NULL, NULL, '2019-04-27 22:29:33', NULL, '2019-04-18 00:00:00', 5, 2, 2, 2, 79, 25),
(10, 1, NULL, 12, '2019-04-27 22:42:41', 65, '2019-04-13 00:00:00', 2, 2, 1, 10, 79, 20),
(11, 2, 1, 13, '2019-01-27 00:00:00', 1, '2019-04-25 00:00:00', 2, 1, 2, 2, NULL, 15),
(12, 1, NULL, 14, '2019-04-27 23:16:18', 15, '2019-05-01 00:00:00', 2, 2, 2, 1, 92, 10),
(13, 1, 2, 15, '2019-05-21 11:45:17', 20, '2019-06-30 00:00:00', 2, 1, 1, 5, 79, 12),
(14, 1, 2, 8, '2019-06-18 00:00:00', 50, '2019-07-16 00:00:00', 2, 1, 2, -1, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

DROP TABLE IF EXISTS `employe`;
CREATE TABLE IF NOT EXISTS `employe` (
  `id_employe` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `sexe` varchar(5) DEFAULT NULL,
  `poste_id` int(11) NOT NULL,
  PRIMARY KEY (`id_employe`),
  KEY `fk_employe_poste1_idx` (`poste_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `employe`
--

INSERT INTO `employe` (`id_employe`, `nom`, `prenom`, `email`, `telephone`, `sexe`, `poste_id`) VALUES
(1, 'Lico ', 'Jean ', 'andriamhajanirinacedric@gmail.com', '+261380012345', 'M', 1),
(2, 'Rodrigo', 'Morales', 'rmorales@mail.com', '+261381203541', 'M', 6),
(3, 'Talata', 'maty', 'tlmt@mail.com', '+261338012345', 'M', 6),
(4, 'Madoo', 'dabo', 'doo@do.co', '+261380025689', 'M', 7),
(5, 'Kortiko', 'koko', 'andriamhajanirinacedric@gmail.com', '+261380012547', 'M', 6),
(6, 'Koit', 'moli', 'koit@ko.ko', '+261372574198', 'M', 6),
(7, 'Rakoto', 'benef', 'benef@bene.be', '+261358912354', 'M', 3);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

DROP TABLE IF EXISTS `entreprise`;
CREATE TABLE IF NOT EXISTS `entreprise` (
  `id_entreprise` int(11) NOT NULL AUTO_INCREMENT,
  `nom_entreprise` varchar(50) NOT NULL,
  `ca_entreprise` double DEFAULT '0',
  `qte_kg` double DEFAULT '0',
  `qte_mga` double DEFAULT '0',
  `logo` varchar(255) DEFAULT '""',
  `utilisateur_id` int(11) NOT NULL,
  `nif_entreprise` varchar(45) DEFAULT NULL,
  `stat_entreprise` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_entreprise`),
  KEY `fk_entreprise_utilisateur1_idx` (`utilisateur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`id_entreprise`, `nom_entreprise`, `ca_entreprise`, `qte_kg`, `qte_mga`, `logo`, `utilisateur_id`, `nif_entreprise`, `stat_entreprise`) VALUES
(1, 'Lico', 51999997, -1, -1, 'uploads/logos_entreprises/logo_1.png', 1, '100236987', '478552136555214'),
(2, 'Madoo', 2500000, -1, -1, 'user_placeholder.png', 2, '888552224488', '6454657895323'),
(3, 'Korti', 6000000, -1, -1, 'user_placeholder.png', 3, '8853232321458', '855221564684321321');

-- --------------------------------------------------------

--
-- Structure de la table `entreprise_compte`
--

DROP TABLE IF EXISTS `entreprise_compte`;
CREATE TABLE IF NOT EXISTS `entreprise_compte` (
  `id_entreprise_compte` int(11) NOT NULL AUTO_INCREMENT,
  `numero_compte` varchar(45) DEFAULT NULL,
  `compte_id` int(11) NOT NULL,
  `entreprise_id` int(11) NOT NULL,
  PRIMARY KEY (`id_entreprise_compte`),
  KEY `fk_entreprise_compte_compte1_idx` (`compte_id`),
  KEY `fk_entreprise_compte_entreprise1_idx` (`entreprise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `entreprise_compte`
--

INSERT INTO `entreprise_compte` (`id_entreprise_compte`, `numero_compte`, `compte_id`, `entreprise_id`) VALUES
(35, '+261340012345', 2, 1),
(37, '+261320078945', 1, 2),
(38, '+261342014796', 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise_employe`
--

DROP TABLE IF EXISTS `entreprise_employe`;
CREATE TABLE IF NOT EXISTS `entreprise_employe` (
  `id_entreprise_employe` int(11) NOT NULL AUTO_INCREMENT,
  `entreprise_id` int(11) NOT NULL,
  `employe_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_entreprise_employe`),
  KEY `fk_entreprise_employe_entreprise1_idx` (`entreprise_id`),
  KEY `fk_entreprise_employe_employe1_idx` (`employe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `entreprise_employe`
--

INSERT INTO `entreprise_employe` (`id_entreprise_employe`, `entreprise_id`, `employe_id`, `position`) VALUES
(1, 1, 1, 1),
(2, 2, 4, 1),
(3, 3, 5, 1);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise_type`
--

DROP TABLE IF EXISTS `entreprise_type`;
CREATE TABLE IF NOT EXISTS `entreprise_type` (
  `id_entreprise_type` int(11) NOT NULL AUTO_INCREMENT,
  `entreprise_id` int(11) NOT NULL,
  `type_entreprise_id` int(11) NOT NULL,
  PRIMARY KEY (`id_entreprise_type`),
  KEY `fk_entreprise_type_entreprise1_idx` (`entreprise_id`),
  KEY `fk_entreprise_type_type_entreprise1_idx` (`type_entreprise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `entreprise_type`
--

INSERT INTO `entreprise_type` (`id_entreprise_type`, `entreprise_id`, `type_entreprise_id`) VALUES
(69, 1, 6),
(70, 1, 8),
(72, 2, 3),
(73, 3, 3);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise_type_aliment`
--

DROP TABLE IF EXISTS `entreprise_type_aliment`;
CREATE TABLE IF NOT EXISTS `entreprise_type_aliment` (
  `id_entreprise_type_aliment` int(11) NOT NULL AUTO_INCREMENT,
  `type_aliment_id` int(11) NOT NULL,
  `entreprise_id` int(11) NOT NULL,
  PRIMARY KEY (`id_entreprise_type_aliment`),
  KEY `fk_entreprise_type_aliment_type_aliment1_idx` (`type_aliment_id`),
  KEY `fk_entreprise_type_aliment_entreprise1_idx` (`entreprise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `entreprise_type_aliment`
--

INSERT INTO `entreprise_type_aliment` (`id_entreprise_type_aliment`, `type_aliment_id`, `entreprise_id`) VALUES
(69, 1, 1),
(70, 6, 1),
(72, 3, 2),
(73, 5, 3);

-- --------------------------------------------------------

--
-- Structure de la table `equipement`
--

DROP TABLE IF EXISTS `equipement`;
CREATE TABLE IF NOT EXISTS `equipement` (
  `id_equipement` int(11) NOT NULL,
  `label` varchar(100) NOT NULL,
  PRIMARY KEY (`id_equipement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `equipement`
--

INSERT INTO `equipement` (`id_equipement`, `label`) VALUES
(1, 'Frigo'),
(2, 'Réfrigérateur'),
(3, 'Transport'),
(4, 'Pas d\'équipement');

-- --------------------------------------------------------

--
-- Structure de la table `etat_commande`
--

DROP TABLE IF EXISTS `etat_commande`;
CREATE TABLE IF NOT EXISTS `etat_commande` (
  `id_etat_commande` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_etat_commande`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etat_commande`
--

INSERT INTO `etat_commande` (`id_etat_commande`, `label`) VALUES
(1, 'Payé'),
(2, 'Payé et Livré');

-- --------------------------------------------------------

--
-- Structure de la table `etat_donation`
--

DROP TABLE IF EXISTS `etat_donation`;
CREATE TABLE IF NOT EXISTS `etat_donation` (
  `id_etat_donation` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_etat_donation`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `etat_donation`
--

INSERT INTO `etat_donation` (`id_etat_donation`, `label`) VALUES
(1, 'Attente de la validation'),
(2, 'Mail envoyé à DHL'),
(3, 'En livraison '),
(4, 'Livré '),
(5, 'Brouillon'),
(6, 'Vendu');

-- --------------------------------------------------------

--
-- Structure de la table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE IF NOT EXISTS `feedback` (
  `id_feedback` int(11) NOT NULL AUTO_INCREMENT,
  `label_feedback_id` int(11) NOT NULL,
  `note` int(11) DEFAULT NULL,
  `commentaire` mediumtext,
  `consommateur_id` int(11) NOT NULL,
  `moment` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_feedback`),
  KEY `fk_feedback_label_feedback1_idx` (`label_feedback_id`),
  KEY `fk_feedback_consommateur1_idx` (`consommateur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `feedback`
--

INSERT INTO `feedback` (`id_feedback`, `label_feedback_id`, `note`, `commentaire`, `consommateur_id`, `moment`) VALUES
(1, 1, 4, NULL, 2, '2019-07-18 00:00:00'),
(2, 2, 2, NULL, 2, NULL),
(3, 3, 1, NULL, 2, NULL),
(4, 4, 5, NULL, 2, NULL),
(5, 5, 5, NULL, 2, NULL),
(6, 1, 5, NULL, 1, NULL),
(7, 2, 1, NULL, 1, NULL),
(8, 3, 2, NULL, 1, NULL),
(9, 4, 1, NULL, 1, NULL),
(10, 5, 5, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `historique`
--

DROP TABLE IF EXISTS `historique`;
CREATE TABLE IF NOT EXISTS `historique` (
  `id_historique` int(11) NOT NULL AUTO_INCREMENT,
  `utilisateur_id` int(11) NOT NULL,
  `action` varchar(100) NOT NULL,
  `parametres` varchar(45) DEFAULT '{}',
  `moment` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_historique`),
  KEY `fk_historique_utilisateur1_idx` (`utilisateur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `information_nutritionnelle`
--

DROP TABLE IF EXISTS `information_nutritionnelle`;
CREATE TABLE IF NOT EXISTS `information_nutritionnelle` (
  `id_information_nutritionnelle` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) NOT NULL,
  `proprietaire` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_information_nutritionnelle`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `information_nutritionnelle`
--

INSERT INTO `information_nutritionnelle` (`id_information_nutritionnelle`, `label`, `proprietaire`) VALUES
(1, 'Hallal', 0),
(2, 'Végétarien', 0),
(3, 'Végétalien', 0),
(4, 'Sans lactose', 0),
(5, 'Sans gluten', 0),
(6, 'Sans porc', 0),
(7, 'Inconnu', 0);

-- --------------------------------------------------------

--
-- Structure de la table `label_feedback`
--

DROP TABLE IF EXISTS `label_feedback`;
CREATE TABLE IF NOT EXISTS `label_feedback` (
  `id_label_feedback` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_label_feedback`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `label_feedback`
--

INSERT INTO `label_feedback` (`id_label_feedback`, `label`) VALUES
(1, 'Service'),
(2, 'Livraison'),
(3, 'Rapidité'),
(4, 'Qualité de la nourriture'),
(5, 'Satisfaction');

-- --------------------------------------------------------

--
-- Structure de la table `magasin`
--

DROP TABLE IF EXISTS `magasin`;
CREATE TABLE IF NOT EXISTS `magasin` (
  `id_magasin` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  `entreprise_id` int(11) NOT NULL,
  `employe_responsable_id` int(11) NOT NULL,
  PRIMARY KEY (`id_magasin`),
  KEY `fk_magasin_entreprise1_idx` (`entreprise_id`),
  KEY `fk_magasin_employe1_idx` (`employe_responsable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `magasin`
--

INSERT INTO `magasin` (`id_magasin`, `nom`, `entreprise_id`, `employe_responsable_id`) VALUES
(1, 'Lico talatamaty', 1, 3);

-- --------------------------------------------------------

--
-- Structure de la table `membre_fw`
--

DROP TABLE IF EXISTS `membre_fw`;
CREATE TABLE IF NOT EXISTS `membre_fw` (
  `id_membre_fw` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `poste_id` int(11) NOT NULL,
  `utilisateur_id` int(11) NOT NULL,
  `pays_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_membre_fw`),
  KEY `fk_poste` (`poste_id`),
  KEY `fk_role` (`utilisateur_id`),
  KEY `fk_pays` (`pays_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `membre_fw`
--

INSERT INTO `membre_fw` (`id_membre_fw`, `nom`, `email`, `tel`, `logo`, `poste_id`, `utilisateur_id`, `pays_id`) VALUES
(1, 'Admin name', 'admin@gmail.com', '0340404500', 'upload_profil/rakoto.jpg', 10, 5, 1),
(8, 'Utilisateur nom 1', NULL, NULL, 'upload_profil/profil.png', 1, 15, 1),
(9, 'Benevole name', NULL, NULL, 'upload_profil/profil.png', 6, 16, 1),
(10, 'Benevole name 2', NULL, NULL, 'upload_profil/profil.png', 6, 17, 1);

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2014_10_15_000000_create_users_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `mois`
--

DROP TABLE IF EXISTS `mois`;
CREATE TABLE IF NOT EXISTS `mois` (
  `id_mois` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(15) NOT NULL,
  PRIMARY KEY (`id_mois`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `mois`
--

INSERT INTO `mois` (`id_mois`, `label`) VALUES
(1, 'Janvier'),
(2, 'Février'),
(3, 'Mars'),
(4, 'Avril'),
(5, 'Mai'),
(6, 'Juin'),
(7, 'Juillet'),
(8, 'Août'),
(9, 'Septembre'),
(10, 'Octobre'),
(11, 'Novembre'),
(12, 'Décembre');

-- --------------------------------------------------------

--
-- Structure de la table `nombre_enfant`
--

DROP TABLE IF EXISTS `nombre_enfant`;
CREATE TABLE IF NOT EXISTS `nombre_enfant` (
  `id_nombre_enfant` int(11) NOT NULL AUTO_INCREMENT,
  `organisation_id` int(11) NOT NULL,
  `tranche_age` int(11) NOT NULL,
  `nombre` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_nombre_enfant`),
  KEY `fk_nombre_enfants_organisation1_idx` (`organisation_id`),
  KEY `fk_nombre_enfants_tranche_age1_idx` (`tranche_age`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `nombre_enfant`
--

INSERT INTO `nombre_enfant` (`id_nombre_enfant`, `organisation_id`, `tranche_age`, `nombre`) VALUES
(1, 1, 1, 40),
(2, 1, 2, 50),
(3, 1, 3, 2),
(4, 1, 4, 1),
(5, 1, 5, 0),
(6, 2, 1, 25),
(7, 2, 2, 0),
(8, 2, 3, 5),
(9, 2, 4, 0),
(10, 2, 5, 0);

-- --------------------------------------------------------

--
-- Structure de la table `organisation`
--

DROP TABLE IF EXISTS `organisation`;
CREATE TABLE IF NOT EXISTS `organisation` (
  `id_organisation` int(11) NOT NULL AUTO_INCREMENT,
  `nom_organisation` varchar(100) NOT NULL,
  `adresse_id` int(11) NOT NULL,
  `type_organisation_id` int(11) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_organisation`),
  KEY `fk_recepteur_adresse1_idx` (`adresse_id`),
  KEY `fk_recepteur_type_organisation1_idx` (`type_organisation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `organisation`
--

INSERT INTO `organisation` (`id_organisation`, `nom_organisation`, `adresse_id`, `type_organisation_id`, `numero`) VALUES
(1, 'Benef1', 90, 3, '80'),
(2, 'organisation  2', 80, 1, '000000');

-- --------------------------------------------------------

--
-- Structure de la table `organisation_beneficiaire_organisation`
--

DROP TABLE IF EXISTS `organisation_beneficiaire_organisation`;
CREATE TABLE IF NOT EXISTS `organisation_beneficiaire_organisation` (
  `id_organisation_beneficiaire_organisation` int(11) NOT NULL AUTO_INCREMENT,
  `beneficiaire_organisation_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  PRIMARY KEY (`id_organisation_beneficiaire_organisation`),
  KEY `fk_organisation_beneficiaire_organisation_beneficiare_organ_idx` (`beneficiaire_organisation_id`),
  KEY `fk_organisation_beneficiaire_organisation_organisation1_idx` (`organisation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `organisation_beneficiaire_organisation`
--

INSERT INTO `organisation_beneficiaire_organisation` (`id_organisation_beneficiaire_organisation`, `beneficiaire_organisation_id`, `organisation_id`) VALUES
(1, 3, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `organisation_employe`
--

DROP TABLE IF EXISTS `organisation_employe`;
CREATE TABLE IF NOT EXISTS `organisation_employe` (
  `id_organisation_employe` int(11) NOT NULL AUTO_INCREMENT,
  `employe_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_organisation_employe`),
  KEY `fk_organisation_employe_employe1_idx` (`employe_id`),
  KEY `fk_organisation_employe_organisation1_idx` (`organisation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `organisation_employe`
--

INSERT INTO `organisation_employe` (`id_organisation_employe`, `employe_id`, `organisation_id`, `position`) VALUES
(1, 7, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `organisation_equipement`
--

DROP TABLE IF EXISTS `organisation_equipement`;
CREATE TABLE IF NOT EXISTS `organisation_equipement` (
  `id_organisation_equipement` int(11) NOT NULL AUTO_INCREMENT,
  `organisation_id` int(11) NOT NULL,
  `equipement_id` int(11) NOT NULL,
  `etat` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_organisation_equipement`),
  KEY `fk_organisation_equipement_organisation1_idx` (`organisation_id`),
  KEY `fk_organisation_equipement_equipement1_idx` (`equipement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `organisation_equipement`
--

INSERT INTO `organisation_equipement` (`id_organisation_equipement`, `organisation_id`, `equipement_id`, `etat`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `organisation_information_nutritionnelle`
--

DROP TABLE IF EXISTS `organisation_information_nutritionnelle`;
CREATE TABLE IF NOT EXISTS `organisation_information_nutritionnelle` (
  `id_organisation_information_nutritionnelle` int(11) NOT NULL AUTO_INCREMENT,
  `information_nutritionnelle_id` int(11) NOT NULL,
  `organisation_id` int(11) NOT NULL,
  PRIMARY KEY (`id_organisation_information_nutritionnelle`),
  KEY `fk_organisation_information_nutritionnelle_information_nutr_idx` (`information_nutritionnelle_id`),
  KEY `fk_organisation_information_nutritionnelle_organisation1_idx` (`organisation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `organisation_information_nutritionnelle`
--

INSERT INTO `organisation_information_nutritionnelle` (`id_organisation_information_nutritionnelle`, `information_nutritionnelle_id`, `organisation_id`) VALUES
(1, 1, 1),
(2, 4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

DROP TABLE IF EXISTS `pays`;
CREATE TABLE IF NOT EXISTS `pays` (
  `id_pays` int(11) NOT NULL,
  `label` varchar(45) NOT NULL,
  `prefixe_tel` varchar(45) NOT NULL,
  PRIMARY KEY (`id_pays`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pays`
--

INSERT INTO `pays` (`id_pays`, `label`, `prefixe_tel`) VALUES
(1, 'Madagascar', '+261'),
(2, 'La Réunion', '+262'),
(3, 'Maurice', '+230'),
(4, 'Comores', '+269');

-- --------------------------------------------------------

--
-- Structure de la table `periode_recuperation_commande`
--

DROP TABLE IF EXISTS `periode_recuperation_commande`;
CREATE TABLE IF NOT EXISTS `periode_recuperation_commande` (
  `id_periode_recuperation_commande` int(11) NOT NULL AUTO_INCREMENT,
  `jour` varchar(45) DEFAULT NULL,
  `horaire_debut` varchar(45) DEFAULT NULL,
  `horaire_fin` varchar(45) DEFAULT NULL,
  `donation_id` int(11) NOT NULL,
  PRIMARY KEY (`id_periode_recuperation_commande`),
  KEY `fk_periode_recuperation_commande_donation1_idx` (`donation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `periode_recuperation_commande`
--

INSERT INTO `periode_recuperation_commande` (`id_periode_recuperation_commande`, `jour`, `horaire_debut`, `horaire_fin`, `donation_id`) VALUES
(2, 'Tous les jours', '15:30', '18:00', 1),
(3, 'Tous les jours', '08:30', '18:00', 2),
(4, 'Tous les jours', '18:00', '19:00', 3),
(5, 'Tous les jours', '18:45', '21:00', 4),
(6, 'Tous les jours', '15:30', '20:30', 5),
(7, 'Lundi', '16:15', '23:00', 7),
(8, 'Lundi', '17:00', '20:00', 8),
(9, 'Lundi', '17:00', '20:00', 9),
(10, 'Mercredi', '19:00', '22:00', 10),
(11, 'Vendredi', '20:00', '23:30', 11),
(12, 'Vendredi', '17:00', '23:00', 12),
(13, 'Mercredi', '11:30', '11:45', 13);

-- --------------------------------------------------------

--
-- Structure de la table `periode_reduction`
--

DROP TABLE IF EXISTS `periode_reduction`;
CREATE TABLE IF NOT EXISTS `periode_reduction` (
  `id_periode_reduction` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) DEFAULT NULL,
  `nombre_jour` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_periode_reduction`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `periode_reduction`
--

INSERT INTO `periode_reduction` (`id_periode_reduction`, `label`, `nombre_jour`) VALUES
(1, '4 semaine', 28),
(2, '3 semaines', 21),
(3, '2 semaines', 14),
(4, '1 semaine', 7);

-- --------------------------------------------------------

--
-- Structure de la table `photo_produit`
--

DROP TABLE IF EXISTS `photo_produit`;
CREATE TABLE IF NOT EXISTS `photo_produit` (
  `id_photo_produit` int(11) NOT NULL AUTO_INCREMENT,
  `produit_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `proprietaire` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_photo_produit`),
  KEY `fk_photo_produit_produit1_idx` (`produit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `poste`
--

DROP TABLE IF EXISTS `poste`;
CREATE TABLE IF NOT EXISTS `poste` (
  `id_poste` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `type` int(11) DEFAULT '1' COMMENT '1:entreprise - 2:organisation -3:membres_fw',
  `proprietaire` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_poste`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `poste`
--

INSERT INTO `poste` (`id_poste`, `label`, `type`, `proprietaire`) VALUES
(1, 'Directeur/Directrice Général', 1, 0),
(2, 'Directeur/Directrice Général', 2, 0),
(3, 'Responsable partenariat', 2, 0),
(4, 'Responsable communication', 2, 0),
(5, 'Fondateur/Fondatrice', 2, 0),
(6, 'Responsable RSE', 1, 0),
(7, 'Responsable Marketing/Commercial', 1, 0),
(8, 'Assistance de la direction', 1, 0),
(9, 'Secrétaire', 1, 0),
(10, 'poste fw 1', 3, 0),
(11, 'poste fw 2', 3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id_produit` int(11) NOT NULL AUTO_INCREMENT,
  `nom_produit` varchar(100) DEFAULT NULL,
  `type_produit_id` int(11) DEFAULT NULL,
  `description` text,
  `poids` double DEFAULT NULL,
  `volume` double DEFAULT NULL,
  `type_aliment_id` int(11) DEFAULT NULL,
  `prix` double DEFAULT NULL,
  `groupement` int(11) DEFAULT NULL COMMENT '1:par unité - 2:totalité - 3:par portion',
  PRIMARY KEY (`id_produit`),
  KEY `fk_produit_type_produit1_idx` (`type_produit_id`),
  KEY `fk_produit_type_aliment1_idx` (`type_aliment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id_produit`, `nom_produit`, `type_produit_id`, `description`, `poids`, `volume`, `type_aliment_id`, `prix`, `groupement`) VALUES
(1, 'Test 1', 1, '', 0.5, 1, 6, 5000, 1),
(2, 'Tset 2', 2, '', 52, 12, 3, 580000, 3),
(3, 'test', 1, '', 15, 110, 2, 1000000, 3),
(4, 'test', 1, '', 15, 110, 2, 1000000, 3),
(5, 'test', 1, '', 15, 110, 2, 1000000, 3),
(6, 'Test4', 2, '', 25, 35, 4, 15, 1),
(7, 'Madoo11', 2, '', 15, 20, 2, 54000, 3),
(8, 'Don 1', 1, '', 10, 5, 3, 500000, 1),
(9, 'Vente 1', 2, '', 5, 28, 2, 520000, 1),
(10, 'Vente test 1', 3, '', 52, 2, 1, 50000, 2),
(11, 'Vente test 1', 3, '', 52, 2, 1, 60000, 2),
(12, 'Vente test firestore', 4, '', 25, 47, 5, 75000, 3),
(13, 'Test produit save', 1, '', 15, 10, 3, 10, 2),
(14, 'Test nouveau produit', 5, '', 8, 10, 3, 100000, 1),
(15, 'Vente produit', 2, '', 20, 10, 2, 52000, 1);

-- --------------------------------------------------------

--
-- Structure de la table `produit_commande`
--

DROP TABLE IF EXISTS `produit_commande`;
CREATE TABLE IF NOT EXISTS `produit_commande` (
  `id_produit_commande` int(11) NOT NULL AUTO_INCREMENT,
  `commande_id` int(11) NOT NULL,
  `qte` int(11) DEFAULT NULL,
  `prix_achat_unite` int(11) DEFAULT NULL,
  `donation_id` int(11) NOT NULL,
  PRIMARY KEY (`id_produit_commande`),
  KEY `fk_produit_commande_commande1_idx` (`commande_id`),
  KEY `fk_produit_commande_donation1_idx` (`donation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produit_commande`
--

INSERT INTO `produit_commande` (`id_produit_commande`, `commande_id`, `qte`, `prix_achat_unite`, `donation_id`) VALUES
(1, 1, 2, 4000, 1),
(2, 1, 1, 85000, 12),
(3, 1, 2, 4000, 1),
(4, 2, 10, 37500, 8),
(5, 2, 10, 45000, 9);

-- --------------------------------------------------------

--
-- Structure de la table `produit_equipement`
--

DROP TABLE IF EXISTS `produit_equipement`;
CREATE TABLE IF NOT EXISTS `produit_equipement` (
  `id_produit_equipement` int(11) NOT NULL AUTO_INCREMENT,
  `produit_id` int(11) NOT NULL,
  `equipement_id` int(11) NOT NULL,
  `etat` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_produit_equipement`),
  KEY `fk_produit_equipement_produit1_idx` (`produit_id`),
  KEY `fk_produit_equipement_equipement1_idx` (`equipement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produit_equipement`
--

INSERT INTO `produit_equipement` (`id_produit_equipement`, `produit_id`, `equipement_id`, `etat`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 4, 0),
(4, 2, 1, 0),
(5, 2, 2, 0),
(6, 2, 4, 1),
(7, 3, 1, 0),
(8, 3, 2, 0),
(9, 3, 4, 0),
(10, 4, 1, 0),
(11, 4, 2, 0),
(12, 4, 4, 0),
(13, 5, 1, 0),
(14, 5, 2, 0),
(15, 5, 4, 0),
(16, 6, 1, 0),
(17, 6, 2, 0),
(18, 6, 4, 0),
(19, 7, 1, 0),
(20, 7, 2, 0),
(21, 7, 4, 1),
(22, 8, 1, 0),
(23, 8, 2, 0),
(24, 8, 4, 1),
(25, 9, 1, 1),
(26, 9, 2, 0),
(27, 9, 4, 0),
(28, 10, 1, 0),
(29, 10, 2, 0),
(30, 10, 4, 1),
(31, 11, 1, 0),
(32, 11, 2, 0),
(33, 11, 4, 1),
(34, 12, 1, 0),
(35, 12, 2, 1),
(36, 12, 4, 0),
(37, 13, 1, 1),
(38, 13, 2, 0),
(39, 13, 4, 0),
(40, 14, 1, 0),
(41, 14, 2, 0),
(42, 14, 4, 0),
(43, 15, 1, 0),
(44, 15, 2, 0),
(45, 15, 4, 0);

-- --------------------------------------------------------

--
-- Structure de la table `produit_information_nutritionnelle`
--

DROP TABLE IF EXISTS `produit_information_nutritionnelle`;
CREATE TABLE IF NOT EXISTS `produit_information_nutritionnelle` (
  `id_produit_information_nutritionnelle` int(11) NOT NULL AUTO_INCREMENT,
  `produit_id` int(11) NOT NULL,
  `information_nutritionnelle_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_produit_information_nutritionnelle`),
  KEY `fk_produit_information_nutritionnelle_produit1_idx` (`produit_id`),
  KEY `fk_produit_information_nutritionnelle_information_nutrition_idx` (`information_nutritionnelle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produit_information_nutritionnelle`
--

INSERT INTO `produit_information_nutritionnelle` (`id_produit_information_nutritionnelle`, `produit_id`, `information_nutritionnelle_id`) VALUES
(2, 1, 7),
(3, 12, 7),
(4, 14, 2);

-- --------------------------------------------------------

--
-- Structure de la table `raison_donation`
--

DROP TABLE IF EXISTS `raison_donation`;
CREATE TABLE IF NOT EXISTS `raison_donation` (
  `id_raison_donation` int(11) NOT NULL AUTO_INCREMENT,
  `raison_surplus_id` int(11) DEFAULT NULL,
  `donation_id` int(11) NOT NULL,
  PRIMARY KEY (`id_raison_donation`),
  KEY `fk_raison_donation_raison_surplus1_idx` (`raison_surplus_id`),
  KEY `fk_raison_donation_donation1_idx` (`donation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `raison_donation`
--

INSERT INTO `raison_donation` (`id_raison_donation`, `raison_surplus_id`, `donation_id`) VALUES
(1, 3, 6),
(2, 2, 11),
(3, 2, 13),
(4, 2, 14);

-- --------------------------------------------------------

--
-- Structure de la table `raison_surplus`
--

DROP TABLE IF EXISTS `raison_surplus`;
CREATE TABLE IF NOT EXISTS `raison_surplus` (
  `id_raison_surplus` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `proprietaire` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_raison_surplus`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `raison_surplus`
--

INSERT INTO `raison_surplus` (`id_raison_surplus`, `label`, `proprietaire`) VALUES
(1, 'La date de péremption est proche', '0'),
(2, 'Coupures électriques (stock de magasin)', '0'),
(3, 'Problème de réfrigération (Transport, stock)', '0'),
(4, 'Problème d\'emballage (mauvaise étiquette, emballage cassé/endommagé)', '0'),
(5, 'La qualité esthétique des produits (fruits et légumes moches ou moins frais)', '0'),
(6, 'Restes/surplus de buffets', '0'),
(7, 'Produit invendable au lendemain (pain, produit cuit,etc...)', '0'),
(8, 'Problème de livraison', '0'),
(9, 'Retour clients', '0');

-- --------------------------------------------------------

--
-- Structure de la table `strategie_reduction`
--

DROP TABLE IF EXISTS `strategie_reduction`;
CREATE TABLE IF NOT EXISTS `strategie_reduction` (
  `id_strategie_reduction` int(11) NOT NULL AUTO_INCREMENT,
  `periode_reduction_id` int(11) DEFAULT NULL,
  `pourcentage_reduction` int(11) DEFAULT NULL,
  `donation_id` int(11) NOT NULL,
  PRIMARY KEY (`id_strategie_reduction`),
  KEY `fk_strategie_reduction_periode_reduction1_idx` (`periode_reduction_id`),
  KEY `fk_strategie_reduction_donation1_idx` (`donation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tache`
--

DROP TABLE IF EXISTS `tache`;
CREATE TABLE IF NOT EXISTS `tache` (
  `id_tache` int(11) NOT NULL AUTO_INCREMENT,
  `label` text NOT NULL,
  `description` text NOT NULL,
  `objectif` text NOT NULL,
  `entreprise_concernee` int(11) DEFAULT NULL,
  `montant_objectif` double DEFAULT NULL,
  `utilisateur_id` int(11) DEFAULT NULL,
  `date_assignation` date DEFAULT NULL,
  `date_echeance` date DEFAULT NULL,
  PRIMARY KEY (`id_tache`),
  KEY `fk_tache` (`utilisateur_id`),
  KEY `fk_tache_entreprise` (`entreprise_concernee`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tache`
--

INSERT INTO `tache` (`id_tache`, `label`, `description`, `objectif`, `entreprise_concernee`, `montant_objectif`, `utilisateur_id`, `date_assignation`, `date_echeance`) VALUES
(2, 'labels', 'description', 'Objectif', 3, 2000, NULL, '2019-07-04', '2019-08-05'),
(12, 'tache1', 'description', 'objectif1', 2, 1000, NULL, '2019-07-10', '2019-07-17');

-- --------------------------------------------------------

--
-- Structure de la table `token`
--

DROP TABLE IF EXISTS `token`;
CREATE TABLE IF NOT EXISTS `token` (
  `id_token` int(11) NOT NULL AUTO_INCREMENT,
  `chaine` varchar(250) DEFAULT NULL,
  `actif` tinyint(1) DEFAULT NULL,
  `expiration` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_token`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `token`
--

INSERT INTO `token` (`id_token`, `chaine`, `actif`, `expiration`) VALUES
(1, '96e7e803nxeb54u916lhskhmsa6fggyvntq9yx2b78ff5ahmax', 1, 1556258264),
(2, 'ra4em9kq3neqhy24mstubyi8f52qbeaury8c37885w6c3a62v9', 1, 1556258672),
(3, '9se41w889e6q3y6f7zey7128rg227z889z88b9aqbzehdst2fs', 1, 1556265941),
(4, 'b5kyb3cqbzxu7apu9ziqd14cm3kubgpl756udaa69nxedyac97', 1, 1556266554);

-- --------------------------------------------------------

--
-- Structure de la table `tranche_age`
--

DROP TABLE IF EXISTS `tranche_age`;
CREATE TABLE IF NOT EXISTS `tranche_age` (
  `id_tranche_age` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_tranche_age`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tranche_age`
--

INSERT INTO `tranche_age` (`id_tranche_age`, `label`) VALUES
(1, '0-5'),
(2, '5-10'),
(3, '10-15'),
(4, '15-18'),
(5, 'Plus de 18');

-- --------------------------------------------------------

--
-- Structure de la table `transporteur`
--

DROP TABLE IF EXISTS `transporteur`;
CREATE TABLE IF NOT EXISTS `transporteur` (
  `id_transporteur` int(11) NOT NULL,
  `label` varchar(45) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_transporteur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `transporteur`
--

INSERT INTO `transporteur` (`id_transporteur`, `label`, `logo`) VALUES
(1, 'DHL', 'uploads/logo/transporteur/DHL.png');

-- --------------------------------------------------------

--
-- Structure de la table `type_aliment`
--

DROP TABLE IF EXISTS `type_aliment`;
CREATE TABLE IF NOT EXISTS `type_aliment` (
  `id_type_aliment` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) NOT NULL,
  `proprietaire` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_type_aliment`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_aliment`
--

INSERT INTO `type_aliment` (`id_type_aliment`, `label`, `proprietaire`) VALUES
(1, 'Aliments secs emballés (Biscuits, chips, etc.)', 0),
(2, 'Fruits et légumes frais', 0),
(3, 'Aliments en conserve', 0),
(4, 'Produits de boulangerie (Pains, patisserie, viennoiserie, etc.)', 0),
(5, 'Aliments cuits', 0),
(6, 'Aliments préparés (Salade, sandwiches, etc.)', 0),
(7, 'Produits laitiers (fromage, yaourt, lait, etc)', 0),
(8, 'Boissons', 0);

-- --------------------------------------------------------

--
-- Structure de la table `type_aliment_information_nutritionnelle`
--

DROP TABLE IF EXISTS `type_aliment_information_nutritionnelle`;
CREATE TABLE IF NOT EXISTS `type_aliment_information_nutritionnelle` (
  `id_type_aliment_information_nutritionnelle` int(11) NOT NULL AUTO_INCREMENT,
  `type_aliment_id` int(11) NOT NULL,
  `information_nutritionnelle_id` int(11) NOT NULL,
  PRIMARY KEY (`id_type_aliment_information_nutritionnelle`),
  KEY `fk_type_aliment_information_nutritionnelle_type_aliment1_idx` (`type_aliment_id`),
  KEY `fk_type_aliment_information_nutritionnelle_information_nutr_idx` (`information_nutritionnelle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `type_entreprise`
--

DROP TABLE IF EXISTS `type_entreprise`;
CREATE TABLE IF NOT EXISTS `type_entreprise` (
  `id_type_entreprise` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  `proprietaire` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_type_entreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_entreprise`
--

INSERT INTO `type_entreprise` (`id_type_entreprise`, `label`, `proprietaire`) VALUES
(1, 'Supermarché', 0),
(2, 'Hôtel', 0),
(3, 'Restaurant', 0),
(4, 'Boulangerie', 0),
(5, 'Importateur', 0),
(6, 'Producteur', 0),
(7, 'Industriel', 0),
(8, 'Distributeur', 0),
(9, 'Agriculteur', 0),
(10, 'Traiteur', 0);

-- --------------------------------------------------------

--
-- Structure de la table `type_organisation`
--

DROP TABLE IF EXISTS `type_organisation`;
CREATE TABLE IF NOT EXISTS `type_organisation` (
  `id_type_organisation` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) NOT NULL,
  `proprietaire` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_type_organisation`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_organisation`
--

INSERT INTO `type_organisation` (`id_type_organisation`, `label`, `proprietaire`) VALUES
(1, 'Orphelinat', 0),
(2, 'Ecole', 0),
(3, 'ONG', 0);

-- --------------------------------------------------------

--
-- Structure de la table `type_produit`
--

DROP TABLE IF EXISTS `type_produit`;
CREATE TABLE IF NOT EXISTS `type_produit` (
  `id_type_produit` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(100) NOT NULL,
  `proprietaire` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_type_produit`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_produit`
--

INSERT INTO `type_produit` (`id_type_produit`, `label`, `proprietaire`) VALUES
(1, 'Biscuit', 0),
(2, 'Boissons', 0),
(3, 'Snack', 1),
(4, 'Plat resto', 1),
(5, 'Produit maritime', 1);

-- --------------------------------------------------------

--
-- Structure de la table `unite_prix`
--

DROP TABLE IF EXISTS `unite_prix`;
CREATE TABLE IF NOT EXISTS `unite_prix` (
  `id_unite_prix` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_unite_prix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `url_temp_param`
--

DROP TABLE IF EXISTS `url_temp_param`;
CREATE TABLE IF NOT EXISTS `url_temp_param` (
  `id_url_temp_param` int(11) NOT NULL AUTO_INCREMENT,
  `param` varchar(255) NOT NULL,
  `moment` datetime DEFAULT NULL,
  PRIMARY KEY (`id_url_temp_param`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `url_temp_param`
--

INSERT INTO `url_temp_param` (`id_url_temp_param`, `param`, `moment`) VALUES
(1, 'ee043b31ce5e0f80c7d8ff563b9573561356c26946cfd7dc0b4d8d013c7a4227b26f90100a0fb82cb8be55298c07b7201ed878bfc44ad9bd1b68902e6f08d60bFGCHpcGk764vomwGAxINXO6vkevI3w1CkU.Cc0oXB3Ig5vHq1wkhBvQIZ4sh7whk', '2019-03-25 16:08:36'),
(2, '51663544963086935baba3277f0c3e947bc2cacbd4ebb65e271a704e4c18bbed153c81634979e67cb9bb2132752924711be2c6d4c5d41edeb33d81e5b1a82e0aN6vpf8NTdmdG.CxU~tntgoIrC6RU5XzGVMqNi4WCz57qNAObzJ7OhoTCVNBkpkZ0', '2019-04-19 14:42:19'),
(3, '5010358066e320411e0a582f1090b94a836f15081cb5a2bdc20ae75bcc973a51925506f620ca30bdde9d802ea95d87959d6106143066715064c973b342242ae9J5u7nPYb3Ey5h3TCqYMhKW4PeKmP5JOY4qef3jivrV5JH~30cMm8AkCkF4r9MoGp', '2019-04-19 14:50:43');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `utilisateur_type_id` int(11) NOT NULL DEFAULT '1',
  `moment_inscription` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` int(11) NOT NULL DEFAULT '1' COMMENT '1 actif 2 desactive',
  PRIMARY KEY (`id_utilisateur`),
  KEY `fk_utilisateur_utilisateur_type1_idx` (`utilisateur_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `pseudo`, `password`, `utilisateur_type_id`, `moment_inscription`, `active`) VALUES
(1, 'Lico', 'e8191d6063fae2ae576815d3705264e88e60e359455039c2f94fcd6aa130f5bc98df3acfa0f1d0956effe0414656b9b4e3677dbc820107f3eb002c35efe50afallyNk9oVPXV/LAhb1PFiQLCad2GdoQEZr2XQVlZcllQ=', 1, '2019-06-16 08:52:00', 1),
(2, 'Madoo', '9307bf4f4807e87c91c0216f2bc803662c284c7b2b376561305094db2da9096b57cdfdfc325883b7857c2a029ca6fe11274aa448e07830b5a9db28a114607b0106qaOP95ky5O90rMYx1FkuGLH1NTe3MrXVp7EJK1VG8=', 1, '2019-07-16 08:52:00', 1),
(3, 'Korti', '7f2581e0e1c5d959618e42c414030fba6f807ecb27abe3584c15d0f23e3332d836218318e874b0b8cfd75f7f7fbc350d6f0a61af7f5723b979eb00cedd8398a1EMyEraL2nuu6ymkY7bPfS92TRe4+SLBp8+9qkUsCy68=', 1, '2019-07-16 08:52:00', 1),
(4, 'Super', '$2y$10$A4ZnkZuWC4/my4vhM3SUN.ANMSd5zNEQtsnynKJ2XqI5dBYQmj5je', 3, '2019-07-16 08:52:00', 1),
(5, 'Admin', '$2y$10$z6ftrsyAwewS7eFHqQVPnepP6Ex0wLC7wF.zndk7lbXagcZkyUK4O', 2, '2019-07-16 08:52:00', 1),
(15, 'Utilisateur1', '$2y$10$RqxO..UblU/cNwxoD71.aeUu2Hn5gj9qhe0PiTrHlGT3Yh9x4gClK', 4, '2019-07-16 08:52:00', 1),
(16, 'Benevole1', '$2y$10$Tm8KFRIilPrZwGtE1d8zDeezBucvw1dAIQyez59nZznCOcO/FwouG', 6, '2019-06-17 14:51:05', 1),
(17, 'Benevole2', '$2y$10$n4.S56nUZfzCwPC1SlWzTOi7it3JWpYDTgfZ61cD3R4G.QBLR.yyW', 6, '2019-07-17 14:51:45', 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur_type`
--

DROP TABLE IF EXISTS `utilisateur_type`;
CREATE TABLE IF NOT EXISTS `utilisateur_type` (
  `id_utilisateur_type` int(11) NOT NULL,
  `label` varchar(255) NOT NULL DEFAULT 'NONE',
  PRIMARY KEY (`id_utilisateur_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur_type`
--

INSERT INTO `utilisateur_type` (`id_utilisateur_type`, `label`) VALUES
(1, 'Entreprise'),
(2, 'Administrateur FoodWise'),
(3, 'Super Administrateur'),
(4, 'Editeur'),
(5, 'Analyste'),
(6, 'Benevole');

-- --------------------------------------------------------

--
-- Structure de la table `wish_list`
--

DROP TABLE IF EXISTS `wish_list`;
CREATE TABLE IF NOT EXISTS `wish_list` (
  `id_wish_list` int(11) NOT NULL AUTO_INCREMENT,
  `consommateur_id` int(11) NOT NULL,
  `produit_id` int(11) NOT NULL,
  PRIMARY KEY (`id_wish_list`),
  KEY `fk_wish_list_consommateur1_idx` (`consommateur_id`),
  KEY `fk_wish_list_produit1_idx` (`produit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `wish_list`
--

INSERT INTO `wish_list` (`id_wish_list`, `consommateur_id`, `produit_id`) VALUES
(1, 2, 8),
(2, 2, 7),
(3, 2, 2),
(4, 1, 4),
(5, 1, 5),
(6, 1, 14),
(7, 1, 15);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `action_consommateur`
--
ALTER TABLE `action_consommateur`
  ADD CONSTRAINT `fk_action_consommateur_consommateur1` FOREIGN KEY (`consommateur_id`) REFERENCES `consommateur` (`id_consommateur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_action_consommateur_donation1` FOREIGN KEY (`donation_id`) REFERENCES `donation` (`id_donation`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `admin_fw`
--
ALTER TABLE `admin_fw`
  ADD CONSTRAINT `fk_admin_fw_utilisateur1` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD CONSTRAINT `fk_adresse_pays1` FOREIGN KEY (`pays_id`) REFERENCES `pays` (`id_pays`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `adresse_entreprise`
--
ALTER TABLE `adresse_entreprise`
  ADD CONSTRAINT `fk_adresse_entreprise_adresse1` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`id_adresse`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_adresse_entreprise_entreprise1` FOREIGN KEY (`entreprise_id`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_commande_consommateur1` FOREIGN KEY (`consommateur_id`) REFERENCES `consommateur` (`id_consommateur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_commande_etat_commande1` FOREIGN KEY (`etat`) REFERENCES `etat_commande` (`id_etat_commande`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `compte`
--
ALTER TABLE `compte`
  ADD CONSTRAINT `fk_compte_pays1` FOREIGN KEY (`pays_id`) REFERENCES `pays` (`id_pays`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `consommateur`
--
ALTER TABLE `consommateur`
  ADD CONSTRAINT `fk_consommateur_utilisateur1` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `contrat`
--
ALTER TABLE `contrat`
  ADD CONSTRAINT `fk_contrat_employe1` FOREIGN KEY (`employe_charge_id`) REFERENCES `employe` (`id_employe`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contrat_entreprise1` FOREIGN KEY (`entreprise_id`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `donation`
--
ALTER TABLE `donation`
  ADD CONSTRAINT `fk_donation_adresse1` FOREIGN KEY (`adresse_ramassage_id`) REFERENCES `adresse` (`id_adresse`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_donation_entreprise1` FOREIGN KEY (`entreprise_id`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_donation_etat_donation1` FOREIGN KEY (`etat_donation_id`) REFERENCES `etat_donation` (`id_etat_donation`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_donation_organisation1` FOREIGN KEY (`organisation_id`) REFERENCES `organisation` (`id_organisation`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_donation_produit1` FOREIGN KEY (`produit_id_produit`) REFERENCES `produit` (`id_produit`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `employe`
--
ALTER TABLE `employe`
  ADD CONSTRAINT `fk_employe_poste1` FOREIGN KEY (`poste_id`) REFERENCES `poste` (`id_poste`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD CONSTRAINT `fk_entreprise_utilisateur1` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `entreprise_compte`
--
ALTER TABLE `entreprise_compte`
  ADD CONSTRAINT `fk_entreprise_compte_compte1` FOREIGN KEY (`compte_id`) REFERENCES `compte` (`id_compte`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_entreprise_compte_entreprise1` FOREIGN KEY (`entreprise_id`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `entreprise_employe`
--
ALTER TABLE `entreprise_employe`
  ADD CONSTRAINT `fk_entreprise_employe_employe1` FOREIGN KEY (`employe_id`) REFERENCES `employe` (`id_employe`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_entreprise_employe_entreprise1` FOREIGN KEY (`entreprise_id`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `entreprise_type`
--
ALTER TABLE `entreprise_type`
  ADD CONSTRAINT `fk_entreprise_type_entreprise1` FOREIGN KEY (`entreprise_id`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_entreprise_type_type_entreprise1` FOREIGN KEY (`type_entreprise_id`) REFERENCES `type_entreprise` (`id_type_entreprise`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `entreprise_type_aliment`
--
ALTER TABLE `entreprise_type_aliment`
  ADD CONSTRAINT `fk_entreprise_type_aliment_entreprise1` FOREIGN KEY (`entreprise_id`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_entreprise_type_aliment_type_aliment1` FOREIGN KEY (`type_aliment_id`) REFERENCES `type_aliment` (`id_type_aliment`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `fk_feedback_consommateur1` FOREIGN KEY (`consommateur_id`) REFERENCES `consommateur` (`id_consommateur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_feedback_label_feedback1` FOREIGN KEY (`label_feedback_id`) REFERENCES `label_feedback` (`id_label_feedback`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `historique`
--
ALTER TABLE `historique`
  ADD CONSTRAINT `fk_historique_utilisateur1` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateur` (`id_utilisateur`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `magasin`
--
ALTER TABLE `magasin`
  ADD CONSTRAINT `fk_magasin_employe1` FOREIGN KEY (`employe_responsable_id`) REFERENCES `employe` (`id_employe`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_magasin_entreprise1` FOREIGN KEY (`entreprise_id`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `membre_fw`
--
ALTER TABLE `membre_fw`
  ADD CONSTRAINT `fk_pays` FOREIGN KEY (`pays_id`) REFERENCES `pays` (`id_pays`),
  ADD CONSTRAINT `fk_poste` FOREIGN KEY (`poste_id`) REFERENCES `poste` (`id_poste`),
  ADD CONSTRAINT `fk_role` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateur` (`id_utilisateur`);

--
-- Contraintes pour la table `nombre_enfant`
--
ALTER TABLE `nombre_enfant`
  ADD CONSTRAINT `fk_nombre_enfants_organisation1` FOREIGN KEY (`organisation_id`) REFERENCES `organisation` (`id_organisation`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_nombre_enfants_tranche_age1` FOREIGN KEY (`tranche_age`) REFERENCES `tranche_age` (`id_tranche_age`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `organisation`
--
ALTER TABLE `organisation`
  ADD CONSTRAINT `fk_recepteur_adresse1` FOREIGN KEY (`adresse_id`) REFERENCES `adresse` (`id_adresse`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_recepteur_type_organisation1` FOREIGN KEY (`type_organisation_id`) REFERENCES `type_organisation` (`id_type_organisation`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `organisation_beneficiaire_organisation`
--
ALTER TABLE `organisation_beneficiaire_organisation`
  ADD CONSTRAINT `fk_organisation_beneficiaire_organisation_beneficiare_organis1` FOREIGN KEY (`beneficiaire_organisation_id`) REFERENCES `beneficiaire_organisation` (`id_beneficiaire_organisation`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_organisation_beneficiaire_organisation_organisation1` FOREIGN KEY (`organisation_id`) REFERENCES `organisation` (`id_organisation`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `organisation_employe`
--
ALTER TABLE `organisation_employe`
  ADD CONSTRAINT `fk_organisation_employe_employe1` FOREIGN KEY (`employe_id`) REFERENCES `employe` (`id_employe`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_organisation_employe_organisation1` FOREIGN KEY (`organisation_id`) REFERENCES `organisation` (`id_organisation`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `organisation_equipement`
--
ALTER TABLE `organisation_equipement`
  ADD CONSTRAINT `fk_organisation_equipement_equipement1` FOREIGN KEY (`equipement_id`) REFERENCES `equipement` (`id_equipement`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_organisation_equipement_organisation1` FOREIGN KEY (`organisation_id`) REFERENCES `organisation` (`id_organisation`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `organisation_information_nutritionnelle`
--
ALTER TABLE `organisation_information_nutritionnelle`
  ADD CONSTRAINT `fk_organisation_information_nutritionnelle_information_nutrit1` FOREIGN KEY (`information_nutritionnelle_id`) REFERENCES `information_nutritionnelle` (`id_information_nutritionnelle`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_organisation_information_nutritionnelle_organisation1` FOREIGN KEY (`organisation_id`) REFERENCES `organisation` (`id_organisation`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `periode_recuperation_commande`
--
ALTER TABLE `periode_recuperation_commande`
  ADD CONSTRAINT `fk_periode_recuperation_commande_donation1` FOREIGN KEY (`donation_id`) REFERENCES `donation` (`id_donation`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `photo_produit`
--
ALTER TABLE `photo_produit`
  ADD CONSTRAINT `fk_photo_produit_produit1` FOREIGN KEY (`produit_id`) REFERENCES `produit` (`id_produit`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `fk_produit_type_aliment1` FOREIGN KEY (`type_aliment_id`) REFERENCES `type_aliment` (`id_type_aliment`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produit_type_produit1` FOREIGN KEY (`type_produit_id`) REFERENCES `type_produit` (`id_type_produit`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `produit_commande`
--
ALTER TABLE `produit_commande`
  ADD CONSTRAINT `fk_produit_commande_commande1` FOREIGN KEY (`commande_id`) REFERENCES `commande` (`id_commande`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produit_commande_donation1` FOREIGN KEY (`donation_id`) REFERENCES `donation` (`id_donation`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `produit_equipement`
--
ALTER TABLE `produit_equipement`
  ADD CONSTRAINT `fk_produit_equipement_equipement1` FOREIGN KEY (`equipement_id`) REFERENCES `equipement` (`id_equipement`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produit_equipement_produit1` FOREIGN KEY (`produit_id`) REFERENCES `produit` (`id_produit`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `produit_information_nutritionnelle`
--
ALTER TABLE `produit_information_nutritionnelle`
  ADD CONSTRAINT `fk_produit_information_nutritionnelle_information_nutritionne1` FOREIGN KEY (`information_nutritionnelle_id`) REFERENCES `information_nutritionnelle` (`id_information_nutritionnelle`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_produit_information_nutritionnelle_produit1` FOREIGN KEY (`produit_id`) REFERENCES `produit` (`id_produit`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `raison_donation`
--
ALTER TABLE `raison_donation`
  ADD CONSTRAINT `fk_raison_donation_donation1` FOREIGN KEY (`donation_id`) REFERENCES `donation` (`id_donation`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_raison_donation_raison_surplus1` FOREIGN KEY (`raison_surplus_id`) REFERENCES `raison_surplus` (`id_raison_surplus`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `strategie_reduction`
--
ALTER TABLE `strategie_reduction`
  ADD CONSTRAINT `fk_strategie_reduction_donation1` FOREIGN KEY (`donation_id`) REFERENCES `donation` (`id_donation`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_strategie_reduction_periode_reduction1` FOREIGN KEY (`periode_reduction_id`) REFERENCES `periode_reduction` (`id_periode_reduction`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `tache`
--
ALTER TABLE `tache`
  ADD CONSTRAINT `fk_tache` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateur` (`id_utilisateur`),
  ADD CONSTRAINT `fk_tache_entreprise` FOREIGN KEY (`entreprise_concernee`) REFERENCES `entreprise` (`id_entreprise`);

--
-- Contraintes pour la table `type_aliment_information_nutritionnelle`
--
ALTER TABLE `type_aliment_information_nutritionnelle`
  ADD CONSTRAINT `fk_type_aliment_information_nutritionnelle_information_nutrit1` FOREIGN KEY (`information_nutritionnelle_id`) REFERENCES `information_nutritionnelle` (`id_information_nutritionnelle`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_type_aliment_information_nutritionnelle_type_aliment1` FOREIGN KEY (`type_aliment_id`) REFERENCES `type_aliment` (`id_type_aliment`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `fk_utilisateur_utilisateur_type1` FOREIGN KEY (`utilisateur_type_id`) REFERENCES `utilisateur_type` (`id_utilisateur_type`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `wish_list`
--
ALTER TABLE `wish_list`
  ADD CONSTRAINT `fk_wish_list_consommateur1` FOREIGN KEY (`consommateur_id`) REFERENCES `consommateur` (`id_consommateur`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_wish_list_produit1` FOREIGN KEY (`produit_id`) REFERENCES `produit` (`id_produit`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
