<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','AccueilController@index');
// Route::get('/','DashboardController@page_dashboard');

Route::get('/liste_utilisateur','UtilisateurController@index');
Route::get('/login_page','UtilisateurController@page_login');


Route::get('/show_profile','MonEspaceController@index');
Route::post('/update_profile','MonEspaceController@updateProfile');
Route::post('/ajouter_utilisateur','MonEspaceController@ajouterUtilisateur');
Route::post('/update_utilisateur','MonEspaceController@updateUtilisateur');
Route::get('/ajouter_tache','MonEspaceController@ajouterTache');
Route::post('/update_tache','MonEspaceController@updateTache');
Route::get('/visualiser_tache','MonEspaceController@getTache');
Route::get('/supprimer_tache','MonEspaceController@dropTache');
Route::get('/supprimer_utilisateur','MonEspaceController@dropUtilisateur');
Route::post('/update_tache_page','MonEspaceController@page_update_tache');
Route::post('/update_tache','MonEspaceController@updateTache');
Route::post('/update_utilsateur_page','MonEspaceController@page_update_utilisateur');

Route::get('/password_page','MonEspaceController@page_password');
Route::post('/reset_password','MonEspaceController@resetPassword');


Route::get('/dashboard','DashboardController@page_dashboard');
Route::get('/dashboard_page','DashboardController@index');
Route::get('/repas_distribue_par_mois_general','DashboardController@getRepas_distribue_mois_general');

Route::get('/repas_mois_enfant','DashboardController@getRepas_mois_enfant');
Route::get('/repas_mois','DashboardController@getRepas_mois');

Route::get('visualiser_detail_aliment_vendu','DashboardController@getDetailAlimentVendu');
Route::get('visualiser_detail_aliment_don','DashboardController@getDetailAlimentDon');

Route::get('visualiser_detail_brouillon','DashboardController@getDetailBrouillon');
Route::get('visualiser_detail_don_encours','DashboardController@getDetailDonEncours');
Route::get('visualiser_detail_vente_encours','DashboardController@getDetailVenteEncours');


Route::get('/dashboard_entreprise','DashboardController@getDashboard_entreprise');
Route::get('/dashboard_receveur','DashboardController@getDashboard_receveur');
Route::get('/repas_distribue_par_mois_receveur','DashboardController@getRepas_distribue_mois_receveur');
Route::get('/dashboard_benevole','DashboardController@getDashboard_benevole');
Route::get('/dashboard_client','DashboardController@getDashboard_client');
Route::get('/dashboard_transport','DashboardController@getDashboard_transport');
Route::get('/dashboard_commande','DashboardController@getDashboard_commande');
Route::get('/dashboard_encours','DashboardController@getDashboard_encours');

 Route::get('/send/email', 'DashboardController@mail');

Route::post('import', 'MonEspaceController@importFile');
 Route::get('export', 'MonEspaceController@export');
 
 
 /** produit ***/
 
 Route::get('produit', 'BddController@getListeProduit');
  Route::get('fiche_produit', 'BddController@getProduit');
  Route::get('update_produit_page', 'BddController@page_updateProduit');
 Route::post('update_produit', 'BddController@updateProduit');
 Route::get('page_ajouter_produit', 'BddController@page_ajoutProduit');
Route::get('vendre', 'BddController@vendre');
Route::get('archiver', 'BddController@archiver');
Route::post('ajouter_produit_don', 'BddController@ajouterProduitDon');
Route::post('ajouter_produit_vente', 'BddController@ajouterProduitVente');
Route::get('exporter_produit', 'BddController@exportProduit');
 
 /**** bdd *****/
 
Route::get('entreprise', 'BddController@getListeEntreprise');
Route::get('page_ajouter_entreprise', 'BddController@page_ajoutEntreprise');
Route::get('supprimer_entreprise', 'BddController@dropEntreprise');
Route::get('information_entreprise', 'BddController@getEntreprise');
Route::get('update_entreprise_page', 'BddController@page_updateEntreprise');
Route::post('update_entreprise', 'BddController@updateEntreprise');
Route::post('ajouter_entreprise', 'BddController@ajouterEntreprise');
Route::get('exporter_entreprise', 'BddController@exportEntreprise');
Route::get('page_contacter_entreprise', 'ContacterController@page_contacterEntreprise');



Route::get('map', 'BddController@index');

Route::get('receveur', 'BddController@getListeReceveur');
Route::get('detail_receveur', 'BddController@getDetailreceveur');
Route::get('fiche_receveur', 'BddController@getFichereceveur');
Route::get('supprimer_receveur', 'BddController@dropReceveur');
Route::get('update_receveur_page', 'BddController@page_updateReceveur');
Route::post('update_receveur', 'BddController@updateReceveur');
Route::post('ajouter_organisation', 'BddController@ajouterReceveur');
 Route::get('exporter_receveur', 'BddController@exportReceveur');
 Route::get('page_contacter_receveur', 'ContacterController@page_contacterReceveur');

Route::get('client', 'BddController@getListeConsommateur');
Route::get('detail_consommateur', 'BddController@getDetailConsommateur');
Route::get('fiche_consommateur', 'BddController@getFicheConsommateur');
Route::get('supprimer_client', 'BddController@dropClient');
Route::get('update_client_page', 'BddController@page_updateClient');
Route::post('update_client', 'BddController@updateClient');
Route::post('ajouter_client', 'BddController@ajouterClient');
 Route::get('exporter_client', 'BddController@exportClient');
 Route::get('page_contacter_client', 'ContacterController@page_contacterClient');

Route::post('ajouter_poste', 'BddController@ajouterPoste');
Route::post('ajouter_poste_organisation', 'BddController@ajouterPosteOrganisation');
Route::post('ajouter_periode_reduction', 'BddController@ajouterPeriodeReduction');
Route::post('ajouter_type_aliment', 'BddController@ajouterTypeAliment');
Route::post('ajouter_type_produit', 'BddController@ajouterTypeProduit');
Route::post('ajouter_type_entreprise', 'BddController@ajouterTypeEntreprise');
Route::post('ajouter_information_nutritionnelle', 'BddController@ajouterInformationNutritionnelle');
Route::post('ajouter_raison_surplus', 'BddController@ajouterRaisonSurplus');
Route::post('teste_pseudo', 'BddController@testePseudo');

/***** autre fonctionnalite  *****/
Route::post('recherche', 'BddController@rechercheGlobale');
Route::post('contacter', 'ContacterController@contacter');

Route::post('update_etat_donation', 'AutreController@updateEtatDonation');

Route::post('/connect', 'Auth\LoginController@authenticate');


Route::get('/logout', function(){
	
    Session::flush();
    Auth::logout();
	
    return Redirect::to("/login_page");
});

Auth::routes();


//Route::get('/home', 'HomeController@index')->name('home');

