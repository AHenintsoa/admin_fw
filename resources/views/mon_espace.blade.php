@extends('template/default')

@section('titre')
    Profile
@endsection

@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif

@if(Session::has('erreur_update_profile'))
<input type="hidden" id="modal_update_profile" name="modal_update_prof" value="{{ Session::get('erreur_update_profile') }}">
@endif

@if(Session::has('erreur_ajouter_profile'))
<input type="hidden" id="modal_creer_profile" name="modal_creer_prof" value="{{ Session::get('erreur_ajouter_profile') }}">
@endif

@if(Session::has('erreur_ajouter_tache'))
<input type="hidden" id="modal_ajouter_tache" name="modal_ajouter_tache" value="{{ Session::get('erreur_ajouter_tache') }}">
@endif




<section class="content">
      <div class="container-fluid">
			<div class="row">
         
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link active" href="#profile" id="bouton_profile" data-toggle="tab">Profile</a></li>
							@if(Session::get('id_utilisateur_type')=='2')
								<li class="nav-item"><a class="nav-link" href="#utilisateur" id="bouton_utilisateur" data-toggle="tab">Gestion des Utilisateurs</a></li>
								<li class="nav-item"><a class="nav-link" href="#tache" id="bouton_tache" data-toggle="tab">Gestion des Tâches</a></li>
							@endif
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     profile    -->
							<div class="active tab-pane" id="profile">
								<div class="card card-primary">
									<div class="card-body">
										<div class="card-body box-profile">
											<div class="text-center">
											  <img class="profilImage profile-user-img img-fluid img-circle"
												   src="{{ asset('img/'.$membre_detail->logo)  }}"
												   alt="User profile picture">
											</div>

											<h3 class="profile-username text-center"><?php echo $membre_detail->nom ;?></h3>

											<p class="text-muted text-center"><?php echo $utilisateur_type->label  ;?></p>
											<div class="text-muted text-center">
												@if( $pays ==null || $poste ==null || $membre_detail->tel =='' || $membre_detail->email =='' )
													<span class="help-block">
														<strong style="color:#FF0000"; >Profil non initialisé , veuillez compléter votre profil</strong>
													</span>
												@endif
												<div>
													<button style="width : 120px;" type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#updateProfile">Mettre à jour </button>
												</div>	
											</div>
											
										</div>
										<stron ><i class="fa fa-user mr-1"></i> Utilisateur</strong>

										<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
											<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
											<div class="col-8">{{ $membre_detail->nom }}</div>
										</div>

										<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
											<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Location</div>
											@if( $pays !=null )
											<div class="col-8">{{ $pays->label }}</div>
											@else  
												<div class="col-8">Non initialisé</div>
											@endif
										</div>
												
										<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
											<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Poste</div>
											@if( $poste !=null ) 
											<div class="col-8">{{ $poste->label }}</div>
											@else  
												<div class="col-8">Non initialisé</div>
											@endif
										</div>

										<strong><i class="fa fa-phone mr-1"></i> Contact </strong>

										<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
											<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Téléphone</div>
											@if( $membre_detail->tel !='' )
											<div class="col-8">{{ $membre_detail->tel }}</div>
											@else  
												<div class="col-8">Non initialisé</div>
											@endif
											
										</div>
										<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
											<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Adresse email</div>
											@if( $membre_detail->email !='' )
												<div class="col-8">{{ $membre_detail->email }}</div>
											@else  
												<div class="col-8">Non initialisé</div>
											@endif
										</div>
										<hr>
									</div>
									<!-- /.card-body -->
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
					 		@if(Session::get('id_utilisateur_type')=='2')
							<!---- Pane      Utilisateur  --->
							<div class="tab-pane" id="utilisateur">
								<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
									<div class="col-md-2"><button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#ajoutUtilisateur">Ajouter Utilisateur </button></div>
			<!----------						<div class="col-4"><a href="{{ url('export') }}"><button type="button" class="btn btn-primary btn-block" >Export data</button></a></div>  -->
								</div>	
			<!----------					<div class="row" style="margin-top: 20px;margin-bottom: 4px;">
									<form action="{{ url('import') }}" method="post" enctype="multipart/form-data">
										{{ csrf_field() }}
										<div class="col-6"><input name="donnee" type="file" class="form-control"></div>	</br>
										<div class="col-6"><input type="submit" class="btn btn-primary btn-block" value="Import data"></div>	
									</form>
									
								</div>							
						-->

								<div class="card">
									<div class="card-header">
									  <h3 class="card-title">Liste des utilisateurs</h3>
									</div>
									<!-- /.card-header -->
									<div class="card-body">
										<div class="card-body">
										  <table id="table_utilisateur" class="table table-bordered table-hover">
											<thead>
											<tr>
											  <th>Nom</th>
											  <th>Rôle</th>
											  <th>Poste</th>
											  <th>Location</th>
											  <th>Téléphone</th>
											  <th>Email</th>
											  <th>Paramètres</th>
											</tr>
											</thead>
											<tbody>
											@foreach( $liste_membre as $data_membre )
											@if($data_membre->role_id!=2)
											<tr>
											  <td>{{ $data_membre->nom }}</td>
											  <td>{{ $data_membre->role }}</td>
											  <td>{{ $data_membre->poste }}</td>
											  <td>{{ $data_membre->pays }}</td>
											  <td>{{ $data_membre->tel }}</td>
											  <td>{{ $data_membre->email }}</td>
											  <td>	
												<form action="{{ url('update_utilsateur_page') }}" method="POST">
												{{ csrf_field() }}
													<input name="type_modification" value="utilisateur" type="hidden">	
													<input name="id_membre_fw" type="hidden" value="{{ $data_membre->id_membre_fw }}">
													<input name="label_role" type="hidden" value="$data_membre->role }}">
													<input name="nom" type="hidden" value="{{ $data_membre->nom }}">
													<input name="label_role" type="hidden" value="{{ $data_membre->role }}">
													<input name="role_id" type="hidden" value="{{ $data_membre->role_id }}">
													<input name="label_poste" type="hidden" value="{{ $data_membre->poste }}">
													<input name="poste_id" type="hidden" value="{{ $data_membre->poste_id }}">
													<input name="label_pays" type="hidden" value="{{ $data_membre->pays }}">
													<input name="pays_id" type="hidden" value="{{ $data_membre->pays_id }}">
													<input name="tel" type="hidden" value="{{ $data_membre->tel }}">
													<input name="email" type="hidden" value="{{ $data_membre->email }}">
													
													<button class="bouton" type="submit"><span class="fa  fa-edit"></span></button>
													
													<button class="bouton" url_supprimer_utilisateur="{{ url('supprimer_utilisateur')}}?utilisateur_id={{$data_membre->utilisateur_id }}" onclick="supprimerUtilisateur( this); return  false;"><span class="fa  fa-remove"></span></button>
												</form>
											  </td>
											</tr>
											@endif
											@endforeach
											
											<tfoot>
											
											</tfoot>
										  </table>
										 </div>
											
									</div>
									<!-- /.card-body -->
								</div>
								<!-- cerd -->
							</div>	
							<!-- /.tab-pane -->
							  
							 <!----- Pane     Tache --> 
							<div class="tab-pane" id="tache">
							<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
								<div class="col-md-2"><button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#ajoutTache">Ajouter Tache </button></div>
							</div>	
								<section class="content">
								  <div class="row">
									<div class="col-12">
									  <div class="card">
										<div class="card-header">
										  <h3 class="card-title">Liste des tâches</h3>
										</div>
										<!-- /.card-header -->
										<div class="card-body">
										  <table id="table_tache" class="table table-bordered table-striped">
											<thead>
											<tr>
											  <th>Label</th>
											  <th>Description</th>
											  <th>Objectif</th>
											  <th>Utilisateur assigné</th>
											  <th>Date d'échéance</th>
											  <th>Paramètres</th>
											</tr>
											</thead>
											<tbody>
											@foreach( $liste_tache as $tache_data )
											<tr>
											  <td>{{ $tache_data->label }}</td>
											   <td>{{ (strlen($tache_data->description)<=10) ? $tache_data->description : substr( $tache_data->description , 0 , 10 ).'...' }}  </td>
											  <td>{{  $tache_data->objectif  }}</td>
											  @if( $tache_data->nom ==null)
													<td>Non-assigné</td>
											  @else
													<td>{{ $tache_data->nom }}</td>
												@endif
											  <td>{{ $tache_data->date_echeance }}</td>
											  <td>
													<button rowId="{{ $tache_data->id_tache }}" onclick="visualiser(this)" class="bouton"><span class="fa  fa-eye"></span></button>
													<button  url_supprimer_tache="{{ url('supprimer_tache')}}?id_tache={{$tache_data->id_tache }}" onclick="supprimerTache(this)"  class="bouton" ><span class="fa  fa-remove"></span></button>
											  </td>
											</tr>
											@endforeach
											</tbody>
											<tfoot>
											<tr>
											 
											</tr>
											</tfoot>
										  </table>
										</div>
										<!-- /.card-body -->
									  </div>
									  <!-- /.card -->
									</div>
									<!-- /.col -->
								  </div>
								  <!-- /.row -->
								</section>
								<!-- /.content -->
								  
							</div>
							 <!-- /.tab-pane -->
							@endif
					<!-- /.tab-content -->
				  </div><!-- /.card-body -->
				</div>
				<!-- /.nav-tabs-custom -->
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
	
	
@endsection

@section('modals')
<!-- Modal -->


<!----modal   ajouter   une     tâche---->
@if(Session::get('id_utilisateur_type')=='2')
<div class="modal fade" id="ajoutTache" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<form action="{{ url('ajouter_tache') }}" method="get">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ajouter tâche</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	 
		{{ csrf_field() }}

		<label>Label</label>
        <input name="label"  class="form-control form-control-lg" type="text" >
		@if ($errors->has('label'))
			<span class="help-block">
				<strong style="color:#FF0000"; >{{ $errors->first('label') }}</strong>
			</span>
		@endif
        <br>
		<label>Description</label>
		<input name="description"  class="form-control form-control-lg" type="text" >
		@if ($errors->has('description'))
			<span class="help-block">
				<strong style="color:#FF0000"; >{{ $errors->first('description') }}</strong>
			</span>
		@endif
        <br>
		<label>Objectif</label>
		<input name="objectif" value="" class="form-control form-control-lg" type="text" >
        @if ($errors->has('objectif'))
			<span class="help-block">
				<strong style="color:#FF0000"; >{{ $errors->first('objectif') }}</strong>
			</span>
		@endif
		<br>
		<label>Utilisateur</label>
		 <select name="utilisateur_id"  class="form-control" >
			<option value="">Non assignée</option>
			@foreach($liste_membre as $membre) { ?>
			  <option value="{{  $membre->utilisateur_id }}">{{ $membre->nom }}</option>
			@endforeach
		</select>
		<label>Date Assignation</label>
		<input name="date_assignation" type="date" class="form-control" id="inputName" >
		@if ($errors->has('date_assignation'))
			<span class="help-block">
				<strong style="color:#FF0000"; >{{ $errors->first('date_assignation') }}</strong>
			</span>
		@endif
		<br>
		<label>Date Echeance</label>
		<input name="date_echeance" type="date" class="form-control" id="inputName">
		@if ($errors->has('date_echeance'))
			<span class="help-block">
				<strong style="color:#FF0000"; >{{ $errors->first('date_echeance') }}</strong>
			</span>
		@endif
		<br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <input type="submit" class="btn btn-primary" value="Enregistrer">
      </div>
	 </form>
	</div>
  </div>
</div>
@endif	
	
<!---mettre    à   jour    profil----->
<div class="modal fade" id="updateProfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Mis à jour profil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <form action="{{ url('update_profile') }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input name="id_membre_fw" value="<?php echo $membre_detail->id_membre_fw ?>" type="hidden">
		<label>Photo de profil</label>
        <input name="logo" value="<?php echo $membre_detail->logo ?>" class="form-control form-control-lg"  type="file">
		@if ($errors->has('logo'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('logo') }}</strong>
				</span>
			@endif
        <br>
		<label>Nom</label>
        <input name="nom" value="<?php echo $membre_detail->nom ?>" class="form-control form-control-lg" type="text" >
        
			@if ($errors->has('nom'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('nom') }}</strong>
				</span>
			@endif
		<br>
		<label>Tel</label>
		<input name="tel" value="<?php echo $membre_detail->tel ?>" class="form-control form-control-lg" type="text" >
        @if ($errors->has('tel'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('tel') }}</strong>
				</span>
			@endif
		<br>
		<label>Email</label>
		<input name="email" value="<?php echo $membre_detail->email ?>" class="form-control form-control-lg" type="email" >
        <br>
		<label>Poste</label>
		<select name="poste_id"  class="form-control">
		<?php foreach($liste_poste as $poste) { ?>
			  <option value="<?php echo $poste->id_poste ?>"><?php echo $poste->label ?></option>
		<?php } ?>  
		</select>
		@if ($errors->has('poste_id'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('poste_id') }}</strong>
				</span>
			@endif
		<label>Pays</label>
		<select name="pays_id"  class="form-control">
		<?php foreach($liste_pays as $pays) { ?>
			  <option value="<?php echo $pays->id_pays ?>"><?php echo $pays->label ?></option>
		<?php } ?>	  
		</select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        <input type="submit" class="btn btn-primary" value="Enregistrer">
      </div>
	 </form> 
    </div>
  </div>
</div>

@if(Session::get('id_utilisateur_type')=='2')
<!----modal   visualiser     tâche---->
<div class="modal fade" id="visualiserTache" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail tâche</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <form action="{{ url('update_tache_page') }}" method="POST">
		{{ csrf_field() }}
		<input name="type_modification" value="tache" type="hidden">
		<input name="id_tache" value="" id="id_tache_v" type="hidden">
		<label>Label</label>
        <input name="label" value="" id="label_v" class="form-control form-control-lg" type="text" readonly>
        <br>
		<label>Description</label>
		<input name="description" value="" id="description_v" class="form-control form-control-lg" type="text" readonly>
        <br>
		<label>Objectif</label>
		<input name="objectif" value="" id="objectif_v" class="form-control form-control-lg" type="text" readonly>
        <br>
		<label>Utilisateur</label>
		<input name="utilisateur_id" value="" id="utilisateur_id_v" class="form-control form-control-lg" type="hidden" >
		<input name="nomUtilisateur" value="" id="utilisateur_v" class="form-control form-control-lg" type="text" readonly>
        <br>
		<label>date_assignation</label>
		<input name="date_assignation" id="date_assignation_v" value="" class="form-control form-control-lg" type="date" readonly>
        <br>
		<label>date_echeance</label>
		<input name="date_echeance" id="date_echeance_v" value="" class="form-control form-control-lg" type="date" readonly>
        <br>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		
        <input type="submit" class="btn btn-primary" value="Mettre à jour">
      </div>
	 </form> 
    </div>
  </div>
</div>

<!----modal   ajouter   utilisateur---->
<div class="modal fade" id="ajoutUtilisateur" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Céer profil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<form class="form-horizontal" action="{{ url('ajouter_utilisateur') }}" method="post">
		  {{ csrf_field() }}
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>

			<div class="col-sm-10">
			  <input name="nom" type="text" class="form-control" id="inputName" >
			</div>
			@if ($errors->has('nom'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('nom') }}</strong>
				</span>
			@endif
		  </div>
		  <div class="form-group">
			<label for="inputEmail" class="col-sm-2 control-label">Pseudo</label>

			<div class="col-sm-10">
			  <input name="pseudo" type="text" class="form-control" id="inputName" >
			</div>
			@if ($errors->has('pseudo'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('pseudo') }}</strong>
				</span>
			@endif
		  </div>
		  <div class="form-group">
		   <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
				<label for="password" class="col-md-4 control-label">Password</label>

			   <div class="col-sm-10">
					<input id="password" type="password" class="form-control" name="password" required>

					@if ($errors->has('password'))
						<span class="help-block">
							<strong style="color:#FF0000"; >{{ $errors->first('password') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

				<div class="col-sm-10">
					<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
				</div>
			</div>
		  </div>
		 
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Email</label>

			<div class="col-sm-10">
			  <input name="email" type="email" class="form-control" id="inputName" >
			</div>
			
		  </div>
		   <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Téléphone</label>

			<div class="col-sm-10">
			  <input name="telephone" type="text" class="form-control" id="inputName">
			</div>
			@if ($errors->has('telephone'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('telephone') }}</strong>
				</span>
			@endif
		  </div>
		   <div class="form-group">
			<label for="inputName2" class="col-sm-2 control-label">Poste</label>
			<div class="col-sm-10">
				<select name="poste_id"  class="form-control">
				@foreach($liste_poste as $poste) { ?>
					  <option value="{{  $poste->id_poste }}">{{ $poste->label }}</option>
				@endforeach
				</select>
			</div>	
		  </div>
		   <div class="form-group">
			<label for="inputName2" class="col-sm-2 control-label">Role</label>
			<div class="col-sm-10">
				<select name="utilisateur_type_id"  class="form-control">
				@foreach($liste_utilisateur_type as $type) { ?>
					  <option value="{{  $type->id_utilisateur_type }}">{{ $type->label }}</option>
				@endforeach
				</select>
			</div>	
		  </div>
		   <div class="form-group">
			<label for="inputName2" class="col-sm-2 control-label">Pays</label>
			<div class="col-sm-10">
				<select name="pays_id"  class="form-control">
				@foreach($liste_pays as $pays) 
					  <option value="{{  $pays->id_pays }}">{{ $pays->label }}</option>
				@endforeach
				</select>
			</div>	
		  </div>
		  <div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			  <input type="submit" class="btn btn-danger" value="insérer">
			</div>
		  </div>
		  
		</form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

@endif

<!---- modal   supprimer      tâche  ----->
<div class="modal fade" id="supprimer_tache" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<form action="{{ url('ajouter_tache') }}" method="get">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Supprimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<a id="supprimer_tache_modal" href="supprimer_tache"><button type="button" class="btn btn-secondary" >Sûr</button></a>
      </div>
	 </form>
	</div>
  </div>
</div>

<!---- modal   supprimer      utilisateur  ----->
<div class="modal fade" id="supprimer_utilisateur" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<form action="{{ url('ajouter_tache') }}" method="get">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Supprimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<a id="supprimer_utilisateur_modal" href=""><button type="button" class="btn btn-secondary" >Sûr</button></a>
      </div>
	 </form>
	</div>
  </div>
</div>

@endsection

@section('custom_script')
<script>
  $(function () {
    
	$("#table_tache").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		

    $("#table_utilisateur").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		

	
    var choix = $("#choixPane").val();
	if(choix){
		console.log(choix);
		if(choix==1){
			$("#bouton_profile").removeAttr("class");
			$("#profile").removeAttr("class");
			$("#bouton_profile").attr("class" , "nav-link");
			$("#profile").attr("class" , "tab-pane" );
			
			$("#tache").removeAttr("class");
			$("#bouton_tache").removeAttr("class");
			$("#bouton_tache").attr("class" , "nav-link active show");
			$("#tache").attr("class" , "tab-pane active show" );
			
		}else if(choix==2){
			$("#bouton_profile").removeAttr("class");
			$("#profile").removeAttr("class");
			$("#bouton_profile").attr("class" , "nav-link");
			$("#profile").attr("class" , "tab-pane" );
			
			$("#utilisateur").removeAttr("class");
			$("#bouton_utilisateur").removeAttr("class");
			$("#bouton_utilisateur").attr("class" , "nav-link active show");
			$("#utilisateur").attr("class" , "tab-pane active show" );
		}else{
			console.log('default');
		}		
	}
	var erreur_update_profile = $("#modal_update_profile").val();
	console.log(erreur_update_profile);
	if(erreur_update_profile){
		$("#updateProfile").modal();
	}
	
	var erreur_creer_profile = $("#modal_creer_profile").val();
	console.log(erreur_creer_profile);
	if(erreur_creer_profile){
		$("#ajoutUtilisateur").modal();
	}
	var erreur_ajoutre_tache = $("#modal_ajouter_tache").val();
	console.log(erreur_ajoutre_tache);
	if(erreur_ajoutre_tache){
		$("#ajoutTache").modal();
	}
	
  });
  
	function visualiser(element) {
                    
		var criteres = $(element).attr('rowID');
		getTache(criteres,function(resp){
			
			var response = resp ;
			console.log(response);
			$('#id_tache_v').val(response.id_tache);
			$('#label_v').val(response.label);
			$('#description_v').val(response.description);
			$('#objectif_v').val(response.objectif);
			$('#utilisateur_v').val(response.nom);
			$('#utilisateur_id_v').val(response.utilisateur_id);
			$('#date_assignation_v').val(response.date_assignation);
			$('#date_echeance_v').val(response.date_echeance);
			$("#visualiserTache").modal();
		})
	}
		
  function getTache(critere ,callBack) {
                  
		var xMLHttprequest = new XMLHttpRequest() ;
		xMLHttprequest.open('GET','visualiser_tache?id_tache='+critere)	;
					
		xMLHttprequest.addEventListener('readystatechange',function(){
			if(xMLHttprequest.readyState==XMLHttpRequest.DONE && xMLHttprequest.status==200) {
								
				var resp = JSON.parse(xMLHttprequest.responseText) ; 
				callBack(resp) ; 
			}
		})
		xMLHttprequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xMLHttprequest.send() ;

	}
	function supprimerTache(element) {
		var lien = $(element).attr('url_supprimer_tache');
		console.log(lien);
		document.getElementById("supprimer_tache_modal").href =lien
		$("#supprimer_tache").modal();
	}
	function supprimerUtilisateur( element) {
	
		var lien = $(element).attr('url_supprimer_utilisateur');
		console.log(lien);
		document.getElementById("supprimer_utilisateur_modal").href =lien
		$("#supprimer_utilisateur").modal();
	}
	
</script>
@endsection