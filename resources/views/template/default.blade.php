<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>@yield('titre')</title>

    <!-- Font Awesome Icons -->
    
    <link rel="stylesheet" href="{{ url('dist/plugins/font-awesome/css/font-awesome.min.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('dist/css/adminlte.min.css') }}">

    <link rel="stylesheet" href="{{ url('css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('dist/plugins/datatables/dataTables.bootstrap4.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('plugins/toastr/toastr.min.css') }}">

    @yield('custom_styles')

    <style type="text/css">
      .content-wrapper {
          background: #fff !important;
      }

      .alert-info, .bg-info, .label-info {
        background-color: #98A7A7 !important;
      }

    </style>

  </head>
  <body class="hold-transition sidebar-mini">
  @if(Session::has('password'))
<input type="hidden" id="erreur_password" name="modal_update_prof" value="{{ Session::get('password') }}">
@endif
@if(Session::has('password_succes'))
<input type="text" id="password_succes" name="modal_ajouter_tache" value="{{ Session::get('password_succes') }}">
@endif

    <input type="hidden" id="site-url" value="{{ url('') }}">
    <div class="wrapper">
      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
        <!-- Left navbar links -->
        <ul class="navbar-nav" style="align-items: center">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
          </li>
		  <li class="nav-item">
             <form class="form-inline ml-3" action="recherche" method="post">
			  {{ csrf_field() }}
			  <div class="input-group input-group-sm">
				<input class="form-control form-control-navbar" type="search" name="mot" placeholder="Search" aria-label="Search">
				<div class="input-group-append">
				  <button class="btn btn-navbar" type="submit">
					<i class="fa fa-search"></i>
				  </button>
				</div>
			  </div>
			</form>
          </li>
		  
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
		<li class="nav-item dropdown">
            <a class="nav-link" title="Changer mot de passe" data-toggle="dropdown" href="#">
              <i class="fa fa-cog"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-mr dropdown-menu-right">
              <a class="dropdown-item">
                <!-- Message Start -->
                <div class="media">
                  <div class="media-body">
                    <h3 class="dropdown-item-title btn"  data-toggle="modal" data-target="#password">
                      Changer mot de passe
                    </h3>
                  </div>
                </div>
                <!-- Message End -->
              </a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link" title="Déconnexion" data-toggle="dropdown" href="#">
              <i class="fa fa-power-off"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-mr dropdown-menu-right">
              <a href="{{ url('logout') }}" class="dropdown-item">
                <!-- Message Start -->
                <div class="media">
                  <div class="media-body">
                    <h3 class="dropdown-item-title">
                      Se déconnecter
                    </h3>
                  </div>
                </div>
                <!-- Message End -->
              </a>
            </div>
          </li>
		  
        </ul>
		
      </nav>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4" style="z-index: 110;">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
          <img src="{{ url('img/icon_foodwise.png') }}" alt="AdminLTE Logo" class="brand-image"
               style="opacity: .8">
          <span class="brand-text font-weight-light">FoodWise</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar" id="main-sidebar">
          <!-- Sidebar user panel (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex"style="align-items: center">
            <div class="image">
              <img src="{{ url('img/homme_white.png') }}" class="img-circle elevation-2" alt="User Image">
            </div>
			@if(Session::get('id_utilisateur_type')=='2')
            <div class="info">
              <a href="{{ url('show_profile') }}" class="d-block">Admin</a>
            </div>
			@else
				 <div class="info">
              <a href="{{ url('show_profile') }}" class="d-block">Employé</a>
            </div>
			@endif
          </div>
		  

      
          <!-- Sidebar Menu -->
          <nav class="mt-2" style="margin-top: 0.5rem !important;">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

              <?php foreach($list_menu as $menu): ?>
                
                 <?php if(Session::get('id_utilisateur_type')=='5' && $menu['menu']['action']=="nos_donnees" )    continue; ?>



                <?php if(array_key_exists("title",$menu)): ?>
                  <li class="nav-header"><?php echo $menu["title"]; ?></li>
                <?php endif; ?>

                <?php if(array_key_exists("menu",$menu)): ?>
                  <li class="nav-item <?php echo (array_key_exists('sous_menu',$menu['menu']))?'has-treeview':''; ?> <?php echo ($action==$menu['menu']['action'])?'menu-open':''; ?>">
                    <a href="<?php echo $menu['menu']['link']; ?>" class="nav-link <?php echo ($action==$menu['menu']['action'])?'active':''; ?>">
                      <i class="nav-icon fa <?php echo $menu['menu']['icon'] ?>"></i>
                      <p>
                        <?php echo $menu["menu"]["name"]; ?>
                      </p>
                    </a>
                    <?php if(array_key_exists('sous_menu',$menu['menu'])): ?>
                      <ul class="nav nav-treeview">
                        <?php foreach($menu['menu']['sous_menu'] as $sous_menu): ?>
                          <li class="nav-item">
                            <a href="<?php echo $sous_menu['link']; ?>" class="nav-link <?php echo ($sous_action==$sous_menu['sous_action'])?'active':''; ?>" style="padding-left: 25px !important;">
                              <i class="nav-icon fa <?php echo $sous_menu['icon'] ?>"></i>
                              <p>
                                <?php echo $sous_menu["name"]; ?>
                              </p>
                            </a>
                          </li>
                        <?php endforeach; ?>
                      </ul>
                    <?php endif; ?>
                  </li>
                <?php endif; ?>

              <?php endforeach; ?>
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <!-- <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?php echo $action; ?></h1>
              </div> --><!-- /.col -->
              <div class="col-sm-6">
                <!-- <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Dashboard v2</li>
                </ol> -->
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            @yield('contenu')
          </div><!--/. container-fluid -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->

      <!-- Main Footer -->
      <!-- <footer class="main-footer"> -->
        <!-- Default to the left -->
    <!--     <strong>Powered by <a href="#">SOFTIMAD</a>.</strong>
      </footer> -->
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
		<!---- modal   password      ----->
<div class="modal fade" id="password" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Mis à jour du mot de passe</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  
		<form class="form-horizontal" method="POST" action="{{ url('reset_password') }}">
		 <div class="modal-body">
				{{ csrf_field() }}
				<div class="form-group">
		   <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
				<label for="password" class="col-md-4 control-label">Password</label>

			   <div class="col-sm-10">
					<input id="password" type="password" class="form-control" name="password" required>

					@if ($errors->has('password'))
						<span class="help-block">
							<strong style="color:#FF0000"; >{{ $errors->first('password') }}</strong>
						</span>
					@endif
				</div>
			</div>

			<div class="form-group">
				<label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

				<div class="col-sm-10">
					<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
				</div>
			</div>
		  </div>
		 </div>
		 <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-primary" value="Save changes">
		</div>
		</form>
		
	</div>
  </div>
</div>

<!---- modal   password   changed   ----->
<div class="modal fade" id="password_succes_modal" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Mot de passe changé</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
	  
		
		 <div class="modal-body">
			
		  </div>
		 <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		</div>
		
	</div>
  </div>
</div>

<!----end modal -->
	@yield('modals')
	

    <script src="{{ url('dist/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ url('dist/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- AdminLTE App -->
    <script src="{{ url('dist/js/adminlte.js') }}"></script>

    <!-- PAGE PLUGINS -->
    <!-- SparkLine -->
    <script src="{{ url('dist/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- jVectorMap -->
    <script src="{{ url('dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ url('dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="{{ url('dist/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>

    <script src="{{ url('dist/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ url('dist/plugins/datatables/dataTables.bootstrap4.js') }}"></script>

    <script type="text/javascript" src="{{ url('plugins/toastr/toastr.min.js') }}"></script>
	<script>
		 $(function () {
			  @if(Session::has('password')) 
			var erreur_password = $("#erreur_password").val();
			console.log(erreur_password);
			if(erreur_password){
				$("#password").modal();
			}
			 @endif 
			 @if(Session::has('password_succes'))
			var password_changed = $("#password_succes").val();
			console.log(password_changed);
			if(password_changed){
				$("#password_succes_modal").modal();
			}
			 @endif 
			
		  });
	</script>
    @yield('custom_script')

  </body>
</html>
