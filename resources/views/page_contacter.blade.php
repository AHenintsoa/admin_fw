@extends('template/default')

@section('titre')
   Envoyer email
@endsection
@section('custom_styles')
    <link rel="stylesheet" href="{{ asset('dist/plugins/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
	
@endsection

@section('contenu')
@if(Session::has('succes'))
<input type="hidden" id="etat_email" name="choix" value="{{ Session::get('succes') }}">
@endif
<section class="content">
      <div class="container-fluid">
			<div class="row">
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link " href="{{ url('entreprise') }}">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									<div class="card-header">
										<h3>Envoye email à @if($type=='entreprise'){{ $entreprise->nom_entreprise }} @elseif($type=='client'){{ $client->nom.' '.$client->prenom }} @else{{ $organisation->nom_organisation }} @endif </h3>
									</div>
									<div class="card-body">
									<input type="hidden" value="{{ csrf_token() }}" id="id_token">
										@if($type=='entreprise')
										<div class="row " style="margin-top: 4px;margin-bottom: 4px;">
											<div class="col-3" style="padding-left:20px;font-weight: bolder;color: #646369;">Destinataire</div>
											<div class="col-9"><select  class="form-control select2" id="id_destinataire" name="email_destinataire" style="width: 100%;" tabindex="-1" aria-hidden="true" >
																@foreach($liste_responsable as $responsable) 
																	<option value="<?php echo trim($responsable->employe->email); ?>"  title=" {{ $responsable->employe->nom }} - Responsable {{ $responsable->position}} " ><?php echo $responsable->employe->email; ?></option>
																	
															  @endforeach
															</select></div>
										</div>
										@elseif($type=='client')
										<div class="row " style="margin-top: 4px;margin-bottom: 4px;">
											<div class="col-3" style="padding-left:20px;font-weight: bolder;color: #646369;">Destinataire</div>
											<div class="col-9"><input type="text" class="form-control" id="id_destinataire" name="email_destinataire" value="{{ $client->mail }}" readonly></div>
										</div>
										@else
										<div class="row " style="margin-top: 4px;margin-bottom: 4px;">
											<div class="col-3" style="padding-left:20px;font-weight: bolder;color: #646369;">Destinataire</div>
											<div class="col-9"><input type="text" class="form-control" id="id_destinataire" name="email_destinataire" value="{{ $email }}" readonly></div>
										</div>
										@endif
										</br>
										<div class="row " style="margin-top: 4px;margin-bottom: 4px;">
											<div class="col-3" style="padding-left:20px;font-weight: bolder;color: #646369;">Sujet</div>
											<div class="col-9"><input type="text" class="form-control" id="id_sujet" name="sujet" placeholder="Le sujet" ></div>
										</div>
										</br>
										<div class="row " style="margin-top: 4px;margin-bottom: 4px;">
											<div class="col-3" style="padding-left:20px;font-weight: bolder;color: #646369;">Message</div>
											<div class="col-9"><textarea type="text" class="textarea" id="id_message" name="message" placeholder="Votre message" ></textarea></div>
										</div>
										</br>
										<div class="row " style="margin-top: 4px;margin-bottom: 4px;">
											<div class="col-9"></div>
											<div class="col-3"><p class="btn btn-success" onclick="confirmer();">Envoyer </p><input type="submit" class="btn"  style ="display : none ;" value="Envoyer" id="id_bouton_valider"></div>
										</div>
										
									</div>
									<!-- /.card -->	  
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
					 <!-- ./card-body -->
				</div>
				<!-- /.card-->
		  </div>
	  <!-- /.col -->
	 </div>
	 <!-- /.row -->
</div>
<!-- /.container-fluid -->
</section>
@endsection
@section('modals')
<!-- etat d envoie-->
<div class="modal fade modal-custumed" id="succes_envoie_mail" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Envoie</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <center><p id="resultat_email"></p></center>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
      </div>
	</div>
  </div>
</div>
<!-- loader-->
<!-- confirmation d envoie-->
<div class="modal fade modal-custumed" id="confirmation" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Etes-vous sûr ?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-succes" onclick="envoyer();" data-dismiss="modal">Envoyer</button>
      </div>
	</div>
  </div>
</div>
<!-- loader-->
<div class="modal fade modal-custumed" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
       
      </div>
      <center><h4>Encours.</h4> <div class="loader"></div></center>
      <div class="modal-footer">
       
      </div>
	</div>
  </div>
</div>
@endsection
@section('custom_script')
<!-- Select2 -->
 <script src="{{ asset('dist/plugins/select2/select2.full.min.js') }}"></script>
 <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
 <script src="{{ asset('plugins/tinymce/tinymce.min.js') }}"></script>
<script type="text/Javascript">
	

$(function () {
	$('.select2').select2() ;// initialisation element select2
	
	tinymce.init({
		theme: "modern" ,
		selector: ".textarea" ,
		themes: "hr undo redo | alignleft aligncenter alignright alignjustify | styleselect | bold italic underline | bullist numlist outdent indent | image | forecolor backcolor | medias visualblocks wordcount",
		plugins: "textcolor , print , table , link , lists , emoticons , code , textcolor , hr visualblocks wordcount" ,
		language: "fr" ,
		height: "250" ,
		relative_urls: "false" ,
		paste_remove_styles: false ,
		paste_remove_span: false ,
		entity_encoding: "raw" ,
		
	}).then(function(e){
		var tiny_mce = tinymce.get(); //return all tinymce used
			tiny_mce[0].getBody().addEventListener('key_up' , function(e){
				 $('#id_message').val(e.target.innerHTML);
			});
			tiny_mce[0].getBody().addEventListener('blur' , function(e){
				 $('#id_message').val(e.target.innerHTML);
			});


	}); 
	 
	
	

});

function confirmer() {
		
		$("#confirmation").modal();
	}
function envoyer() {
	var destinataire = $('#id_destinataire').val();
	var sujet = $('#id_sujet').val();
	var message = $('#id_message').val();
	console.log('message :'+message);
	var token = $('#id_token').val();
	var x =new FormData();
	x.append('email_destinataire' , destinataire);	
	x.append('_token' , token);	
	x.append('sujet' , sujet);	
	x.append('message' , message);	
	console.log(x);
		$("#loadMe").modal();
		var lien = '{{ url('contacter')}}';
		console.log(lien);
		$.ajax({
		   url : 'http://localhost/admin_fw/public/contacter',
		   method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				console.log(response);
			   if(response=='succes'){
				  $("#loadMe").modal('toggle');
				  $("#envoie_mail_profil").modal('toggle');
				toastr.success('Envoie mail réussie','Notification', {timeout:5000});
			  }else{
				  $("#loadMe").modal('toggle');
				  $("#envoie_mail_profil").modal('toggle');
				  toastr.error('Echec d\'envoie du mail.','Notification', {timeout:5000});
				
			}
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				$("#loadMe").modal('toggle');
				$("#envoie_mail_profil").modal('toggle');
				toastr.error('Echec d\'envoie du mail.','Notification', {timeout:5000});
				// console.log(statut);
				// console.log(erreur);
		   }
		});  
		$("#loadMe").modal();
	}
  
  
</script>
@endsection
