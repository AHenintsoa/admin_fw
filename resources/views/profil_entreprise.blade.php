@extends('template/default')

@section('titre')
   Profil entreprise
@endsection

@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif


<section class="content">
      <div class="container-fluid">
			<div class="row">
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link active" href="{{ url('entreprise') }}">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									<div class="card-body">
										<h5>Action </h5>
										<div class="row"> 
											<div class="col-2"> 
												<a href= "{{ url('update_entreprise_page') }}?id_entreprise={{ $entreprise->id_entreprise }}" ><button type="button" class="btn btn-success">Mettre à jour</button></a>
											</div>
											<div class="col-2"> 
												<button  url_supprimer_entreprise="{{ url('supprimer_entreprise')}}?id_entreprise={{$entreprise->id_entreprise }}" onclick="supprimerEntreprise(this)"   class="btn btn-danger">Supprimer</button>
											</div>
										</div>
									</div>
									<div class="card-body">
										  <div class="row">
													<div class="col">
														<strong>
															<i class="fa fa-address-card mr-1"></i>
															 Information complémentaire
														</strong>
														<div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">NIF</div>
																<div class="col-8"><?php echo (!empty($entreprise->nif_entreprise))?$entreprise->nif_entreprise:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">STAT</div>
																<div class="col-8"><?php echo (!empty($entreprise->stat_entreprise))?$entreprise->stat_entreprise:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Chiffre d'affaire</div>
																<div class="col-8"><?php echo (!empty($entreprise->ca_entreprise))?$entreprise->ca_entreprise:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
															</div>
														</div>
														<hr>
														<strong>
															<i class="fa fa-building mr-1"></i>
															 Type de l'entreprise 
														</strong>
														<div>
															<?php if(empty($entreprise_type)): ?>
																<span style="margin-left: 16px;font-style: italic;margin-top: 8px;display: inline-block;">(Non défini)</span>
															<?php endif; ?>
															<?php foreach ($entreprise_type as  $type) : ?>
															  <div class="tag-mp"><?php echo $type->label; ?></div>
															<?php endforeach; ?>
														</div>
														<hr>
														<strong>
															<i class="fa fa-user mr-1"></i> 
																Responsable numéro 1
														</strong>
														<div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
																<div class="col-8"><?php echo $employe_info_responsable[0]->nom." ".$employe_info_responsable[0]->prenom; ?></div>
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Poste</div>
																<div class="col-8"><?php echo (!empty($poste_employe_responsable[0]->label))?$poste_employe_responsable[0]->label:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Email</div>
																<div class="col-8"><?php echo $employe_info_responsable[0]->email; ?></div>
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Téléphone</div>
																<div class="col-8"><?php echo $employe_info_responsable[0]->telephone; ?></div>
															</div>
														</div>
														<hr>
														<strong>
															<i class="fa fa-user mr-1"></i> 
																Responsable numéro 2
														</strong>
														<?php if(count($employe_info_responsable)==2): ?>
															<div>
																<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																	<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
																	<div class="col-8"><?php echo $employe_info_responsable[1]->nom." ".$employe_info_responsable[1]->prenom; ?></div>
																</div>
																<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																	<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Poste</div>
																	<div class="col-8"><?php echo $poste_employe_responsable[1]->label; ?></div>
																</div>
																<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																	<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Email</div>
																	<div class="col-8"><?php echo $employe_info_responsable[1]->email; ?></div>
																</div>
																<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																	<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Téléphone</div>
																	<div class="col-8"><?php echo $employe_info_responsable[1]->telephone; ?></div>
																</div>
															</div>
														<?php else: ?>
															<br>
															<span style="margin-left: 16px;font-style: italic;">Aucun</span>
														<?php endif; ?>

													</div>

													<div class="col">
														<strong>
															<i class="fa fa-map-marker mr-1"></i> 
																Adresse
														</strong>
														<?php if(empty($adresse_entreprise)): ?>
															<br>
															<span style="margin-left: 16px;font-style: italic;margin-top: 8px;display: inline-block;">(Non défini)</span>
														<?php endif; ?>
														<ul style="margin-top: 13px;">
															@for( $i = 0 ; $i < count( $adresse_entreprise ) ; $i++ ) 
																<li>
																	<?php if($adresse_entreprise[$i]->label){ echo $adresse_entreprise[$i]->label.", ".$adresse_entreprise[$i]->code_postal." - ".$adresse_entreprise[$i]->ville.", ".$pays[$i]->label; }else{ echo "<span style='font-style: italic;display: inline-block;'>(Non défini)</span>"; } ?>
																</li>
															@endfor
														</ul>
														<hr>
														<strong>
															<i class="fa fa-gift mr-1"></i> 
																Type d'aliments
														</strong>
														<div>
															<?php if(empty($type_aliment)): ?>
																<span style="margin-left: 16px;font-style: italic;margin-top: 8px;display: inline-block;">(Non défini)</span>
															<?php endif; ?>
															<?php foreach ($type_aliment as  $type) : ?>
															  <div class="tag-mp"><?php echo $type->label; ?></div>
															<?php endforeach; ?>
														</div>
														<hr>
														<strong>
															<i class="fa fa-credit-card mr-1"></i>
															 Information paiement
														</strong>
														<div>
															<?php if(count($compte_entreprise)==0): ?>
																<span style="margin-left: 16px;font-style: italic;margin-top: 8px;display: inline-block;">(Non défini)</span>
															<?php endif; ?>
															<?php for ($i= 0 ; $i < count( $compte_entreprise ) ; $i++):  ?>
																<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																	<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;"><?php echo $compte_entreprise[$i]->label; ?></div>
																	<div class="col-6"><?php echo $numero_compte_entreprise[$i]->numero_compte; ?></div>
																</div>
															<?php endfor; ?>
														</div>
														<hr>


										
														</div>
													  <!-- /.col -->
													</div>
													<!-- /.row -->
												
										  </div>
										  <!-- ./card-body -->
									</div>
									<!-- /.card -->
								</div>
								<!-- /.tab-pane -->
							</div>
							<!-- /.tab-content -->
						</div>
						 <!-- ./card-body -->
					</div>
					<!-- /.card-->
			  </div>
          <!-- /.col -->
		 </div>
		 <!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection
@section('modals')
<!---- modal   supprimer      tâche  ----->
<div class="modal fade" id="supprimer_entreprise" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Supprimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<a id="supprimer_entreprise_button" href="supprimer_tache"><button type="button" class="btn btn-secondary" >Sûr</button></a>
      </div>
	
	</div>
  </div>
</div>

@endsection
@section('custom_script')
<script type="text/Javascript">
	

	$(function () {
		
		

	});
  
function supprimerEntreprise(element) {
		var lien = $(element).attr('url_supprimer_entreprise');
		console.log(lien);
		document.getElementById("supprimer_entreprise_button").href =lien+"&return=0"
		
		$("#supprimer_entreprise").modal({ backdrop: 'static', keyboard: false });
	}
  
  
</script>
@endsection