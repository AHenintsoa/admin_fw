@extends('template/default')

@section('titre')
   Receveur
@endsection

@section('contenu')
@if(Session::has('erreur_update'))
<input type="hidden" id="erreur_update" name="choix" value="{{ Session::get('erreur_update') }}">
@endif

 @if(Session::get('id_utilisateur_type')!='5')
<section class="content">
      <div class="container-fluid">
			<div class="row">
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link" href="{{ url('entreprise') }}">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link active" href="">Receveur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
							
								<div class="card card-primary">
									
									<div class="card-body">
										<div class="row">
											<div class="col-3"><button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#ajouter_receveur">Ajouter Organisation </button></div>
											<div class="col-3"><a href="{{ url('exporter_receveur') }}"><button  class="btn btn-primary btn-block">Export </button></a></div>
										</div>
										</br>
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Information sur les receveurs</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_liste_receveur_table" class="table table-bordered table-hover">
												<thead>
												<tr><!-- (nom d ‘entité, type d’entité, nom complet personne de contact, mail, tel facebook, adresse)            -->
													  <th>Organisation </th>
													  <th>Type</th>
													  <th>Contact de l'organisation </th>
													  
													  <th>Adresse de l'organisation</th>
														<th>Action</th>
												</tr>
												</thead>
												<tbody>
												@for( $i = 0 ; $i < count($receveurs) ; $i++)
												<tr>
													  <td>{{$receveurs[$i]->nom_organisation }}
													  <td>{{$type[$i]->label }}</td> 
													  <td>{{$receveurs[$i]->numero }}</td>
													  
													  <td>{{$adresse[$i]->label }}</td>
													  <td> <a href="{{ url('update_receveur_page') }}?id_organisation={{$receveurs[$i]->id_organisation}}" ><button  class="bouton"><span class="fa  fa-pencil"></span></button></a>
														<button  class="bouton" row_id_organisation="{{ $receveurs[$i]['id_organisation'] }}" onclick="visualiserDetail(this)"><span class="fa  fa-eye"></span></button>
														<button  url_supprimer_receveur="{{ url('supprimer_receveur')}}?id_organisation={{$receveurs[$i]->id_organisation }}" onclick="supprimerReceveur(this)"  class="bouton" ><span class="fa  fa-remove"></span></button> 
														<a href="{{ url('page_contacter_receveur') }}?id_organisation={{$receveurs[$i]->id_organisation}}" ><button class="bouton" title="contacter"><span class="fa  fa-envelope"></span></button></a></td>
												</tr>
												@endfor
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										
									  </div>
									  <!-- ./card-body -->
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
					 
					
					<!-- /.tab-content -->
				  </div><!-- /.card-body -->
				</div>
				<!-- /.nav-tabs-custom -->
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection
@section('modals')
<!---- modal   supprimer      receveur  ----->
<div class="modal fade" id="supprimer_receveur" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Supprimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<a id="supprimer_receveur_button" href=""><button type="button" class="btn btn-secondary" >Sûr</button></a>
      </div>
	
	</div>
  </div>
</div>

<!----modal   ajouter     receveur---->
<div class="modal fade" id="ajouter_receveur" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ajouter Organisation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <form action="{{ url('ajouter_organisation') }}" method="POST">
		{{ csrf_field() }}
		<strong>
			<i class="fa fa-address-card mr-1"></i>
			 Information de l'organisation
		</strong>
		<div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
				<div class="col-8"><input type="text" class="form-control" name="nom_organisation" value="" placeholder="Entrez le nom de l'organisation"></div>
				@if ($errors->has('nom_organisation'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('nom_organisation') }}</strong>
				</span>
				@endif
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Numero</div>
				<div class="col-8"><input type="text" class="form-control" name="numero" value="" placeholder="Entrez le numero de l'entreprise"></div>
				@if ($errors->has('numero'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('numero') }}</strong>
				</span>
				@endif
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Type de l'organisation</div>
				<div class="col-8">
					<select  class="form-control select2" id="" name="id_type_organisation"  data-placeholder="Choisir le type de votre entreprise*" style="width: 100%;" tabindex="-1" aria-hidden="true">
					@foreach($liste_type_receveur as $type)
						<option value="{{ $type->id_type_organisation }}" > {{ $type->label }}</option>
					 @endforeach
					 </select>
				</div>
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Quartier</div>
				<div class="col-8"><input type="text" class="form-control" name="label_adresse" value="" placeholder="Entrez le nom de votre quartier"></div>
				@if ($errors->has('label_adresse'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('label_adresse') }}</strong>
				</span>
				@endif
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Coordonnées (Latitude / longitude)</div>
				<div class="col-3"><input type="text" class="form-control" id ="id_latitude" name="latitude_adresse" value=""  placeholder="Entrez la latitude"></div>
				<div class="col-3"><input type="text" class="form-control" id ="id_longitude" name="longitude_adresse" value=""  placeholder="Entrez la longitude"></div>
				<div class="col-2"><a  class="btn btn-default btn-block" onclick="modifierAdresse(this)"  title="Sélectionner les coordonnées sur une carte" ><i class="fa fa-map"></i> </a></div>
				@if ($errors->has('latitude_adresse'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('latitude_adresse') }}</strong>
				</span>
				@endif
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Code postal</div>
				<div class="col-8"><input type="text" class="form-control" name="code_postal_adresse" value="" placeholder="Entrez votre code postal"></div>
				@if ($errors->has('code_postal_adresse'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('code_postal_adresse') }}</strong>
				</span>
				@endif
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Ville</div>
				<div class="col-8"><input type="text" class="form-control" name="ville_adresse" value="" placeholder="Entrez le nom de votre ville"></div>
				@if ($errors->has('ville_adresse'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('ville_adresse') }}</strong>
				</span>
				@endif
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Pays</div>
				<div class="col-8">
					<select  class="form-control select2" id="" name="id_pays"  data-placeholder="Choisir le type de votre entreprise*" style="width: 100%;" tabindex="-1" aria-hidden="true">
					@foreach($liste_pays as $pays)
						<option value="{{ $pays->id_pays }}"  > {{ $pays->label }}</option>
					 @endforeach
					 </select>
				</div>
			</div>
		</div>
		</hr>
		<strong>
			<i class="fa fa-address-card mr-1"></i>
			 Information sur le responsable
		</strong>
		<div>
			
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom du responsable</div>
				<div class="col-8"><input type="text" class="form-control" name="nom_responsable" value="" placeholder="Entrez le nom du responsable"></div>
				@if ($errors->has('nom_responsable'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('nom_responsable') }}</strong>
				</span>
				@endif
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Prénom du responsable</div>
				<div class="col-8"><input type="text" class="form-control" name="prenom_responsable" value="" placeholder="Entrez le prénom du responsable"></div>
				@if ($errors->has('prenom_responsable'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('prenom_responsable') }}</strong>
				</span>
				@endif
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Email du responsable</div>
				<div class="col-8"><input type="email" class="form-control" name="email_responsable" value="" placeholder="Entrez l'email du responsable"></div>
				@if ($errors->has('email_responsable'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('email_responsable') }}</strong>
				</span>
				@endif
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Téléphone du responsable</div>
				<div class="col-8"><input type="text" class="form-control" name="telephone_responsable" value="" placeholder="Entrez le contact du responsable"></div>
				@if ($errors->has('telephone_responsable'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('telephone_responsable') }}</strong>
				</span>
				@endif
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Poste</div>
				<div class="col-2" ><a id="id_bouton_ajout_responsable" class="btn btn-default btn-block"  onclick="ajouterPoste(this )"><i class="fa fa-plus"></i></a></div>
				<div class="col-6">
					<select  class="form-control select2" id="id_id_poste" name="id_poste"  data-placeholder="Choisir le type de votre entreprise*" style="width: 100%;" tabindex="-1" aria-hidden="true">
					@foreach($liste_poste as $poste)
						<option value="{{ $poste->id_poste }}"  > {{ $poste->label }}</option>
					 @endforeach
					 </select>
				</div>
			</div>
			
		</div>
		</hr>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		
        <input type="submit" class="btn btn-primary" value="Mettre à jour">
      </div>
	 </form> 
    </div>
  </div>
</div>

<!-- modal map -->
<div class="modal fade" id="changer_coordonees" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Veuillez cliquer sur la carte</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-content">
        <center><div id='map' style='width: 400px; height: 300px; '></div></center>
		
      </div>
      
      <div class="modal-footer">
       
		
      </div>
	
	</div>
  </div>
</div>

<!-- modal detail client -->

<div class="modal fade" id="id_visualiserDetail" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail suplémentaire</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
<!--<div id="id_visualiserDetail" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail suplémentaire</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>-->	  
      <div class="modal-body">
	   <div class="row">
	   
		<div class="col-md-12">
			<strong>
				<i class="fa fa-address-card mr-1"></i>
				 Information de l'organisation
			</strong>
			
			<div>
			<!--	<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
					<div class="col-6"></div>
					
				</div>
				<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Numero</div>
					<div class="col-6"></div>
					
				</div>
				<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Type de l'organisation</div>
					<div class="col-6"></div>
				</div>-->
				<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					
					<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Quartier</div>
					<div class="col-6"><p  id="id_quartier"></p></div>
					
				</div>
				<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Coordonnées (Latitude / longitude)</div>
					<div class="col-6"><p  id="id_coordonnee"></p></div>
				</div>
				<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Code postal</div>
					<div class="col-6"><p  id="id_code_postal"></p></div>
				</div>
				<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Ville</div>
					<div class="col-6"><p  id="id_ville"></p></div>
				</div>
				<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Pays</div>
					<div class="col-6"><p  id="id_pays"></p></div>
				</div>
			</div>
			</hr>
			<strong>
				<i class="fa fa-address-card mr-1"></i>
				 Information sur le responsable
			</strong>
			<div >
					<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
						<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom du responsable</div>
						<div class="col-6"><p  id="id_nom"></p></div>
					</div>
					<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
						<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Prénom du responsable</div>
						<div class="col-6"><p  id="id_prenom"></p></div>
					</div>
					<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
						<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Email du responsable</div>
						<div class="col-6"><p  id="id_mail"></p></div>
					</div>
					<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
						<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Téléphone du responsable</div>
						<div class="col-6"><p  id="id_tel"></p></div>
					</div>
			</div>
			</div>
			
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
	 </form> 
    </div>
  </div>
</div>
<!-- modal loader -->
<div class="modal fade modal-custumed" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
       
      </div>
      <center><h4>Encours.</h4> <div class="loader"></div></center>
      <div class="modal-footer">
       
      </div>
	</div>
  </div>
</div>
<!----modal   ajouter   poste---->
<div class="modal fade" id="id_ajout_poste" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau poste </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		
		 <input type="hidden" value="{{ csrf_token() }}" id="id_token">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>

			<div class="col-sm-10">
			  <input name="po" type="text" class="form-control" id="id_nouveau_poste" >
			</div>
			
		  </div>
		 
      </div>
     <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" onclick="ajouter_poste_ajax()">Ajouter</button>
      </div>
    </div>
  </div>
</div>
@endif
@endsection
@section('custom_script')
<script type="text/Javascript">
	

	$(function () {
		
		 if($("#erreur_update").val() =='1'){
			 $("#ajouter_receveur").modal();
		 }
		$("#id_liste_receveur_table").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							  "aaSorting": [[0, 'asc']]
							});
		

	});
function ajouterPoste(element) {
	$("#id_ajout_poste").modal();
}	
function ajouter_poste_ajax() {
		var label_poste = $('#id_nouveau_poste').val();
		 var token = $('#id_token').val();
		var x =new FormData();
		x.append('label' , label_poste);	
		x.append('_token' , token);	
		$("#loadMe").modal();
		$("#id_ajout_poste").modal('toggle');
		var lien = '{{ url('ajouter_poste_organisation')}}' ;
		$.ajax({
		   url : lien,
		    method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				// console.log(resp);
				var html = '<option value ="'+ response.id_poste+'"  selected>'+response.label+' </option>';
				var select = $("#id_id_poste");
				select.append(html);
				select.select2("val" , select.select2("val"));
				$("#loadMe").modal('toggle');
				
				
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				
		   }
		});  
		
		
	}
 function visualiserDetail(element) {
		var id_organisation = $(element).attr('row_id_organisation');
		var lien = "{{ url('detail_receveur') }}?id_organisation="+id_organisation ;
		console.log(lien);
		$("#loadMe").modal();
		$.ajax({
		   url : lien,
		   type : 'GET',
		  dataType : 'text',
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				var resp = JSON.parse(response);
				console.log(resp);
				// var div_parent = document.getElementById('id_parent');
				// while (div_parent.firstChild) {
					// div_parent.removeChild(div_parent.firstChild);
				// }
				document.getElementById('id_nom').innerHTML =  resp.receveur_employe.nom;
				document.getElementById('id_prenom').innerHTML = resp.receveur_employe.prenom ;
				document.getElementById('id_mail').innerHTML = resp.receveur_employe.email;
				document.getElementById('id_tel').innerHTML = resp.receveur_employe.telephone;
				document.getElementById('id_quartier').innerHTML = resp.adresse.label;
				document.getElementById('id_ville').innerHTML = resp.adresse.ville;
				document.getElementById('id_coordonnee').innerHTML = resp.adresse.latitude+" - "+resp.adresse.longitude;
				document.getElementById('id_code_postal').innerHTML = resp.adresse.code_postal;
					
				$("#loadMe").modal('toggle');
				$("#id_visualiserDetail").modal();
				
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				// console.log(statut);
				// console.log(erreur);
		   }
		});  
		
		
	} 
function supprimerReceveur(element) {
		var lien = $(element).attr('url_supprimer_receveur');
		console.log(lien);
		document.getElementById("supprimer_receveur_button").href =lien
		$("#supprimer_receveur").modal();
	}
 function modifierAdresse(element) {
	  mapboxgl.accessToken = 'pk.eyJ1IjoibmphcnkiLCJhIjoiY2p1ZHU0NWYzMDJ1MTRkbW16N2pmczh6YiJ9.i3r9euc0dZsYchVvQfdG5A';
		$("#loadMe").modal();
		var map = new mapboxgl.Map({
		container: 'map',
		country : "mg",
		style: 'mapbox://styles/mapbox/streets-v9'
		});
		
		map.on('click', function (e) {
		//document.getElementById('info').innerHTML =
		// e.point is the x, y coordinates of the mousemove event relative
		// to the top-left corner of the map
		
		// var x = JSON.stringify(e.lngLat) ;
		var x = e.lngLat ;
		console.log(x) ;
		
		var id_adresse = $(element).attr('row_adresse');
		$('#id_longitude').val( x.lng );
		$('#id_latitude').val( x.lat );
		$("#changer_coordonees").modal('toggle');
		
		});
		map.on('load', function () {
			
			// *** Add zoom and rotation controls to the map ...
			map.addControl(new mapboxgl.NavigationControl());

			$("#loadMe").hide();
			$("#changer_coordonees").modal();

		});
		/* var el = document.createElement('div');
        el.className = 'marker';
        el.style.backgroundImage = '{{ asset("img/marker_fw.png") }}';
        el.style.width = '35px';
        el.style.height = '35px';
        el.style.backgroundRepeat = 'no-repeat';
        el.style.backgroundSize = 'contain';

        //console.log( el.style.backgroundImage );

        var marker = new mapboxgl.Marker(el);
		new mapboxgl.Marker(el)
				.setLngLat(34, 13)
				.addTo(map);*/
		
		// var lien = $(element).attr('url_supprimer_entreprise');
		// console.log(lien);
		// document.getElementById("supprimer_entreprise_button").href =lien+"&return=0"
		
		
	} 
  
</script>
@endsection