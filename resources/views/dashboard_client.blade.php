@extends('template/default')

@section('titre')
    Tableau de bord
@endsection

@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif


<section class="content">
      <div class="container-fluid">
			<div class="row">
         
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_page') }}" >Général</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_entreprise') }}" >Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_receveur') }}" >Organisation</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_benevole') }}" >Bénévole</a></li>
							<li class="nav-item"><a class="nav-link active" href="" >Consommateur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_commande') }}" >Commande</a></li>	
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_encours') }}" >Activité encours</a></li>
						</ul>
						<button id="download" onclick="getImage()" class="bouton-sticky" title="Exporter en pdf"><img src="{{ asset('/img/Downloads-icon.png') }}" class="exportImage" alt="exporter"></button>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     profile    -->
							<div class="active tab-pane" id="consommateur">
								<div class="card card-primary">
									<div id="capture" >
									<div class="card-body">
										<div class="row">
										  <div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3>{{ number_format( (float)$nombre_consommateur , 0 , "," , " " )}} </h3>

												<p>Nombre de clients</p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											</div>
										  </div>
										  <!-- ./col -->
										  @if($jour_frequent!=null)
										  <div class="col-lg-4 col-8">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3>{{ $jour_frequent }} </h3>
												<h3>de {{ $heure_frequent }} heure</h3>

												<p>Moments les plus frequents</p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											</div>
										  </div>
										  @endif
										  
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Produits de préférence globale</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_preference_globale" class="table table-bordered table-hover">
												<thead>
												<tr>
												  <th>Nom du Produit</th>												 
												</tr>
												</thead>
												<tbody>
												@foreach($produits_preference_globaux as $produit)
												<tr>
												  <td>{{ $produit->nom_produit  }}</td>
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Produits de préférence par client</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_preference_client" class="table table-bordered table-hover">
												<thead>
												<tr>
												  <th>Consommateur</th>
												  <th>Nombre de produit vendu</th>
												 
												</tr>
												</thead>
												<tbody>
												@foreach($produits_preference_par_client as $produit)
												<tr>
												  <td>{{ $produit->nom.' '.$produit->prenom  }}</td>
												  <td>{{ $produit->nom_produit }} </td>
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										  <!-- ./row -->
										  <center>
											<h4>Sélectionner période</h4>
											<form  action="{{ url('dashboard_client') }}" method="get">
												<div class="row">
													
													{{ csrf_field() }}
													<div class="col"></div>
												  <div class="col">
														<label for="inputName">Début</label>
														  <input style="width : 200px;" name="date_debut" type="date" class="form-control" id="inputName" value="<?php if($debut_periode !=0) echo $debut_periode ?>" >
														  @if ($errors->has('date_debut'))
																<span class="help-block">
																	<strong style="color:#FF0000"; >{{ $errors->first('date_debut') }}</strong>
																</span>
															@endif
													</div>
												  <div class="col">
													<label for="inputName" >Fin</label>
													 <input style="width : 200px;" name="date_fin" type="date" value="<?php if($fin_periode !=0) echo $fin_periode ?>" class="form-control" id="inputName" >
													  @if ($errors->has('date_fin'))
															<span class="help-block">
																<strong style="color:#FF0000"; >{{ $errors->first('date_fin') }}</strong>
															</span>
														@endif
													</div>
												  <!--<div class="w-100"></div>-->
												  <!--<div class="col">3</div>-->
												  <div class="col">
													</br>
													<input type="submit" value="valider" class="btn btn-secondary" style="width : 200px; padding-top:20px ;">
													</div>
													<div class="col"></div>
												</div>
												</form>
											</center>
											</br>
										 <!-- <div class=row>
											 <div class="col-md-12">
											 <center>
												<h4>Sélectionner période</h4>
												<form class="form-horizontal" action="{{ url('dashboard_client') }}" method="get">
												  {{ csrf_field() }}
												  <div class="form-group">
													<label for="inputName" class="col-sm-2 control-label">Début</label>
													<div class="col-2">
													  <input name="date_debut" type="date" class="form-control" id="inputName" value="<?php if($debut_periode !=0) echo $debut_periode ?>" >
													  @if ($errors->has('date_debut'))
															<span class="help-block">
																<strong style="color:#FF0000"; >{{ $errors->first('date_debut') }}</strong>
															</span>
														@endif
													</div>
													<label for="inputName" class="col-sm-2 control-label">Fin</label>
													<div class="col-2">
													  <input name="date_fin" type="date" value="<?php if($fin_periode !=0) echo $fin_periode ?>" class="form-control" id="inputName" >
													  @if ($errors->has('date_fin'))
															<span class="help-block">
																<strong style="color:#FF0000"; >{{ $errors->first('date_fin') }}</strong>
															</span>
														@endif
													</div>
													</br>
													<div class="col-6">
														<input type="submit" value="valider" class="btn btn-secondary">
													</div>
												  </div>
												</form>
											</center>	
											</div>	
										</div>-->
										<!--row-->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Achat total par client</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_achat_client" class="table table-bordered table-hover">
												<thead>
												<tr>
												  <th>Nom du client</th>
												  <th>Montant total</th>
												 
												</tr>
												</thead>
												<tbody>
												@if(count($achat_par_client)==0)
													<tr>	
														<th colspan="2"> <p>aucun résultat </p></th>
													</tr>	
												@endif
												@foreach($achat_par_client as $achat)
												<tr>
												  <td>{{$achat->nom.' '.$achat->prenom  }}</td>
												  <td>Ar {{ number_format( (float)$achat->montant_total , 0 , "," , " " ) }} </td>
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Réductions la plus choisi par client</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_recuction_client" class="table table-bordered table-hover">
												<thead>
												<tr>
												  <th>Nom du client</th>
												  <th>Montant total</th>
												 
												</tr>
												</thead>
												<tbody>
												@if(count($best_reduction)==0)
													<tr>	
														<th colspan="2"> <p>aucun résultat </p></th>
													</tr>	
												@endif
												@foreach($best_reduction as $reduction)
												<tr>
												  <td>{{$reduction->nom.' '.$reduction->prenom  }}</td>
												  <td>{{ number_format( (float)$reduction->reduc , 1 , "," , " " ) }} %</td>
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Produits les plus vendus</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_produit_plus_vendu" class="table table-bordered table-hover">
												<thead>
												<tr>
												  <th>Nom du Produit</th>
												  <th>Nombre de produit vendu</th>
												 
												</tr>
												</thead>
												<tbody>
												@if(count($produit_plus_vendu)==0)
													<tr>	
														<th colspan="2"> <p>aucun résultat </p></th>
													</tr>	
												@endif
												@foreach($produit_plus_vendu as $produit)
												<tr>
												  <td>{{ $produit->nom_produit  }}</td>
												  <td>{{ number_format( (float)$produit->nombre , 0 , "," , " " ) }} </td>
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Satisfaction des clients</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_satisfaction_client" class="table table-bordered table-hover">
												<thead>
												<tr>
												  <th>Service</th>
												  <th>Note</th>
												 
												</tr>
												</thead>
												<tbody>
												@if(count($Satisfaction_client)==0)
													<tr>	
														<th colspan="2"> <p>aucun résultat </p></th>
													</tr>	
												@endif
												@foreach($Satisfaction_client as $satisfaction)
												<tr>
												  <td>{{ $satisfaction->label }}</td>
												  <td>{{ $satisfaction->note }} </td>
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
									  </div>
									  <!-- ./card-body -->
									 </div> 
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
					 
							
					<!-- /.tab-content -->
				  </div><!-- /.card-body -->
				</div>
				<!-- /.nav-tabs-custom -->
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
	
	
@endsection

@section('modals')
<!-- Modal -->


@endsection

@section('custom_script')
<script type="text/javascript" src="{{ asset('plugins/html2canvaspdf/js/html2canvas.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/html2canvaspdf/js/jsPdf.debug.js') }}"></script>
<script>
  $(function () {
	  @if(Session::has('date_client'))
			var elmnt = document.getElementById("id_achat_client");
			elmnt.scrollIntoView(); 
		@endif
	$("#id_preference_globale").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
	$("#id_preference_client").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
	$("#id_achat_client").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
	$("#id_recuction_client").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
	$("#id_produit_plus_vendu").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
	$("#id_satisfaction_client").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
	
  });
  function getImage(){
	 
          html2canvas(document.getElementById('capture'), {
              onrendered:function(canvas) {

                  var contentWidth = canvas.width;
                  var contentHeight = canvas.height;

                  // Une page pdf indique la hauteur de la toile générée par la page html;
                  var pageHeight = contentWidth / 595.28 * 841.89;
                  // la hauteur de la page HTML n'est pas générée pdf
                  var leftHeight = contentHeight;
                  // offset de page pdf
                  var position = 0;
                 // a4 format de papier [595.28, 841.89], toile générée par page HTML en largeur et hauteur de l'image pdf
                  var imgWidth = 555.28;
                  var imgHeight = 555.28/contentWidth * contentHeight;

                  var pageData = canvas.toDataURL('image/jpeg', 1.0);

                  var pdf = new jsPDF('', 'pt', 'a4');
                 // Il y a deux hauteurs à distinguer, l'une est la hauteur réelle de la page html et la hauteur de la page qui a généré le pdf (841.89)
                  // Lorsque le contenu ne dépasse pas la plage affichée sur la page pdf, aucune pagination n'est requise.
                  if (leftHeight < pageHeight) {
                      pdf.addImage(pageData, 'JPEG', 20, 0, imgWidth, imgHeight );
                  } else {
                      while(leftHeight > 0) {
                          pdf.addImage(pageData, 'JPEG', 20, position, imgWidth, imgHeight)
                          leftHeight -= pageHeight;
                          position -= 841.89;
                          // Éviter d'ajouter des pages vierges
                          if(leftHeight > 0) {
                              pdf.addPage();
                          }
                      }
                  }

                  pdf.save('content.pdf');
              }
          })
      
}
function getImage(){
	 
          html2canvas(document.getElementById('capture'), {
              onrendered:function(canvas) {

                  var contentWidth = canvas.width;
                  var contentHeight = canvas.height;

                  // Une page pdf indique la hauteur de la toile générée par la page html;
                  var pageHeight = contentWidth / 595.28 * 841.89;
                  // la hauteur de la page HTML n'est pas générée pdf
                  var leftHeight = contentHeight;
                  // offset de page pdf
                  var position = 0;
                 // a4 format de papier [595.28, 841.89], toile générée par page HTML en largeur et hauteur de l'image pdf
                  var imgWidth = 555.28;
                  var imgHeight = 555.28/contentWidth * contentHeight;

                  var pageData = canvas.toDataURL('image/jpeg', 1.0);

                  var pdf = new jsPDF('', 'pt', 'a4');
                 // Il y a deux hauteurs à distinguer, l'une est la hauteur réelle de la page html et la hauteur de la page qui a généré le pdf (841.89)
                  // Lorsque le contenu ne dépasse pas la plage affichée sur la page pdf, aucune pagination n'est requise.
                  if (leftHeight < pageHeight) {
                      pdf.addImage(pageData, 'JPEG', 20, 0, imgWidth, imgHeight );
                  } else {
                      while(leftHeight > 0) {
                          pdf.addImage(pageData, 'JPEG', 20, position, imgWidth, imgHeight)
                          leftHeight -= pageHeight;
                          position -= 841.89;
                          // Éviter d'ajouter des pages vierges
                          if(leftHeight > 0) {
                              pdf.addPage();
                          }
                      }
                  }

                  pdf.save('content.pdf');
              }
          })
      
}
</script>

@endsection