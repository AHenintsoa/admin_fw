<?php 
function typeProduitSelected($type_produit, $allTypes){
		foreach($allTypes as $t){
			if($t->id_type_produit == $type_produit->id_type_produit){
				return true;
			}
		}
		return false;
	}
	function typeAlimentSelected($type_aliment, $allTypesAl){
		foreach($allTypesAl as $t){
			if($t->id_type_aliment == $type_aliment->id_type_aliment){
				return true;
			}
		}
		return false;
	}
	$jours = array('Tous les jours', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
	
	
?>
@extends('template/default')

@section('titre')
    Ajouter produit
@endsection
@section('custom_styles')
    <link rel="stylesheet" href="{{ asset('dist/plugins/select2/select2.min.css') }}">
	<!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="{{ asset('dist/plugins/timepicker/bootstrap-timepicker.min.css') }}">
	<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.js'></script>
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.css' rel='stylesheet' />
  
@endsection
@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif

 @if(Session::get('id_utilisateur_type')!='5')
<section class="content">
      <div class="container-fluid">
			<div class="row">
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link " href="{{ url('entreprise') }}">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link active" href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="card-body">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link active" href="#don" id="bouton_utilisateur" data-toggle="tab">Don</a></li>
								<li class="nav-item"><a class="nav-link" href="#vente" id="bouton_tache" data-toggle="tab">Vente</a></li>
							
						</ul>
						<div class="tab-content">
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="don">
								<div class="card card-primary">
									<div class="card-body">
									<form action="{{ url('ajouter_produit_don') }}" method="post" enctype="multipart/form-data">
									{{ csrf_field() }}
										<div class="row">
											<div class="col-md-6">
												<strong>
													<i class="fa fa-address-card mr-1"></i>
													 Information sur le produit
												</strong>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</label>
													<div class="input-group mb-3 " id="id_div_nom_produit_don">
														<input  class="form-control" type="text" id="id_nom_produit_don" name="nom_produit"  placeholder="Nom du produit" value="">
													</div>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Description (facultatif)</label>
													<textarea  name="description" class="form-control" rows="3" placeholder="Description du produit"> </textarea>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Poids du produit</label>
													<div class="input-group mb-3 " id="id_div_poids_produit_don">
														<input  class="form-control" id="id_poids_produit_don" type="text" name="poids"  placeholder="Poids du produit" value="">
														<div class="input-group-append">
															<span class="input-group-text">Kg</span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Volume du produit (facultatif)</label>
													<div class="input-group mb-3 " id="id_div_volume_don">
														<input  class="form-control" type="text" id="id_volume_don" name="volume"  placeholder="Volume du produit" value="">
														<div class="input-group-append">
															<span class="input-group-text"><var>m<sup>3</sup></var></span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Quantite</label>
													<div class="input-group mb-3 " id="id_div_quantite_don">
														<input  class="form-control" id="id_quantite_don" type="number" name="quantite"  placeholder="Quantite du produit" value="">
													</div>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Prix du produit</label>
													<div class="input-group mb-3 " id="id_div_prix_don">
														<input  class="form-control" id="id_prix_don" type="text" name="prix"  placeholder="Prix du produit" value="">
														<div class="input-group-append">
															<span class="input-group-text">MGA</span>
														</div>
													</div>
												</div>
												
												<div class="row form-group">
													<div class="col-6">
														<label style="padding-left:20px;font-weight: bolder;color: #646369;">
														<input class="radio-choix-type-date" id="input-choix-date-peremption1" checked type="radio"  name="type_date" value=1  />
														Date de péremption
														</label>
													</div>
													<div class="col-6" >
														<label  style="padding-left:20px;font-weight: bolder;color: #646369;">
														<input id="input-choix-date-production1" type="radio" name="type_date" value=2 />
														Date de production
														</label>
													</div>
													<div class="input-group " id="id_div_date_don">
														<input id="id_date_don" class="form-control" name="date_peremption" type="date" placeholder="Date de péremption" >
														<div class="input-group-append">
															<span class="input-group-text">
																<i class="fa fa-calendar"></i>
															</span>
														</div>
													</div>
												</div>
													
											</div>
											<!--col-->
											<div class="col-md-6">
												<div class="form-group">
													<label class="check" style="padding-left:20px;font-weight: bolder;color: #646369;">
														<input id="input-refrigere1" type="checkbox" value="1" name="equipement1" class="flat-red" /> Le produit nécessite d'être réfrigéré
													</label>
													<label class="check" style="padding-left:20px;font-weight: bolder;color: #646369;">
														<input id="input-congele1" name="equipement2" value="1" type="checkbox" class="flat-red"  /> Le produit nécessite d'être congelé
													</label>
													<label class="check" style="padding-left:20px;font-weight: bolder;color: #646369;">
														<input id="input-consoDirect1" name="equipement4" value="1" type="checkbox" class="flat-red"  /> Le produit nécessite ni d'être réfrigéré ni d'être congelé
													</label>
												</div>
												<div class="form-group " id="id_div_type_produit_don">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Type du produit</label>
													<select  class="form-control select2"  id="id_id_type_produit_don"  name="id_type_produit"  data-placeholder="Choisir le type du produit" style="width: 100%;" tabindex="-1" aria-hidden="true" id="id_select_type_entreprise">
														<option></option>
														<?php foreach($type_produit as $type): ?>
														<option value="<?php echo $type->id_type_produit; ?>"><?php echo $type->label; ?></option>
														<?php endforeach; ?>
													</select>
													<div >
														 <a  onclick="" data-toggle="modal" data-target="#id_ajout_type_produit" title="Ajouter un type de produit" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
													</div>
												</div>
												<div class="form-group " id="id_div_type_aliment_don">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Catégorie</label>
													<select  class="form-control select2"  id="id_id_type_aliment_don"  name="id_type_aliment"  data-placeholder="Choisir la catégorie du produit" style="width: 100%;" tabindex="-1" aria-hidden="true" id="id_select_type_entreprise">
														<option></option>
														<?php foreach($type_aliment as $type): ?>
														<option value="<?php echo $type->id_type_aliment; ?>" ><?php echo $type->label; ?></option>
														<?php endforeach; ?>
													</select>
													<div >
														 <a  onclick="" data-toggle="modal" data-target="#id_ajout_type_aliment" title="Ajouter une type d'aliment" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
													</div>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Groupement du produit</label>
													<select  class="form-control select2" id="id_id_poste2" name="groupement"   data-placeholder="Choisir le groupement du produit" style="width: 100%;" tabindex="-1" aria-hidden="true">
														<option value="1" >Par unité</option>
														<option value="2"  >Totalité</option>
														<option value="3"  >Par portion</option>
													</select>
												</div>
												<div class="form-group " id="id_div_information_nutritionnelle_don">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Information nutritionnelle</label>
													<select  class="form-control select2" id="id_id_informaton_nutritionnelle_don" name="id_information_nutritionnelle[]" multiple="multiple"  data-placeholder="Choisir l'information nutritionnelle" style="width: 100%;" tabindex="-1" aria-hidden="true">
														<option value="" ></option>
														@foreach($information_nutritionnelle as $info)
															<option value="{{ $info->id_information_nutritionnelle  }}"  >{{ $info->label  }}</option>
														@endforeach
													</select>
													<div >
														 <a  onclick="" data-toggle="modal" data-target="#id_ajout_info_nut" title="Ajouter une information nutritionnelle" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
													</div>
												</div>
												<div class="form-group " id="id_div_raison_surplus_don">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Raison de surplus</label>
													<select  class="form-control select2" id="id_id_raison_surplus_don" name="id_raison_surplus[]" multiple="multiple"  data-placeholder="Choisir la raison de surplus" style="width: 100%;" tabindex="-1" aria-hidden="true">
														<option value="" ></option>
														@foreach($raison_surplus as $raison)
															<option value="{{ $raison->id_raison_surplus  }}"  >{{ $raison->label  }}</option>
														@endforeach
													</select>
													<div >
														 <a  onclick="" data-toggle="modal" data-target="#id_ajout_raison_surplus" title="Ajouter une raison de surplus" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
													</div>
												</div>
												
												<div class="form-group " id="id_div_photo_don">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Photo(s) du produit</label>
													<input name="photo[]" value="" id="id_photo_don" multiple="multiple" class="form-control form-control-lg"   type="file">
												</div>
												<div class="form-group " id="id_div_entreprise_don">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Entreprise</label>
													<select  class="form-control select2" id="id_id_entreprise_don" name="id_entreprise"  data-placeholder="Choisir l'entreprise concernée" style="width: 100%;" tabindex="-1" aria-hidden="true">
														<option value="" ></option>
														@foreach($entreprise as $entreprise_detail)
															<option value="{{ $entreprise_detail->id_entreprise  }}"  >{{ $entreprise_detail->nom_entreprise  }}</option>
														@endforeach
													</select>
												</div>
												<hr>
												
												<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4"></div>
													<div class="col-4"></div>
													<div class="col-4"><p class="btn btn-secondary" onclick="valider_formulaire_don();">ajouter </p><input type="submit" class="btn"  style ="display : none ;" value="Ajouter" id="id_bouton_valider_don"></div>
												</div>	
											</div>
											<!-- /.col -->
											</form>
										</div>
										<!-- /.row -->
										</div>
										<!-- ./card-body -->
									</div>
									<!-- /.card -->
							  </div>
							  <!-- /.tab-pane -->
						<div class="tab-pane" id="vente">
								<div class="card card-primary">
									<div class="card-body">
									<form action="{{ url('ajouter_produit_vente') }}" method="post" enctype="multipart/form-data">
									{{ csrf_field() }}
										<div class="row">
											<div class="col-md-6">
												<strong>
													<i class="fa fa-address-card mr-1"></i>
													 Information sur le produit
												</strong>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</label>
													<div class="input-group mb-3 " id="id_div_nom_produit">
														<input  class="form-control" type="text" id="id_nom_produit" name="nom_produit"  placeholder="Nom du produit" value="">
													</div>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Description (facultatif)</label>
													<textarea  name="description" class="form-control" rows="3" placeholder="Description du produit"> </textarea>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Poids du produit</label>
													<div class="input-group mb-3 " id="id_div_poids_produit">
														<input  class="form-control" id="id_poids_produit" type="text" name="poids"  placeholder="Poids du produit" value="">
														<div class="input-group-append">
															<span class="input-group-text">Kg</span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Volume du produit (facultatif)</label>
													<div class="input-group mb-3 " id="id_div_volume">
														<input  class="form-control" type="text" id="id_volume" name="volume"  placeholder="Volume du produit" value="">
														<div class="input-group-append">
															<span class="input-group-text"><var>m<sup>3</sup></var></span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Quantite</label>
													<div class="input-group mb-3 " id="id_div_quantite">
														<input  class="form-control" id="id_quantite" type="number" name="quantite"  placeholder="Quantite du produit" value="">
													</div>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Quantite minimum d'achat (facultatif)</label>
													<div class="input-group mb-3 " id="id_div_quantite_minimum">
														<input  class="form-control" type="number" id="id_quantite_minimum" name="quantite_minimum"  placeholder="Quantite minimum d'achat" value="">
													</div>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Prix du produit</label>
													<div class="input-group mb-3 " id="id_div_prix">
														<input  class="form-control" id="id_prix" type="text" name="prix"  placeholder="Prix du produit" value="">
														<div class="input-group-append">
															<span class="input-group-text">MGA</span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Reduction par defaut</label>
													<div class="input-group mb-3 " id="id_div_reduction">
														<input  class="form-control" id="id_reduction_defaut" type="text" name="reduction"  placeholder="Reduction par defaut" value="">
														<div class="input-group-append">
															<span class="input-group-text">%</span>
														</div>
													</div>
												</div>
												<div class="row form-group">
													<div class="col-6">
														<label style="padding-left:20px;font-weight: bolder;color: #646369;">
														<input class="radio-choix-type-date" id="input-choix-date-peremption1" checked type="radio"  name="type_date" value=1  />
														Date de péremption
														</label>
													</div>
													<div class="col-6" >
														<label  style="padding-left:20px;font-weight: bolder;color: #646369;">
														<input id="input-choix-date-production1" type="radio" name="type_date" value=2 />
														Date de production
														</label>
													</div>
													<div class="input-group " id="id_div_date">
														<input id="id_date" class="form-control" name="date_peremption" type="date" placeholder="Date de péremption" >
														<div class="input-group-append">
															<span class="input-group-text">
																<i class="fa fa-calendar"></i>
															</span>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label  style="padding-left:20px;font-weight: bolder;color: #646369;">Stratégie marketing du prix (facultatif) <sup style="cursor: pointer !important;" id="info_strategie_reduc" class="info_strategie_reduc tippy" title="La stratégie marketing vous aide à établir automatiquement  un système de réduction du prix selon l’approximation de la date de péremption du produit." > ( i ) </sup> </label>
													<!-- DEBUT STRATEGIE PAR SEMAINE -->
													<div class="form-group">
														@for($i=1 ; $i<5 ; $i++)
														<label style="padding-left:20px;font-weight: bolder;color: #646369;">{{ $i }} semaine(s)</label>
														<div class="input-group mb-3 id_div_autre_reduction{{$i}}">
															<input  class="form-control" type="text" id="id_autre_reduction{{$i}}" name="reduction{{$i}}"  placeholder="Reduction par defaut" value="">
															<div class="input-group-append">
																<span class="input-group-text">%</span>
															</div>
														</div>
														@endfor
													</div>
												</div>
												<div id="id_div_autre_periode">
												<input type="hidden" value="0" id="id_compteur_nouvelle_periode">
													<div class="form-group">
														<label style="padding-left:20px;font-weight: bolder;color: #646369;">Autres période de réduction</label>
														<div class="row">
															<div class="col-5 ">
																<select  class="form-control select2 class_periode_reduction" id="id_autre_periode"   name="id_periode_reduction0"  data-placeholder="Choisir une autre période" style="width: 100%;" tabindex="-1" aria-hidden="true" id="id_select_type_entreprise">
																	<option></option>
																	<?php foreach($autre_periode as $periode) { ?>
																	<option value="<?php echo $periode->id_periode_reduction; ?>"><?php echo $periode->label; ?></option>
																	<?php } ?>
																</select>
																<div >
																	 <a  onclick="" data-toggle="modal" data-target="#id_ajout_perdiode_reduction" title="Ajouter période" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
																</div>
															</div>
															<div class="input-group mb-3 col-6  " >
																<input  class="form-control autre_reduction"  type="text" name="autre_reduction0"   placeholder="Reduction" value="">
																<div class="input-group-append">
																	<span class="input-group-text">%</span>
																</div>
															</div>
															<div class="col-1" >
																<a id="id_bouton_ajout_responsable" class="btn btn-default btn-block" row_id="2" onclick="ajouterPeriode(this )"><i class="fa fa-plus"></i></a>
															</div>
														</div>
													</div>
												</div>
												<div class="row">
															<div class="col-5  div_autre_periode"></div>
															<div class="col-5  div_autre_reduction"></div>
												</div>			
											</div>
											<!--col-->
											<div class="col-md-6">
												<div class="form-group">
													<label class="check" style="padding-left:20px;font-weight: bolder;color: #646369;">
														<input id="input-refrigere1" type="checkbox" value="1" name="equipement1" class="flat-red" /> Le produit nécessite d'être réfrigéré
													</label>
													<label class="check" style="padding-left:20px;font-weight: bolder;color: #646369;">
														<input id="input-congele1" name="equipement2" value="1" type="checkbox" class="flat-red"  /> Le produit nécessite d'être congelé
													</label>
													<label class="check" style="padding-left:20px;font-weight: bolder;color: #646369;">
														<input id="input-consoDirect1" name="equipement4" value="1" type="checkbox" class="flat-red"  /> Le produit nécessite ni d'être réfrigéré ni d'être congelé
													</label>
												</div>
												<div class="form-group " id="id_div_type_produit">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Type du produit</label>
													<select  class="form-control select2"  id="id_id_type_produit"  name="id_type_produit"  data-placeholder="Choisir le type du produit" style="width: 100%;" tabindex="-1" aria-hidden="true" id="id_select_type_entreprise">
														<option></option>
														<?php foreach($type_produit as $type): ?>
														<option value="<?php echo $type->id_type_produit; ?>"><?php echo $type->label; ?></option>
														<?php endforeach; ?>
													</select>
													<div >
														 <a  onclick="" data-toggle="modal" data-target="#id_ajout_type_produit" title="Ajouter un type de produit" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
													</div>
												</div>
												<div class="form-group " id="id_div_type_aliment">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Catégorie</label>
													<select  class="form-control select2"  id="id_id_type_aliment"  name="id_type_aliment"  data-placeholder="Choisir la catégorie du produit" style="width: 100%;" tabindex="-1" aria-hidden="true" id="id_select_type_entreprise">
														<option></option>
														<?php foreach($type_aliment as $type): ?>
														<option value="<?php echo $type->id_type_aliment; ?>" ><?php echo $type->label; ?></option>
														<?php endforeach; ?>
													</select>
													<div >
														 <a  onclick="" data-toggle="modal" data-target="#id_ajout_type_aliment" title="Ajouter une type d'aliment" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
													</div>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Groupement du produit</label>
													<select  class="form-control select2" id="id_id_poste2" name="groupement"   data-placeholder="Choisir le groupement du produit" style="width: 100%;" tabindex="-1" aria-hidden="true">
														<option value="1" >Par unité</option>
														<option value="2"  >Totalité</option>
														<option value="3"  >Par portion</option>
													</select>
												</div>
												<div class="form-group " id="id_div_information_nutritionnelle">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Information nutritionnelle</label>
													<select  class="form-control select2" id="id_id_informaton_nutritionnelle" name="id_information_nutritionnelle[]" multiple="multiple"  data-placeholder="Choisir l'information nutritionnelle" style="width: 100%;" tabindex="-1" aria-hidden="true">
														<option value="" ></option>
														@foreach($information_nutritionnelle as $info)
															<option value="{{ $info->id_information_nutritionnelle  }}"  >{{ $info->label  }}</option>
														@endforeach
													</select>
													<div >
														 <a  onclick="" data-toggle="modal" data-target="#id_ajout_info_nut" title="Ajouter une information nutritionnelle" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
													</div>
												</div>
												<div class="form-group " id="id_div_raison_surplus">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Raison de surplus</label>
													<select  class="form-control select2" id="id_id_raison_surplus" name="id_raison_surplus[]" multiple="multiple"  data-placeholder="Choisir la raison de surplus" style="width: 100%;" tabindex="-1" aria-hidden="true">
														<option value="" ></option>
														@foreach($raison_surplus as $raison)
															<option value="{{ $raison->id_raison_surplus  }}"  >{{ $raison->label  }}</option>
														@endforeach
													</select>
													<div >
														 <a  onclick="" data-toggle="modal" data-target="#id_ajout_raison_surplus" title="Ajouter une raison de surplus" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
													</div>
												</div>
												
												<div class="form-group " id="id_div_photo">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Photo(s) du produit</label>
													<input name="photo[]" value="" id="id_photo" multiple="multiple" class="form-control form-control-lg"   type="file">
												</div>
												<hr>
												<strong>
													<i class="fa fa-address-card mr-1"></i>
													 Information sur l'entreprise
												</strong>
												<div class="form-group " id="id_div_entreprise">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Entreprise</label>
													<select  class="form-control select2" id="id_id_entreprise" name="id_entreprise"  data-placeholder="Choisir l'entreprise concernée" style="width: 100%;" tabindex="-1" aria-hidden="true">
														<option value="" ></option>
														@foreach($entreprise as $entreprise_detail)
															<option value="{{ $entreprise_detail->id_entreprise  }}"  >{{ $entreprise_detail->nom_entreprise  }}</option>
														@endforeach
													</select>
												</div>
												<div class="form-group">
													<label style="padding-left:20px;font-weight: bolder;color: #646369;">Lieu de collecte</label>
													 <label style="margin-right: 10px;">
														<input  class="radio-lieu-ramassage" id="id_adresse_courant" value="1" type="radio" name="lieuRamassage"  checked onclick="adresseCourant(this)"/>
													Adresse courant
													</label>
													<label>
														<input id="id_adresse_nouvelle"  type="radio" value="2" name="lieuRamassage"  onclick="autreAdresse(this)"/>
													Autre
													</label>
														<input id="id_type_adresse"  type="hidden" value="1" /> <!-- teste type adresse 1 si courant -->
													<div id="id_autre_adresse_ramassage">
														
													</div>
												</div>
												<div  id="id_div_autre_moment_collecte">
												<input type="hidden" value="0" id="id_compteur_nouvelle_moment">
													<div class="form-group">
														<label style="padding-left:20px;font-weight: bolder;color: #646369;">Période de collecte</label>
														<div class="row">
															<div class="col-3 id_div_jour"><label style="padding-left:20px;font-weight: bolder;color: #646369;">Jours</label></div>
															<div class="col-4 id_div_heure_debut"><label style="padding-left:20px;font-weight: bolder;color: #646369;">Debut</label></div>
															<div class="col-4 id_div_heure_fin"><label style="padding-left:20px;font-weight: bolder;color: #646369;">Fin</label></div>
														</div>
														<div class="row">
															<div class="col-3">
																<select  class="form-control select2" id="id_id_pays" name="jour"  data-placeholder="Choisir l'entreprise concernée" style="width: 100%;" tabindex="-1" aria-hidden="true">
																	@for( $i = 0 ; $i < count($jours) ;$i++)
																		<option value="{{ $jours[$i]  }}"  >{{ $jours[$i]  }}</option>
																	@endfor
																</select>
															</div>
															<div class="col-4 bootstrap-timepicker">
																<div class="form-group">
																	<div class="input-group">
																		<input type="text" class="form-control timepicker heure_debut" name="heure_debut">

																		<div class="input-group-append">
																			<span class="input-group-text"><i class="fa fa-clock-o"></i></span>
																		</div>
																	</div>	
																</div>
																<!-- /.input group -->
															  </div>
															  <div class="col-4 bootstrap-timepicker">
																	<div class="form-group">
																		<div class="input-group">
																			<input type="text" class="form-control timepicker heure_fin" name="heure_fin">
																			<div class="input-group-append">
																				<span class="input-group-text"><i class="fa fa-clock-o"></i></span>
																			</div>
																		</div>
																		<!-- /.input group -->
																	</div>
															  </div>
															<div class="col-1" >
																<a id="id_bouton_ajout_responsable" class="btn btn-default btn-block" row_id="2" onclick="ajouterMoment(this)"><i class="fa fa-plus"></i></a>
															</div>
														</div>
													</div>	
												</div>
												<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4"></div>
													<div class="col-4"></div>
													<div class="col-4"><p class="btn btn-secondary" onclick="valider_formulaire();">ajouter </p><input type="submit" class="btn"  style ="display : none ;" value="Ajouter" id="id_bouton_valider"></div>
												</div>	
											</div>
											<!-- /.col -->
											</form>
										</div>
										<!-- /.row -->
										</div>
										<!-- ./card-body -->
									</div>
									<!-- /.card -->
							  </div>
							  <!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
					<!-- /.tab-pane -->
					</div>
				<!-- ./card-body -->
				</div>
				<!-- ./card-body -->
			</div>
			 <!-- ./card -->
		 </div>
		 <!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection
@section('modals')
<!----modal   ajouter  autre periode---->
<div class="modal fade" id="id_ajout_perdiode_reduction" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Autre période</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<div class="row">
						<div class="col-4"><label style="padding-left:20px;font-weight: bolder;color: #646369;">Jours</label></div>
						<div class="col-4"><label style="padding-left:20px;font-weight: bolder;color: #646369;">Semaines</label></div>
						<div class="col-4"><label style="padding-left:20px;font-weight: bolder;color: #646369;">Mois</label></div>
					</div>
					<input type="hidden" value="{{ csrf_token() }}" id="id_token_periode">
					<div class="row">
						<div class="col-4 id_div_nombre_jour_periode"><input name="jour" type="number" value="0" class="form-control" min="0" id="id_jour_periode" ></div>
						<div class="col-4"><input name="id_div_nombre_semaine_periode" type="number" value="0"  class="form-control" min="0" id="id_semaine_periode" ></div>
						<div class="col-4"><input name="id_div_nombre_mois_periode" type="number" value="0"  class="form-control" min="0" id="id_mois_periode" ></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
				<button type="button" class="btn btn-secondary" onclick="ajouter_periode_reduction_ajax()" >Ajouter</button>
			</div>
		</div>
	</div>
</div>
<!----modal   ajouter   type produit---->
<div class="modal fade" id="id_ajout_type_produit" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau type de produit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_produit">
			<div class="col-sm-10 id_div_nouveau_type_produit">
			  <input name="label" type="text" class="form-control" id="id_type_produit_label" >
			</div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" row_id="1" onclick="ajouter_type_produit_ajax(this)" >Ajouter</button>
	</div>
    </div>
  </div>
</div>
<!----modal   ajouter   type aliment---->
<div class="modal fade" id="id_ajout_type_aliment" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau type d'aliment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_aliment">
			<div class="col-sm-10 id_div_nouveau_type_aliment">
			  <input name="label" type="text" class="form-control" id="id_type_aliment_label" >
			</div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" row_id="1" onclick="ajouter_type_aliment_ajax(this)" >Ajouter</button>
	</div>
    </div>
  </div>
</div>
<!----modal   ajouter   info nutri---->
<div class="modal fade" id="id_ajout_info_nut" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouvelle information nutritionnelle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_info_nut">
			<div class="col-sm-10 id_div_nouveau_information_nutritionnelle">
			  <input name="label" type="text" class="form-control" id="id_info_nut_label" >
			</div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" row_id="1" onclick="ajouter_info_nutritionnelle_ajax(this)" >Ajouter</button>
	</div>
    </div>
  </div>
</div>
<!----modal   ajouter  raison surplus---->
<div class="modal fade" id="id_ajout_raison_surplus" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouvelle raison de surplus</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_raison_surplus">
			<div class="col-sm-10 id_div_nouvelle_raison">
			  <input name="label" type="text" class="form-control" id="id_raison_surplus_label" >
			</div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" row_id="1" onclick="ajouter_raison_surplus_ajax(this)" >Ajouter</button>
	</div>
    </div>
  </div>
</div>

<!----modal   ajouter   type produit---->
<div class="modal fade" id="id_ajout_type_produit_don" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau type de produit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_produit">
			<div class="col-sm-10 id_div_nouveau_type_produit">
			  <input name="label" type="text" class="form-control" id="id_type_produit_label" >
			</div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" row_id="2" onclick="ajouter_type_produit_ajax(this)" >Ajouter</button>
	</div>
    </div>
  </div>
</div>
<!----modal   ajouter   type aliment---->
<div class="modal fade" id="id_ajout_type_aliment_don" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau type d'aliment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_aliment">
			<div class="col-sm-10 id_div_nouveau_type_aliment">
			  <input name="label" type="text" class="form-control" id="id_type_aliment_label" >
			</div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" row_id="2" onclick="ajouter_type_aliment_ajax(this)" >Ajouter</button>
	</div>
    </div>
  </div>
</div>
<!----modal   ajouter   info nutri---->
<div class="modal fade" id="id_ajout_info_nut_don" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouvelle information nutritionnelle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_info_nut">
			<div class="col-sm-10 id_div_nouveau_information_nutritionnelle">
			  <input name="label" type="text" class="form-control" id="id_info_nut_label" >
			</div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" row_id="2" onclick="ajouter_info_nutritionnelle_ajax(this)" >Ajouter</button>
	</div>
    </div>
  </div>
</div>
<!----modal   ajouter  raison surplus---->
<div class="modal fade" id="id_ajout_raison_surplus_don" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouvelle raison de surplus</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_raison_surplus">
			<div class="col-sm-10 id_div_nouvelle_raison">
			  <input name="label" type="text" class="form-control" id="id_raison_surplus_label" >
			</div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" row_id="2" onclick="ajouter_raison_surplus_ajax(this)" >Ajouter</button>
	</div>
    </div>
  </div>
</div>
<!-- modal map -->
<div class="modal fade" id="changer_coordonees" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Veuillez cliquer sur la carte</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-content">
        <center><div id='map' style='width: 400px; height: 300px; '></div></center>
		
      </div>
      
      <div class="modal-footer">
       
		
      </div>
	
	</div>
  </div>
</div>

<!-- modal loader -->
<div class="modal fade modal-custumed" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
       
      </div>
      <center><h4>Encours.</h4> <div class="loader"></div></center>
      <div class="modal-footer">
       
      </div>
	</div>
  </div>
</div>
@endif
@endsection
@section('custom_script')
 <script src="{{ asset('dist/plugins/select2/select2.full.min.js') }}"></script>
<!-- bootstrap time picker -->
<script src="{{ asset('dist/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>

<script type="text/Javascript">
	

	$(function () {
		$('.select2').select2() ;// initialisation element select2
		$('.timepicker').timepicker({
		  showInputs: false,
		  use24hours: true,
		  format: 'HH:mm'
		})
		
	});
function valider_formulaire_don() {
	var reg = /^\d+$/;//regex pour chiffre
	
	var teste = 1; // pour valider
	$(".classe_erreur").remove();
	//produit
	
	var nom_produit = $("#id_nom_produit_don").val();
	if(!nom_produit) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_nom_produit_don").append(html);
		teste = 0;
		console.log('1');
	}
	var poids = $("#id_poids_produit_don").val();
	if(!poids) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_poids_produit_don").append(html);
		teste = 0;
		console.log('2');
	}
	if(reg.test(poids)==false && poids){
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
		$("#id_div_volume_don").append(html);
		teste = 0;
		console.log('3');
	}
	var volume = $("#id_volume_don").val();
	if(reg.test(volume)==false && volume) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
		$("#id_div_volume_don").append(html);
		teste = 0;
		console.log('4');
	}
	
	var quantite = $("#id_quantite_don").val();
	if(!quantite) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_quantite_don").append(html);
		teste = 0;
		console.log('5');
	}
	if(reg.test(quantite)==false && quantite) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
		$("#id_div_quantite_don").append(html);
		teste = 0;
		console.log('6');
	}
	
	var prix = $("#id_prix_don").val();
	if(!prix) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_prix_don").append(html);
		teste = 0;
		console.log('8');
	}
	if(reg.test(prix)==false && prix) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
		$("#id_div_prix_don").append(html);
		teste = 0;
		console.log('9');
	}
	
	var date = $("#id_date_don").val();
	if(!date) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_date_don").append(html);
		teste = 0;
		console.log('12');
	}
	
	var id_type_produit = $("#id_id_type_produit_don").val();
	if(!id_type_produit) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_type_produit_don").append(html);
		teste = 0;
		console.log('19');
	}
	var id_type_aliment = $("#id_id_type_aliment_don").val();
	if(!id_type_aliment) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_type_aliment_don").append(html);
		teste = 0;
		console.log('20');
	}
	var id_id_entreprise_don = $("#id_id_entreprise").val();
	if(!id_type_aliment) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_entreprise_don").append(html);
		teste = 0;
		console.log('20');
	}
	var id_info_nut_label = $("#id_id_informaton_nutritionnelle_don").val();
	if(id_info_nut_label.length==0) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_information_nutritionnelle_don").append(html);
		teste = 0;
		console.log('21');
	}
	var photo = $("#id_photo_don").val();
	if(photo.length==0) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_photo_don").append(html);
		teste = 0;
		console.log('22');
	}
	var raison = $("#id_id_raison_surplus_don").val();
	if(raison.length==0) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_raison_surplus_don").append(html);
		teste = 0;
		console.log('23');
	}
	
	
	  console.log('eto'+teste);
	if(teste == 1) {
		
		 $("#id_bouton_valider_don").click() ;
	}
	
}	
function valider_formulaire() {
	var reg = /^\d+$/;//regex pour chiffre
	
	var teste = 1; // pour valider
	$(".classe_erreur").remove();
	//produit
	
	var nom_produit = $("#id_nom_produit").val();
	if(!nom_produit) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_nom_produit").append(html);
		teste = 0;
		console.log('1');
	}
	var poids = $("#id_poids_produit").val();
	if(!poids) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_poids_produit").append(html);
		teste = 0;
		console.log('2');
	}
	if(reg.test(poids)==false && poids){
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
		$("#id_div_volume").append(html);
		teste = 0;
		console.log('3');
	}
	var volume = $("#id_volume").val();
	if(reg.test(volume)==false && volume) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
		$("#id_div_volume").append(html);
		teste = 0;
		console.log('4');
	}
	
	var quantite = $("#id_quantite").val();
	if(!quantite) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_quantite").append(html);
		teste = 0;
		console.log('5');
	}
	if(reg.test(quantite)==false && quantite) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
		$("#id_div_quantite").append(html);
		teste = 0;
		console.log('6');
	}
	var quantite_minimum = $("#id_quantite_minimum").val();
	if(reg.test(quantite_minimum)==false && quantite_minimum) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
		$("#id_div_quantite_minimum").append(html);
		teste = 0;
		console.log('7');
	}
	var prix = $("#id_prix").val();
	if(!prix) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_prix").append(html);
		teste = 0;
		console.log('8');
	}
	if(reg.test(prix)==false && prix) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
		$("#id_div_prix").append(html);
		teste = 0;
		console.log('9');
	}
	var reduction = $("#id_reduction_defaut").val();
	if(!reduction) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_reduction").append(html);
		teste = 0;
		console.log('10');
	}
	if(reg.test(reduction)==false && reduction) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
		$("#id_div_reduction").append(html);
		teste = 0;
		console.log('11');
	}
	var date = $("#id_date").val();
	if(!date) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_date").append(html);
		teste = 0;
		console.log('12');
	}
	var liste_div_autre_reduction = $('.div_autre_reduction');
	var liste_autre_reduction = $('.autre_reduction');
	var liste_div_autre_periode = $('.div_autre_periode');
	var liste_autre_periode = $('.class_periode_reduction');
	for(var i = 0 ; i < liste_autre_reduction.length ; i++){
		var reduc = liste_autre_reduction.get(i) ;
		var periode = liste_autre_periode.get(i) ;
		if(!reduc.value && i!=0 ){
			var html = '<div class="classe_erreur">'
							+'<span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span>'
						+'</div>';
			var div = document.createElement("div");	
			div.innerHTML = html;
			liste_div_autre_reduction.get(i).append(div);
			
			teste = 0;
		console.log('13'+i);
		}
		if( !periode.value && i!=0 ){
			var html = '<div class="classe_erreur">'
							+'<span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span>'
						+'</div>';
			var div = document.createElement("div");	
			div.innerHTML = html;
			
			liste_div_autre_periode.get(i).append(div);
			teste = 0;
		console.log('132'+i);
		}
		if(reg.test(reduc.value)==false && reduc.value!=''){
			var html = '<div class="classe_erreur">'
							+'<span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span>'
						+'</div>';
			var div = document.createElement("div");	
			div.innerHTML = html;
			liste_div_autre_reduction.get(i).append(div);
			teste = 0;
		console.log('14'+i);
		}
	}
	var teste_type_adresse =$("#id_type_adresse").val();
	
	var label_adresse = $("#id_label_adresse").val();
	if(!label_adresse && teste_type_adresse=='2') {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_label_adresse").append(html);
		teste = 0;
		console.log('15');
	}
	var ville_adresse = $("#id_ville_adresse").val();
	if(!ville_adresse && teste_type_adresse=='2') {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_ville_adresse").append(html);
		teste = 0;
		console.log('16');
	}
	var adresse_long = $("#id_longitude").val();
	var adresse_lat = $("#id_latitude").val();
	if((!adresse_long || !adresse_lat) && teste_type_adresse=='2') {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span><div>';
		$("#id_div_coordonne").append(html);
		teste = 0;
		console.log('17');
	}
	if(( (!reg.test(adresse_long) && adresse_long!='') || (!reg.test(adresse_lat) && adresse_lat!='')) && teste_type_adresse==2) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span><div>';
		$("#id_div_coordonne").append(html);
		teste = 0;
		console.log('18');
	}
	var id_type_produit = $("#id_id_type_produit").val();
	if(!id_type_produit) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_type_produit").append(html);
		teste = 0;
		console.log('19');
	}
	var id_type_aliment = $("#id_id_type_aliment").val();
	if(!id_type_aliment) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_type_aliment").append(html);
		teste = 0;
		console.log('20');
	}
	var id_info_nut_label = $("#id_id_informaton_nutritionnelle").val();
	if(id_info_nut_label.length==0) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_information_nutritionnelle").append(html);
		teste = 0;
		console.log('21');
	}
	var photo = $("#id_photo").val();
	if(photo.length==0) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_photo").append(html);
		teste = 0;
		console.log('22');
	}
	var raison = $("#id_id_raison_surplus").val();
	if(raison.length==0) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_raison_surplus").append(html);
		teste = 0;
		console.log('23');
	}
	var id_entreprise = $("#id_id_entreprise").val();
	if(!id_entreprise) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_entreprise").append(html);
		teste = 0;
		console.log('24');
	}
	
	
	
	  console.log('eto'+teste);
	if(teste == 1) {
		
		 $("#id_bouton_valider").click() ;
	}
	
}	
	
function supprimerPeriode(element) {
	var id = $(element).attr('row_id');
	$("#id_div_autre_periode"+id).remove();
}
function supprimerMoment(element) {
	var id = $(element).attr('row_id');
	$("#id_div_autre_moment"+id).remove();
}
function ajouterMoment(element ){
	var id = $('#id_compteur_nouvelle_moment').val();
	// console.log(id_adresse);
	id = parseInt(id)+1;
	// console.log(id_adresse);
	var html =
	'<div id="id_div_autre_moment'+id+'">'
		+'<div class="form-group">'
			+'<label style="padding-left:20px;font-weight: bolder;color: #646369;">Période de collecte</label>'
			+'<div class="row">'
				+'<div class="col-3"><label style="padding-left:20px;font-weight: bolder;color: #646369;">Jours</label></div>'
				+'<div class="col-4"><label style="padding-left:20px;font-weight: bolder;color: #646369;">Debut</label></div>'
				+'<div class="col-4"><label style="padding-left:20px;font-weight: bolder;color: #646369;">Fin</label></div>'
			+'</div>'
			+'<div class="row">'
				+'<div class="col-3">'
					+'<select  class="form-control select2" id="id_id_pays" name="autre_jour'+id+'"  data-placeholder="Choisir l\'entreprise concernée" style="width: 100%;" tabindex="-1" aria-hidden="true">'
						+'@for( $i = 0 ; $i < count($jours) ;$i++)'
							+'<option value="{{ $jours[$i]  }}"  >{{ $jours[$i]  }}</option>'
						+'@endfor'
					+'</select>'
				+'</div>'
				+'<div class="col-4 bootstrap-timepicker">'
					+'<div class="form-group">'
						+'<div class="input-group">'
							+'<input type="text" class="form-control timepicker heure_debut" name="heure_debut'+id+'">'

							+'<div class="input-group-append">'
								+'<span class="input-group-text"><i class="fa fa-clock-o"></i></span>'
							+'</div>'
						+'</div>'	
					+'</div>'
					<!-- /.input group -->
				 +' </div>'
				  +'<div class="col-4 bootstrap-timepicker">'
						+'<div class="form-group">'
							+'<div class="input-group">'
								+'<input type="text" class="form-control timepicker heure_fin" name="heure_fin'+id+'">'
								+'<div class="input-group-append">'
									+'<span class="input-group-text"><i class="fa fa-clock-o"></i></span>'
								+'</div>'
							+'</div>'
							<!-- /.input group -->
						+'</div>'
				  +'</div>'
				+'<div class="col-1" >'
					+'<a id="id_bouton_ajout_responsable" class="btn btn-default btn-block" row_id="'+id+'" onclick="supprimerMoment(this)"><i class="fa fa-minus"></i></a>'
				+'</div>'
			+'</div>'
		+'</div>'	
	+'</div>';
	
	$("#id_div_autre_moment_collecte").append(html);
	
	$('#id_compteur_nouvelle_moment').val( id );
}	
function ajouterPeriode(element ) {
	var id = $('#id_compteur_nouvelle_periode').val();
	// console.log(id_adresse);
	id = parseInt(id)+1;
	// console.log(id_adresse);
	var html =
	'<div id="id_div_autre_periode'+id+'">'
		+'<div class="form-group">'
			+'<label style="padding-left:20px;font-weight: bolder;color: #646369;">Autres période de réduction</label>'
			+'<div class="row">'
				+'<div class="col-5">'
					+'<select  class="form-control select2 class_periode_reduction"   name="id_periode_reduction'+id+'"  data-placeholder="Choisir le type du produit" style="width: 100%;" tabindex="-1" aria-hidden="true" id="id_select_type_entreprise">'
						+'<option></option>'
						+'<?php foreach($autre_periode as $periode) { ?>'
						+'<option value="<?php echo $periode->id_periode_reduction; ?>"><?php echo $periode->label; ?></option>'
						+'<?php } ?>'
					+'</select>'
					+'<div >'
						 +'<a  onclick="" data-toggle="modal" data-target="#id_ajout_perdiode_reduction" title="Ajouter période" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>'
					+'</div>'
				+'</div>'
				+'<div class="input-group mb-3 col-6 ">'
					+'<input  class="form-control autre_reduction" type="text" id="id_autre_reduction'+id+'" name="autre_reduction'+id+'"  placeholder="Reduction" value="">'
					+'<div class="input-group-append">'
						+'<span class="input-group-text">%</span>'
					+'</div>'
				+'</div>'
				+'<div class="col-1" >'
					+'<a id="id_bouton_ajout_responsable" class="btn btn-default btn-block" row_id="'+id+'" onclick="supprimerPeriode(this )"><i class="fa fa-minus"></i></a>'
				+'</div>'
			+'</div>'
			+'<div class="row">'
					+'<div class="col-5  div_autre_periode"></div>'
					+'<div class="col-5  div_autre_reduction"></div>'
			+'</div>'
		+'</div>'
	+'</div>';
	
	$("#id_div_autre_periode").append(html);
	
	$('#id_compteur_nouvelle_periode').val( id );
}	

function ajouter_periode_reduction_ajax() {
	var nbjour = parseInt( $('#id_jour_periode').val() );
	var nbsemaine = parseInt( $('#id_semaine_periode').val() );
	var nbmois = parseInt( $('#id_mois_periode').val() );
	
	var nbtotal = nbjour + nbsemaine*7 + nbmois*30 ;
	var label='';
	if(nbtotal>7){
		var nbre_semaines = nbtotal/7 ;
		label = label + parseInt(nbre_semaines) + ' semaine(s)'
		if( nbre_semaines%7 !=0 ){
			var nbre_jours = nbtotal%7 ;
			label = label +'-' + nbre_jours+' jour(s)'
		}
	}else{
		label = nbtotal+" jour(s)" ;
	}
	
	 var token = $('#id_token_periode').val();
	var x =new FormData();
	x.append('label' , label);	
	x.append('nombre_jour' , nbtotal);	
	x.append('_token' , token);	
	$("#loadMe").modal({ backdrop: 'static', keyboard: false });
	var lien = '{{ url('ajouter_periode_reduction')}}' ;
	$.ajax({
	   url : lien,
		method : 'POST',
	  data : x,
		processData : false ,
		contentType : false ,
	   success : function(response, statut){ // success est toujours en place, bien sûr !
			
			var liste_select = $('.class_periode_reduction');
			// var div = document.createElement('div');
			var html = '<option value ="'+ response.id_periode_reduction+'" >'+response.label+' </option>';
			// div.innerHTML = html;
			// console.log(liste_select.length)
			// console.log(div)
			for(var i = 0 ;i< liste_select.length ;i++){
				liste_select.get(i).innerHTML += html;
			}
			console.log(liste_select.get(0));
			$("#loadMe").modal('toggle');
			$("#id_ajout_perdiode_reduction").modal('toggle');
			
	   },

	   error : function(resultat, statut, erreur){
			console.log("erreur");
			
	   }
	});  
}	
// var type_produit = $("#id_type_aliment_label").val();
	// if(!type_produit) {
		// var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		// $("#id_div_nouveau_type_aliment").append(html);
		// teste = 0;
	// }
function ajouter_type_produit_ajax(element) {
	var type = $(element).attr('row_id');
	var label = $('#id_type_produit_label').val();
	 if(!label) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_nouveau_type_produit").append(html);
	 }else{	
		 var token = $('#id_token_produit').val();
		var x =new FormData();
		x.append('label' , label);	
		x.append('_token' , token);	
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var lien = '{{ url('ajouter_type_produit')}}' ;
		$.ajax({
		   url : lien,
			method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				// console.log(resp);
				var html = '<option value ="'+ response.id_type_produit+'"  selected>'+response.label+' </option>';
				if(type=='1'){
					var select = $("#id_id_type_produit");
					select.append(html);
					select.select2("val" , select.select2("val"));
					}
				else{
					var select = $("#id_id_type_produit_don");
					select.append(html);
					select.select2("val" , select.select2("val"));
					}
				$("#loadMe").modal('toggle');
				$("#id_ajout_type_produit").modal('toggle');
				
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				
		   }
		});  
	 }
}
function ajouter_type_aliment_ajax(element) {
	var type = $(element).attr('row_id');
	var label = $('#id_type_aliment_label').val();
	  if(!label) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_nouveau_type_aliment").append(html);
	 }else{	
		 var token = $('#id_token_aliment').val();
		var x =new FormData();
		x.append('label' , label);	
		x.append('_token' , token);	
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var lien = '{{ url('ajouter_type_aliment')}}' ;
		$.ajax({
		   url : lien,
			method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				// console.log(resp);
				var html = '<option value ="'+ response.id_type_aliment+'"  selected>'+response.label+' </option>';
				if(type=='1') {
					var select = $("#id_id_type_aliment");
					select.append(html);
					select.select2("val" , select.select2("val"));
				}
				else{ 
					var select = $("#id_id_type_aliment_don");
					select.append(html);
					select.select2("val" , select.select2("val"));
				}
				$("#loadMe").modal('toggle');
				$("#id_ajout_type_aliment").modal('toggle');
				
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				
		   }
		});  
	 }
}
function ajouter_info_nutritionnelle_ajax(element) {
	var type = $(element).attr('row_id');
	var label = $('#id_info_nut_label').val();
	 if(!label) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_nouveau_information_nutritionnelle").append(html);
	 }else{	
		 var token = $('#id_token_info_nut').val();
		var x =new FormData();
		x.append('label' , label);	
		x.append('_token' , token);	
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var lien = '{{ url('ajouter_information_nutritionnelle')}}' ;
		$.ajax({
		   url : lien,
			method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				// console.log(resp);
				var html = '<option value ="'+ response.id_information_nutritionnelle+'"  selected>'+response.label+' </option>';
				
				if(type=='1') {
					var select = $("#id_id_informaton_nutritionnelle");
					select.append(html);
					select.select2("val" , select.select2("val"));
				}
				else  
					var select = $("#id_id_informaton_nutritionnelle_don");
					select.append(html);
					select.select2("val" , select.select2("val"));
				;
				$("#loadMe").modal('toggle');
				$("#id_ajout_info_nut").modal('toggle');
				
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				
		   }
		});
	 }		
}
function ajouter_raison_surplus_ajax(element) {
	var type = $(element).attr('row_id');
	var label = $('#id_raison_surplus_label').val();
	 if(!label) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_nouvelle_raison").append(html);
	 }else{	
		 var token = $('#id_token_raison_surplus').val();
		var x =new FormData();
		x.append('label' , label);	
		x.append('_token' , token);	
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var lien = '{{ url('ajouter_raison_surplus')}}' ;
		$.ajax({
		   url : lien,
			method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				// console.log(resp);
				var html = '<option value ="'+ response.id_raison_surplus+'"  selected>'+response.label+' </option>';
				if(type=='1') {
					var select = $("#id_id_raison_surplus");
					select.append(html);
					select.select2("val" , select.select2("val").concat(response.id_raison_surplus));
				}
				else {
					var select = $("#id_id_raison_surplus_don");
					select.append(html);
					select.select2("val" , select.select2("val").concat(response.id_raison_surplus));
				}
				$("#loadMe").modal('toggle');
				$("#id_ajout_raison_surplus").modal('toggle');
				
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				
		   }
		});  
	 }
}
	
function autreAdresse(element ) {
	var div = document.getElementById('id_autre_adresse_ramassage');
		//remove tr and th
		while (div.firstChild) {
		  div.removeChild(div.firstChild);
		}
	var html =
		'<div class="form-group">'
			+'<div id="id_div_label_adresse"><input  class="form-control" type="text" name="label" id="id_label_adresse" placeholder="Quartier" value=""></div></br>'
			+'<div id="id_div_ville_adresse"><input  class="form-control" type="ville" name="label" id="id_ville_adresse" placeholder="Ville" value=""></div></br>'
			+'<div id="id_div_code_postal_adresse><input  class="form-control" type="text" name="code_postal" id="id_code_postal_adresse" placeholder="Code postal" value=""></div></br>'
			+'<div class="row " id="id_div_coordonne">'
				+'<div class="col-6"><input  class="form-control" type="text" id="id_latitude" name="latitude"  placeholder="Latitude" value=""></div>'
			+'	<div class="col-5"><input  class="form-control" type="text" id="id_longitude" name="longitude"  placeholder="Longitude" value=""></div>'
			+'<div class="col-1"><a  class="btn btn-default btn-block"  onclick="modifierNouvelleAdresse()"  title="Sélectionner les coordonnées sur une carte" ><i class="fa fa-map"></i> </a></div>'
			+'</div>'
		
		+'	</div></br>'
			+'<select  class="form-control select2" id="id_id_pays" name="pays_id"  data-placeholder="Choisir l\'entreprise concernée" style="width: 100%;" tabindex="-1" aria-hidden="true">'
				+'@foreach($pays as $p)'
					+'<option value="{{ $p->id_pays  }}"  >{{ $p->label  }}</option>'
				+'@endforeach'
			+'</select>'
		+'</div>';
	
	$("#id_autre_adresse_ramassage").append(html);
	$("#id_type_adresse").val('2');
	
}
function adresseCourant(element ) {
	var div = document.getElementById('id_autre_adresse_ramassage');
		//remove tr and th
		while (div.firstChild) {
		  div.removeChild(div.firstChild);
		}
		$("#id_type_adresse").val('1');
} 
function modifierNouvelleAdresse() {
	  mapboxgl.accessToken = 'pk.eyJ1IjoibmphcnkiLCJhIjoiY2p1ZHU0NWYzMDJ1MTRkbW16N2pmczh6YiJ9.i3r9euc0dZsYchVvQfdG5A';
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var map = new mapboxgl.Map({
		container: 'map',
		country : "mg",
		style: 'mapbox://styles/mapbox/streets-v9'
		});
		
		map.on('click', function (e) {
	
		var x = e.lngLat ;
		// console.log(x) ;
		
		$('#id_latitude').val( x.lng );
		$('#id_longitude').val( x.lat );
		$("#changer_coordonees").modal('toggle');
		
		});
		map.on('load', function () {
			
			// *** Add zoom and rotation controls to the map ...
			map.addControl(new mapboxgl.NavigationControl());

			$("#loadMe").modal('toggle');
			$("#changer_coordonees").modal({ backdrop: 'static', keyboard: false });

		});
	} 

  
</script>
@endsection


