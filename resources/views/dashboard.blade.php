@extends('template/default')

@section('titre')
    Tableau de bord
@endsection
@section('custom_styles')
   
   
@endsection
@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif


<section class="content">
      <div class="container-fluid">
			<div class="row">
         
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link active" href="#profile" id="bouton_profile" data-toggle="tab">Général</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_entreprise') }}" >Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_receveur') }}" >Organisation</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_benevole') }}" >Bénévole</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_client') }}" >Consommateur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_commande') }}" >Commande</a></li>	
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_encours') }}" >Activité encours</a></li>
							<button id="download" onclick="getImage()" class="bouton-sticky" title="Exporter en pdf"><img src="{{ asset('/img/Downloads-icon.png') }}" class="exportImage" alt="exporter"></button>
								
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     profile    -->
							<div class="active tab-pane" id="profile">
								<div class="card card-primary">
									<div class="card-body">
											<center>

											<h4>Sélectionner période</h4>
											<form  action="{{ url('dashboard_page') }}" method="get">
												<div class="row">
													
													{{ csrf_field() }}
													<div class="col"></div>
												  <div class="col">
														<label for="inputName">Début</label>
														  <input style="width : 200px;" name="date_debut" type="date" class="form-control" id="inputName" value="<?php if($debut_periode !=0) echo $debut_periode ?>" >
														  @if ($errors->has('date_debut'))
																<span class="help-block">
																	<strong style="color:#FF0000"; >{{ $errors->first('date_debut') }}</strong>
																</span>
															@endif
													</div>
												  <div class="col">
													<label for="inputName" >Fin</label>
													 <input style="width : 200px;" name="date_fin" type="date" value="<?php if($fin_periode !=0) echo $fin_periode ?>" class="form-control" id="inputName" >
													  @if ($errors->has('date_fin'))
															<span class="help-block">
																<strong style="color:#FF0000"; >{{ $errors->first('date_fin') }}</strong>
															</span>
														@endif
													</div>
												  <!--<div class="w-100"></div>-->
												  <!--<div class="col">3</div>-->
												  <div class="col">
													</br>
													<input type="submit" value="valider" class="btn btn-secondary" style="width : 200px; padding-top:20px ;">
													</div>
													<div class="col"></div>
												</div>
												</form>
											</center>
											</br>
								<div id="capture" >
										<div class="row">
										  <div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3>{{ $total_aliments_sauves}} kg</h3>

												<p>Nourriture sauvé </p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											</div>
										  </div>
										  <!-- ./col -->
										  <div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3> {{ $nombre_repas_orphelinat }} </h3>

												<p>Total Repas données aux orphelinats </p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											</div>
										  </div>
										  <!-- ./col -->
									
										
										 <div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3>{{ $total_repas_sauves}} </h3>

												<p>Nombre total de repas sauvé </p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											</div>
										  </div>
										  <!-- ./col -->
										 <div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3>{{ $total_co2_sauve }} kg</h3>

												<p>co2 sauvé </p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											</div>
										  </div>
										  <!-- ./col -->
										<div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3>{{ $nombre_repas_par_vente }} </h3>

												<p>Nombre de repas vendu</p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											</div>
										  </div>
										  <!-- ./col -->
										
										   <div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3>Ar {{ $valeur_monetaire_sauvee_vente}} </h3>

												<p>Montant sauvé par vente</p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											  <div class="small-box-footer">sur {{$nombre_produits_concerne_vente}} produits <i class="fa fa-arrow-circle-right"></i></div>
											</div>
										  </div>
										  <!-- ./col -->
										 <div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3>Ar {{ $valeur_monetaire_sauvee_donation}} </h3>

												<p>Montant sauvé par donation</p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											  <div class="small-box-footer">sur {{$nombre_produits_concerne_donation}} produits <i class="fa fa-arrow-circle-right"></i></div>
											</div>
										  </div>
										  <!-- ./col -->
										  <div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3>Ar {{ $somme_valeur_monetaire}} </h3>

												<p>Montant sauvé</p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											  <div class="small-box-footer">sur {{$somme_produit_concerne}} produits <i class="fa fa-arrow-circle-right"></i></div>
											</div>
										  </div>
										  <!-- ./col -->
										 <div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3> Ar {{ $chiffre_affaire }} </h3>

												<p>Chiffre d'afffaire</p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											</div>
										  </div>
										  <!-- ./col -->
										
										</div>
										<!-- /.row -->
										<div class="row">
												<div class="col-md-12">
												  <div class="card">
													<div class="card-header">
													  <h3 class="card-title">Valeur monétaire en moyenne par produit sauvées par donation</h3>
													</div>
													<!-- /.card-header -->
													<div class="card-body table-custumed">
													  <table id="id_va_don" class="table table-bordered table-hover">
														<thead>
														<tr>
														  <th>Nom du produit</th>
														  <th>Valeur monétaire moyenne</th>
														   
														</tr>
														</thead>
														<tbody>
														@if(count($valeur_monetaire_moyenne_par_produit_sauvee_donation)==0)
															<tr>	
																<th colspan="2"> <p>aucun résultat </p></th>
																
															</tr>	
														@endif
														@foreach($valeur_monetaire_moyenne_par_produit_sauvee_donation as $valeur)
														<tr>
														  <td>{{$valeur->nom_produit }}</td>
														  <td>Ar {{ number_format($valeur->valeur_monetaire_moyenne , 0 , "," , " ") }}</td>
														  
														</tr>
														@endforeach
														
														</tbody>
														<tfoot>
														
														</tfoot>
													  </table>
													</div>
													<!-- /.card-body -->
												  </div>
												  <!-- ./card -->
												</div>
												<!-- ./col -->
											</div>
											 <!-- ./row -->
										<div class="row">
												<div class="col-md-12">
												  <div class="card">
													<div class="card-header">
													  <h3 class="card-title">Valeur monétaire en moyenne par produit sauvées par vente</h3>
													</div>
													<!-- /.card-header -->
													<div class="card-body table-custumed">
													  <table id="id_va_vente" class="table table-bordered table-hover">
														<thead>
														<tr>
														  <th>Nom du produit</th>
														  <th>Valeur monétaire moyenne</th>
														   
														</tr>
														</thead>
														<tbody>
														@if(count($valeur_monetaire_moyenne_par_produit_sauvee_vente)==0)
															<tr>	
																<th colspan="2"> <p>aucun résultat </p></th>
																
															</tr>	
														@endif
														@foreach($valeur_monetaire_moyenne_par_produit_sauvee_vente as $valeur)
														<tr>
														  <td>{{$valeur->nom_produit }}</td>
														  <td>Ar {{ number_format($valeur->valeur_monetaire_moyenne , 0 , "," , " ") }}</td>
														  
														</tr>
														@endforeach
														
														</tbody>
														<tfoot>
														
														</tfoot>
													  </table>
													</div>
													<!-- /.card-body -->
												  </div>
												  <!-- ./card -->
												</div>
												<!-- ./col -->
											</div>
											 <!-- ./row -->
											 <!---------------------------------------------------->
										
									  <div class="row">
										  <div class="col-md-12">
										  <div class="form-group">
												<div class="col-2">
													<select id="id_annee_mois"  class="form-control" onchange="repasParMois()">
														 <?php 
															$currentYear = (int)date("Y");
															for($date=(int)date("Y"); $date>2017; $date--){
																  if($currentYear == $date){
																	echo '<option id="id_select_mois" selected  value="'.$date.'">Cette année</option>';
																  }
																  else{
																	echo '<option value="'.$date.'">L\'année '.$date.'</option>';
																  }
															} ?>
													</select>
												</div>	
											</div>
											
											<div class="card">
											  <div class="card-header no-border">
												<div class="d-flex justify-content-between">
												  <h3 class="card-title">Nombre moyenne des repas distribués par mois</h3>
												</div>
											  </div>
											  <div class="card-body">
												
												<div class="position-relative mb-4">
													<div id="id_loader_div_1">
													<div class="loader" id="id_loader1"></div>
													</div>
													<div class="chart1" id="id_chart1">
														<canvas id="id_repas_mois" height="200"></canvas>
													</div>	
												</div>               
											  </div>
											</div>
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											<div class="card">
											  <div class="card-header no-border">
												<div class="d-flex justify-content-between">
												  <h3 class="card-title">Nombre moyenne des repas reçus par enfant par mois</h3>
												</div>
											  </div>
											  <div class="card-body">
												<div class="position-relative mb-4">
													<div id="id_loader_div_2">
														<div class="loader" id="id_loader2"></div>
													</div>
													<div class="chart2" id="id_chart2">
													  <canvas id="id_repas_mois_enfant" height="200"></canvas>
													 </div>
												</div>               
											  </div>
											</div>
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
								</div>
									  </div>
									  <!-- ./card-body -->
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
					 
							<!---- Pane      Utilisateur  --->
							<div class="tab-pane" id="utilisateur">
								
							</div>	
							<!-- /.tab-pane -->
							  
							 <!----- Pane     Tache --> 
							<div class="tab-pane" id="tache">
							
							</div>
							 <!-- /.tab-pane -->
					
					<!-- /.tab-content -->
				  </div><!-- /.card-body -->
				</div>
				<!-- /.nav-tabs-custom -->
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
	
	
@endsection

@section('modals')
<!-- Modal -->


@endsection

@section('custom_script')

<!-- ChartJS 1.0.2
<script src="{{ asset('dist/plugins/chartjs-old/Chart.min.js') }}"></script>
-->
<!-- PAGE SCRIPTS -->

<!--<script src="{{ asset('dist/js/pages/dashboard2.js') }}"></script>-->
<script src="{{ asset('dist/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('dist/js/demo.js') }}"></script>
<script src="{{ asset('dist/js/pages/dashboard3.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/html2canvaspdf/js/html2canvas.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/html2canvaspdf/js/jsPdf.debug.js') }}"></script>
<script>
  $(function () {

	$('#id_va_don').dataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
	$("#id_va_vente").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
	
  });

function getImage(){
	 		$("body").css("cursor", "progress");
          html2canvas(document.getElementById('capture'), {
              onrendered:function(canvas) {

                  var contentWidth = canvas.width;
                  var contentHeight = canvas.height;

                  // Une page pdf indique la hauteur de la toile générée par la page html;
                  var pageHeight = contentWidth / 595.28 * 841.89;
                  // la hauteur de la page HTML n'est pas générée pdf
                  var leftHeight = contentHeight;
                  // offset de page pdf
                  var position = 0;
                 // a4 format de papier [595.28, 841.89], toile générée par page HTML en largeur et hauteur de l'image pdf
                  var imgWidth = 555.28;
                  var imgHeight = 555.28/contentWidth * contentHeight;

                  var pageData = canvas.toDataURL('image/jpeg', 1.0);

                  var pdf = new jsPDF('', 'pt', 'a4');
                 // Il y a deux hauteurs à distinguer, l'une est la hauteur réelle de la page html et la hauteur de la page qui a généré le pdf (841.89)
                  // Lorsque le contenu ne dépasse pas la plage affichée sur la page pdf, aucune pagination n'est requise.
                  if (leftHeight < pageHeight) {
                      pdf.addImage(pageData, 'JPEG', 20, 0, imgWidth, imgHeight );
                  } else {
                      while(leftHeight > 0) {
                          pdf.addImage(pageData, 'JPEG', 20, position, imgWidth, imgHeight)
                          leftHeight -= pageHeight;
                          position -= 841.89;
                          // Éviter d'ajouter des pages vierges
                          if(leftHeight > 0) {
                              pdf.addPage();
                          }
                      }
                  }
                   $("body").css("cursor", "default");
                  pdf.save('content.pdf');
              }
          })
      
}

</script>
@endsection