@extends('template/default')

@section('titre')
    Fiche produit
@endsection

@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif


<section class="content">
      <div class="container-fluid">
			<div class="row">
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link " href="{{ url('entreprise') }}">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link active" href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									<div class="card-body">
										<h5>Action </h5>
										<div class="row"> 
											<div class="col-2"> 
												<a href= "{{ url('update_produit_page') }}?id_produit={{ $produit->id_produit }}" ><button type="button" class="btn btn-success">Mettre à jour</button></a>
											</div>
											
										</div>
									</div>
									<div class="card-body">
										  <div class="row">
													<div class="col-md-6">
														<strong>
															<i class="fa fa-address-card mr-1"></i>
															 Information sur le produit
														</strong>
														<div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
																<div class="col-8"><?php echo (!empty($produit->nom_produit))?$produit->nom_produit:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Entreprise propriétaire</div>
																<div class="col-8"><?php echo (!empty($entreprise_proprietaire->nom_entreprise))?$entreprise_proprietaire->nom_entreprise:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Description du produit</div>
																<div class="col-8"><?php echo (!empty($produit->description))?$produit->description:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Poids du produit</div>
																<div class="col-8"><?php echo (!empty($produit->poids))?$produit->poids.' kg' :'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Volume du produit</div>
																<div class="col-8"><?php echo (!empty($produit->volume))?$produit->volume.' l':'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Type du produit</div>
																<div class="col-8"><?php echo (!empty($type_produit->label))?$type_produit->label:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Type d'aliment</div>
																<div class="col-8"><?php echo (!empty($type_aliment->label))?$type_aliment->label:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Prix du produit</div>
																<div class="col-8"><?php echo (!empty($type_aliment->label))?'Ar '.$produit->prix:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Groupement du produit</div>
																@if($produit->groupement=='1')<div class="col-8">Par unité</div>
																@elseif($produit->groupement=='2')<div class="col-8">Totalité</div>
																@else($produit->groupement=='3')<div class="col-8">Par portion</div>
																@endif
															</div>
															<div class="row " style="margin-top: 4px;margin-bottom: 4px;">
															<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Stockage</div>
															<div class="col-8">
																@if(count($stockages)==0)
																	<span style="font-style: italic;">(Aucun)</span>
																@else
																@foreach($stockages as $stockage)
																	<li>{{ $stockage }}</li>
																@endforeach
																@endif
															</div>
														</div>
														</div>
														<hr>
													</div>
													<!-- /.col -->
													<div class="col-md-6">
														<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
															<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Information nutritionnelle</div>
															<div class="col-8">
																@if(count($information_nutritionnelles)==0)
																	<span style="font-style: italic;">(Non définie)</span>
																@else
																<ul>@foreach($information_nutritionnelles as $info)
																	<li>{{ $info->label }}</li>
																</ul>@endforeach
																@endif
															</div>
														</div>
														<div class="row " style="margin-top: 4px;margin-bottom: 4px;">
															<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Raison de surplus</div>
															<div class="col-8">
																@if( count( $produit->donation->raison_surplus()->get() ) ==0 )
																	<span style="font-style: italic;">(Non définie)</span>
																@else
																
																<ul>@foreach( $produit->donation->raison_surplus()->get() as $raison )
																	<li>{{ $raison->label }}</li>
																</ul>@endforeach
																
																@endif
															</div>
														</div>
													
														<div>
														<strong>
															<i class="fa fa-address-card mr-1"></i>
															 Image
														</strong>
														@if( count($photos)==0) 
															<span style="font-style: italic;">(Aucune)</span>
														@endif
														</div>
														@foreach($photos as $photo)
															<div style="float : left;"><img class="imaged" width="250 px" height="250 px" src="{{ asset('img/'.$photo->url)  }}"</div>
														@endforeach
														
													</div>
													<!-- /.col -->
												</div>
												<!-- /.row -->
										  </div>
										  <!-- ./card-body -->
									</div>
									<!-- /.card -->
								</div>
								<!-- /.tab-pane -->
							</div>
							<!-- /.tab-content -->
						</div>
						 <!-- ./card-body -->
					</div>
					<!-- /.card-->
			  </div>
          <!-- /.col -->
		 </div>
		 <!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection
@section('modals')
<!---- modal   supprimer      tâche  ----->
<div class="modal fade" id="supprimer_entreprise" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Supprimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<a id="supprimer_entreprise_button" href="supprimer_tache"><button type="button" class="btn btn-secondary" >Sûr</button></a>
      </div>
	
	</div>
  </div>
</div>

@endsection
@section('custom_script')
<script type="text/Javascript">
	

	$(function () {
		
		

	});
  
function supprimerEntreprise(element) {
		var lien = $(element).attr('url_supprimer_entreprise');
		console.log(lien);
		document.getElementById("supprimer_entreprise_button").href =lien+"&return=0"
		
		$("#supprimer_entreprise").modal({ backdrop: 'static', keyboard: false });
	}
  
  
</script>
@endsection