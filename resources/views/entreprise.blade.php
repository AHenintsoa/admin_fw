@extends('template/default')

@section('titre')
   Entreprise
@endsection

@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif
 @if(Session::get('id_utilisateur_type')!='5')

<section class="content">
      <div class="container-fluid">
			<div class="row">
         
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link active" href="">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									<div class="card-body">
										<div class="row">
											<div class="col-3"><a href="{{ url('page_ajouter_entreprise') }}"><button type="button" class="btn btn-primary btn-block" >Ajouter Entreprise </button></div>
											<div class="col-3"><a href="{{ url('exporter_entreprise') }}"><button  class="btn btn-primary btn-block">Export </button></a></div>
										
										</div>
										</br>
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Information sur les entreprises</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_liste_entreprise_table" class="table table-bordered table-hover">
												<thead>
												<tr><!--(nom de l’entité, groupe, type de l’entité, nom complet personne  de contact, mail, tel, adresse, ca type d’aliments, etc.)             -->
												  <th>Nom</th>
												  <th>Type</th>
												  <th>Action</th>
												 
												</tr>
												</thead>
												<tbody>
												@for( $i= 0 ; $i < count( $entreprises) ; $i++)
												<tr>
												  <td>{{$entreprises[$i]->nom_entreprise }}</td>
												  <td>
													 @for($j = 0 ; $j< count($entreprise_type[$i]) ; $j++)
														 @if( $j+1 <count($entreprise_type[$i]) &&  $j+1 != count($entreprise_type[$i]) ) 
															{{ $entreprise_type[$i][$j]->label }} ,
														@elseif( $j+1 == count($entreprise_type) )
															{{$entreprise_type[$i][$j]->label }}
														@else
															{{$entreprise_type[$i][$j]->label }}
														@endif
													@endfor
													</td>
													 <td><a href="{{ url('update_entreprise_page') }}?id_entreprise={{ $entreprises[$i]->id_entreprise }}" ><button class="bouton"><span class="fa  fa-pencil"></span></button></a>
													   <a href="{{ url('information_entreprise') }}?id_entreprise={{$entreprises[$i]->id_entreprise}}" ><button class="bouton"><span class="fa  fa-eye"></span></button></a>
														<button  url_supprimer_entreprise="{{ url('supprimer_entreprise')}}?utilisateur_id={{$entreprises[$i]->utilisateur_id }}" onclick="supprimerEntreprise(this)"  class="bouton"><span class="fa  fa-remove"></span></button> 
														<a href="{{ url('page_contacter_entreprise') }}?id_entreprise={{$entreprises[$i]->id_entreprise}}" ><button class="bouton" title="contacter"><span class="fa  fa-envelope"></span></button></a>
													</td>
												</tr>
												@endfor
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										
									  </div>
									  <!-- ./card-body -->
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
					 
					
					<!-- /.tab-content -->
				  </div><!-- /.card-body -->
				</div>
				<!-- /.nav-tabs-custom -->
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection
@section('modals')
<!---- modal   supprimer      tâche  ----->
<div class="modal fade" id="supprimer_entreprise" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Supprimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<a id="supprimer_entreprise_button" href="supprimer_tache"><button type="button" class="btn btn-secondary" >Sûr</button></a>
      </div>
	
	</div>
  </div>
</div>
@endif
@endsection
@section('custom_script')
<script type="text/Javascript">
	
	$(function () {
	

		$("#id_liste_entreprise_table").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		

	});
  
function supprimerEntreprise(element) {
		var lien = $(element).attr('url_supprimer_entreprise');
		console.log(lien);
		document.getElementById("supprimer_entreprise_button").href =lien
		$("#supprimer_entreprise").modal({ backdrop: 'static', keyboard: false });
	}
  
  
</script>
@endsection