@extends('template/default')

@section('titre')
    Fiche consommateur
@endsection

@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif


<section class="content">
      <div class="container-fluid">
			<div class="row">
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link " href="{{ url('entreprise') }}">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link active" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									<div class="card-body">
										<h5>Action </h5>
										<div class="row"> 
											<div class="col-2"> 
												<a href= "{{ url('update_client_page') }}?id_consommateur={{ $consommateur->id_consommateur }}" ><button type="button" class="btn btn-success">Mettre à jour</button></a>
											</div>
											
										</div>
									</div>
									<div class="card-body">
										<center><h4><strong>
											<i class="fa fa-address-card mr-1"></i>
											Information sur le client
										</strong></h4></center>
										<div class="row">
											<div class="col">
												<div>
													<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
														<div class="col-8"><?php echo (!empty($consommateur->nom))?$consommateur->nom.' '.$consommateur->prenom:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
													</div>
													<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Mail</div>
														<div class="col-8"><?php echo (!empty($consommateur->mail))?$consommateur->mail:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
													</div>
												</div>
											</div>
											<!--col-->
											<div class="col">
												<div>	
													<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Numero de téléphone</div>
														<div class="col-6"><?php echo (!empty($consommateur->numero_telephone))?$consommateur->numero_telephone:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
													</div>
													<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Point de livraison</div>
														<div class="col-6"><?php echo (!empty($consommateur->adresse_de_livraison))?$consommateur->adresse_de_livraison :'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
													</div>
												</div>
											</div>
											<!-- /.col -->	
										</div>
										<!-- /.row -->
										</br>
										<center><h4><strong>
											<i class="fa fa-address-card mr-1"></i>
											 Son activité
										</strong></h4></center>
										<div class="row">
											<div class="col">
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-7" style="padding-left:20px;font-weight: bolder;color: #646369;">Achat effectué</div>
													<div class="col-5"><?php echo (!empty($achat->montant_total))? number_format( (float)$achat->montant_total , 0 , "," , " " ).' MGA' :'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
												</div>
											</div>
											<!-- /.col -->
											<div class="col">	
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-7" style="padding-left:20px;font-weight: bolder;color: #646369;">Reduction préféréé</div>
													<div class="col-5">{{ number_format( (float)$best_reduction->reduc , 2 , "," , " " ).' %' }}</div>
												</div>
											</div>
											<!-- /.col -->
										</div>
										<!--row-->
										<div class="row">
											<div class="col">
												<div class="card">
													<div class="card-header">
														<h3 class="card-title">Moyenne du prix par produit</h3>
													</div>
													<!-- /.card-header -->
													<div class="card-body table-custumed">
														<table id="id_prix_table" class="table table-bordered table-hover">
															<thead>
																<tr><!-- nom, prénom, operateurs, nr tel,            -->
																	<th>Nom du produit</th>
																	<th>Prix moyenne</th>
																</tr>
															</thead>
															<tbody>
																@foreach($moyenne_prix_par_produit as $produit )
																<tr>
																	<td>{{ $produit->nom_produit }}   </td>
																	<td>{{ number_format( (float)$produit->prix_moyen , 0 , "," , " " ).' MGA' }}</td>
																</tr>
																@endforeach
															</tbody>
															<tfoot>
														
															</tfoot>
														</table>
													</div>
													<!-- /.card-body -->
												</div>
												<!-- ./card -->
											</div>
											<!-- /.col -->
											<div class="col">
												<div class="card">
													<div class="card-header">
														<h3 class="card-title">Moyenne de reduction par produit</h3>
													</div>
													<!-- /.card-header -->
													<div class="card-body table-custumed">
														<table id="id_reduction_table" class="table table-bordered table-hover">
															<thead>
																<tr><!-- nom, prénom, operateurs, nr tel,            -->
																	<th>Nom du produit</th>
																	<th>Reduction moyenne</th>
																</tr>
															</thead>
															<tbody>
																@foreach($moyenne_reduction as $produit )
																<tr>
																	<td>{{ $produit->nom_produit }}   </td>
																	<td>{{ number_format( (float)$produit->moyenne_remise , 2 , "," , " " ).' %' }}</td>
																</tr>
																@endforeach
															</tbody>
															<tfoot>
														
															</tfoot>
														</table>
													</div>
													<!-- /.card-body -->
												</div>
												<!-- ./card -->
											</div>
											<!-- /.col -->
										</div>
										<!-- /.row -->
									</div>
									<!-- ./card-body -->
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
					 <!-- ./card-body -->
				</div>
				<!-- /.card-->
			  </div>
          <!-- /.col -->
		 </div>
		 <!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection
@section('modals')
<!---- modal   supprimer      tâche  ----->
<div class="modal fade" id="supprimer_entreprise" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Supprimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<a id="supprimer_entreprise_button" href="supprimer_tache"><button type="button" class="btn btn-secondary" >Sûr</button></a>
      </div>
	
	</div>
  </div>
</div>

@endsection
@section('custom_script')
<script type="text/Javascript">
	

	$(function () {
		$("#id_reduction_table").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							  "aaSorting": [[0, 'asc']]
							});
		$("#id_prix_table").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							  "aaSorting": [[0, 'asc']]
							});					
		
		

	});
  
function supprimerEntreprise(element) {
		var lien = $(element).attr('url_supprimer_entreprise');
		console.log(lien);
		document.getElementById("supprimer_entreprise_button").href =lien+"&return=0"
		
		$("#supprimer_entreprise").modal();
	}
  
  
</script>
@endsection