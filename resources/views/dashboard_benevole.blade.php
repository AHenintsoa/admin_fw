@extends('template/default')

@section('titre')
    Tableau de bord
@endsection

@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif


<section class="content">
      <div class="container-fluid">
			<div class="row">
         
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_page') }}" data-toggle="tab">Général</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_entreprise') }}" >Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_receveur') }}" >Organisation</a></li>
							<li class="nav-item"><a class="nav-link  active" href="" >Bénévole</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_client') }}" >Consommateur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_commande') }}" >Commande</a></li>	
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_encours') }}" >Activité encours</a></li>
						</ul>
						<button id="download" onclick="getImage()" class="bouton-sticky" title="Exporter en pdf"><img src="{{ asset('/img/Downloads-icon.png') }}" class="exportImage" alt="exporter"></button>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     profile    -->
							<div class="active tab-pane" id="benevole">
								<div class="card card-primary">
									<div id="capture" >
									<div class="card-body">
										<div class="row">
										  <div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3>{{ number_format( (float)$nombre_benevoles->nombre_total , 0, "," , " ")}} </h3>

												<p>Nombre total de bénévoles </p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											</div>
										  </div>
										  <!-- ./col -->
										
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Nombre de nouveau bénévole de ce mois</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_beneficiaire" class="table table-bordered table-hover">
												<thead>
												<tr>
												  <th>Nom</th>
												</tr>
												</thead>
												<tbody>
												@foreach($nombre_benevoles_ce_mois as $benevole)
												<tr>
												  <td>{{$benevole->nom }}</td>
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
									  </div>
									  <!-- ./card-body -->
									</div>
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
					 
							<!---- Pane      Utilisateur  --->
							<div class="tab-pane" id="utilisateur">
								
							</div>	
							<!-- /.tab-pane -->
							  
							 <!----- Pane     Tache --> 
							<div class="tab-pane" id="tache">
							
							</div>
							 <!-- /.tab-pane -->
					
					<!-- /.tab-content -->
				  </div><!-- /.card-body -->
				</div>
				<!-- /.nav-tabs-custom -->
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
	
	
@endsection

@section('modals')
<!-- Modal -->


@endsection

@section('custom_script')
<script type="text/javascript" src="{{ asset('plugins/html2canvaspdf/js/html2canvas.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/html2canvaspdf/js/jsPdf.debug.js') }}"></script>

<!-- PAGE SCRIPTS -->
<script>
  $(function () {
	$("#id_beneficiaire").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
	
  });
  function getImage(){
	 
          html2canvas(document.getElementById('capture'), {
              onrendered:function(canvas) {

                  var contentWidth = canvas.width;
                  var contentHeight = canvas.height;

                  // Une page pdf indique la hauteur de la toile générée par la page html;
                  var pageHeight = contentWidth / 595.28 * 841.89;
                  // la hauteur de la page HTML n'est pas générée pdf
                  var leftHeight = contentHeight;
                  // offset de page pdf
                  var position = 0;
                 // a4 format de papier [595.28, 841.89], toile générée par page HTML en largeur et hauteur de l'image pdf
                  var imgWidth = 555.28;
                  var imgHeight = 555.28/contentWidth * contentHeight;

                  var pageData = canvas.toDataURL('image/jpeg', 1.0);

                  var pdf = new jsPDF('', 'pt', 'a4');
                 // Il y a deux hauteurs à distinguer, l'une est la hauteur réelle de la page html et la hauteur de la page qui a généré le pdf (841.89)
                  // Lorsque le contenu ne dépasse pas la plage affichée sur la page pdf, aucune pagination n'est requise.
                  if (leftHeight < pageHeight) {
                      pdf.addImage(pageData, 'JPEG', 20, 0, imgWidth, imgHeight );
                  } else {
                      while(leftHeight > 0) {
                          pdf.addImage(pageData, 'JPEG', 20, position, imgWidth, imgHeight)
                          leftHeight -= pageHeight;
                          position -= 841.89;
                          // Éviter d'ajouter des pages vierges
                          if(leftHeight > 0) {
                              pdf.addPage();
                          }
                      }
                  }

                  pdf.save('content.pdf');
              }
          })
      
}
</script>
@endsection