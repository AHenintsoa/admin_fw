@extends('template/default')

@section('titre')
    Profile
@endsection

@section('contenu')
 @if(Session::get('id_utilisateur_type')!='5')
<section class="content">
      <div class="container-fluid">
			<div class="row">
         
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
				 
				 
				 @if(isset($tache['type_modification']) && $tache['type_modification'] =='tache')
				<div class="card-header p-2">
					<h1>Mis à jour </h1>
				 </div><!-- /.card-header -->
				 <div class="tab-pane" id="settings">
				 
                    <form class="form-horizontal" action="{{ url('update_tache') }}" method="POST">
                      {{ csrf_field() }}
					  <div class="form-group">
					  <input name="id_tache" class="form-control" type="hidden"  value="{{ $tache['id_tache'] }}">
                        <label for="inputName"  class="col-sm-2 control-label">Label</label>
                        <div class="col-sm-12">
                          <input type="text" class="form-control" id="inputName" name="label" value="{{ $tache['label']}}" placeholder="Label">
                        </div>
						@if ($errors->has('label'))
							<span class="help-block">
								<strong style="color:#FF0000"; >{{ $errors->first('label') }}</strong>
							</span>
						@endif
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-12">
                          <input type="text" class="form-control" id="inputEmail" name="description" value="{{ $tache['description'] }}"  placeholder="description">
                        </div>
						@if ($errors->has('description'))
							<span class="help-block">
								<strong style="color:#FF0000"; >{{ $errors->first('description') }}</strong>
							</span>
						@endif
                      </div>
                      <div class="form-group">
                        <label for="inputName2" class="col-sm-2 control-label">Objectif</label>
                        <div class="col-sm-12">
                          <input type="text" class="form-control" id="inputName2" name="objectif" value="{{ $tache['objectif'] }}"  placeholder="objectif">
                        </div>
						@if ($errors->has('objectif'))
							<span class="help-block">
								<strong style="color:#FF0000"; >{{ $errors->first('objectif') }}</strong>
							</span>
						@endif
                      </div>
                      <div class="form-group">
						<label for="inputName2" class="col-sm-2 control-label">Utilisateur</label>
						<div class="col-sm-6">
							<select name="utilisateur_id"  class="form-control">
							<option value="{{  $tache['utilisateur_id'] }}">{{ $tache['nomUtilisateur'] }}</option>
							@foreach($liste_membre_fw as $membre_fw) 
								  <option value="{{  $membre_fw->utilisateur_id }}">{{ $membre_fw->nom }}</option>
							@endforeach
							</select>
						</div>	
					  </div>
                      <div class="form-group">
                        <label for="inputSkills" class="col-sm-2 control-label">Date d'assignation</label>

                        <div class="col-sm-12">
                          <input type="date"  class="form-control" id="inputSkills" name="date_assignation" value="{{ $tache['date_assignation']}}"  placeholder="date d ' assignation">
                        </div>
						
						<label for="inputSkills" class="col-sm-2 control-label">Date d'échéance</label>

                        <div class="col-sm-12">
                          <input type="date"   class="form-control" id="inputSkills" name="date_echeance" value="{{ $tache['date_echeance']}}"  placeholder="date d'echeance">
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <input type="submit" class="btn btn-danger" value="Mettre à jour">
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
				  @else
				   <div class="card-header p-2">
					<h1>Mis à jour </h1>
				  </div><!-- /.card-header -->
					<div class="tab-pane" id="settings">
				 
                    <form class="form-horizontal" action="{{ url('update_utilisateur') }}" method="POST">
                      {{ csrf_field() }}
					  <div class="form-group">
					  <input name="id_membre_fw" class="form-control" type="hidden"  value="{{ $profile['id_membre_fw'] }}">
                        <label for="inputName"  class="col-sm-2 control-label">Nom</label>
                        <div class="col-sm-12">
                          <input type="text" class="form-control" id="inputName" name="nom" value="{{ $profile['nom']}}" placeholder="">
                        </div>
						@if ($errors->has('nom'))
							<span class="help-block">
								<strong style="color:#FF0000"; >{{ $errors->first('nom') }}</strong>
							</span>
						@endif
                      </div>
                      <div class="form-group">
                        <label for="inputEmail" class="col-sm-2 control-label">tel</label>

                        <div class="col-sm-12">
                          <input type="text" class="form-control" id="inputEmail" name="tel" value="{{ $profile['tel'] }}"  placeholder="">
                        </div>
						@if ($errors->has('tel'))
							<span class="help-block">
								<strong style="color:#FF0000"; >{{ $errors->first('tel') }}</strong>
							</span>
						@endif
                      </div>
                      <div class="form-group">
                        <label for="inputName2" class="col-sm-2 control-label">Email</label>

                        <div class="col-sm-12">
                          <input type="email" class="form-control" id="inputName2" name="email" value="{{ $profile['email'] }}"  placeholder="">
                        </div>
                      </div>
                      <div class="form-group">
						<label for="inputName2" class="col-sm-2 control-label">Poste</label>
						<div class="col-sm-6">
							<select name="poste_id"  class="form-control">
							<option value="{{  $profile['poste_id'] }}">{{ $profile['label_poste'] }}</option>
							@foreach($poste as $poste_detail) 
								  <option value="{{  $poste_detail['id_poste'] }}">{{ $poste_detail['label'] }}</option>
							@endforeach
							</select>
						</div>	
					  </div>
					   <div class="form-group">
						<label for="inputName2" class="col-sm-2 control-label">Role</label>
						<div class="col-sm-6">
							<select name="role_id"  class="form-control">
							<option value="{{  $profile['role_id'] }}">{{ $profile['label_role'] }}</option>
							@foreach($utilisateur_type as $type) { ?>
								  <option value="{{  $type['id_utilisateur_type'] }}">{{ $type['label'] }}</option>
							@endforeach
							</select>
						</div>	
					  </div>
					   <div class="form-group">
						<label for="inputName2" class="col-sm-2 control-label">Pays</label>
						<div class="col-sm-6">
							<select name="pays_id"  class="form-control">
								<option value="{{  $profile['pays_id'] }}">{{ $profile['label_pays'] }}</option>
								@foreach($pays as $pays_data) { ?>
									<option value="{{  $pays_data->id_pays }}">{{ $pays_data->label }}</option>
								@endforeach
							</select>
						</div>	
					  </div>
                      
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <input type="submit" class="btn btn-success" value="Mettre à jour">
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->  
				  @endif
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
	
@endif	
@endsection



@section('custom_script')
<script>
function typeSelected($type, $allTypes){
		foreach($allTypes as $t){
			if($t->id_type_entreprise == $type->id_type_entreprise){
				return true;
			}
		}
		return false;
	}
  $(function () {
    
	 $("#table_tache").DataTable();
    $("#table_utilisateur").DataTable();
    
  });
</script>
@endsection