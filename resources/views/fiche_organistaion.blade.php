@extends('template/default')

@section('titre')
    Fiche receveur
@endsection

@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif


<section class="content">
      <div class="container-fluid">
			<div class="row">
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link " href="{{ url('entreprise') }}">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link active" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									<div class="card-body">
										<h5>Action </h5>
										<div class="row"> 
											<div class="col-2"> 
												<a href= "{{ url('update_receveur_page') }}?id_organisation={{ $receveur->id_organisation }}" ><button type="button" class="btn btn-success">Mettre à jour</button></a>
											</div>
											
										</div>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-6">
												<strong>
													<i class="fa fa-address-card mr-1"></i>
													Information sur l'organisation
												</strong>
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom:</div>
													<div class="col-8"><?php echo (!empty($receveur->nom_organisation))?$receveur->nom_organisation:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
												</div>
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Numero:</div>
													<div class="col-8"><?php echo (!empty($receveur->numero))?$receveur->numero:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
												</div>
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Type:</div>
													<div class="col-8"><?php echo (!empty($receveur->type_organisation->label))?$receveur->type_organisation->label:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
												</div>
												</br>
												<strong>
													<i class="fa fa-address-card mr-1"></i>
													Information sur le responsable
												</strong>
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom:</div>
													<div class="col-8"><?php echo (!empty($receveur_employe->nom))?$receveur_employe->nom.' '.$receveur_employe->prenom:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
												</div>
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Mail:</div>
													<div class="col-8"><?php echo (!empty($receveur_employe->email))?$receveur_employe->email:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
												</div>
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Tel:</div>
													<div class="col-8"><?php echo (!empty($receveur_employe->telephone))?$receveur_employe->telephone:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
												</div>
											</div>
											<!--col-->
											<div class="col-6">
												<strong>
													<i class="fa fa-address-card mr-1"></i>
													Emplacement de l'organisation
												</strong>
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Quartier:</div>
													<div class="col-8"><?php echo (!empty($receveur->adresse->label))?$receveur->adresse->label:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
												</div>
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Ville:</div>
													<div class="col-8"><?php echo (!empty($receveur->adresse->ville))?$receveur->adresse->ville:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
												</div>
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Longitude:</div>
													<div class="col-8"><?php echo (($receveur->adresse->longitude))?$receveur->adresse->longitude:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
												</div>
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Latitude:</div>
													<div class="col-8"><?php echo (($receveur->adresse->latitude))?$receveur->adresse->latitude:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
												</div>
												<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Pays:</div>
													<div class="col-8"><?php echo (!empty($receveur->adresse->pays->label))?$receveur->adresse->pays->label:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
												</div>
											</div>
											<!--col-->
											
										</div>
										<!-- /.row -->
									</div>
									<!-- ./card-body -->
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
					 <!-- ./card-body -->
				</div>
				<!-- /.card-->
			  </div>
          <!-- /.col -->
		 </div>
		 <!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection
@section('modals')
<!---- modal   supprimer      tâche  ----->
<div class="modal fade" id="supprimer_entreprise" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Supprimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<a id="supprimer_entreprise_button" href="supprimer_tache"><button type="button" class="btn btn-secondary" >Sûr</button></a>
      </div>
	
	</div>
  </div>
</div>

@endsection
@section('custom_script')
<script type="text/Javascript">
	

	$(function () {
		$("#id_reduction_table").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							  "aaSorting": [[0, 'asc']]
							});
		$("#id_prix_table").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							  "aaSorting": [[0, 'asc']]
							});					
		
		

	});
  
function supprimerEntreprise(element) {
		var lien = $(element).attr('url_supprimer_entreprise');
		console.log(lien);
		document.getElementById("supprimer_entreprise_button").href =lien+"&return=0"
		
		$("#supprimer_entreprise").modal();
	}
  
  
</script>
@endsection