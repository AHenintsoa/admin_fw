@extends('template/default')

@section('titre')
  Clients
@endsection

@section('contenu')
@if(Session::has('erreur_ajout'))
<input type="hidden" id="erreur_ajout" name="choix" value="{{ Session::get('erreur_ajout') }}">
@endif
 @if(Session::get('id_utilisateur_type')!='5')

<section class="content">
      <div class="container-fluid">
			<div class="row">
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							
							<li class="nav-item"><a class="nav-link" href="{{ url('entreprise') }}">Entreprise</a></li>

							<li class="nav-item"><a class="nav-link" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link active" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
							
								<div class="card card-primary">
									
									<div class="card-body">
										<div class="row">
											<div class="col-3"><button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#ajouter_client">Ajouter Client </button></div>
											<div class="col-3"><a href="{{ url('exporter_client') }}"><button  class="btn btn-primary btn-block">Export </button></a></div>
										</div>
										</br>
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Information sur les clients</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_liste_receveur_table" class="table table-bordered table-hover">
												<thead>
												<tr><!-- nom, prénom, operateurs, nr tel,            -->
													  <th>Nom complet</th>
													  <th>Adresse de livraison</th>
													  <th>Numero de téléphone</th>
														<th>Action</th>
												</tr>
												</thead>
												<tbody>
												@for( $i = 0 ; $i < count($consommateurs) ; $i++)
												<tr>
													  <td>{{ $consommateurs[$i]['nom'] }} {{  $consommateurs[$i]['prenom'] }} </td>
													  <td>{{ $consommateurs[$i]['adresse_de_livraison'] }}</td>
													  <td>{{ $consommateurs[$i]['numero_telephone'] }}</td> 
													   <td> <a href="{{ url('update_client_page') }}?id_consommateur={{$consommateurs[$i]['id_consommateur']}}" ><button  class="bouton"><span class="fa  fa-pencil"></span></button></a>
													   <button  class="bouton" row_id_consommateur="{{ $consommateurs[$i]['id_consommateur'] }}" onclick="visualiserDetail(this)"><span class="fa  fa-eye"></span></button>
														<button  url_supprimer_client="{{ url('supprimer_client')}}?utilisateur_id={{$consommateurs[$i]['utilisateur_id'] }}" onclick="supprimerClient(this)"  class="bouton" ><span class="fa  fa-remove"></span></button> 
														<a href="{{ url('page_contacter_client') }}?id_consommateur={{$consommateurs[$i]->id_consommateur}}" ><button class="bouton" title="contacter"><span class="fa  fa-envelope"></span></button></a></td>
												 </tr>
												@endfor
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										
									  </div>
									  <!-- ./card-body -->
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
					 
					
					<!-- /.tab-content -->
				  </div><!-- /.card-body -->
				</div>
				<!-- /.nav-tabs-custom -->
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection
@section('modals')
<!---- modal   supprimer      client  ----->
<div class="modal fade" id="supprimer_client" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Supprimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<a id="supprimer_client_button" href=""><button type="button" class="btn btn-secondary" >Sûr</button></a>
      </div>
	
	</div>
  </div>
</div>



<!-- modal detail client -->

<div class="modal fade" id="id_visualiserDetail" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail suplémentaire</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	 
					<center>
					 <h5>
						<i class="fa fa-address-card mr-1"></i>
						 Information globale
					</h5>
						<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
							<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Total Achat (MGA)</div>
							<div class="col-6"><p  id="id_achat"></p></div>
						</div>
						<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
							<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Réduction préférée</div>
							<div class="col-6" id="id_reduction"></div>
						</div>
						</hr>
						<h5>
						<i class="fa fa-address-card mr-1"></i>
						 Moyenne du prix par produit
						 </h5>
						 <div id="id_moyenne_prix_par_produit">
							
						</div>
						</hr>
						<h5>
						<i class="fa fa-address-card mr-1"></i>
						 Moyenne de la remise par produit
						 </h5>
						<div id="id_remise_par_produit">
							
						</div>
					
					</center>
					<hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
	 </form> 
    </div>
  </div>
</div>
<!-- modal no info -->
<div class="modal fade" id="id_visualiserDetailVide" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail suplémentaire</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	 
		<center>
		 <h5>
			<i class="fa fa-address-card mr-1"></i>
			 Information vide
		</h5>
			
		</center>
		<hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
	 </form> 
    </div>
  </div>
</div>


<!----modal   ajouter     receveur---->
<div class="modal fade" id="ajouter_client" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ajouter Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <form action="{{ url('ajouter_client') }}" method="POST">
		{{ csrf_field() }}
		<strong>
			<i class="fa fa-address-card mr-1"></i>
			 Nouveau client
		</strong>
		<div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
				<div class="col-6"><input type="text" class="form-control" name="nom" value="" placeholder="Entrez le nom du client">
					@if ($errors->has('nom'))
					<span class="help-block">
						<strong style="color:#FF0000"; >{{ $errors->first('nom') }}</strong>
					</span>
					@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Prénom</div>
				<div class="col-6"><input type="text" class="form-control" name="prenom" value="" placeholder="Entrez le prénom du client">
					@if ($errors->has('prenom'))
					<span class="help-block">
						<strong style="color:#FF0000"; >{{ $errors->first('prenom') }}</strong>
					</span>
					@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Mail</div>
				<div class="col-6"><input type="email" class="form-control" name="mail" value="" placeholder="Entrez le mail du client">
					@if ($errors->has('mail'))
					<span class="help-block">
						<strong style="color:#FF0000"; >{{ $errors->first('mail') }}</strong>
					</span>
					@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Numero de téléphone</div>
				<div class="col-6"><input type="text" class="form-control" name="numero_telephone" value="" placeholder="Entrez le numero de téléphone du client">
				@if ($errors->has('numero_telephone'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('numero_telephone') }}</strong>
				</span>
				@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Adresse de livraison</div>
				<div class="col-6"><input type="text" class="form-control" name="adresse_de_livraison" value="" placeholder="Entrez le point de livrason">
				@if ($errors->has('adresse_de_livraison'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('adresse_de_livraison') }}</strong>
				</span>
				@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Mobile banking</div>
				<div class="col-6"><input type="text" class="form-control" name="mobile_banking" value="" placeholder="Entrez le mobile banking">
				@if ($errors->has('mobile_banking'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('mobile_banking') }}</strong>
				</span>
				@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Pseudo</div>
				<div class="col-6"><input type="text" class="form-control" name="pseudo" value="" placeholder="Entrez le pseudo pour le client">
				@if ($errors->has('pseudo'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('pseudo') }}</strong>
				</span>
				@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Mot de passe</div>
				<div class="col-6"><input id="password" type="password" class="form-control" name="password" required>
				@if ($errors->has('password'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('password') }}</strong>
				</span>
				@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Conformation du mot de passe</div>
				<div class="col-6"><input id="password-confirm" type="password" class="form-control" name="password_confirmation" required></div>
				
			</div>
			
			
		</div>
		
			
		</hr>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		
        <input type="submit" class="btn btn-primary" value="Mettre à jour">
      </div>
	 </form> 
    </div>
  </div>
</div>

<!-- modal loader -->
<div class="modal fade modal-custumed" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
       
      </div>
      <center><h4>Encours.</h4> <div class="loader"></div></center>
      <div class="modal-footer">
       
      </div>
	</div>
  </div>
</div>
@endif
@endsection
@section('custom_script')
<script type="text/Javascript">
	

	$(function () {
		
		 if($("#erreur_ajout").val() =='1'){
			 $("#ajouter_client").modal();
		 }
		$("#id_liste_receveur_table").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							  "aaSorting": [[0, 'asc']]
							});
		

	});
  
function supprimerClient(element) {
		var lien = $(element).attr('url_supprimer_client');
		console.log(lien);
		document.getElementById("supprimer_client_button").href =lien
		$("#supprimer_client").modal();
	}
 function visualiserDetail(element) {
		var id_consommateur = $(element).attr('row_id_consommateur');
		var lien = "{{ url('detail_consommateur') }}?id_consommateur="+id_consommateur ;
		console.log(lien);
		$("#loadMe").modal();
		$.ajax({
		   url : lien,
		   type : 'GET',
		  dataType : 'text',
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				var resp = JSON.parse(response);
				if(resp.achat.length==0){
					
					$("#loadMe").modal('toggle');
					$("#id_visualiserDetailVide").modal();
				}else{
					document.getElementById('id_achat').innerHTML = 'AR '+ formaterNombre(resp.achat[0].montant_total+'') ;
					document.getElementById('id_reduction').innerHTML = resp.best_reduction[0].reduc +" %";
					
					var div_prix = document.getElementById('id_moyenne_prix_par_produit');
					while (div_prix.firstChild) {
						div_prix.removeChild(div_prix.firstChild);
					}
					for(var i=0 ; i<resp.moyenne_prix_par_produit.length ; i++ ){
						var div_row   = document.createElement('div') ;
						div_row.className="row";
						div_row.style="margin-top: 4px;margin-bottom: 4px;"
						
						var div_col1   = document.createElement('div') ;
						div_col1.className="col-6";
						div_col1.style="padding-left:20px;font-weight: bolder;color: #646369;";
						div_col1.innerHTML = resp.moyenne_prix_par_produit[i].nom_produit ;
						div_row.appendChild(div_col1) ;
						
						var div_col2   = document.createElement('div') ;
						div_col2.className="col-6";
						div_col2.innerHTML = 'Ar ' + formaterNombre(resp.moyenne_prix_par_produit[i].prix_moyen+'') ;
						div_row.appendChild(div_col2) ;
						
						div_prix.appendChild(div_row) ;
					}
					var div_remise = document.getElementById('id_remise_par_produit');
					while (div_remise.firstChild) {
						div_remise.removeChild(div_remise.firstChild);
					}
					for(var i=0 ; i<resp.moyenne_reduction.length ; i++ ){
						var div_row   = document.createElement('div') ;
						div_row.className="row";
						div_row.style="margin-top: 4px;margin-bottom: 4px;"
						
						var div_col1   = document.createElement('div') ;
						div_col1.className="col-6";
						div_col1.style="padding-left:20px;font-weight: bolder;color: #646369;";
						div_col1.innerHTML = resp.moyenne_reduction[i].nom_produit ;
						div_row.appendChild(div_col1) ;
						
						var div_col2   = document.createElement('div') ;
						div_col2.className="col-6";
						div_col2.innerHTML = resp.moyenne_reduction[i].moyenne_remise +' %';
						div_row.appendChild(div_col2) ;
						
						div_remise.appendChild(div_row) ;
					}
					$("#loadMe").modal('toggle');
				$("#id_visualiserDetail").modal();
				}
				
				
			   
			   // console.log(response["achat"]);
			   
			 /*var div_canv2 = document.getElementById('id_chart2');
				while (div_canv2.firstChild) {
					div_canv2.removeChild(div_canv2.firstChild);
				}
				var can_vaox  = document.createElement('canvas') ;
				can_vaox.id="id_repas_mois_enfant";
				can_vaox.height="200";
				div_canv2.appendChild(can_vaox) ;*/
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				// console.log(statut);
				// console.log(erreur);
		   }
		});  
		
		
	} 
  function formaterNombre(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ' ' + '$2');
        }
        return x1 + x2;
    }
</script>
@endsection