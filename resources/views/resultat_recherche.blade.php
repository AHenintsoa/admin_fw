@extends('template/default')

@section('titre')
   Resultat de recherche
@endsection

@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif


<section class="content">
      <div class="container-fluid">
			<div class="row">
         
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link" href="">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									<div class="card-body">
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Resultat du recherche : '{{ $mot_cle }}'</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_resultat_table" class="table table-bordered table-hover">
												<thead>
												<tr><!--(nom de l’entité, groupe, type de l’entité, nom complet personne  de contact, mail, tel, adresse, ca type d’aliments, etc.)             -->
												  <th>Resultat</th>
												  <th>Type</th>
												  <th>Action</th>
												 
												</tr>
												</thead>
												<tbody>
												@for( $i= 0 ; $i < count( $data) ; $i++)
												<tr>
													<td>{{$data[$i]->nom }}</td>
													<td> {{ $data[$i]->client }}</td>
													<td>
														@if($data[$i]->client=='produit')<a href="{{ url('fiche_produit') }}?id_produit={{$data[$i]->id_consommateur}}" ><button class="bouton"><span class="fa  fa-eye"></span></button></a>
														@elseif($data[$i]->client=='client')	<a href="{{ url('fiche_consommateur') }}?id_consommateur={{$data[$i]->id_consommateur}}" ><button class="bouton"><span class="fa  fa-eye"></span></button></a>
														@elseif($data[$i]->client=='organisation')	<a href="{{ url('fiche_receveur') }}?id_organisation={{$data[$i]->id_consommateur}}" ><button class="bouton"><span class="fa  fa-eye"></span></button></a>
														@else <a href="{{ url('information_entreprise') }}?id_entreprise={{$data[$i]->id_consommateur}}" ><button class="bouton"><span class="fa  fa-eye"></span></button></a>
														@endif
													</td>
												</tr>
												@endfor
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										
									  </div>
									  <!-- ./card-body -->
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
					 
					
					<!-- /.tab-content -->
				  </div><!-- /.card-body -->
				</div>
				<!-- /.nav-tabs-custom -->
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection
@section('modals')


@endsection
@section('custom_script')
<script type="text/Javascript">
	
	$(function () {
	

		$("#id_resultat_table").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		

	});
  

  
  
</script>
@endsection