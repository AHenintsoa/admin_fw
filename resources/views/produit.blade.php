@extends('template/default')

@section('titre')
  Produits
@endsection

@section('contenu')
@if(Session::has('erreur_ajout'))
<input type="hidden" id="erreur_ajout" name="choix" value="{{ Session::get('erreur_ajout') }}">
@endif

 @if(Session::get('id_utilisateur_type')!='5')
<section class="content">
      <div class="container-fluid">
			<div class="row">
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link" href="{{ url('entreprise') }}">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link active" href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
							
								<div class="card card-primary">
									
									<div class="card-body">
										<div class="row">
											<div class="col-3"><a href="{{ url('page_ajouter_produit') }}"><button  class="btn btn-primary btn-block">Ajouter Produit </button></a></div>
											<div class="col-3"><a href="{{ url('exporter_produit') }}"><button  class="btn btn-primary btn-block">Export </button></a></div>
										</div>
										</br>
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Liste des produits</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_liste_produit_table" class="table table-bordered table-hover">
												<thead>
												<tr><!-- nom, prénom, operateurs, nr tel,            -->
													  <th>Nom du produit</th>
													  <th>Type</th>
														<th>Catégorie</th>
														<th>Action</th>
												</tr>
												</thead>
												<tbody>
												@foreach($produits as $produit )
												<tr>
													  <td>{{ $produit->nom_produit }}   </td>
													  <td>{{ $produit->type_produit->label }}</td>
													 <td>{{ $produit->type_aliment->label }}</td>
													 
													   <td> <a href="{{ url('update_produit_page') }}?id_produit={{$produit->id_produit}}" ><button  class="bouton"><span class="fa  fa-pencil"></span></button></a>
													  <a href="{{url('fiche_produit')}}?id_produit={{ $produit->id_produit }}"> <button  class="bouton"  ><span class="fa  fa-eye"></span></button></a>
														
													 
												 </tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Liste des produits en vente</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_liste_produit_vente_table" class="table table-bordered table-hover">
												<thead>
												<tr><!-- nom, prénom, operateurs, nr tel,            -->
													  <th>Nom du produit</th>
													  <th>Type</th>
														<th>Catégorie</th>
														<th>Action</th>
												</tr>
												</thead>
												<tbody>
												@foreach($produits_a_vendre as $produit )
												<tr>
													  <td>{{ $produit->nom_produit }}   </td>
													  <td>{{ $produit->type_produit->label }}</td>
													 <td>{{ $produit->type_aliment->label }}</td>
														<td> <a href="{{ url('update_produit_page') }}?id_produit={{$produit->id_produit}}" ><button  class="bouton"><span class="fa  fa-pencil"></span></button></a>
															 <a href="{{url('fiche_produit')}}?id_produit={{ $produit->id_produit }}"> <button  class="bouton"  ><span class="fa  fa-eye"></span></button></a>
															<a href="{{ url('archiver') }}?id_produit={{$produit->id_produit}}"  title="Archiver" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-minus"></i> Mettre en archive</span></a>
															
														</td> 
												 </tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Liste des produits en archive</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_liste_produit_archive_table" class="table table-bordered table-hover">
												<thead>
												<tr><!-- nom, prénom, operateurs, nr tel,            -->
													  <th>Nom du produit</th>
													  <th>Type</th>
														<th>Catégorie</th>
														<th>Action</th>
												</tr>
												</thead>
												<tbody>
												@foreach($produits_archive as $produit )
												<tr>
													  <td>{{ $produit->nom_produit }}   </td>
													  <td>{{ $produit->type_produit->label }}</td>
													 <td>{{ $produit->type_aliment->label }}</td>
														<td> <a href="{{ url('update_produit_page') }}?id_produit={{$produit->id_produit}}" ><button  class="bouton"><span class="fa  fa-pencil"></span></button></a>
														    <a href="{{url('fiche_produit')}}?id_produit={{ $produit->id_produit }}"> <button  class="bouton"  ><span class="fa  fa-eye"></span></button></a>
															<a href="{{ url('vendre') }}?id_produit={{$produit->id_produit}}"  title="Mettre en vente" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Mettre en vente</span></a>
															
														</td>
												 </tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										
									  </div>
									  <!-- ./card-body -->
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
					 
					
					<!-- /.tab-content -->
				  </div><!-- /.card-body -->
				</div>
				<!-- /.nav-tabs-custom -->
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection
@section('modals')
<!---- modal   supprimer      client  ----->
<div class="modal fade" id="supprimer_client" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Supprimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<a id="supprimer_client_button" href=""><button type="button" class="btn btn-secondary" >Sûr</button></a>
      </div>
	
	</div>
  </div>
</div>



<!-- modal detail client -->

<div class="modal fade" id="id_visualiserDetail" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail suplémentaire</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	 
					<center>
					 <h5>
						<i class="fa fa-address-card mr-1"></i>
						 Information globale
					</h5>
						<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
							<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Total Achat (MGA)</div>
							<div class="col-6"><p  id="id_achat"></p></div>
						</div>
						<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
							<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;">Réduction préférée</div>
							<div class="col-6" id="id_reduction"></div>
						</div>
						</hr>
						<h5>
						<i class="fa fa-address-card mr-1"></i>
						 Moyenne du prix par produit
						 </h5>
						 <div id="id_moyenne_prix_par_produit">
							
						</div>
						</hr>
						<h5>
						<i class="fa fa-address-card mr-1"></i>
						 Moyenne de la remise par produit
						 </h5>
						<div id="id_remise_par_produit">
							
						</div>
					
					</center>
					<hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
	 </form> 
    </div>
  </div>
</div>
<!-- modal no info -->
<div class="modal fade" id="id_visualiserDetailVide" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail suplémentaire</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	 
		<center>
		 <h5>
			<i class="fa fa-address-card mr-1"></i>
			 Information vide
		</h5>
			
		</center>
		<hr>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
	 </form> 
    </div>
  </div>
</div>


<!----modal   ajouter     receveur---->
<div class="modal fade" id="ajouter_client" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ajouter Client</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <form action="{{ url('ajouter_client') }}" method="POST">
		{{ csrf_field() }}
		<strong>
			<i class="fa fa-address-card mr-1"></i>
			 Nouveau client
		</strong>
		<div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
				<div class="col-6"><input type="text" class="form-control" name="nom" value="" placeholder="Entrez le nom du client">
					@if ($errors->has('nom'))
					<span class="help-block">
						<strong style="color:#FF0000"; >{{ $errors->first('nom') }}</strong>
					</span>
					@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Prénom</div>
				<div class="col-6"><input type="text" class="form-control" name="prenom" value="" placeholder="Entrez le prénom du client">
					@if ($errors->has('prenom'))
					<span class="help-block">
						<strong style="color:#FF0000"; >{{ $errors->first('prenom') }}</strong>
					</span>
					@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Mail</div>
				<div class="col-6"><input type="email" class="form-control" name="mail" value="" placeholder="Entrez le mail du client">
					@if ($errors->has('mail'))
					<span class="help-block">
						<strong style="color:#FF0000"; >{{ $errors->first('mail') }}</strong>
					</span>
					@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Numero de téléphone</div>
				<div class="col-6"><input type="text" class="form-control" name="numero_telephone" value="" placeholder="Entrez le numero de téléphone du client">
				@if ($errors->has('numero_telephone'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('numero_telephone') }}</strong>
				</span>
				@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Adresse de livraison</div>
				<div class="col-6"><input type="text" class="form-control" name="adresse_de_livraison" value="" placeholder="Entrez le point de livrason">
				@if ($errors->has('adresse_de_livraison'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('adresse_de_livraison') }}</strong>
				</span>
				@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Mobile banking</div>
				<div class="col-6"><input type="text" class="form-control" name="mobile_banking" value="" placeholder="Entrez le mobile banking">
				@if ($errors->has('mobile_banking'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('mobile_banking') }}</strong>
				</span>
				@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Pseudo</div>
				<div class="col-6"><input type="text" class="form-control" name="pseudo" value="" placeholder="Entrez le pseudo pour le client">
				@if ($errors->has('pseudo'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('pseudo') }}</strong>
				</span>
				@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Mot de passe</div>
				<div class="col-6"><input id="password" type="password" class="form-control" name="password" required>
				@if ($errors->has('password'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('password') }}</strong>
				</span>
				@endif
				</div>
				
			</div>
			<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
				<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Conformation du mot de passe</div>
				<div class="col-6"><input id="password-confirm" type="password" class="form-control" name="password_confirmation" required></div>
				
			</div>
			
			
		</div>
		
			
		</hr>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		
        <input type="submit" class="btn btn-primary" value="Mettre à jour">
      </div>
	 </form> 
    </div>
  </div>
</div>

<!-- modal loader -->
<div class="modal fade modal-custumed" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
       
      </div>
      <center><h4>Encours.</h4> <div class="loader"></div></center>
      <div class="modal-footer">
       
      </div>
	</div>
  </div>
</div>
 @endif
@endsection
@section('custom_script')
<script type="text/Javascript">
	

	$(function () {
		
		 if($("#erreur_ajout").val() =='1'){
			 $("#ajouter_client").modal({ backdrop: 'static', keyboard: false });
		 }
		$("#id_liste_produit_table").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							  "aaSorting": [[0, 'asc']]
							});
		$("#id_liste_produit_vente_table").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							  "aaSorting": [[0, 'asc']]
							});
		$("#id_liste_produit_archive_table").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							  "aaSorting": [[0, 'asc']]
							});
		

	});
  
function supprimerClient(element) {
		var lien = $(element).attr('url_supprimer_client');
		console.log(lien);
		document.getElementById("supprimer_client_button").href =lien
		$("#supprimer_client").modal({ backdrop: 'static', keyboard: false });
	}
 
  function formaterNombre(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ' ' + '$2');
        }
        return x1 + x2;
    }
</script>
@endsection