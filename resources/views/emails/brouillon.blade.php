

<div>
    @if(isset($defaut) && $defaut==0 )
    Bonjour {{ $nom_entreprise }} , vous n'avez pas encore rempli les champs suivant : "
	
	 @for($i= 0 ; $i < count($liste_info) ; $i++)    
		 {{$liste_info[$i] }}
		 @if($i+1== count($liste_info))
			  <?php break;   ?>
		 @endif
		 ,
	 @endfor
	 " lors du saisie du formulaire de don/vente . 
	 @elseif(isset($defaut) && $defaut==1)
	Bonjour {{ $nom_entreprise }} , vous n'avez pas encore validé votre don/vente ".
	
	 @for($i= 0 ; $i < count($liste_info) ; $i++)    
		 {{$liste_info[$i] }}
		 @if($i+1== count($liste_info))
			  <?php break;   ?>
		 @endif
		 ,
	 @endfor
	
	@else
		Bonjour {{ $nom_entreprise }} , vous n'avez pas encore rempli vos informations personelles afin de pouvoir sauver vos produits par donation ou  vente ".
	 @endif
</div>
<br />
Veuillez suivre le lien suivant pour compléter votre formulaire :
<a href="{{$link}}">ici </a> .

