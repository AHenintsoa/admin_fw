@extends('template/default')

@section('titre')
   Mis à jour
@endsection
@section('custom_styles')
    <link rel="stylesheet" href="{{ asset('dist/plugins/select2/select2.min.css') }}">
	<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.js'></script>
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.css' rel='stylesheet' />
@endsection

@section('contenu')

			<div class="row">
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link" href="{{ url('entreprise') }}">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link active" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									<div class="card-body">
										<!--<div class="row">-->
											<form action="{{ url('update_receveur') }}" method="post">
											{{ csrf_field() }}
											<input type="hidden" class="form-control" name="id_organisation" value="{{ $receveur->id_organisation }}" >
											<input type="hidden" class="form-control" name="id_adresse" value="{{ $adresse->id_adresse }}" >
											<input type="hidden" class="form-control" name="id_employe" value="{{ $receveur_employe->id_employe }}" >
											<!--<div class="col">-->
												<strong>
													<i class="fa fa-address-card mr-1"></i>
													 Information de l'organisation
												</strong>
												<div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
														<div class="col-6"><input type="text" class="form-control" name="nom_organisation" value="{{ $receveur->nom_organisation }}" ></div>
														@if ($errors->has('nom_organisation'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('nom_organisation') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Numero</div>
														<div class="col-6"><input type="text" class="form-control" name="numero" value="{{ $receveur->numero }}" ></div>
														@if ($errors->has('numero'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('numero') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Type de l'organisation</div>
														<div class="col-6">
															<select  class="form-control select2" id="" name="id_type_organisation"  data-placeholder="Choisir le type de votre entreprise*" style="width: 100%;" tabindex="-1" aria-hidden="true">
															@foreach($liste_type_receveur as $type)
																<option value="{{ $type->id_type_organisation }}" <?php if( $type->id_type_organisation == $receveur->type_organisation_id) echo "selected"?> > {{ $type->label }}</option>
															 @endforeach
															 </select>
														</div>
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Quartier</div>
														<div class="col-6"><input type="text" class="form-control" name="label_adresse" value="{{ $adresse->label }}" ></div>
														@if ($errors->has('label_adresse'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('label_adresse') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Coordonnées (Latitude / longitude)</div>
														<div class="col-3"><input type="text" class="form-control" id ="id_latitude" name="latitude_adresse" value="{{ $adresse->latitude }}" ></div>
														<div class="col-2"><input type="text" class="form-control" id ="id_longitude" name="longitude_adresse" value="{{ $adresse->longitude }}"  ></div>
														<div class="col-1"><a  class="btn btn-default btn-block" onclick="modifierAdresse(this)"  title="Sélectionner les coordonnées sur une carte" ><i class="fa fa-map"></i> </a></div>
														@if ($errors->has('id_latitude'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('id_latitude') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Code postal</div>
														<div class="col-6"><input type="text" class="form-control" name="code_postal_adresse" value="{{ $adresse->code_postal }}" ></div>
														@if ($errors->has('code_postal_adresse'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('code_postal_adresse') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Ville</div>
														<div class="col-6"><input type="text" class="form-control" name="ville_adresse" value="{{ $adresse->ville }}" ></div>
														@if ($errors->has('ville_adresse'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('ville_adresse') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Pays</div>
														<div class="col-6">
															<select  class="form-control select2" id="" name="id_pays"  data-placeholder="Choisir le type de votre entreprise*" style="width: 100%;" tabindex="-1" aria-hidden="true">
															@foreach($liste_pays as $pays)
																<option value="{{ $pays->id_pays }}" <?php if( $pays->id_pays == $adresse->pays_id) echo "selected"?> > {{ $pays->label }}</option>
															 @endforeach
															 </select>
														</div>
													</div>
												</div>
												</hr>
												<strong>
													<i class="fa fa-address-card mr-1"></i>
													 Information sur le responsable
												</strong>
												<div>
													
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom du responsable</div>
														<div class="col-6"><input type="text" class="form-control" name="nom_responsable" value="{{ $receveur_employe->nom }}" ></div>
														@if ($errors->has('nom_responsable'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('nom_responsable') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Prénom du responsable</div>
														<div class="col-6"><input type="text" class="form-control" name="prenom_responsable" value="{{ $receveur_employe->prenom }}" ></div>
														@if ($errors->has('prenom_responsable'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('prenom_responsable') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Email du responsable</div>
														<div class="col-6"><input type="text" class="form-control" name="email_responsable" value="{{ $receveur_employe->email }}" ></div>
														@if ($errors->has('email_responsable'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('email_responsable') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Téléphone du responsable</div>
														<div class="col-6"><input type="text" class="form-control" name="telephone_responsable" value="{{ $receveur_employe->telephone }}" ></div>
														@if ($errors->has('telephone_responsable'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('telephone_responsable') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Poste</div>
														<div class="col-1" ><a id="id_bouton_ajout_responsable" class="btn btn-default btn-block"  onclick="ajouterPoste(this )"><i class="fa fa-plus"></i></a></div>
														<div class="col-5">
															<select  class="form-control select2" id="id_id_poste" name="id_poste"  data-placeholder="Choisir le type de votre entreprise*" style="width: 100%;" tabindex="-1" aria-hidden="true">
															@foreach($liste_poste as $poste)
																<option value="{{ $poste->id_poste }}"  <?php if($poste->id_poste == $receveur_employe->poste_id) echo 'selected' ?> > {{ $poste->label }}</option>
															 @endforeach
															 </select>
														</div>
													</div>
												</div>
												</hr>
												<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-6"></div>
													<div class="col-2" style="padding-left:20px;font-weight: bolder;color: #646369;"><a href="{{ url('receveur') }}"><span class="fa fa-backward"></span></a></div>
													<div class="col-2"><input type="submit" class="btn btn-success" value="Mettre à jour" ></div>
												</div>
												
												
											 <!-- </div>-->
											  <!-- /.col -->
											  </form>
											<!--</div>-->
											<!-- /.row -->
												
										  </div>
										  <!-- ./card-body -->
									</div>
									<!-- /.card -->
								</div>
								<!-- /.tab-pane -->
							</div>
							<!-- /.tab-content -->
						</div>
						 <!-- ./card-body -->
					</div>
					<!-- /.card-->
			  </div>
          <!-- /.col -->
		 </div>
		 <!-- /.row -->
 @if(Session::get('id_utilisateur_type')!='5')	
@endsection
@section('modals')

<!-- modal map -->
<div class="modal fade" id="changer_coordonees" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Veuillez cliquer sur la carte</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-content">
        <center><div id='map' style='width: 400px; height: 300px; '></div></center>
		
      </div>
      
      <div class="modal-footer">
       
		
      </div>
	
	</div>
  </div>
</div>
<!-- modal loader -->
<div class="modal fade modal-custumed" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
       
      </div>
      <center><h4>Encours.</h4> <div class="loader"></div></center>
      <div class="modal-footer">
       
      </div>
	</div>
  </div>
</div>
<!----modal   ajouter   poste---->
<div class="modal fade" id="id_ajout_poste" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau poste </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		
		 <input type="hidden" value="{{ csrf_token() }}" id="id_token">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>

			<div class="col-sm-10">
			  <input name="po" type="text" class="form-control" id="id_nouveau_poste" >
			</div>
			
		  </div>
		 
      </div>
     <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" onclick="ajouter_poste_ajax()">Ajouter</button>
      </div>
    </div>
  </div>
</div>
@endif
@endsection
@section('custom_script')
<!-- Select2 -->
<script src="{{ asset('dist/plugins/select2/select2.full.min.js') }}"></script>
<script type="text/Javascript">
	$(function () {
		$('.select2').select2() ;// initialisation element select2
		
	});
	function ajouterPoste(element) {
	$("#id_ajout_poste").modal({
     backdrop: 'static',
     keyboard: false
 });
}	
  function ajouter_poste_ajax() {
		var label_poste = $('#id_nouveau_poste').val();
		 var token = $('#id_token').val();
		var x =new FormData();
		x.append('label' , label_poste);	
		x.append('_token' , token);	
		$("#loadMe").modal({
     backdrop: 'static',
     keyboard: false
 });
		$("#id_ajout_poste").modal('toggle');
		var lien = '{{ url('ajouter_poste_organisation')}}' ;
		$.ajax({
		   url : lien,
		    method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				// console.log(resp);
				var html = '<option value ="'+ response.id_poste+'"  selected>'+response.label+' </option>';
				var select = $("#id_id_poste");
				select.append(html);
				select.select2("val" , select.select2("val"));
				$("#loadMe").modal('toggle');
				
				
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				
		   }
		});  
		
		
	}
 function modifierAdresse(element) {
	  mapboxgl.accessToken = 'pk.eyJ1IjoibmphcnkiLCJhIjoiY2p1ZHU0NWYzMDJ1MTRkbW16N2pmczh6YiJ9.i3r9euc0dZsYchVvQfdG5A';
		$("#loadMe").modal({
     backdrop: 'static',
     keyboard: false
 });
		var map = new mapboxgl.Map({
		container: 'map',
		country : "mg",
		style: 'mapbox://styles/mapbox/streets-v9'
		});
		
		map.on('click', function (e) {
		//document.getElementById('info').innerHTML =
		// e.point is the x, y coordinates of the mousemove event relative
		// to the top-left corner of the map
		
		// var x = JSON.stringify(e.lngLat) ;
		var x = e.lngLat ;
		console.log(x) ;
		
		var id_adresse = $(element).attr('row_adresse');
		$('#id_longitude').val( x.lng );
		$('#id_latitude').val( x.lat );
		$("#changer_coordonees").modal('toggle');
		
		});
		map.on('load', function () {
			
			// *** Add zoom and rotation controls to the map ...
			map.addControl(new mapboxgl.NavigationControl());

			$("#loadMe").modal('toggle');
			$("#changer_coordonees").modal({
     backdrop: 'static',
     keyboard: false
 });

		});
		/* var el = document.createElement('div');
        el.className = 'marker';
        el.style.backgroundImage = '{{ asset("img/marker_fw.png") }}';
        el.style.width = '35px';
        el.style.height = '35px';
        el.style.backgroundRepeat = 'no-repeat';
        el.style.backgroundSize = 'contain';

        //console.log( el.style.backgroundImage );

        var marker = new mapboxgl.Marker(el);
		new mapboxgl.Marker(el)
				.setLngLat(34, 13)
				.addTo(map);*/
		
		// var lien = $(element).attr('url_supprimer_entreprise');
		// console.log(lien);
		// document.getElementById("supprimer_entreprise_button").href =lien+"&return=0"
		
		$("#changer_coordonees").modal({
     backdrop: 'static',
     keyboard: false
 });
	}
  
</script>
@endsection

