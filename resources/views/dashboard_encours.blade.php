@extends('template/default')

@section('titre')
    Dashboard
@endsection
@section('custom_styles')
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
	<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.js'></script>
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.css' rel='stylesheet' />
@endsection

@section('contenu')
@if(Session::has('succes'))
<input type="hidden" id="etat_email" name="choix" value="{{ Session::get('succes') }}">
@endif


<section class="content">
      <div class="container-fluid">
			<div class="row">
         
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_page') }}">Général</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_entreprise') }}" >Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_receveur') }}" >Organisation</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_benevole') }}" >Bénévole</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_client') }}" >Consommateur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_commande') }}" >Commande</a></li>	
							<li class="nav-item"><a class="nav-link  active" href="" >Activité encours</a></li>
						</ul>
						<button id="download" onclick="getImage()" class="bouton-sticky" title="Exporter en pdf"><img src="{{ asset('/img/Downloads-icon.png') }}" class="exportImage" alt="exporter"></button>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									<div id="capture" >
									<div class="card-body">
										
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Donation en cours</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_donation" class="table table-bordered table-hover">
												<thead>
												<tr>
												 <th>Date</th>
													<th>Donateur</th>
												
												  <th>Receveur</th>
												  <th>Nom du produit</th>
												 <th>Quantité</th>
												 <th>Poids</th>
												 <th>Etat</th>
												  <th>Action</th>
												 
												</tr>
												</thead>
												<tbody>
												
												@foreach($donation_encours as $donation)
												<tr>
												  <td>{{$donation->date }}</td>
												  <td>{{$donation->nom_entreprise }}</td>
												  <td>{{$donation->nom_organisation }}</td>
												  <td>{{$donation->nom_produit }}</td>
												  <td> {{ number_format( (float)$donation->quantite , 0 , "," , " " ) }} </td>
												 <td> {{ number_format( (float)$donation->poids , 0 , "," , " " ) }} kg</td>
												 <td id="id_etat_donation{{ $donation->id_donation }}"> {{$donation->label }}</td>
												<td><button row_don_donation_id="{{ $donation->id_donation }}" onclick="visualiserDon(this)" class="bouton"><span class="fa  fa-eye"></span></button>
													<button row_don_donation_id="{{ $donation->id_donation }}"  row_id_etat_actuel="{{ $donation->etat_donation_id }}" onclick="modal_changer_etat(this)" class="bouton"><span class="fa  fa-cog"></span></button>
												</td>
												</tr>
												
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Vente en cours</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_vente" class="table table-bordered table-hover">
												<thead>
												<tr>
												  <th>Date</th>
												  <th>Consommateur</th>
												  <th>Quantité des produits</th>
												  <th>Poids</th>
												  <th>Somme du montant</th>
												 <th>Etat</th>
												 <th>Action</th>
												 
												</tr>
												</thead>
												<tbody>
												@foreach($vente_encours as $vente)
												<tr>
												  <td>{{$vente->date  }}</td>
												  <td>{{$vente->nom." ".$vente->prenom  }}</td>
												   <td> {{ number_format( (float)$vente->quantite , 0 , "," , " " ) }} </td>
												   <td> {{ number_format( (float)$vente->poids , 1, "," , " " ) }}  kg</td>
												 <td>Ar {{ number_format( (float)$vente->montant , 0 , "," , " " ) }} </td>
												 <td>{{$vente->label }}</td>
												<td><button row_vente_donation_id="{{ $vente->id_commande }}" onclick="visualiserVente(this)" class="bouton"><span class="fa  fa-eye"></span></button></td>
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Stratégie marketing en cours</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_strategie" class="table table-bordered table-hover">
												<thead>
												<tr>
												<th>Entreprise</th>
												  <th>Objectif</th>
												  <th>Montant objectif</th>
												 <th>Date échéance</th>
												 
												</tr>
												</thead>
												<tbody>
												@foreach($strategies_entreprise_encours as $strategie)
												<tr>
												  <td>{{$strategie->nom_entreprise }}</td>
												  <td>{{$strategie->objectif  }}</td>
												 <td>Ar {{ number_format( (float)$strategie->montant_objectif , 0 , "," , " " ) }} </td>
												 <td>{{$strategie->date_echeance }}</td>
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Les problèmes des entreprises</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_brouillon" class="table table-bordered table-hover">
												<thead>
												<tr>
													<th>Entreprise</th>
													<th>Défaut</th>
													<th>Detail</th>
												  
												 
												</tr>
												</thead>
												<tbody>
												<?php  $indice_defaut = 0 ; ?>
												@foreach($donation_brouillon as $brouillon)
												<tr>
												  <td>{{$brouillon->nom_entreprise }}</td>
												  <td>{{$type_probleme_brouillon[$indice_defaut]  }}</td>
												  @if($type_probleme_brouillon[$indice_defaut] == 'information(s) manquante(s)' )
													<td>	
														<button row_donation_id="{{ $brouillon->id_donation }}" onclick="visualiserBrouillon(this)" class="bouton"><span class="fa  fa-eye"></span></button>
														<button row_donation_id_email="{{ $brouillon->id_donation }}" row_defaut="0" onclick="sendEmail(this)" class="bouton"><span class="fa  fa-envelope"></span></button>
													</td>
													@else
													<td>
														<button row_donation_id_email="{{ $brouillon->id_donation }}" row_defaut="1" onclick="sendEmail(this)" class="bouton"><span class="fa  fa-envelope"></span></button>
													</td>	
													@endif
												</tr>
												<?php $indice_defaut++?>
												@endforeach
												@for( $i=0 ; $i<count($liste_profil['profil_incomplet']) ;$i++)
												<tr>
													<td>{{ $liste_profil['profil_incomplet'][$i]->nom_entreprise }}</td>
													<td>Profil non-initialisé</td>
													<td><button  onclick="visualiserProfilManquant(this , {{ json_encode($liste_profil['champs_manquant'][$i]) }} )" class="bouton"><span class="fa  fa-eye"></span></button>
														<button  value_email="{{ $liste_profil['profil_incomplet'][$i]->email }}" value_entreprise="{{ $liste_profil['profil_incomplet'][$i]->id_entreprise }}" onclick="confirmationEmailProfil(this)" class="bouton"><span class="fa  fa-envelope"></span></button>
													</td>
												</tr>
												@endfor
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										</div>
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										
									  </div>
									  <!-- ./card-body -->

								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
					 
					
					<!-- /.tab-content -->
				  </div><!-- /.card-body -->
				</div>
				<!-- /.nav-tabs-custom -->
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection

@section('modals')
<!-- Modal -->
<!----modal   changer   etat donation ---->
<div class="modal fade" id="id_changer_etat_donation" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Changer l'état de la donation </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		
		 <input type="hidden" value="{{ csrf_token() }}" id="id_token">
		<input type="hidden" value="" id="id_donation_name" >
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Etat</label>

			<div class="col-sm-10" id="id_div_selection_etat">
			  
			</div>
			
		  </div>
		 
      </div>
     <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" onclick="changer_etat_donation_ajax()">Valider</button>
      </div>
    </div>
  </div>
</div>


<!----modal   visualiser  info   profil ---->
<div class="modal fade" id="id_visualiserProfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Information(s) manquante(s) du profil</h5>
		
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  
		<div id="detail_profil" >  </div>
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
	 </form> 
    </div>
  </div>
</div>

<!----modal   visualiser    detail   brouillon---->
<div class="modal fade" id="id_visualiserBrouillon" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Information(s) manquante(s) du brouillon</h5>
		
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  
		<div id="detail_brouillon" >  </div>
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
	 </form> 
    </div>
  </div>
</div>

<!---- confirmation d'envoie de      mail  ----->
<div class="modal fade modal-custumed" id="envoie_mail" style=" background: none;"  tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Veuillez confirmer l'envoie</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<a id="envoyer_email" href=""><button type="button" class="btn btn-secondary" onclick="loading_envoie_email()">Confirmer</button></a>
      </div>
	</div>
  </div>
</div>
<!---- confirmation d'envoie de      mail  pour  profil ----->
<div class="modal fade modal-custumed" id="envoie_mail_profil" style=" background: none;"  tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Veuillez confirmer l'envoie</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<button type="button" class="btn btn-secondary" id="envoyer_email_button" onclick="sendEmailProfil(this)">Confirmer</button>
      </div>
	</div>
  </div>
</div>

<div class="modal fade modal-custumed" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
       
      </div>
      <center><h4>Encours.</h4> <div class="loader"></div></center>
      <div class="modal-footer">
       
      </div>
	</div>
  </div>
</div>
<!---- succes d'envoie de      mail  ----->
<div class="modal fade modal-custumed" id="succes_envoie_mail" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Envoie</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <center><p id="resultat_email"></p></center>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
      </div>
	</div>
  </div>
</div>

<!-- visualiser detail  don-->
<div id="id_visualiserDon" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail du don</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <div class="row">
		<div class="col" id="id_col1_don">
		
		</div>
		<div class="col" id="id_col2_don" style="margin-left : 10px;">
			
		</div>
	  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
	 </form> 
    </div>
  </div>
</div>
<!-- visualiser detail  vente-->
<div id="id_visualiserVente" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail de l'achat</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <div class="row">
			  <div class="col-md-12">
				 <div class="card">
				
				<!-- /.card-header -->
				<div class="card-body table-custumed">
				  <table id="id_detail_vente_table" class="table table-bordered table-hover">
					<thead>
					<tr>
						<th>Produit</th>
						<th>Quantité</th>
						<th>Prix unitaire (Ar)</th>
						<th>Total (Ar)</th>
						<th>Volume (m<sup>3</sup>)</th>
						<th>Description</th>
					  <th>Type du produit</th>
					  <th>Type aliment</th>
					  
					</tr>
					</thead>
					<tbody id="id_t_body_vente">
					
					</tbody>
					<tfoot>
					
					</tfoot>
				  </table>
				</div>
				<!-- /.card-body -->
			  </div>
			  <!-- ./card -->
			  </div>
			  <!-- /.col -->
			</div>
			<!-- /.row -->
										
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
	 </form> 
    </div>
  </div>
</div>

@endsection
@section('custom_script')
<script src="{{ asset('dist/plugins/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/html2canvaspdf/js/html2canvas.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/html2canvaspdf/js/jsPdf.debug.js') }}"></script>
<script type="text/Javascript">
	
	$(function () {
		$('.select2').select2() ;// initialisation element select2
		
		var etat_email = $("#etat_email").val();
		console.log(etat_email);
		if(etat_email==1){
			document.getElementById("resultat_email").innerHTML = "Succes" ;
			$("#succes_envoie_mail").modal();
		}
		if(etat_email==-1){
			document.getElementById("resultat_email").innerHTML = "Erreur de connexion" ;
			$("#succes_envoie_mail").modal();
		}
		
		$("#id_donation").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		$("#id_vente").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		$("#id_strategie").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		$("#id_brouillon").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		$("#id_detail_vente_table").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									"decimal": " ",
									"thousands": ".",
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		


	});
	function changer_etat_donation_ajax() {
	 var id_donation = $('#id_donation_name').val();
	 var id_etat_donation = $('#id_etat_donation').val();
	 var token = $('#id_token').val();
	 var x =new FormData();
	x.append('id_donation' , id_donation);	
	x.append('id_etat_donation' , id_etat_donation);	
	x.append('_token' , token);	
	
		$("#loadMe").modal();
		var lien = "{{ url('update_etat_donation')}}";
		console.log(lien);
		$.ajax({
		   url : lien,
		   method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		 
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				console.log(response);
				if(response.etat_notification_entreprise == 1 && response.etat_notification_organisation == 1){
					/**suppression contenu selection**/
					var td= document.getElementById('id_etat_donation'+id_donation);
					// remove detail 
					while (td.firstChild) {
					  td.removeChild(td.firstChild);
					} 
					/**fin suppression */
					var html = response.etat_donation_label;
			
					$("#id_etat_donation"+id_donation).append(html);
				}
				$("#loadMe").modal('toggle');
				$("#id_changer_etat_donation").modal('toggle');
				if(response.etat_notification_entreprise==1 &&  response.etat_notification_organisation==1){
					toastr.success('Notification des concérnés réussie','Notification', {timeout:5000});
				}else{
					toastr.error('Echec d\'envoie de notification aux concérnés ','Notification', {timeout:5000});
				}
		   },

		   error : function(error){
				console.log(error);
				toastr.error('Erreur de connexion .Echec d\'envoie de notification aux concérnés ','Notification', {timeout:5000});
		   }
		});  
		
		
	} 
	function modal_changer_etat(element){
		var id_donation = $(element).attr('row_don_donation_id');
		$("#id_donation_name").val(id_donation);
		
		/**suppression contenu selection**/
		var div_col1= document.getElementById('id_div_selection_etat');
		// remove detail 
		while (div_col1.firstChild) {
		  div_col1.removeChild(div_col1.firstChild);
		} 
		/**fin suppression */
		
		// var etat_actuel = parseInt( $(element).attr('row_id_etat_actuel') );
		var etat_actuel = $(element).attr('row_id_etat_actuel') ;
		var selected = 'selected';
		// console.log(typeof(parseInt( etat_actuel)));
		// console.log(selected);
		var liste_etat_donation = JSON.parse('{!! $liste_etat_donation !!}');
		
		console.log("liste_etat_donation",liste_etat_donation);
		
		var options = liste_etat_donation.map(function(etat){return '<option value="'+etat.id_etat_donation+'" '+((etat.id_etat_donation==etat_actuel)?'selected':'')+'>'+etat.label+'</option>'}).join("");
		var html =
				'<select  class="form-control select2" id="id_etat_donation" name="id_poste"  data-placeholder="Choisir le type de votre entreprise*" style="width: 100%;" tabindex="-1" aria-hidden="true">'
				+options+'</select>' ;
				 // console.log(html);
		
		
		
				 
		$("#id_div_selection_etat").append(html);
		$("#id_changer_etat_donation").modal();
		
	}
	function visualiserDon(element) {
		$("#loadMe").modal();
         var div_col1= document.getElementById('id_col1_don');
		// remove detail 
		while (div_col1.firstChild) {
		  div_col1.removeChild(div_col1.firstChild);
		}   
		var div_col2= document.getElementById('id_col2_don');
		// remove detail 
		while (div_col2.firstChild) {
		  div_col2.removeChild(div_col2.firstChild);
		}           
		var criteres = $(element).attr('row_don_donation_id');
		console.log(criteres);
		getDetail_don(criteres,function(response){
			// var response = resp ;
			 console.log(response);
			 
			 var br = document.createElement('br');
			 
			 var volume_don= document.createElement("p");
			 volume_don.innerHTML = 'Volume total :' +response[0].volume+' m<sup>3</sup>';
			 div_col1.appendChild(volume_don);
			 div_col1.appendChild(br);
			 var type_aliment= document.createElement("p");
			 type_aliment.innerHTML = ' Type d \'aliment : ' +response[0].type_aliment;
			 div_col1.appendChild(type_aliment);
			 div_col1.appendChild(br);
			 var type_produit= document.createElement("p");
			 type_produit.innerHTML = 'Type du produit : ' +response[0].type_produit;
			 div_col1.appendChild(type_produit);
			 div_col1.appendChild(br);
			 
			 var description= document.createElement("p");
			 description.innerHTML = 'Description du produit :' +response[0].description ;
			 div_col2.appendChild(description);
			 div_col2.appendChild(br);
			 var raison= document.createElement("p");
			 raison.innerHTML = 'Raison du surplus : ';
			 div_col2.appendChild(raison);
			 if(response[0].raison_surplus!=null){
				 var ulElement = document.createElement('ul');
				div_col2.appendChild(ulElement);
				 for(var i = 0 ; i< response.length ; i++){
					 var liElement = document.createElement('li');
					 liElement.innerHTML = response[i].raison_surplus;
					 ulElement.appendChild(liElement);
				 }
			 }else{
				 raison.innerHTML += ' aucun' ; 
			 }
			$('#loadMe').modal('toggle');
			$("#id_visualiserDon").modal();
		})
	}
	function getDetail_don(critere ,callBack) {
		 $.ajax({
		   url : 'visualiser_detail_don_encours?id_donation='+critere,
		   type : 'GET',
		   dataType : 'json',
		   success : function(response, statut){ // success est toujours en place, bien sûr !
			   callBack(response);
			   //console.log(response);
		   },

		   error : function(resultat, statut, erreur){

		   }
		});   
	}
	function visualiserVente(element) {
		$("#loadMe").modal({
			//	backdrop: false
			 backdrop: "static", //remove ability to close modal with click
			  keyboard: false, //remove option to close with keyboard
			  show: true //Display loader!
			});
         
		var tbody= document.getElementById('id_t_body_vente');
		// remove detail 
		while (tbody.firstChild) {
		  tbody.removeChild(tbody.firstChild);
		}           
		var criteres = $(element).attr('row_vente_donation_id');
		console.log(criteres);
		getDetail_vente(criteres,function(response){
			// var response = resp ;
			 console.log(response);
			 for(var i = 0 ; i<response.length ; i++){
				var trElement = document.createElement('tr');
				var td1 = document.createElement('td');
				td1.innerHTML = response[i].nom_produit ;
				var td2 = document.createElement('td');
				td2.innerHTML = response[i].qte ;
				var td3 = document.createElement('td');
				td3.innerHTML = response[i].prix_unitaire ;
				var td4 = document.createElement('td');
				td4.innerHTML = response[i].montant ;
				var td5 = document.createElement('td');
				td5.innerHTML = response[i].volume ;
				var td6 = document.createElement('td');
				td6.innerHTML = response[i].description ;
				var td7 = document.createElement('td');
				td7.innerHTML = response[i].type_produit ;
				var td8 = document.createElement('td');
				td8.innerHTML = response[i].type_aliment ;
				trElement.appendChild(td1);
				trElement.appendChild(td2);
				trElement.appendChild(td3);
				trElement.appendChild(td4);
				trElement.appendChild(td5);
				trElement.appendChild(td6);
				trElement.appendChild(td7);
				trElement.appendChild(td8);
				tbody.appendChild(trElement);
			 }
			 $('#loadMe').modal('toggle');
			$("#id_visualiserVente").modal();
		})
	}
	function getDetail_vente(critere ,callBack) {
		 $.ajax({
		   url : 'visualiser_detail_vente_encours?id_commande='+critere,
		   type : 'GET',
		   dataType : 'json',
		   success : function(response, statut){ // success est toujours en place, bien sûr !
			   callBack(response);
			   // console.log(response);
		   },

		   error : function(resultat, statut, erreur){

		   }
		});   
	}
	function sendEmail(element) {
		var lien = $(element).attr('row_donation_id_email');
		var defaut = $(element).attr('row_defaut');
		// defaut = 0 manquant ; 1 non validé
		if(defaut==0) {
			document.getElementById("envoyer_email").href ="{{url('/send/email')}}?id_donation="+lien+"&defaut="+defaut;
		}else{
			document.getElementById("envoyer_email").href ="{{url('/send/email')}}?id_donation="+lien+"&defaut="+defaut;
		}
		$("#envoie_mail").modal();
	}
	function confirmationEmailProfil(element) {
		var email = $(element).attr('value_email');
		var entreprise = $(element).attr('value_entreprise');
		// defaut = 0 manquant ; 1 non validé
		
			var but= document.getElementById("envoyer_email_button");
			$('#envoyer_email_button').attr('contact', email);
			$('#envoyer_email_button').attr('nom_entreprise', entreprise);
		$("#envoie_mail_profil").modal();
	}
	function sendEmailProfil(element ) {
		console.log('eto');
		var email =  $(element).attr('contact');
		var entreprise=  $(element).attr('nom_entreprise');
		console.log('http://localhost/admin_fw/public/send/email?email='+email+'&entreprise='+entreprise);
		$.ajax({
		   // url : 'http://localhost/admin_fw/public/send/email?email='+email+'&entreprise='+entreprise,
		   url : "{{ url('email') }} ?email="+email+"&entreprise="+entreprise,
		   type : 'GET',
		  dataType : 'text',
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				console.log(response);
			   if(response=='ok'){
				  $("#loadMe").modal('toggle');
				  $("#envoie_mail_profil").modal('toggle');
				document.getElementById("resultat_email").innerHTML = "Envoie de l'email succes" ;
				$("#succes_envoie_mail").modal();
			  }else{
				document.getElementById("resultat_email").innerHTML = "Erreur de connexion" ;
				$("#succes_envoie_mail").modal();
			}
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				$("#loadMe").modal('toggle');
				 $("#envoie_mail_profil").modal('toggle');
				document.getElementById("resultat_email").innerHTML = "Echec de connexion" ;
				$("#succes_envoie_mail").modal();
				// console.log(statut);
				// console.log(erreur);
		   }
		});  
		$("#loadMe").modal();
	}
	function loading_envoie_email() {
		 $('#envoie_mail').modal('toggle');
		$("#loadMe").modal({
			  backdrop: "static", //remove ability to close modal with click
			  keyboard: false, //remove option to close with keyboard
			  show: true //Display loader!
			});
			
	}
	function visualiserBrouillon(element) {
		$("#loadMe").modal({
			//	backdrop: false
			 backdrop: "static", //remove ability to close modal with click
			  keyboard: false, //remove option to close with keyboard
			  show: true //Display loader!
			});
         var div = document.getElementById('detail_brouillon');
		//remove detail 
		while (div.firstChild) {
		  div.removeChild(div.firstChild);
		}           
		var criteres = $(element).attr('row_donation_id');
		console.log(criteres);
		getDetail_brouillon(criteres,function(resp){
			var response = resp ;
			 $('#loadMe').modal('toggle');
			var liste_details= document.getElementById("detail_brouillon");
			var ulElement = document.createElement('ul') ;
			var liElement;
			
			for( var x= 0 ; x < response.length ; x++){
				liElement = document.createElement('li') ; 
				liElement.innerHTML = response[x] ;
				ulElement.appendChild(liElement) ; 
			}
			liste_details.appendChild(ulElement);
			$("#id_visualiserBrouillon").modal();
		})
	}
	function visualiserProfilManquant(element , infos) {
		
			// console.log(infos);
			// var liste_info= json_decode(infos);
			for(var i = 0 ; i< infos.length ; i++){
				console.log(infos[i]);
			}
         var div = document.getElementById('detail_profil');
		//remove detail 
		while (div.firstChild) {
		  div.removeChild(div.firstChild);
		}           
			 // $('#loadMe').modal('toggle');
			var liste_details= document.getElementById("detail_profil");
			var ulElement = document.createElement('ul') ;
			var liElement;
			
			for( var x= 0 ; x < infos.length ; x++){
				liElement = document.createElement('li') ; 
				liElement.innerHTML = infos[x] ;
				ulElement.appendChild(liElement) ; 
			}
			liste_details.appendChild(ulElement);
			$("#id_visualiserProfil").modal();
		
	}
  
	 function getDetail_brouillon(critere ,callBack) {
                  
		var xMLHttprequest = new XMLHttpRequest() ;
		xMLHttprequest.open('GET',"{{ url('visualiser_detail_brouillon?id_donation=')}}"+critere)	;
		xMLHttprequest.addEventListener('readystatechange',function(){
			if(xMLHttprequest.readyState==XMLHttpRequest.DONE && xMLHttprequest.status==200) {
				var resp = JSON.parse(xMLHttprequest.responseText) ; 
				console.log("etoxxxxx");
				callBack(resp) ; 
			}else{
				console.log('request failed');
			}
		})
		xMLHttprequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xMLHttprequest.send() ;

	}
	
  function getImage(){
	 
          html2canvas(document.getElementById('capture'), {
              onrendered:function(canvas) {

                  var contentWidth = canvas.width;
                  var contentHeight = canvas.height;

                  // Une page pdf indique la hauteur de la toile générée par la page html;
                  var pageHeight = contentWidth / 595.28 * 841.89;
                  // la hauteur de la page HTML n'est pas générée pdf
                  var leftHeight = contentHeight;
                  // offset de page pdf
                  var position = 0;
                 // a4 format de papier [595.28, 841.89], toile générée par page HTML en largeur et hauteur de l'image pdf
                  var imgWidth = 555.28;
                  var imgHeight = 555.28/contentWidth * contentHeight;

                  var pageData = canvas.toDataURL('image/jpeg', 1.0);

                  var pdf = new jsPDF('', 'pt', 'a4');
                 // Il y a deux hauteurs à distinguer, l'une est la hauteur réelle de la page html et la hauteur de la page qui a généré le pdf (841.89)
                  // Lorsque le contenu ne dépasse pas la plage affichée sur la page pdf, aucune pagination n'est requise.
                  if (leftHeight < pageHeight) {
                      pdf.addImage(pageData, 'JPEG', 20, 0, imgWidth, imgHeight );
                  } else {
                      while(leftHeight > 0) {
                          pdf.addImage(pageData, 'JPEG', 20, position, imgWidth, imgHeight)
                          leftHeight -= pageHeight;
                          position -= 841.89;
                          // Éviter d'ajouter des pages vierges
                          if(leftHeight > 0) {
                              pdf.addPage();
                          }
                      }
                  }

                  pdf.save('content.pdf');
              }
          })
      
}
  
</script>
@endsection