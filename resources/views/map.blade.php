@extends('template/default')

@section('titre')
   Map
@endsection
@section('custom_styles')
    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.js'></script>
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.css' rel='stylesheet' />
@endsection

@section('contenu')
<section class="content">
<div id='map' style='width: 400px; height: 300px;'></div>
<pre id='info'></pre>
<div class="loader" id="load_map"> </div>
</section>
    <!-- /.content -->
@endsection
@section('modals')


@endsection
@section('custom_script')
<script>
$(function () {
		
		mapboxgl.accessToken = 'pk.eyJ1IjoibmphcnkiLCJhIjoiY2p1ZHU0NWYzMDJ1MTRkbW16N2pmczh6YiJ9.i3r9euc0dZsYchVvQfdG5A';
		var map = new mapboxgl.Map({
		container: 'map',
		country : "mg",
		style: 'mapbox://styles/mapbox/streets-v9'
		});
		
		map.on('click', function (e) {
		document.getElementById('info').innerHTML =
		// e.point is the x, y coordinates of the mousemove event relative
		// to the top-left corner of the map
		JSON.stringify(e.point) + '<br />' +
		// e.lngLat is the longitude, latitude geographical position of the event
		JSON.stringify(e.lngLat);
		});
		 var el = document.createElement('div');
        el.className = 'marker';
        el.style.backgroundImage = '{{ asset("img/marker_fw.png") }}';
        el.style.width = '35px';
        el.style.height = '35px';
        el.style.backgroundRepeat = 'no-repeat';
        el.style.backgroundSize = 'contain';

        //console.log( el.style.backgroundImage );

        var marker = new mapboxgl.Marker(el);
		new mapboxgl.Marker(el)
				.setLngLat(34, 13)
				.addTo(map);
		
		
		
	});
	

</script>
@endsection
