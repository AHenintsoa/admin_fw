<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('titre')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">


	  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('dist/css/adminlte.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ url('plugins/iCheck/square/blue.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @yield('custom_styles')

    <style type="text/css">
      .content-wrapper {
          background: #fff !important;
      }

      .alert-info, .bg-info, .label-info {
        background-color: #98A7A7 !important;
      }
	  

    </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#">Food<b>Wise</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Veuillez vous identifier</p>
		@if (Session::has('message'))
		   <div class="alert alert-info">{{ Session::get('message') }}</div>
		@endif
		@if (Session::has('login_erreur'))
		   <div class="alert alert-info">{{ Session::get('login_erreur') }}</div>
		@endif

      <form action="{{ url('connect') }}" method="POST">
		{{ csrf_field() }}
		<div class="form-group has-feedback">
          <input name="pseudo" type="text" class="form-control {{ $errors->has('pseudo') ? 'is-invalid' : '' }}" placeholder="Nom de votre entreprise"  value="{{ old('pseudo') }}">
          <span class="fa fa-envelope form-control-feedback"></span>
		  {!! $errors->first('pseudo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group has-feedback">
          <input name="password" type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Mot de passe"  value="{{ old('password') }}">
          <span class="fa fa-lock form-control-feedback"></span>
		  {!! $errors->first('password', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="row" style="margin-bottom: 10px;">
          <!-- /.col -->
          <div class="col-6 offset-3">
            <button type="submit" class="btn btn-primary btn-block btn-flat" name="valider_login">S'identifier</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
    <script src="{{ url('dist/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
    <script src="{{ url('dist/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ url('dist/plugins/iCheck/icheck.min.js') }}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  })
</script>
</body>
</html>
