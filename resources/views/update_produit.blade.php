@extends('template/default')

@section('titre')
    Modifier produit
@endsection
@section('custom_styles')
    <link rel="stylesheet" href="{{ asset('dist/plugins/select2/select2.min.css') }}">
@endsection
@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif

 @if(Session::get('id_utilisateur_type')!='5')
<section class="content">
      <div class="container-fluid">
			<div class="row">
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link " href="{{ url('entreprise') }}">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link active" href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									
									<div class="card-body">
										  <div class="row">
													<div class="col">
														<strong>
															<i class="fa fa-address-card mr-1"></i>
															 Information sur le produit
														</strong>
														<form action="{{ url('update_produit') }}" method="post" enctype="multipart/form-data">
														<input type="hidden" class="form-control" name="id_produit" value="{{ $produit->id_produit }}" >
														{{ csrf_field() }}
														<div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
																<div class="col-8"><input type="text" class="form-control" name="nom_produit" value="{{ $produit->nom_produit }}" >
																@if ($errors->has('nom_produit'))
																	<span class="help-block">
																		<strong style="color:#FF0000"; >{{ $errors->first('nom_produit') }}</strong>
																	</span>
																@endif
																</div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Description du produit</div>
																<div class="col-8"><input type="text" class="form-control" name="description" value="{{ $produit->description }}" ></div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Poids du produit</div>
																<div class="col-8"><input type="text" class="form-control" name="poids" value="{{ $produit->poids }}" >
																@if ($errors->has('poids'))
																	<span class="help-block">
																		<strong style="color:#FF0000"; >{{ $errors->first('poids') }}</strong>
																	</span>
																@endif
																</div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Volume du produit</div>
																<div class="col-8"><input type="text" class="form-control" name="volume" value="{{ $produit->volume }}" >
																@if ($errors->has('volume'))
																	<span class="help-block">
																		<strong style="color:#FF0000"; >{{ $errors->first('volume') }}</strong>
																	</span>
																@endif
																</div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Type du produit</div>
																<div class="col-8">
																	<select  class="form-control select2"  id="id_id_type_produit"  name="id_type_produit"  data-placeholder="Choisir le type du produit" style="width: 100%;" tabindex="-1" aria-hidden="true" id="id_select_type_entreprise">
																		<option></option>
																		<?php foreach($type_produit as $type): ?>
																		<option value="<?php echo $type->id_type_produit; ?>" <?php if($produit->type_produit_id == $type->id_type_produit) echo "selected"; ?>><?php echo $type->label; ?></option>
																		<?php endforeach; ?>
																	</select>
																	<div >
																		 <a  onclick="" data-toggle="modal" data-target="#id_ajout_type_produit" title="Ajouter un type de produit" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
																	</div>
																	@if ($errors->has('id_type_produit'))
																		<span class="help-block">
																			<strong style="color:#FF0000"; >{{ $errors->first('id_type_produit') }}</strong>
																		</span>
																	@endif
																
																</div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Catégorie</div>
																<div class="col-8">
																	<select  class="form-control select2"  id="id_id_type_aliment"  name="id_type_aliment"  data-placeholder="Choisir la catégorie du produit" style="width: 100%;" tabindex="-1" aria-hidden="true" id="id_select_type_entreprise">
																		<option></option>
																		<?php foreach($type_aliment as $type): ?>
																		<option value="<?php echo $type->id_type_aliment; ?>" <?php if($produit->type_aliment_id == $type->id_type_aliment) echo "selected"; ?>><?php echo $type->label; ?></option>
																		<?php endforeach; ?>
																	</select>
																	<div >
																		 <a  onclick="" data-toggle="modal" data-target="#id_ajout_type_aliment" title="Ajouter une type d'aliment" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
																	</div>
																	
																	@if ($errors->has('id_type_aliment'))
																		<span class="help-block">
																			<strong style="color:#FF0000"; >{{ $errors->first('id_type_aliment') }}</strong>
																		</span>
																	@endif
																</div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Prix du produit</div>
																<div class="col-8"><input type="text" class="form-control" name="prix" value="{{ $produit->prix }}" >
																	@if ($errors->has('prix'))
																			<span class="help-block">
																				<strong style="color:#FF0000"; >{{ $errors->first('prix') }}</strong>
																			</span>
																	@endif
																	</div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Groupement du produit</div>
																<div class="col-8">
																	<select  class="form-control select2" id="id_id_poste2" name="groupement"   data-placeholder="Choisir le sexe*" style="width: 100%;" tabindex="-1" aria-hidden="true">
																		<option value="1"  <?php if( $produit->groupement=='1')  echo "selected"?>>Par unité</option>
																		<option value="2"  <?php if( $produit->groupement=='2')  echo "selected"?>>Totalité</option>
																		<option value="3"  <?php if( $produit->groupement=='3')  echo "selected"?>>Par portion</option>
																	</select>
																</div>
															</div>
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Raison de surplus</div>
																<div class="col-8">
																	<select  class="form-control select2" id="id_id_raison_surplus_don" name="id_raison_surplus[]" multiple="multiple"  data-placeholder="Choisir la raison de surplus" style="width: 100%;" tabindex="-1" aria-hidden="true">
																		<option value="" ></option>
																		@foreach($liste_raison_surplus as $raison)
																			<option value="{{ $raison->id_raison_surplus  }}"  <?php if(raison_selected( $raison,$raison_surplus)) echo "selected"; ?> >{{ $raison->label  }}</option>
																		@endforeach
																	</select>
																	<div >
																		 <a  onclick="" data-toggle="modal" data-target="#id_ajout_raison_surplus" title="Ajouter une raison de surplus" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
																	</div>
																	@if ($errors->has('id_raison_surplus'))
																			<span class="help-block">
																				<strong style="color:#FF0000"; >{{ $errors->first('id_raison_surplus') }}</strong>
																			</span>
																	@endif
																</div>
															</div>
															<div class="row " style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Information nutritionnelle</div>
																<div class="col-8">
																	<select  class="form-control select2" id="id_id_raison_surplus" name="id_information_nutritionnelle[]" multiple="multiple"  data-placeholder="Choisir la raison de surplus" style="width: 100%;" tabindex="-1" aria-hidden="true">
																		<option value="" ></option>
																		@foreach($liste_info_nut as $info)
																			<option value="{{ $info->id_information_nutritionnelle  }}"  <?php if(info_nut_selected( $info,$informationNutritionnelle)) echo "selected"; ?> >{{ $info->label  }}</option>
																		@endforeach
																	</select>
																	<div >
																		 <a  onclick="" data-toggle="modal" data-target="#id_ajout_info_nut" title="Ajouter une information nutritionnelle" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
																	</div>
																	@if ($errors->has('id_information_nutritionnelle'))
																			<span class="help-block">
																				<strong style="color:#FF0000"; >{{ $errors->first('id_information_nutritionnelle') }}</strong>
																			</span>
																	@endif
																</div>
															</div>
															<!--<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Photo(s) du produit</div>
																<div class="col-8"><input name="photo[]" value="<?php//  echo $photo_value ?>" multiple="multiple" class="form-control form-control-lg"  type="file">
															</div>-->
															<div class="row aligner_liste" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;"></div>
																<div class="col-2" style="padding-left:20px;font-weight: bolder;color: #646369;"><a href="{{ url('produit') }}" title="Liste de produits"><span class="fa fa-backward"></span></a></div>
																<div class="col-2"><input type="submit" class="btn btn-success" value="Mettre à jour" ></div>
															</div>
															
															</form>
														</div>
														<hr>
													</div>
													<!-- /.col -->
													
												</div>
												<!-- /.row -->
										  </div>
										  <!-- ./card-body -->
									</div>
									<!-- /.card -->
								</div>
								<!-- /.tab-pane -->
							</div>
							<!-- /.tab-content -->
						</div>
						 <!-- ./card-body -->
					</div>
					<!-- /.card-->
			  </div>
          <!-- /.col -->
		 </div>
		 <!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection
@section('modals')
<!----modal   ajouter   type produit---->
<div class="modal fade" id="id_ajout_type_produit" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau type de produit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_produit">
			<div class="col-sm-10 id_div_nouveau_type_produit">
			  <input name="label" type="text" class="form-control" id="id_type_produit_label" >
			</div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" row_id="1" onclick="ajouter_type_produit_ajax(this)" >Ajouter</button>
	</div>
    </div>
  </div>
</div>
<!----modal   ajouter   type aliment---->
<div class="modal fade" id="id_ajout_type_aliment" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau type d'aliment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_aliment">
			<div class="col-sm-10 id_div_nouveau_type_aliment">
			  <input name="label" type="text" class="form-control" id="id_type_aliment_label" >
			</div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" row_id="1" onclick="ajouter_type_aliment_ajax(this)" >Ajouter</button>
	</div>
    </div>
  </div>
</div>
<!----modal   ajouter   info nutri---->
<div class="modal fade" id="id_ajout_info_nut" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouvelle information nutritionnelle</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_info_nut">
			<div class="col-sm-10 id_div_nouveau_information_nutritionnelle">
			  <input name="label" type="text" class="form-control" id="id_info_nut_label" >
			</div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" row_id="1" onclick="ajouter_info_nutritionnelle_ajax(this)" >Ajouter</button>
	</div>
    </div>
  </div>
</div>
<!----modal   ajouter  raison surplus---->
<div class="modal fade" id="id_ajout_raison_surplus" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouvelle raison de surplus</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_raison_surplus">
			<div class="col-sm-10 id_div_nouvelle_raison">
			  <input name="label" type="text" class="form-control" id="id_raison_surplus_label" >
			</div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" row_id="1" onclick="ajouter_raison_surplus_ajax(this)" >Ajouter</button>
	</div>
    </div>
  </div>
</div>
<!-- modal loader -->
<div class="modal fade modal-custumed" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
       
      </div>
      <center><h4>Encours.</h4> <div class="loader"></div></center>
      <div class="modal-footer">
       
      </div>
	</div>
  </div>
</div>
@endif

@endsection
@section('custom_script')
 <script src="{{ asset('dist/plugins/select2/select2.full.min.js') }}"></script>

<script type="text/Javascript">
	

	$(function () {
		$('.select2').select2() ;// initialisation element select2
		

	});
	function ajouter_type_produit_ajax(element) {
	var type = $(element).attr('row_id');
	var label = $('#id_type_produit_label').val();
	 if(!label) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_nouveau_type_produit").append(html);
	 }else{	
		 var token = $('#id_token_produit').val();
		var x =new FormData();
		x.append('label' , label);	
		x.append('_token' , token);	
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var lien = '{{ url('ajouter_type_produit')}}' ;
		$.ajax({
		   url : lien,
			method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				// console.log(resp);
				var html = '<option value ="'+ response.id_type_produit+'"  selected>'+response.label+' </option>';
				var select = $("#id_id_type_produit");
				select.append(html);
				select.select2("val" , select.select2("val"));
				$("#loadMe").modal('toggle');
				$("#id_ajout_type_produit").modal('toggle');
				
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				
		   }
		});  
	 }
}
function ajouter_type_aliment_ajax(element) {
	var type = $(element).attr('row_id');
	var label = $('#id_type_aliment_label').val();
	  if(!label) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_nouveau_type_aliment").append(html);
	 }else{	
		 var token = $('#id_token_aliment').val();
		var x =new FormData();
		x.append('label' , label);	
		x.append('_token' , token);	
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var lien = '{{ url('ajouter_type_aliment')}}' ;
		$.ajax({
		   url : lien,
			method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				// console.log(resp);
				var html = '<option value ="'+ response.id_type_aliment+'"  selected>'+response.label+' </option>';
				var select = $("#id_id_type_aliment");
				select.append(html);
				select.select2("val" , select.select2("val"));
				$("#loadMe").modal('toggle');
				$("#id_ajout_type_aliment").modal('toggle');
				
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				
		   }
		});  
	 }
}
function ajouter_info_nutritionnelle_ajax(element) {
	var label = $('#id_info_nut_label').val();
	 if(!label) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_nouveau_information_nutritionnelle").append(html);
	 }else{	
		 var token = $('#id_token_info_nut').val();
		var x =new FormData();
		x.append('label' , label);	
		x.append('_token' , token);	
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var lien = '{{ url('ajouter_information_nutritionnelle')}}' ;
		$.ajax({
		   url : lien,
			method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				// console.log(resp);
				var html = '<option value ="'+ response.id_information_nutritionnelle+'"  selected>'+response.label+' </option>';
				
				var select = $("#id_id_informaton_nutritionnelle");
				select.append(html);
				select.select2("val" , select.select2("val").concat(response.id_information_nutritionnelle));
				$("#loadMe").modal('toggle');
				$("#id_ajout_info_nut").modal('toggle');
				
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				
		   }
		});
	 }		
}
function ajouter_raison_surplus_ajax(element) {
	var label = $('#id_raison_surplus_label').val();
	 if(!label) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_nouvelle_raison").append(html);
	 }else{	
		 var token = $('#id_token_raison_surplus').val();
		var x =new FormData();
		x.append('label' , label);	
		x.append('_token' , token);	
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var lien = '{{ url('ajouter_raison_surplus')}}' ;
		$.ajax({
		   url : lien,
			method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				// console.log(resp);
				var html = '<option value ="'+ response.id_raison_surplus+'"  selected>'+response.label+' </option>';
				var select = $("#id_id_raison_surplus");
				select.append(html);
				select.select2("val" , select.select2("val").concat(response.id_raison_surplus));
				$("#loadMe").modal('toggle');
				$("#id_ajout_raison_surplus").modal('toggle');
				
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				
		   }
		});  
	 }
}
	

  
</script>
@endsection


<?php 
function typeProduitSelected($type_produit, $allTypes){
		foreach($allTypes as $t){
			if($t->id_type_produit == $type_produit->id_type_produit){
				return true;
			}
		}
		return false;
	}
	function typeAlimentSelected($type_aliment, $allTypesAl){
		foreach($allTypesAl as $t){
			if($t->id_type_aliment == $type_aliment->id_type_aliment){
				return true;
			}
		}
		return false;
	}
	function raison_selected($raison, $allTypesAl){
		foreach($allTypesAl as $t){
			if($t->id_raison_surplus == $raison->id_raison_surplus){
				return true;
			}
		}
		return false;
	}
	function info_nut_selected($info, $allTypesAl){
		foreach($allTypesAl as $t){
			if($t->id_information_nutritionnelle == $info->id_information_nutritionnelle){
				return true;
			}
		}
		return false;
	}
	
	
	
	
?>