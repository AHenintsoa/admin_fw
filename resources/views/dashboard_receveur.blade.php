@extends('template/default')

@section('titre')
    Dashboard
@endsection

@section('contenu')
@if(Session::has('choix'))
<input type="hidden" id="choixPane" name="choix" value="{{ Session::get('choix') }}">
@endif


<section class="content">
      <div class="container-fluid">
			<div class="row">
         
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_page') }}">Général</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_entreprise') }}" >Entreprise</a></li>
							<li class="nav-item"><a class="nav-link  active" href="" >Organisation</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_benevole') }}" >Bénévole</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_client') }}" >Consommateur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_commande') }}" >Commande</a></li>	
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_encours') }}" >Activité encours</a></li>
						</ul>
						<button id="download" onclick="getImage()" class="bouton-sticky" title="Exporter en pdf"><img src="{{ asset('/img/Downloads-icon.png') }}" class="exportImage" alt="exporter"></button>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									<div id="capture" >
									<div class="card-body">
										<div class="row">
										  <div class="col-lg-4 col-6">
											<!-- small box -->
												<div class="small-box bg-info">
												  <div class="inner">
													<h3>{{ number_format( (float)$nombre_receveur->nombre_receveur  , 0 , "," , " " )  }} </h3>

													<p>Nombre de receveur </p>
												  </div>
												  <div class="icon">
													<i class="ion ion-bag"></i>
												  </div>
												</div>
											</div>
											<div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3>{{ number_format( (float)$total_beneficiaire->total  , 0 , "," , " " ) }} </h3>

												<p>Nombre total d'enfant bénéficiaire </p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											</div>
										  </div>
										  <!-- ./col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Nombre de bénéficiaires enfants</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_nombre_beneficiaire" class="table table-bordered table-hover">
												<thead>
												<tr>
												  <th>Nom Organisation</th>
												  <th>Nombre de bénéficiaire</th>
												 
												</tr>
												</thead>
												<tbody>
												@foreach($nombre_beneficiaire as $beneficiaire)
												<tr>
												  <td>{{$beneficiaire->nom_organisation }}</td>
												  <td> {{ number_format( (float)$beneficiaire->nombre , 0 , "," , " " ) }} </td>
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Repas distribués en moyenne par mois</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_repas_mois" class="table table-bordered table-hover" >
												 <div class="form-group">
													<label for="inputName2" class="col-sm-2 control-label">Selectionner année</label>
													<div class="col-2">
														<select id="id_annee_mois"  class="form-control" onchange="findRepasMois()">
															 <?php 
																$currentYear = (int)date("Y");
																for($date=(int)date("Y"); $date>2017; $date--){
																	  if($currentYear == $date){
																		echo '<option id="id_select_mois" selected="selected" value="'.$date.'">Cette année</option>';
																	  }
																	  else{
																		echo '<option  value="'.$date.'">L\'année '.$date.'</option>';
																	  }
																} ?>
														</select>
													</div>	
												  </div>
												<thead>
												<tr>
													<th>Nom Organisation</th>
													<th style="width:40%;">Janvier</th>
													<th style="width:40%;">Février</th>
													<th style="width:40%;">Mars</th>
													<th style="width:40%;">Avril</th>
													<th style="width:40%;">Mai</th>
													<th style="width:40%;">Juin</th>
													<th style="width:40%;">Juillet</th>
													<th style="width:40%;">Août</th>
													<th style="width:40%;">Septembre</th>
													<th style="width:40%;">Octobre</th>
													<th style="width:40%;">Novembre</th>
													<th style="width:40%;">Décembre</th>
												</tr>
												</thead>
												<tbody id="id_tbody_mois">
												@foreach($repas_en_moyenne_par_mois as $repas)
												<tr>
												  <td>{{$repas[0]['nom_organisation'] }}</td>
													@for($i = 0 ; $i <12 ; $i++)
												  <td> {{ number_format( (float)$repas[$i]['repas'] , 1 , "," , " " ) }} </td>
													@endfor
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Repas reçus en moyenne par enfant par mois</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_repas_mois_enfantss" class="table table-bordered table-hover" >
												 <div class="form-group" style="space-between !important">
													<label for="inputName2" class="col-sm-2 control-label">Selectionner année</label>
													<div class="col-2">
														<select id="id_annee_mois_enfant"  class="form-control" onchange="findRepasMoisEnfant()">
															 <?php 
																$currentYear = (int)date("Y");
																for($date=(int)date("Y"); $date>2017; $date--){
																	  if($currentYear == $date){
																		echo '<option id="id_select_mois" selected  value="'.$date.'">Cette année</option>';
																	  }
																	  else{
																		echo '<option value="'.$date.'">L\'année '.$date.'</option>';
																	  }
																} ?>
														</select>
													</div>	
												  </div>
												<thead>
												<tr>
													<th>Nom Organisation</th>
													<th style="width:40%;">Janvier</th>
													<th style="width:40%;">Février</th>
													<th style="width:40%;">Mars</th>
													<th style="width:40%;">Avril</th>
													<th style="width:40%;">Mai</th>
													<th style="width:40%;">Juin</th>
													<th style="width:40%;">Juillet</th>
													<th style="width:40%;">Août</th>
													<th style="width:40%;">Septembre</th>
													<th style="width:40%;">Octobre</th>
													<th style="width:40%;">Novembre</th>
													<th style="width:40%;">Décembre</th>
												</tr>
												</thead>
												<tbody id="id_tbody_mois_enfant">
												@foreach($repas_en_moyenne_par_mois_par_enfant as $repas)
												<tr>
												  <td>{{$repas[0]['nom_organisation'] }}</td>
													@for($i = 0 ; $i <12 ; $i++)
												  <td> {{ number_format( (float)$repas[$i]['repas'] , 1 , "," , " " ) }} </td>
													@endfor
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										
										
										<center>
											<h4>Sélectionner période</h4>
											<form  action="{{ url('dashboard_receveur') }}" method="get">
												<div class="row">
													
													{{ csrf_field() }}
													<div class="col"></div>
												  <div class="col">
														<label for="inputName">Début</label>
														  <input style="width : 200px;" name="date_debut" type="date" class="form-control" id="inputName" value="<?php if($debut_periode !=0) echo $debut_periode ?>" >
														  @if ($errors->has('date_debut'))
																<span class="help-block">
																	<strong style="color:#FF0000"; >{{ $errors->first('date_debut') }}</strong>
																</span>
															@endif
													</div>
												  <div class="col">
													<label for="inputName" >Fin</label>
													 <input style="width : 200px;" name="date_fin" type="date" value="<?php if($fin_periode !=0) echo $fin_periode ?>" class="form-control" id="inputName" >
													  @if ($errors->has('date_fin'))
															<span class="help-block">
																<strong style="color:#FF0000"; >{{ $errors->first('date_fin') }}</strong>
															</span>
														@endif
													</div>
												  <!--<div class="w-100"></div>-->
												  <!--<div class="col">3</div>-->
												  <div class="col">
													</br>
													<input type="submit" value="valider" class="btn btn-secondary" style="width : 200px; padding-top:20px ;">
													</div>
													<div class="col"></div>
												</div>
												</form>
											</center>
											</br>
										<!--<div class=row>
											 <div class="col-md-12">
											 <center>
												<h4>Sélectionner période</h4>
												<form class="form-horizontal" action="{{ url('dashboard_receveur') }}" method="get">
												  {{ csrf_field() }}
												  <div class="form-group">
													<label for="inputName" class="col-sm-2 control-label">Début</label>
													<div class="col-2">
													  <input name="date_debut" type="date" class="form-control" id="inputName" value="<?php if($debut_periode !=0) echo $debut_periode ?>" >
													  @if ($errors->has('date_debut'))
															<span class="help-block">
																<strong style="color:#FF0000"; >{{ $errors->first('date_debut') }}</strong>
															</span>
														@endif
													</div>
													<label for="inputName" class="col-sm-2 control-label">Fin</label>
													<div class="col-2">
													  <input name="date_fin" type="date" value="<?php if($fin_periode !=0) echo $fin_periode ?>" class="form-control" id="inputName" >
													  @if ($errors->has('date_fin'))
															<span class="help-block">
																<strong style="color:#FF0000"; >{{ $errors->first('date_fin') }}</strong>
															</span>
														@endif
													</div>
													</br>
													<div class="col-6">
														<input type="submit" value="valider" class="btn btn-secondary">
													</div>
												  </div>
												</form>
											</center>	
											</div>	
										</div>-->
										<!--row-->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Aliments reçus par receveur</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_aliment_receveur" class="table table-bordered table-hover">
												<thead>
												<tr>
												  <th>Nom Organisation</th>
												  <th>Poids des produits reçus</th>
												 
												</tr>
												</thead>
												<tbody>
												@if(count($aliments_recus_par_receveur_poids)==0)
													<tr>	
														<th colspan="2"> <p>aucun résultat </p></th>
													</tr>	
												@endif
												@foreach($aliments_recus_par_receveur_poids as $aliment)
												<tr>
												  <td>{{$aliment->nom_organisation }}</td>
												  <td> {{ number_format( (float)$aliment->poids , 1 , "," , " " ) }} kg</td>
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Nombre d'aliments reçus par receveur</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_nombre_aliment_receveur" class="table table-bordered table-hover">
												<thead>
												<tr>
													<th>Nom Organisation</th>
													<th>Nombre d'aliments</th>
													
												</tr>
												</thead>
												<tbody>
												@if(count($aliments_recus_par_receveur_nombre)==0)
													<tr>	
														<th colspan="2"> <p>aucun résultat </p></th>
													</tr>	
												@endif
												@foreach($aliments_recus_par_receveur_nombre as $aliment)
												<tr>
													<td>{{$aliment->nom_organisation }}</td>
													<td>{{ number_format( (float)$aliment->nombre_aliments, 0 , "," , " " ) }}</td>
													
												</tr>
												@endforeach
												</tbody>
												<tfoot>
												
												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										<div class="row">
										  <div class="col-md-12">
											 <div class="card">
											<div class="card-header">
											  <h3 class="card-title">Nombre de repas par enfants recu par receveur</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body table-custumed">
											  <table id="id_nombre_repas_receveur" class="table table-bordered table-hover">
												<thead>
												<tr>
													<th>Nom Organisation</th>
													<th>Nombre de repas</th>
													
												</tr>
												</thead>
												<tbody>
												@if(count($repas_recus_par_receveur_poids_par_enfant)==0)
													<tr>	
														<th colspan="2"> <p>aucun résultat </p></th>
													</tr>	
												@endif
												@foreach($repas_recus_par_receveur_poids_par_enfant as $repas)
												<tr>
													<td>{{$repas->nom_organisation }}</td>
													<td>{{ number_format( (float)$repas->nombre_repas, 1 , "," , " " ) }}</td>
												</tr>
												@endforeach
												</tbody>
												<tfoot>

												</tfoot>
											  </table>
											</div>
											<!-- /.card-body -->
										  </div>
										  <!-- ./card -->
										  </div>
										  <!-- /.col -->
										</div>
										<!-- /.row -->
										
									  </div>
									  <!-- ./card-body -->
									 </div> 
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
					 
					
					<!-- /.tab-content -->
				  </div><!-- /.card-body -->
				</div>
				<!-- /.nav-tabs-custom -->
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection

@section('custom_script')
<script type="text/javascript" src="{{ asset('plugins/html2canvaspdf/js/html2canvas.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/html2canvaspdf/js/jsPdf.debug.js') }}"></script>
<script type="text/Javascript">
	function setSelectedIndex(s, i){
		s.options[i-1].selected = true;
		return;
	}

	$(function () {
		@if(Session::has('date_receveur'))
			var elmnt = document.getElementById("id_aliment_receveur");
			elmnt.scrollIntoView(); 
		@endif
		setSelectedIndex(document.getElementById("id_annee_mois_enfant"),1);
		setSelectedIndex(document.getElementById("id_annee_mois"),1);

		 // var option1= document.getElementById("id_select_mois");
		 // var select1= option1.setAttribute("selected" , "selected");
		 // console.log(option1.getAttribute("selected"));
		
		$("#id_nombre_beneficiaire").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		$("#id_repas_mois").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		$("#id_repas_mois_enfantss").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		$("#id_nombre_aliment_receveur").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		$("#id_aliment_receveur").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		$("#id_nombre_repas_receveur").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "decimal": " ",
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});
		$("#id_nombre_aliment_recu_receveur").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									"decimal": " ",
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  }
							});	


	});
  
	function findRepasMoisEnfant () {
		var table = document.getElementById('id_tbody_mois_enfant');
		var criteres = document.getElementById('id_annee_mois_enfant').value  ; 
		//var criteres = 0 ; 
		//remove tr and th
		while (table.firstChild) {
		  table.removeChild(table.firstChild);
		}
		getRepasMoisEnfant(criteres,function(resp){
			var trElement ;
			var tdElement ; 
			var response = resp ;
			console.log(response);
			if(response[0].length==0){
				trElement = document.createElement('tr') ;
				
					tdElement = document.createElement('td') ; 
					//<th colspan="2"> <p>aucun résultat </p></th>
					tdElement.setAttribute('colspan' , '13' );
					tdElement.innerHTML = "aucun resultat";
					trElement.appendChild(tdElement) ; 
				
				table.appendChild(trElement) ; 
			}else{
				for(var x= 0 ; x < response.length ; x++){
					trElement = document.createElement('tr') ;
					tdElement = document.createElement('td') ; 
					tdElement.innerHTML = response[x][0].nom_organisation ;
					trElement.appendChild(tdElement) ; 
					for(var i=0;i<response[x].length;i++) {
						tdElement = document.createElement('td') ; 
						tdElement.innerHTML = response[x][i].repas;
						trElement.appendChild(tdElement) ; 	
					}
					table.appendChild(trElement) ; 
				}
			}
		} )

	}	
	function findRepasMois () {
		var table = document.getElementById('id_tbody_mois');
		var criteres = document.getElementById('id_annee_mois').value  ; 
		//remove tr and th
		while (table.firstChild) {
		  table.removeChild(table.firstChild);
		}
		getRepasMois(criteres,function(resp){
			var trElement ;
			var tdElement ; 
			var response = resp ;
			console.log(response);
			if(response[0].length==0){
				trElement = document.createElement('tr') ;
				tdElement = document.createElement('th') ; 
					//<th colspan="2"> <p>aucun résultat </p></th>
					tdElement.setAttribute('colspan' , '13' );
					tdElement.innerHTML = "aucun resultat";
					trElement.appendChild(tdElement) ; 
				table.appendChild(trElement) ; 
			}else{
				for(var x= 0 ; x < response.length ; x++){
					trElement = document.createElement('tr') ;
					tdElement = document.createElement('th') ; 
					tdElement.innerHTML = response[x][0].nom_organisation ;
					trElement.appendChild(tdElement) ; 
					for(var i=0;i<response[x].length;i++) {
						tdElement = document.createElement('td') ;
						tdElement.innerHTML = response[x][i].repas; 
						trElement.appendChild(tdElement) ; 	
					}
					table.appendChild(trElement) ; 
				}
			}
		} )

	}
	function getRepasMoisEnfant(critere ,callBack) {
			  
		var xMLHttprequest = new XMLHttpRequest() ;
		xMLHttprequest.open( 'GET', '{{url('repas_mois_enfant?annee=')}}'+critere )	;
					
		xMLHttprequest.addEventListener('readystatechange',function(){
			if(xMLHttprequest.readyState==XMLHttpRequest.DONE && xMLHttprequest.status==200) {
								
				var resp = JSON.parse(xMLHttprequest.responseText) ; 
				callBack(resp) ; 
			}
		})
		//var parameter = 'username='+critere;
		//console.log(parameter) ;
		xMLHttprequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		//xMLHttprequest.send(parameter) ;
		xMLHttprequest.send() ;
	}
	function getRepasMois(critere ,callBack) {
			  
		var xMLHttprequest = new XMLHttpRequest() ;
		xMLHttprequest.open( 'GET', '{{url('repas_mois?annee=')}}'+critere )	;
					
		xMLHttprequest.addEventListener('readystatechange',function(){
			if(xMLHttprequest.readyState==XMLHttpRequest.DONE && xMLHttprequest.status==200) {
								
				var resp = JSON.parse(xMLHttprequest.responseText) ; 
				callBack(resp) ; 
			}
		})
		//var parameter = 'username='+critere;
		//console.log(parameter) ;
		xMLHttprequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		//xMLHttprequest.send(parameter) ;
		xMLHttprequest.send() ;
	}
  function getImage(){
	 
          html2canvas(document.getElementById('capture'), {
              onrendered:function(canvas) {

                  var contentWidth = canvas.width;
                  var contentHeight = canvas.height;

                  // Une page pdf indique la hauteur de la toile générée par la page html;
                  var pageHeight = contentWidth / 595.28 * 841.89;
                  // la hauteur de la page HTML n'est pas générée pdf
                  var leftHeight = contentHeight;
                  // offset de page pdf
                  var position = 0;
                 // a4 format de papier [595.28, 841.89], toile générée par page HTML en largeur et hauteur de l'image pdf
                  var imgWidth = 555.28;
                  var imgHeight = 555.28/contentWidth * contentHeight;

                  var pageData = canvas.toDataURL('image/jpeg', 1.0);

                  var pdf = new jsPDF('', 'pt', 'a4');
                 // Il y a deux hauteurs à distinguer, l'une est la hauteur réelle de la page html et la hauteur de la page qui a généré le pdf (841.89)
                  // Lorsque le contenu ne dépasse pas la plage affichée sur la page pdf, aucune pagination n'est requise.
                  if (leftHeight < pageHeight) {
                      pdf.addImage(pageData, 'JPEG', 20, 0, imgWidth, imgHeight );
                  } else {
                      while(leftHeight > 0) {
                          pdf.addImage(pageData, 'JPEG', 20, position, imgWidth, imgHeight)
                          leftHeight -= pageHeight;
                          position -= 841.89;
                          // Éviter d'ajouter des pages vierges
                          if(leftHeight > 0) {
                              pdf.addPage();
                          }
                      }
                  }

                  pdf.save('content.pdf');
              }
          })
      
}
  
</script>
@endsection