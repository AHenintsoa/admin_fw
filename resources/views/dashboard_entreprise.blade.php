@extends('template/default')

@section('titre')
    Dashboard générl
@endsection

@section('contenu')
<section class="content">
      <div class="container-fluid">
			<div class="row">
         
			  <!-- /.col -->
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link " href="{{ url('dashboard_page') }}">Général</a></li>
							<li class="nav-item"><a class="nav-link active" href="" >Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_receveur') }}" >Organisation</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_benevole') }}" >Bénévole</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_client') }}" >Consommateur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_commande') }}" >Commande</a></li>	
							<li class="nav-item"><a class="nav-link" href="{{ url('dashboard_encours') }}" >Activité encours</a></li>
						</ul>
						<button id="download" onclick="getImage()" class="bouton-sticky" title="Exporter en pdf"><img src="{{ asset('/img/Downloads-icon.png') }}" class="exportImage" alt="exporter"></button>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     entreprise    -->
							<div class="active tab-pane" id="entreprise">
								<div class="card card-primary">
									<div id="capture" >
									<div class="card-body">
										<div class="row">
										  <div class="col-lg-4 col-6">
											<!-- small box -->
											<div class="small-box bg-info">
											  <div class="inner">
												<h3>{{ $liste_entreprise}} </h3>

												<p>Nombre d'entreprise </p>
											  </div>
											  <div class="icon">
												<i class="ion ion-bag"></i>
											  </div>
											</div>
										  </div>
										  </div>
										  <!-- ./col -->
										  <div class="row">
											<div class="col-md-12">
												 <div class="card">
													<div class="card-header">
													  <h3 class="card-title">Nouvelles entreprises de ce mois</h3>
													</div>
													<!-- /.card-header -->
													<div class="card-body table-custumed">
													  <table id="id_nouvelles_entreprises" class="table table-bordered table-hover">
														<thead>
														<tr>
														  <th>Label</th>
														  <th>Objectif</th>
														  <th>KPI</th>
														</tr>
														</thead>
														<tbody>
														
														@foreach($liste_entreprise_ce_mois as $entreprise_ce_mois)
														<tr>
														  <td>{{ $entreprise_ce_mois->nom_entreprise }}</td>
														  <td>{{ $entreprise_ce_mois->objectif }}</td>
														  <td>{{ number_format( (float)$entreprise_ce_mois->kpi , 1, "," , " ") }} %</td>
														</tr>
														@endforeach 
														
														
														</tbody>
														<tfoot>
														
														</tfoot>
													  </table>
													</div>
													<!-- /.card-body -->
												  </div>
												  <!-- ./card -->
												</div>
												 <!-- ./col -->
											</div>
											 <!-- ./row -->
											 <center>
											<h4>Sélectionner période</h4>
											<form  action="{{ url('dashboard_entreprise') }}" method="get">
												<div class="row">
													
													{{ csrf_field() }}
													<div class="col"></div>
												  <div class="col">
														<label for="inputName">Début</label>
														  <input style="width : 200px;" name="date_debut" type="date" class="form-control" id="inputName" value="<?php if($debut_periode !=0) echo $debut_periode ?>" >
														  @if ($errors->has('date_debut'))
																<span class="help-block">
																	<strong style="color:#FF0000"; >{{ $errors->first('date_debut') }}</strong>
																</span>
															@endif
													</div>
												  <div class="col">
													<label for="inputName" >Fin</label>
													 <input style="width : 200px;" name="date_fin" type="date" value="<?php if($fin_periode !=0) echo $fin_periode ?>" class="form-control" id="inputName" >
													  @if ($errors->has('date_fin'))
															<span class="help-block">
																<strong style="color:#FF0000"; >{{ $errors->first('date_fin') }}</strong>
															</span>
														@endif
													</div>
												  <!--<div class="w-100"></div>-->
												  <!--<div class="col">3</div>-->
												  <div class="col">
													</br>
													<input type="submit" value="valider" class="btn btn-secondary" style="width : 200px; padding-top:20px ;">
													</div>
													<div class="col"></div>
												</div>
												</form>
											</center>
											</br>
											<!-- <div class=row>
											 <div class="col-md-12">
											 <center>
												<h4>Sélectionner période</h4>
												<form class="form-horizontal" action="{{ url('dashboard_entreprise') }}" method="get">
												  {{ csrf_field() }}
												  <div class="form-group">
													<label for="inputName" class="col-sm-2 control-label">Début</label>
													<div class="col-2">
													  <input name="date_debut" type="date" class="form-control" id="inputName" value="<?php if($debut_periode !=0) echo $debut_periode ?>" >
													  @if ($errors->has('date_debut'))
															<span class="help-block">
																<strong style="color:#FF0000"; >{{ $errors->first('date_debut') }}</strong>
															</span>
														@endif
													</div>
													<label for="inputName" class="col-sm-2 control-label">Fin</label>
													<div class="col-2">
													  <input name="date_fin" type="date" value="<?php if($fin_periode !=0) echo $fin_periode ?>" class="form-control" id="inputName" >
													  @if ($errors->has('date_fin'))
															<span class="help-block">
																<strong style="color:#FF0000"; >{{ $errors->first('date_fin') }}</strong>
															</span>
														@endif
													</div>
													</br>
													<div class="col-6">
														<input type="submit" value="valider" class="btn btn-secondary">
													</div>
												  </div>
												</form>
											</center>	
											</div>	
										</div>-->
										<!--row-->
											 <div class="row">
												<div class="col-md-12">
												  <div class="card">
													<div class="card-header">
													  <h3 class="card-title">Aliments vendus par entreprise</h3>
													</div>
													<!-- /.card-header -->
													<div class="card-body table-custumed">
													  <table id="id_aliments_vendus" class="table table-bordered table-hover">
														<thead>
														<tr>
														  <th>Entreprise</th>
														  <th>Quantité</th>
														  <th>Détail</th>
														</tr>
														</thead>
														<tbody>
														@if(count($nombre_aliments_vendus)==0)
															<tr>	
																<th colspan="3"> <p>aucun résultat </p></th>
															</tr>	
														@endif
														@foreach($nombre_aliments_vendus as $aliments_vendus)
														<tr>
														  <td>{{$aliments_vendus->nom_entreprise }}</td>
														  <td>{{number_format( (float)$aliments_vendus->quantite , 0 , "," , " " )  }}</td>
														  <th><button row_vendu_entreprise_id="{{ $aliments_vendus->id_entreprise }}" onclick="visualiserAlimentVendu(this)" class="bouton"><span class="fa  fa-eye"></span></button></th>
														</tr>
														@endforeach
														</tbody>
														<tfoot>
														
														</tfoot>
													  </table>
													</div>
													<!-- /.card-body -->
												  </div>
												  <!-- ./card -->
												</div>
												<!-- ./col -->
											</div>
											 <!-- ./row --> 
											  <div class="row">
												<div class="col-md-12">
												  <div class="card">
													<div class="card-header">
													  <h3 class="card-title">Aliments donnés par entreprise</h3>
													</div>
													<!-- /.card-header -->
													<div class="card-body table-custumed">
													  <table id="id_aliments_dons" class="table table-bordered table-hover">
														<thead>
														<tr>
														  <th>Entreprise</th>
														  <th>Quantité</th>
														  <th>Détail</th>
														</tr>
														</thead>
														<tbody>
														@if(count($nombre_aliments_donnes)==0)
															<tr>	
																<th colspan="3"> <p>aucun résultat </p></th>
															</tr>	
														@endif
														@foreach($nombre_aliments_donnes as $aliments_donnes)
														<tr>
														  <td>{{$aliments_donnes->nom_entreprise }}</td>
														  <td>{{number_format( (float)$aliments_donnes->quantite , 0 , "," , " " ) }}</td>
														  <th><button row_don_entreprise_id="{{ $aliments_donnes->id_entreprise }}" onclick="visualiserAlimentDon(this)" class="bouton"><span class="fa  fa-eye"></span></button></th>
														</tr>
														@endforeach
														
														</tbody>
														<tfoot>
														
														</tfoot>
													  </table>
													</div>
													<!-- /.card-body -->
												  </div>
												  <!-- ./card -->
												</div>
												<!-- ./col -->
											</div>
											 <!-- ./row -->
											 <div class="row">
												<div class="col-md-12">
												  <div class="card">
													<div class="card-header">
													  <h3 class="card-title">Aliments vendus par entreprise</h3>
													</div>
													<!-- /.card-header -->
													<div class="card-body table-custumed">
													  <table id="id_aliments_vendus_dons" class="table table-bordered table-hover">
														<thead>
														<tr>
														  <th>Entreprise</th>
														  <th>Quantité</th>
														  
														</tr>
														</thead>
														<tbody>
														@if(count($nombre_aliments_vendus_dons)==0)
															<tr>	
																<th colspan="2"> <p>aucun résultat </p></th>
																
															</tr>	
														@endif
														@foreach($nombre_aliments_vendus_dons as $aliments_vendus)
														<tr>
														  <td>{{$aliments_vendus->nom_entreprise }}</td>
														  <td>{{ number_format( (float)$aliments_vendus->quantite , 0 , "," , " " )}}</td>
														</tr>
														@endforeach
														</tbody>
														<tfoot>
														
														</tfoot>
													  </table>
													</div>
													<!-- /.card-body -->
												  </div>
												  <!-- ./card -->
												</div>
												<!-- ./col -->
											</div>
											 <!-- ./row -->
											<div class="row">
												<div class="col-md-12"> 
												  <div class="card">
													<div class="card-header">
													  <h3 class="card-title">Chiffre d'affaire des entreprises</h3>
													</div>
													<!-- /.card-header -->
													<div class="card-body table-custumed">
													  <table id="id_ca_entreprise" class="table table-bordered table-hover">
														<thead>
														<tr>
														  <th>Entreprise</th>
														  <th>CA</th>
														</tr>
														</thead>
														<tbody>
														@if(count($ca_entreprise)==0)
															<tr>	
																<th colspan="2"> <p>aucun résultat </p></th>
															</tr>	
														@endif
														@foreach($ca_entreprise as $ca)
														<tr>
														  <td>{{$ca->nom_entreprise }}</td>
														  <td>Ar {{ number_format( (float)$ca->montant , 0 , "," , " " ) }}</td>
														</tr>
														@endforeach
														</tbody>
														<tfoot>
														
														</tfoot>
													  </table>
													</div>
													<!-- /.card-body -->
												  </div>
												  <!-- ./card -->
												  </div>
												<!-- ./col -->
											</div>
											 <!-- ./row -->
											 <div class="row">
												<div class="col-md-12"> 
												  <div class="card">
													<div class="card-header">
													  <h3 class="card-title">Raisons de surplus</h3>
													</div>
													<!-- /.card-header -->
													<div class="card-body table-custumed">
													  <table id="id_raisons_surplus" class="table table-bordered table-hover">
														<thead>
														<tr>
														  <th>Entreprise</th>
														  <th>Raison</th>
														   <th>Fréquence</th>
														</tr>
														</thead>
														<tbody>
														@if(count($raison_surplus)==0)
															<tr>	
																<th colspan="3"> <p>aucun résultat </p></th>
															</tr>	
														@endif
														@foreach($raison_surplus as $raison)
														<tr>
														  <td>{{$raison->nom_entreprise }}</td>
														  <td>{{$raison->label }}</td>
														  <td>{{number_format( (float)$raison->frequence , 0 , "," , " " )  }}</td>
														</tr>
														@endforeach
														
														</tbody>
														<tfoot>
														
														</tfoot>
													  </table>
													</div>
													<!-- /.card-body -->
												  </div>
												  <!-- ./card -->
												  </div>
												<!-- ./col -->
											</div>
											 <!-- ./row -->
											 <div class="row">
												<div class="col-md-12">
												  <div class="card">
													<div class="card-header">
													  <h3 class="card-title">Les organisations ayant obtenu des dons</h3>
													</div>
													<!-- /.card-header -->
													<div class="card-body table-custumed">
													  <table id="id_organisation" class="table table-bordered table-hover">
														<thead>
														<tr>
														  <th>Entreprise</th>
														  <th>Organisation</th>
														  <th>Produit</th>
														   <th>Quantité</th>
															<th>Moment</th>
														</tr>
														</thead>
														<tbody>
														@if(count($receveur)==0)
															<tr>	
																<th colspan="5"> <p>aucun résultat </p></th>
															</tr>	
														@endif
														@foreach($receveur as $organisation)
														<tr>
														  <td>{{$organisation->nom_entreprise }}</td>
														  <td>{{$organisation->nom_organisation }}</td>
														  <td>{{$organisation->nom_produit }}</td>
														  <td>{{number_format( (float)$organisation->quantite , 0 , "," , " " )  }}</td>
														  <td>{{$organisation->moment }}</td>
														</tr>
														@endforeach
														
														</tbody>
														<tfoot>
														
														</tfoot>
													  </table>
													</div>
													<!-- /.card-body -->
												  </div>
												  <!-- ./card -->
												</div>
												<!-- ./col -->
											</div>
											 <!-- ./row -->
									  </div>
									  <!-- ./card-body -->
									</div> 
								</div>
								<!-- /.card -->
							</div>
							<!-- /.tab-pane -->
					 <!-- /.tab-content -->
				  </div><!-- /.card-body -->
				</div>
				<!-- /.nav-tabs-custom -->
			  </div>
          <!-- /.col -->
		 </div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection

@section('modals')
<!-- Modal -->
<!----modal   visualiser     aliments vendus---->
<div class="modal fade" id="id_visualiserAlimentVendu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail Vente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  
		<div id="detail_aliment_vendu" >  </div>
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
	 </form> 
    </div>
  </div>
</div>

<!----modal   visualiser     aliments donnés---->
<div class="modal fade" id="id_visualiserAlimentDon" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Détail Don</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  
		<div id="detail_aliment_don" >  </div>
		
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
	 </form> 
    </div>
  </div>
</div>

<!-- modal loader-->
<div class="modal fade modal-custumed" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
       
      </div>
      <center> <h4>Encours.</h4><div class="loader"></div></center>
      <div class="modal-footer">
       
      </div>
	</div>
  </div>
</div>
@endsection

@section('custom_script')
<script type="text/javascript" src="{{ asset('plugins/html2canvaspdf/js/html2canvas.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/html2canvaspdf/js/jsPdf.debug.js') }}"></script>
<script>
  $(function () {
	   @if(Session::has('date_entreprise'))
			var elmnt = document.getElementById("id_aliments_vendus");
			elmnt.scrollIntoView(); 
		@endif
	$("#id_nouvelles_entreprises").DataTable(
							{"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							   "aaSorting": [[2, 'desc']]
							});
	$("#id_aliments_vendus").DataTable({"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							   "aaSorting": [[1, 'desc']]
							});
	$("#id_aliments_dons").DataTable({"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							   "aaSorting": [[2, 'desc']]
							});
	$("#id_aliments_vendus_dons").DataTable({"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							   "aaSorting": [[1, 'desc']]
							});
	$("#id_ca_entreprise").DataTable({"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							   "aaSorting": [[1, 'desc']]
							});
	$("#id_raisons_surplus").DataTable({"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							   "aaSorting": [[2, 'desc']]
							});
	$("#id_organisation").DataTable({"language": {
									"paginate": {
											"first":      "Début",
											"last":       "Dernière",
										  "previous": "Précédente",
										  "next":"Suivante",
									},
									 "info": "Afficher page _PAGE_ sur _PAGES_",
									 "lengthMenu":     "Afficher _MENU_ éléments",
									 "search":         "Recherche:",
									 "zeroRecords":    "Aucun élément trouvé",
									 "infoEmpty":      "Afficher 0 de 0 sur 0 élément",
									  "infoFiltered":   "(filtré sur _MAX_ total élément(s)",
							  },
							   "aaSorting": [[3, 'desc']]
							});
  });
  
  function visualiserAlimentVendu(element) {
	  $("#loadMe").modal();
         var div = document.getElementById('detail_aliment_vendu');
		//remove detail 
		while (div.firstChild) {
		  div.removeChild(div.firstChild);
		}           
		var criteres = $(element).attr('row_vendu_entreprise_id');
		console.log(criteres);
		getDetail_aliment_vendu(criteres,function(resp){
			var response = resp ;
			console.log(response);
			var liste_details= document.getElementById("detail_aliment_vendu");
			var ulElement = document.createElement('ul') ;
			var liElement;
			
			for( var x= 0 ; x < response.length ; x++){
				liElement = document.createElement('li') ; 
				liElement.innerHTML = response[x].quantite +"  "+response[x].nom_produit+"(s)" ;
				ulElement.appendChild(liElement) ; 
			}
			liste_details.appendChild(ulElement);
			$("#loadMe").modal('toggle');
			$("#id_visualiserAlimentVendu").modal();
		})
	}
	function visualiserAlimentDon(element) {
		$("#loadMe").modal();
         var div = document.getElementById('detail_aliment_don');
		//remove detail 
		while (div.firstChild) {
		  div.removeChild(div.firstChild);
		}           
		var criteres = $(element).attr('row_don_entreprise_id');
		console.log(criteres);
		getDetail_aliment_don(criteres,function(resp){
			console.log('eto2');	
			var response = resp ;
			console.log(response);
			var liste_details= document.getElementById("detail_aliment_don");
			var ulElement = document.createElement('ul') ;
			var liElement;
			
			for( var x= 0 ; x < response.length ; x++){
				liElement = document.createElement('li') ; 
				liElement.innerHTML = response[x].quantite +"  "+response[x].nom_produit+"(s)" ;
				ulElement.appendChild(liElement) ; 
			}
			liste_details.appendChild(ulElement);
			$("#loadMe").modal('toggle');
			$("#id_visualiserAlimentDon").modal();
		})
	}
		
  function getDetail_aliment_vendu(critere ,callBack) {
                  
		var xMLHttprequest = new XMLHttpRequest() ;
		xMLHttprequest.open('GET','visualiser_detail_aliment_vendu?id_entreprise='+critere)	;
		xMLHttprequest.addEventListener('readystatechange',function(){
			if(xMLHttprequest.readyState==XMLHttpRequest.DONE && xMLHttprequest.status==200) {
				var resp = JSON.parse(xMLHttprequest.responseText) ; 
				callBack(resp) ; 
			}else{
				console.log('request failed');
			}
		})
		xMLHttprequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xMLHttprequest.send() ;

	}
  function getDetail_aliment_don(critere ,callBack) {
                  
		var xMLHttprequest = new XMLHttpRequest() ;
		xMLHttprequest.open('GET','visualiser_detail_aliment_don?id_entreprise='+critere)	;
		console.log('eto1');			
		xMLHttprequest.addEventListener('readystatechange',function(){
			console.log('eto2.1');	
			if(xMLHttprequest.readyState==XMLHttpRequest.DONE && xMLHttprequest.status==200) {
				console.log('eto2');					
				var resp = JSON.parse(xMLHttprequest.responseText) ; 
				callBack(resp) ; 
			}else{
				console.log('request failed');
			}
		})
		xMLHttprequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xMLHttprequest.send() ;

	}
	function getImage(){
	 
          html2canvas(document.getElementById('capture'), {
              onrendered:function(canvas) {

                  var contentWidth = canvas.width;
                  var contentHeight = canvas.height;

                  // Une page pdf indique la hauteur de la toile générée par la page html;
                  var pageHeight = contentWidth / 595.28 * 841.89;
                  // la hauteur de la page HTML n'est pas générée pdf
                  var leftHeight = contentHeight;
                  // offset de page pdf
                  var position = 0;
                 // a4 format de papier [595.28, 841.89], toile générée par page HTML en largeur et hauteur de l'image pdf
                  var imgWidth = 555.28;
                  var imgHeight = 555.28/contentWidth * contentHeight;

                  var pageData = canvas.toDataURL('image/jpeg', 1.0);

                  var pdf = new jsPDF('', 'pt', 'a4');
                 // Il y a deux hauteurs à distinguer, l'une est la hauteur réelle de la page html et la hauteur de la page qui a généré le pdf (841.89)
                  // Lorsque le contenu ne dépasse pas la plage affichée sur la page pdf, aucune pagination n'est requise.
                  if (leftHeight < pageHeight) {
                      pdf.addImage(pageData, 'JPEG', 20, 0, imgWidth, imgHeight );
                  } else {
                      while(leftHeight > 0) {
                          pdf.addImage(pageData, 'JPEG', 20, position, imgWidth, imgHeight)
                          leftHeight -= pageHeight;
                          position -= 841.89;
                          // Éviter d'ajouter des pages vierges
                          if(leftHeight > 0) {
                              pdf.addPage();
                          }
                      }
                  }

                  pdf.save('content.pdf');
              }
          })
      
}
</script>
@endsection