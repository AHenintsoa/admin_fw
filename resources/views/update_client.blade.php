@extends('template/default')

@section('titre')
   Mis à jour
@endsection
@section('custom_styles')
    <link rel="stylesheet" href="{{ asset('dist/plugins/select2/select2.min.css') }}">
	<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.js'></script>
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.css' rel='stylesheet' />
@endsection

@section('contenu')
 @if(Session::get('id_utilisateur_type')!='5')
			<div class="row">
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link " href="{{ url('entreprise') }}">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link active" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									<div class="card-body">
										<!--<div class="row">-->
											<form action="{{ url('update_client') }}" method="post">
											{{ csrf_field() }}
											<input type="hidden" class="form-control" name="id_consommateur" value="{{ $consommateur->id_consommateur }}" >
											
											<!--<div class="col">-->
												<strong>
													<i class="fa fa-address-card mr-1"></i>
													 Modifier information 
												</strong>
												<div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
														<div class="col-6"><input type="text" class="form-control" name="nom" value="{{ $consommateur->nom }}" ></div>
														@if ($errors->has('nom'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('nom') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Prénom</div>
														<div class="col-6"><input type="text" class="form-control" name="prenom" value="{{ $consommateur->prenom }}" ></div>
														@if ($errors->has('prenom'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('prenom') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Mail</div>
														<div class="col-6"><input type="email" class="form-control" name="mail" value="{{ $consommateur->mail }}" ></div>
														@if ($errors->has('mail'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('mail') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Adresse de livraison</div>
														<div class="col-6"><input type="text" class="form-control" name="adresse_de_livraison" value="{{ $consommateur->adresse_de_livraison }}" ></div>
														@if ($errors->has('adresse_de_livraison'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('adresse_de_livraison') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Numero de téléphone</div>
														<div class="col-6"><input type="text" class="form-control" name="numero_telephone" value="{{ $consommateur->numero_telephone }}" ></div>
														@if ($errors->has('numero_telephone'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('numero_telephone') }}</strong>
														</span>
														@endif
													</div>
													<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
														<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Mobile banking</div>
														<div class="col-6"><input type="text" class="form-control" name="mobile_banking" value="{{ $consommateur->mobile_banking }}" ></div>
														@if ($errors->has('mobile_banking'))
														<span class="help-block">
															<strong style="color:#FF0000"; >{{ $errors->first('mobile_banking') }}</strong>
														</span>
														@endif
													</div>
													
													
												</div>
												</hr>
												
												<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
													<div class="col-6"></div>
													<div class="col-2" style="padding-left:20px;font-weight: bolder;color: #646369;"><a href="{{ url('client') }}" title="Liste des clients"><span class="fa fa-backward"></span></a></div>
													<div class="col-2"><input type="submit" class="btn btn-success" value="Mettre à jour" ></div>
												</div>
												
												
											 <!-- </div>-->
											  <!-- /.col -->
											  </form>
											<!--</div>-->
											<!-- /.row -->
												
										  </div>
										  <!-- ./card-body -->
									</div>
									<!-- /.card -->
								</div>
								<!-- /.tab-pane -->
							</div>
							<!-- /.tab-content -->
						</div>
						 <!-- ./card-body -->
					</div>
					<!-- /.card-->
			  </div>
          <!-- /.col -->
		 </div>
		 <!-- /.row -->
	
@endsection
@section('modals')

<!-- modal map -->
<div class="modal fade" id="changer_coordonees" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Veuillez cliquer sur la carte</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-content">
        <center><div id='map' style='width: 400px; height: 300px; '></div></center>
		
      </div>
      
      <div class="modal-footer">
       
		
      </div>
	
	</div>
  </div>
</div>
@endif
@endsection
@section('custom_script')
<!-- Select2 -->
<script src="{{ asset('dist/plugins/select2/select2.full.min.js') }}"></script>
<script type="text/Javascript">
	$(function () {
		$('.select2').select2() ;// initialisation element select2
		
	});
  
 
	
  
</script>
@endsection

