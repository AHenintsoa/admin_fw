@extends('template/default')

@section('titre')
  Ajout 
@endsection
@section('custom_styles')
    <link rel="stylesheet" href="{{ asset('dist/plugins/select2/select2.min.css') }}">
	<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.js'></script>
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.2.0/mapbox-gl.css' rel='stylesheet' />
@endsection
 @if(Session::get('id_utilisateur_type')!='5')
@section('contenu')
<section class="content">
      <div class="container-fluid">
			<div class="row">
			  <div class="col-md-12">
				<div class="card">
					<div class="card-header p-2">
						<ul class="nav nav-pills">
							<li class="nav-item"><a class="nav-link active" href="{{ url('entreprise') }}">Entreprise</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('receveur') }}">Receveur</a></li>
							<li class="nav-item"><a class="nav-link" href="{{ url('client') }}">Client</a></li>
							<li class="nav-item"><a class="nav-link " href="{{ url('produit') }}">Produit</a></li>
						</ul>
					</div><!-- /.card-header -->
					<div class="card-body">
						<div class="tab-content">
							
							<!-- ---- Pane     receveur    -->
							<div class="active tab-pane" id="receveur">
								<div class="card card-primary">
									<div class="card-body">
									<h3>Ajout </h3>
										  <div class="row">
										   
											<form action="{{ url('ajouter_entreprise') }}" method="post">
											{{ csrf_field() }}
											
											<input type="hidden" id="id_compteur_nnouvelle_adresse" value="0">
											
													<div class="col">
														<strong>
															<i class="fa fa-address-card mr-1"></i>
															 Information complémentaire
														</strong>
														<div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom*</div>
																<div class="col-8" id="id_div_nom_entreprise"><input type="text" class="form-control" name="nom_entreprise" id="id_nom_entreprise" placeholder="Entrez le nom de l'entreprise " ></div>
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">NIF</div>
																<div class="col-8" id="id_div_nif_entreprise"><input type="text" class="form-control" name="nif_entreprise" id="id_nif_entreprise" placeholder="Entrez le NIF de l'entreprise" > </div>
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">STAT</div>
																<div class="col-8" id="id_div_stat_entreprise"><input type="text" class="form-control" name="stat_entreprise" id="id_stat_entreprise" placeholder="Entrez le STAT de l'entreprise" > </div>
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Chiffre d'affaire</div>
																<div class="col-8" id="id_div_ca_entreprise"><input type="text" class="form-control" name="ca_entreprise" id="id_ca_entreprise" placeholder="Entrez le chiffre d'affaire de l'entreprise"  ></div>
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Pseudo*</div>
																<div class="col-8" id="id_div_pseudo"><input type="text" class="form-control" name="pseudo" id="id_pseudo" value="" placeholder="Entrez le pseudo pour l'entreprise"></div>
																<input type="hidden" value="{{ csrf_token() }}" id="id_token_pseudo">
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Mot de passe*</div>
																<div class="col-8"><input id="id_password" type="password" class="form-control" name="password" placeholder="Entrez le chiffre un mot de passe"    ></div>
																
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Conformation du mot de passe*</div>
																<div class="col-8" id="id_div_password"><input id="id_password_confirm" type="password" class="form-control" name="password_confirmation"  placeholder="Confirmez le mot de passe"  ></div>
															</div>
														</div>
														<hr>
														<strong>
															<i class="fa fa-building mr-1"></i>
															 Type de l'entreprise 
														</strong>
														<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
															 <div class="col-1" ><a class="btn btn-default btn-block"  onclick="ajout_type_entreprise(this )"><i class="fa fa-plus"></i></a></div>
															  <div class="col-11"  id="id_div_type_entreprise">
																	<select  class="form-control select2" id="id_id_type_entreprise" name="id_type_entreprise[]" class="form-control select2 select2-hidden-accessible" multiple="multiple" data-placeholder="Choisir le type de votre entreprise*" style="width: 100%;" tabindex="-1" aria-hidden="true" >
																		<option></option>
																	<?php foreach($liste_type_entreprise as $type): ?>
																		<option value="<?php echo $type->id_type_entreprise; ?>" ><?php echo $type->label; ?></option>
																	  <?php endforeach; ?>
																	</select>
															  </div>
														  </div>
														<hr>
														<div id='id_div_responsable'>
															<strong>
																<i class="fa fa-user mr-1"></i> 
																	Responsable numéro 1
																	
															</strong>
															<div>
																<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																	<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom*</div>
																	
																		<div class="col-7" id="id_div_nom_responsable1"><input type="text" class="form-control" name="nom_responsable1" id="id_nom_responsable1" placeholder="Entrez le nom du responsable" > </div>
																		<div class="col-1" ><a id="id_bouton_ajout_responsable" class="btn btn-default btn-block"  onclick="ajouterResponsable(this )"><i class="fa fa-plus"></i></a></div>
																	
																</div>
																<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																	<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Prénom*</div>
																	<div class="col-8" id="id_div_prenom_responsable1"><input type="text" class="form-control" name="prenom_responsable1" id="id_prenom_responsable1" placeholder="Entrez le prénom du responsable" ></div>
																</div>
																<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																	<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Poste*</div>
																	<div class="col-1" ><a id="id_bouton_ajout_responsable" class="btn btn-default btn-block" row_id="1" onclick="ajouterPoste(this )"><i class="fa fa-plus"></i></a></div>
																	<div class="col-7" id="id_div_poste1">
																			<select  class="form-control select2"  name="id_poste1" id="id_id_poste1" class="form-control select2 select2-hidden-accessible"  data-placeholder="Choisir le poste*" style="width: 100%;" tabindex="-1" aria-hidden="true">
																				<option></option>
																					<?php foreach($liste_poste as $poste): ?>
																						<option value="<?php echo $poste->id_poste; ?>"   ><?php echo $poste->label; ?></option>
																					<?php endforeach; ?>
																				</select>
																	  </div>
																	  
																</div>
																<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																	<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Sexe*</div>
																	<div class="col-8">
																			<select  class="form-control select2" id="" name="sexe_responsable1" class="form-control select2 select2-hidden-accessible"  data-placeholder="Choisir le sexe*" style="width: 100%;" tabindex="-1" aria-hidden="true">
																				<option value="M"  >Mr</option>
																				<option value="F" >Mme</option>
																					
																				</select>
																	  </div>
																</div>
																<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																	<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Email*</div>
																	<div class="col-8" id="id_div_email_responsable1"><input type="email" class="form-control" id="id_email_responsable1" name="email_responsable1" placeholder="Entrez le mail du responsable" ></div>
																</div>
																<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																	<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Téléphone*</div>
																	<div class="col-8" id="id_div_telephone_responsable1"><input type="text" class="form-control" name="telephone_responsable1" id="id_telephone_responsable1" placeholder="Entrez le numero de téléphone du responsable" ></div>
																</div>
															</div>
															<hr>
															
														</div>
													</div>

													<div class="col">
														<strong>
															<i class="fa fa-map-marker mr-1"></i> 
																Adresse
														</strong>
														
														<div id="id_liste_adresse">
															<h5>Adresse</h5>
														
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Place*</div>
																<div class="col-7" id="id_div_adresse_label"><input type="text" class="form-control" id ="id_adresse_label" name="adresse_label" placeholder="Entrez le nom du quartier" ></div>
																
																	<!--<div class="col-1" ><a class="btn btn-default btn-block" row_adresse="1" onclick="supprimerAdresse(this)"><i class="fa fa-remove"></i></a></div>-->
																
																	<div class="col-1" ><a class="btn btn-default btn-block" row_adresse="1" onclick="ajouterAdresse(this )"><i class="fa fa-plus"></i></a></div>
																
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Ville*</div>
																<div class="col-8" id="id_div_adresse_ville"><input type="text" class="form-control" name="adresse_ville" id="id_adresse_ville" placeholder="Entrez le nom de la ville" ></div>
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Code Postal*</div>
																<div class="col-8" id="id_div_adresse_code_postal"><input type="text" class="form-control" name="adresse_code_postal" id="id_adresse_code_postal" placeholder="Entrez le code postal" ></div>
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Longitude - Latitude*</div>
																<div class="col-4" id="id_div_adresse_coordonnes"><input type="text" class="form-control" id ="id_longitude"   name="adresse_longitude" placeholder="Entrez le longitude" ></div>
																<div class="col-3"><input type="text" class="form-control" id ="id_latitude"  name="adresse_latitude" placeholder="Entrez le latitude" ></div>
																<div class="col-1"><a  class="btn btn-default btn-block" onclick="modifierAdresse(this)" row_adresse="1"  title="Sélectionner les coordonnées sur une carte" ><i class="fa fa-map"></i> </a></div>
															</div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
																<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Pays*</div>
																<div class="col-8">
																	<select  class="form-control select2" id="id_id_poste1" name="adresse_pays_id"  data-placeholder="choisir pays" style="width: 100%;" tabindex="-1" aria-hidden="true">
																		@foreach($liste_pays as $pays)
																			<option value="<?php echo $pays->id_pays; ?>"  ><?php echo $pays->label; ?></option>
																		@endforeach
																	</select>
																 </div>
															</div>
														</div>
														<hr>
														<strong>
															<i class="fa fa-gift mr-1"></i> 
																Type d'aliments
														</strong>
														<div>
															<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >
																 <div class="col-1" ><a class="btn btn-default btn-block"  onclick="ajout_type_aliment(this )"><i class="fa fa-plus"></i></a></div>
																 <div class="col-11" id="id_div_type_aliment">
																	<select  class="form-control select2" id="id_id_type_aliment" name="id_type_aliment[]"  multiple="multiple" data-placeholder="Choisir le type de votre produit*" style="width: 100%;" tabindex="-1" aria-hidden="true" >
																		<option></option>
																	<?php foreach($liste_type_aliment as $type): ?>
																		<option value="<?php echo $type->id_type_aliment; ?>" ><?php echo $type->label; ?></option>
																	  <?php endforeach; ?>
																	</select>
																  </div>
															</div>
														</div>
														<hr>
														<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
															<div class="col-4"></div>
															<div class="col-4"></div>
															<div class="col-4"><p class="btn btn-secondary" onclick="valider_formulaire();">ajouter </p><input type="submit" class="btn"  style ="display : none ;" value="Ajouter" id="id_bouton_valider"></div>
														</div>
													  </div>
													  <!-- /.col -->
													  </form>
													</div>
													<!-- /.row -->
												
										  </div>
										  <!-- ./card-body -->
									</div>
									<!-- /.card -->
								</div>
								<!-- /.tab-pane -->
							</div>
							<!-- /.tab-content -->
						</div>
						 <!-- ./card-body -->
					</div>
					<!-- /.card-->
			  </div>
          <!-- /.col -->
		 </div>
		 <!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>
    <!-- /.content -->
@endsection
@section('modals')
<!---- modal   supprimer      tâche  ----->
<div class="modal fade" id="supprimer_entreprise" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Supprimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">non</button>
		<a id="supprimer_entreprise_button" href="supprimer_tache"><button type="button" class="btn btn-secondary" >Sûr</button></a>
      </div>
	
	</div>
  </div>
</div>
<!-- modal map -->
<div class="modal fade" id="changer_coordonees" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Veuillez cliquer sur la carte</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-content">
        <center><div id='map' style='width: 400px; height: 300px; '></div></center>
		
      </div>
      
      <div class="modal-footer">
       
		
      </div>
	
	</div>
  </div>
</div>
<!----modal   ajouter   poste---->
<div class="modal fade" id="id_ajout_poste" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau poste </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		
		 <input type="hidden" value="{{ csrf_token() }}" id="id_token_poste">
		 <input type="hidden" value="" id="responsable_x">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>

			<div class="col-sm-10">
			  <input name="po" type="text" class="form-control" id="id_nouveau_poste" >
			</div>
			@if ($errors->has('type_aliment_label'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('type_aliment_label') }}</strong>
				</span>
			@endif
		  </div>
		 
      </div>
     <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" onclick="ajouter_poste_ajax()">Ajouter</button>
      </div>
    </div>
  </div>
</div>
<!----modal   ajouter   type_aliemnts---->
<div class="modal fade" id="id_ajout_type_aliment" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau type d'aliment </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		
		 <input type="hidden" value="{{ csrf_token() }}" id="id_token_aliment">
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>

			<div class="col-sm-10">
			  <input name="type_aliment_label" type="text" class="form-control" id="id_type_aliment_label" >
			</div>
			@if ($errors->has('type_aliment_label'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('type_aliment_label') }}</strong>
				</span>
			@endif
		  </div>
		 
      </div>
     <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" onclick="ajouter_type_aliment_ajax()">Ajouter</button>
      </div>
    </div>
  </div>
</div>
<!----modal   ajouter   type_entreprise---->
<div class="modal fade" id="id_ajout_type_entreprise" tabindex="-1" role="dialog" aria-labelledby="ajoutUtilisateur" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau type d'entreprise</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		
		  
		  <div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Nom</label>
			<input type="hidden" value="{{ csrf_token() }}" id="id_token_entreprise">
			<div class="col-sm-10">
			  <input name="type_entreprise_label" type="text" class="form-control" id="id_type_entreprise_label" >
			</div>
			@if ($errors->has('type_entreprise_label'))
				<span class="help-block">
					<strong style="color:#FF0000"; >{{ $errors->first('type_entreprise_label') }}</strong>
				</span>
			@endif
		  </div>
		 
		  
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
		<button type="button" class="btn btn-secondary" onclick="ajouter_type_entreprise_ajax()" >Ajouter</button>
	</div>
    </div>
  </div>
</div>
<!-- modal loader -->
<div class="modal fade modal-custumed" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="t2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		
      <div class="modal-header">
       
      </div>
      <center><h4>Encours.</h4> <div class="loader"></div></center>
      <div class="modal-footer">
       
      </div>
	</div>
  </div>
</div>
@endif
@endsection
@section('custom_script')
<!-- Select2 -->
 <script src="{{ asset('dist/plugins/select2/select2.full.min.js') }}"></script>
<script type="text/Javascript">
	

$(function () {
	$('.select2').select2() ;// initialisation element select2
	var id_nouveau_adresse = 0;
	// console.log(id_nouveau_adresse);
	
	
	

});
 
function valider_formulaire() {
	var teste = 1; // pour valider
	$(".classe_erreur").remove();
	//info entreprise
	var nom_entreprise = $("#id_nom_entreprise").val();
	if(!nom_entreprise) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_nom_entreprise").append(html);
		teste = 0;
		
	}
	// var nif_entreprise = $("#id_nif_entreprise").val();
	// if(!nif_entreprise) {
		// var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		// $("#id_div_nif_entreprise").append(html);
		// teste = 0;
	// }
	// var stat_entreprise = $("#id_stat_entreprise").val();
	// if(!stat_entreprise) {
		// var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		// $("#id_div_stat_entreprise").append(html);
		// teste = 0;
	// }
	// var type_entreprise = $("#id_id_type_entreprise").val();
	// if(type_entreprise.length == 0) {
		// var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		// $("#id_div_type_entreprise").append(html);
		// teste = 0;
	// }
	var ca_entreprise = $("#id_ca_entreprise").val();
	var reg = /^\d+$/;
	// console.log(reg.test(ca_entreprise));
	if(reg.test(ca_entreprise)==false && ca_entreprise) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
		$("#id_div_ca_entreprise").append(html);
		teste = 0;
	}
	// var type_aliment = $("#id_id_type_aliment").val();
	// console.log(type_aliment);
	// if(type_aliment.length==0) {
		// var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		// $("#id_div_type_aliment").append(html);
		// teste = 0;
	// }
	
	//user
	
	var password = $("#id_password").val();
	var confirm_password = $("#id_password_confirm").val();
	if(!password || !confirm_password ) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_password").append(html);
		teste = 0;
	}
	
	if(password != confirm_password) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Mots de passe différents</strong> </span></div>';
		$("#id_div_password").append(html);
		teste = 0;
	}
	if(password.length < 6 && password) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Veuillez entrer au moins 6 caractères </strong> </span></div>';
		$("#id_div_password").append(html);
		teste = 0;
	}
	//teste doublon pseudo
	
	var pseudo = $('#id_pseudo').val();
	if(pseudo!=''){
		 var token = $('#id_token_pseudo').val();
		 var x =new FormData();
		x.append('pseudo' , pseudo);	
		x.append('_token' , token);	
		var lien = "{{ url('teste_pseudo')}}";
			// console.log(lien);
			$.ajax({
			   url : lien,
			   method : 'POST',
			  data : x,
				processData : false ,
				contentType : false ,
			 
			   success : function(response, statut){ // success est toujours en place, bien sûr !
					// var resp = JSON.parse(response);
					// console.log(response);
					if(response){
						var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Veillez choisir un autre</strong> </span></div>';
						$("#id_div_pseudo").append(html);
						teste = 0;
					}
					
			   },

			   error : function(error){
					// console.log(error);
					
			   }
			});  
	}
	if(!pseudo){
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_pseudo").append(html);
		teste = 0;
	}
	//adresse
	var adresse_label1 = $("#id_adresse_label").val();
	if(!adresse_label1) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_adresse_label").append(html);
		teste = 0;
	}
	var adresse_ville1 = $("#id_adresse_ville").val();
	if(!adresse_ville1) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_adresse_ville").append(html);
		teste = 0;
	}
	var adresse_code_postal1 = $("#id_adresse_code_postal").val();
	if(!adresse_code_postal1) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_adresse_code_postal").append(html);
		teste = 0;
	}
	var adresse_long = $("#id_longitude").val();
	var adresse_lat = $("#id_latitude").val();
	if(!adresse_long || !adresse_lat) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span><div>';
		$("#id_div_adresse_coordonnes").append(html);
		teste = 0;
	}
		if( (!valider_coordonnees(adresse_long) && adresse_long!='') || (!valider_coordonnees(adresse_lat) && adresse_lat!='') ) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span><div>';
		$("#id_div_adresse_coordonnes").append(html);
		teste = 0;
	}
	
	
	//nouvelle adresse
	var liste_div_label = $('.div_nouvelle_adresse_label');
	var liste_nouvelle_adresse_label = $('.nouvelle_adresse_label');
	for(var i = 0 ; i < liste_nouvelle_adresse_label.length ; i++){
		var nouvelle_adresse_label = liste_nouvelle_adresse_label.get(i) ;
		if(!nouvelle_adresse_label.value){
			var html = '<div class="classe_erreur">'
							+'<span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span>'
						+'</div>';
			var div = document.createElement("div");	
			div.innerHTML = html;
			liste_div_label.get(i).append(div);
			teste = 0;
		}
	}
	
	var liste_div_ville = $('.div_nouvelle_adresse_ville');
	var liste_nouvelle_adresse_ville = $('.nouvelle_adresse_ville');
	for(var i = 0 ; i < liste_nouvelle_adresse_ville.length ; i++){
		var nouvelle_adresse_ville = liste_nouvelle_adresse_ville.get(i) ; 
		if(!nouvelle_adresse_ville.value){
			var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
			var div = document.createElement("div");	
			div.innerHTML = html;
			liste_div_ville.get(i).append(div);
			teste = 0;
		}
	}
	var liste_div_code_postal = $('.div_nouvelle_adresse_code_postal');
	var liste_nouvelle_adresse_code_postal = $('.nouvelle_adresse_code_postal');
	for(var i = 0 ; i < liste_nouvelle_adresse_code_postal.length ; i++){
		var nouvelle_adresse_code_postal = liste_nouvelle_adresse_code_postal.get(i) ; 
		if(!nouvelle_adresse_code_postal.value){
			var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
			var div = document.createElement("div");	
			div.innerHTML = html;
			liste_div_code_postal.get(i).append(div);
			teste = 0;
		}
	}
	var liste_div_coordonnees = $('.div_nouvelle_adresse_coordonnees');
	var liste_latitude_nouvelle_adresse_ = $('.latitude_nouvelle_adresse');
	var liste_longitude_nouvelle_adresse_ = $('.longitude_nouvelle_adresse');
	for(var i = 0 ; i < liste_latitude_nouvelle_adresse_.length ; i++){
		var nouvelle_adresse_latitude = liste_latitude_nouvelle_adresse_.get(i) ; 
		var nouvelle_adresse_longitude = liste_longitude_nouvelle_adresse_.get(i) ; 
		if(!nouvelle_adresse_latitude.value || !nouvelle_adresse_longitude.value){
			var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
			var div = document.createElement("div");	
			div.innerHTML = html;
			liste_div_coordonnees.get(i).append(div);
			teste = 0;
		}
		console.log(nouvelle_adresse_latitude.value);
		if( (!valider_coordonnees(nouvelle_adresse_latitude.value) && nouvelle_adresse_latitude.value!='') || (!valider_coordonnees(nouvelle_adresse_longitude.value) && nouvelle_adresse_longitude.value!='') ) {
			var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
			var div = document.createElement("div");	
			div.innerHTML = html;
			liste_div_coordonnees.get(i).append(div);
			teste = 0;
		}
	}
	//RESPONSABLE
	var nom_responsable1 = $("#id_nom_responsable1").val();
	if(!nom_responsable1) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_nom_responsable1").append(html);
		teste = 0;
	}
		
	var prenom_responsable1 = $("#id_prenom_responsable1").val();
	if(!prenom_responsable1) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_prenom_responsable1").append(html);
		teste = 0;
	}
	var id_poste1 = $("#id_id_poste1").val();
	if(!id_poste1) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_poste1").append(html);
		teste = 0;
	}
	var email_responsable1 = $("#id_email_responsable1").val();
	if(!email_responsable1) {
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_email_responsable1").append(html);
		teste = 0;
	}
	// console.log(validateEmail(email_responsable1));
	if(!validateEmail(email_responsable1) && email_responsable1) {
			var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
			$("#id_div_email_responsable1").append(html);
			teste = 0;
		}
	var telephone_responsable1 = $("#id_telephone_responsable1").val();
	
	if(!telephone_responsable1){
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
		$("#id_div_telephone_responsable1").append(html);
		teste = 0;
	}
	if(!reg.test(telephone_responsable1) && telephone_responsable1){
		var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
		$("#id_div_telephone_responsable1").append(html);
		teste = 0;
	}

	
	var div_responsable2 = $("#id_nouvel_employe_responsable2").val(); ;
	// console.log(div_responsable2);
	if(div_responsable2=='misy'){
		console.log(div_responsable2);
		var nom_responsable2 = $("#id_nom_responsable2").val();
		if(!nom_responsable2) {
			var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
			$("#id_div_nom_responsable2").append(html);
			teste = 0;
		}
			
		var prenom_responsable2 = $("#id_prenom_responsable2").val();
		if(!prenom_responsable2) {
			var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
			$("#id_div_prenom_responsable2").append(html);
			teste = 0;
		}
		var id_poste2 = $("#id_id_poste2").val();
		if(!id_poste2) {
			var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
			$("#id_div_poste2").append(html);
			teste = 0;
		}
		var email_responsable2 = $("#id_email_responsable2").val();
		if(!email_responsable2) {
			var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
			$("#id_div_email_responsable2").append(html);
			teste = 0;
		}
		// console.log(validateEmail(email_responsable2));
		if(!validateEmail(email_responsable2) && email_responsable2) {
			var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
			$("#id_div_email_responsable2").append(html);
			teste = 0;
		}
		var telephone_responsable2 = $("#id_telephone_responsable2").val();
		if(!telephone_responsable2){
			var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Champs obligatoire</strong> </span></div>';
			$("#id_div_telephone_responsable2").append(html);
			teste = 0;
		}
		if(!reg.test(telephone_responsable2) && telephone_responsable2){
			var html = '<div class="classe_erreur"><span class="help-block"> <strong style="color:#FF0000"; >Format incorrect</strong> </span></div>';
			$("#id_div_telephone_responsable2").append(html);
			teste = 0;
		}
	}
	
	 // console.log('eto'+teste);
	if(teste == 1) {
		
		$("#id_bouton_valider").click() ;
	}
	
}	
 function valider_coordonnees(value){
        /*var result = parseFloat(value.toString().replace(',', '.'));
        if( !isNaN(result) ){
            return true;
        }
        else{
            return false;
        }*/
        var reg = new RegExp("^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}");
        if( reg.exec(latitude) ) {
		 	return true;
		} else {
		 return false;
		}
    }
 function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function ajouter_poste_ajax() {
		var label_poste = $('#id_nouveau_poste').val();
		var responsable = $('#responsable_x').val();
		 var token = $('#id_token_entreprise').val();
		var x =new FormData();
		x.append('label' , label_poste);	
		x.append('_token' , token);	
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var lien = '{{ url('ajouter_poste')}}' ;
		$.ajax({
		   url : lien,
		    method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				// console.log(resp);
				var html = '<option value ="'+ response.id_poste+'"  selected>'+response.label+' </option>';
				if(responsable == 1){ 
					var select = $("#id_id_poste1");
					select.append(html);
					select.select2("val" , select.select2("val"));
				}
				else {
					var select = $("#id_id_poste2");
					select.append(html);
					select.select2("val" , select.select2("val"));
				}
				$("#loadMe").modal('toggle');
				$("#id_ajout_poste").modal('toggle');
				
		   },

		   error : function(resultat, statut, erreur){
				console.log("erreur");
				
		   }
		});  
		
		
	}


	function ajouter_type_aliment_ajax() {
	 var label_type_aliment = $('#id_type_aliment_label').val();
	 var token = $('#id_token_entreprise').val();
	 var x =new FormData();
	x.append('label' , label_type_aliment);	
	x.append('_token' , token);	
	 // console.log('tafiditra');
		// var label_type_entreprise = $('#id_type_entreprise_label').val();
		// console.log(label_type_aliment);
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var lien = "{{ url('ajouter_type_aliment')}}";
		// console.log(lien);
		$.ajax({
		   url : lien,
		   method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		 
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				console.log(response);
				var html = '<option value ="'+ response.id_type_aliment+'"  selected>'+response.label+' </option>';
				var select = $("#id_id_type_aliment");
				select.append(html);
				select.select2("val" , select.select2("val").concat(response.id_type_aliment));
				$("#loadMe").modal('toggle');
				$("#id_ajout_type_aliment").modal('toggle');
				
		   },

		   error : function(error){
				console.log(error);
				
		   }
		});  
		
		
	} 
	
 function ajouter_type_entreprise_ajax() {
	 var label_type_entreprise = $('#id_type_entreprise_label').val();
	 var token = $('#id_token_entreprise').val();
	 var x =new FormData();
	x.append('label' , label_type_entreprise);	
	x.append('_token' , token);	
	 // console.log('tafiditra');
		// var label_type_entreprise = $('#id_type_entreprise_label').val();
		// console.log(label_type_entreprise);
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var lien = "{{ url('ajouter_type_entreprise')}}";
		// console.log(lien);
		$.ajax({
		   url : lien,
		   method : 'POST',
		  data : x,
			processData : false ,
			contentType : false ,
		 
		   success : function(response, statut){ // success est toujours en place, bien sûr !
				// var resp = JSON.parse(response);
				console.log(response);
				var html = '<option value ="'+ response.id_type_entreprise+'"  selected>'+response.label+' </option>';
				var select = $("#id_id_type_entreprise");
				select.append(html);
				select.select2("val" , select.select2("val").concat(response.id_type_entreprise));
				$("#loadMe").modal('toggle');
				$("#id_ajout_type_entreprise").modal('toggle');
				
		   },

		   error : function(error){
				console.log(error);
				
		   }
		});  
		
		
	} 
function ajout_type_aliment(element) {
	
	$("#id_ajout_type_aliment").modal({ backdrop: 'static', keyboard: false });
}
function ajout_type_entreprise(element) {
	
	$("#id_ajout_type_entreprise").modal({ backdrop: 'static', keyboard: false });
}
function ajouterPoste(element) {
	// var criteres = $(element).attr('rowID');
	var responsable = $(element).attr('row_id');
	if(responsable==1)$('#responsable_x').val( 1 );
	else $('#responsable_x').val( 2 );
	$("#id_ajout_poste").modal({ backdrop: 'static', keyboard: false });
}

function ajouterAdresse(element ) {
	var id_adresse = $('#id_compteur_nnouvelle_adresse').val();
	// console.log(id_adresse);
	id_adresse = parseInt(id_adresse)+1;
	// console.log(id_adresse);
	var html =
	'<div id="id_div_nouvelle_adresse_'+id_adresse+'">'
		+'<h5>Nouvelle adresse</h5>'
		+'<input type="hidden" class="form-control" id ="id_adresse" name="id_nouvelle_adresse_'+id_adresse+'" value="'+id_adresse+'" >'
		+'<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >'
			+'<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Place*</div>'
			+'<div class="col-7 div_nouvelle_adresse_label" ><input type="text" class="form-control nouvelle_adresse_label"  name="nouvelle_adresse_label_'+id_adresse+'"  value=""  placeholder="Entrez le nom du quartier"></div>'
			+'<div class="col-1" ><a class="btn btn-default btn-block" row_adresse="'+id_adresse+'" onclick="supprimerNouvelleAdresse(this)"><i class="fa fa-remove"></i></a></div>'
			
		+'</div>'
		+'<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >'
		+'<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Ville*</div>'
		+'<div class="col-8 div_nouvelle_adresse_ville" ><input type="text" class="form-control nouvelle_adresse_ville" name="nouvelle_adresse_ville_'+id_adresse+'" id="id_nouvelle_adresse_ville_'+id_adresse+'" value=""  placeholder="Entrez le nom de la ville"></div>'
		+'</div>'
		+'<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >'
		+'<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Code Postal*</div>'
		+'<div class="col-8 div_nouvelle_adresse_code_postal" ><input type="text" class="form-control nouvelle_adresse_code_postal" name="nouvelle_adresse_code_postal_'+id_adresse+'" id="id_nouvelle_adresse_code_postal_'+id_adresse+'" value=""  placeholder="Entrez le code postal"></div>'
		+'</div>'
		+'<div class="row" style="margin-top: 4px;margin-bottom: 4px;">'
		+'<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;" >Longitude - Latitude*</div>'
		+'<div class="col-4 div_nouvelle_adresse_coordonnees"><input type="text" class="form-control longitude_nouvelle_adresse" id ="id_longitude_nouvelle_adresse_'+id_adresse+'" name="nouvelle_adresse_longitude_'+id_adresse+'"   placeholder="Entrez le longitude"></div>'
		+'<div class="col-3"><input type="text" class="form-control latitude_nouvelle_adresse" id ="id_latitude_nouvelle_adresse_'+id_adresse+'" name="nouvelle_adresse_latitude_'+id_adresse+'"   placeholder="Entrez le latitude"></div>'
		+'</div>'
		+'<div class="col-1"><a  class="btn btn-default btn-block"  onclick="modifierNouvelleAdresse(this)" row_adresse="'+id_adresse+'"  title="Sélectionner les coordonnées sur une carte" ><i class="fa fa-map"></i> </a></div>'
		+'</div>'
		+'<div class="row" style="margin-top: 4px;margin-bottom: 4px;">'
			+'<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Pays*</div>'
			+'<div class="col-8">'
					+'<select  class="form-control select2" id="id_nouvelle_adresse_pays_id_'+id_adresse+'" name="nouvelle_adresse_pays_id_'+id_adresse+'"  data-placeholder="" style="width: 100%;" tabindex="-1" aria-hidden="true">'								
							+'@foreach($liste_pays as $pays)'
								+'<option value="<?php echo $pays->id_pays; ?>">  <?php echo $pays->label; ?></option>'
							+'@endforeach'
						+'</select>'
			 +'</div>'
		+'</div>'
	+'</div>' ;
	$("#id_liste_adresse").append(html);
	
	$('#id_compteur_nnouvelle_adresse').val( id_adresse );
}
function ajouterResponsable(element ) {
	var html =
		'<div id ="id_div_responsable2" >'
			+'<strong>'
				+'<i class="fa fa-user mr-1"></i> '
					+'Responsable numéro 2'
					+'<input type="hidden" class="form-control" id="id_nouvel_employe_responsable2" value="misy" >'
			+'</strong>'
		+'<div>'
			+'<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >'
				+'<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom*</div>'
				+'<div class="col-7" id="id_div_nom_responsable2"><input type="text" class="form-control" name="nom_responsable2" id="id_nom_responsable2" value=""  placeholder="Entrez le nom du responsable"></div>'
				+'<div class="col-1" ><a class="btn btn-default btn-block"  onclick="supprimerResponsable(this)"><i class="fa fa-remove"></i></a></div>'
			+'</div>'
			+'<div class="row" style="margin-top: 4px;margin-bottom: 4px;" >'
				+'<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Prénom*</div>'
				+'<div class="col-8" id="id_div_prenom_responsable2"><input type="text" class="form-control" name="prenom_responsable2" id="id_prenom_responsable2" value="" placeholder="Entrez le prénom du responsable"></div>'
			+'</div>'
			+'<div class="row" style="margin-top: 4px;margin-bottom: 4px;"  >'
				+'<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Poste*</div>'
				+'<div class="col-1" ><a id="id_bouton_ajout_responsable" class="btn btn-default btn-block" row_id="2" onclick="ajouterPoste(this )"><i class="fa fa-plus"></i></a></div>'
				+'<div class="col-7" id="id_div_poste2">'
						+'<select  class="form-control select2"  name="id_poste2" id="id_id_poste2" class="form-control select2 select2-hidden-accessible"  data-placeholder="" style="width: 100%;" tabindex="-1" aria-hidden="true">'
								+'<option value="" ></option>'
								+'@foreach($liste_poste as $poste)'
							+		'<option value="<?php echo $poste->id_poste; ?>" ><?php echo $poste->label; ?></option>'
							+	'@endforeach'
							+'</select>'
				+ ' </div>'
			+'</div>'
			+'<div class="row" style="margin-top: 4px;margin-bottom: 4px;">'
			+	'<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Sexe*</div>'
				+'<div class="col-8">'
						+'<select  class="form-control select2" id="" name="sexe_responsable2" class="form-control select2 select2-hidden-accessible"  data-placeholder="Choisir le sexe*" style="width: 100%;" tabindex="-1" aria-hidden="true">'
							+'<option value="M"  >Mr</option>'
							+'<option value="F" >Mme</option>'
								
							+'</select>'
				 + '</div>'
			+'</div>'
			+'<div class="row" style="margin-top: 4px;margin-bottom: 4px;"  >'
				+'<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Email*</div>'
				+'<div class="col-8" id="id_div_email_responsable2"><input type="text" class="form-control" name="email_responsable2" id="id_email_responsable2" value="" placeholder="Entrez le mail du responsable"></div>'
			+'</div>'
			+'<div class="row" style="margin-top: 4px;margin-bottom: 4px;"  >'
				+'<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Téléphone*</div>'
				+'<div class="col-8" id="id_div_telephone_responsable2"><input type="text" class="form-control" name="telephone_responsable2" id="id_telephone_responsable2" value="" placeholder="Entrez le numero de téléphone du responsable"></div>'
			+'</div>'
		+'</div>'
	+'</div>';
	
	$("#id_div_responsable").append(html);
	$("#id_bouton_ajout_responsable").removeAttr("onclick");
	$("#id_bouton_ajout_responsable").attr("onclick" , "false;");
}	
function supprimerNouvelleAdresse(element) {
	var id_adresse = $(element).attr('row_adresse');
	$("#id_div_nouvelle_adresse_"+id_adresse).remove();
}	
function supprimerAdresse(element) {
	var id_adresse = $(element).attr('row_adresse');
	$("#id_div_adresse_"+id_adresse).remove();
}
function supprimerResponsable(element) {
	$("#id_div_responsable2").remove();
	$("#id_bouton_ajout_responsable").removeAttr("onclick");
	$("#id_bouton_ajout_responsable").attr("onclick" , "ajouterResponsable(this );");
}
	
function supprimerEntreprise(element) {
		var lien = $(element).attr('url_supprimer_entreprise');
		// console.log(lien);
		document.getElementById("supprimer_entreprise_button").href =lien+"&return=0"
		
		$("#supprimer_entreprise").modal({ backdrop: 'static', keyboard: false });
	}
  function modifierAdresse(element) {
	  mapboxgl.accessToken = 'pk.eyJ1IjoibmphcnkiLCJhIjoiY2p1ZHU0NWYzMDJ1MTRkbW16N2pmczh6YiJ9.i3r9euc0dZsYchVvQfdG5A';
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var map = new mapboxgl.Map({
		container: 'map',
		country : "mg",
		style: 'mapbox://styles/mapbox/streets-v9'
		});
		
		map.on('click', function (e) {
	
		var x = e.lngLat ;
		// console.log(x) ;
		
		var id_adresse = $(element).attr('row_adresse');
		$('#id_longitude'+id_adresse).val( x.lng );
		$('#id_latitude'+id_adresse).val( x.lat );
		$("#changer_coordonees").modal('toggle');
		
		});
		map.on('load', function () {
			
			// *** Add zoom and rotation controls to the map ...
			map.addControl(new mapboxgl.NavigationControl());

			$("#loadMe").modal('toggle');
			$("#changer_coordonees").modal({ backdrop: 'static', keyboard: false });

		});
	}
  function modifierNouvelleAdresse(element) {
	  mapboxgl.accessToken = 'pk.eyJ1IjoibmphcnkiLCJhIjoiY2p1ZHU0NWYzMDJ1MTRkbW16N2pmczh6YiJ9.i3r9euc0dZsYchVvQfdG5A';
		$("#loadMe").modal({ backdrop: 'static', keyboard: false });
		var map = new mapboxgl.Map({
		container: 'map',
		country : "mg",
		style: 'mapbox://styles/mapbox/streets-v9'
		});
		
		map.on('click', function (e) {
	
		var x = e.lngLat ;
		// console.log(x) ;
		
		var id_adresse = $(element).attr('row_adresse');
		$('#id_latitude_nouvelle_adresse_'+id_adresse).val( x.lng );
		$('#id_latitude_nouvelle_adresse_'+id_adresse).val( x.lat );
		$("#changer_coordonees").modal('toggle');
		
		});
		map.on('load', function () {
			
			// *** Add zoom and rotation controls to the map ...
			map.addControl(new mapboxgl.NavigationControl());

			$("#loadMe").modal('toggle');
			$("#changer_coordonees").modal({ backdrop: 'static', keyboard: false });

		});
	}
  
  
</script>
@endsection
<?php 
function typeSelected($type, $allTypes){
		foreach($allTypes as $t){
			if($t->id_type_entreprise == $type->id_type_entreprise){
				return true;
			}
		}
		return false;
	}
	function typeAlimentSelected($typeAl, $allTypesAl){
		foreach($allTypesAl as $t){
			if($t->id_type_aliment == $typeAl->id_type_aliment){
				return true;
			}
		}
		return false;
	}
	
	
	
?>