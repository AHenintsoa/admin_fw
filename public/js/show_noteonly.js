
$(document).ready(function() {
    $('#tnote').DataTable({
	"searching":false,
	"paging":   false,
	});
	addPeriodeListener();
	addUESelectListener();
	addECUEListener();
	addExamListener();
} );

function addExamListener(){
	$("#dropdown9 li a").on("click",function(){
		var sparc=$("#iparcour").val();
		var ssem=$("#isemestre").val();
		$("#dropdown9").attr("value",$(this).attr("value"));
		var site_url=$("#site").val();
		var url=site_url+"/resultat/loadResultNoteOnlyJSON/"+sparc+"/"+ssem+"/";
		$("a[data-activates='dropdown9']").html($(this).html());
		//modifier tableaux
		var tableaux=$("#tnote").DataTable({
			destroy: true,
	    	"searching":false,
			"paging":   false,
	    	"ajax":{
	    		"url": url,
	    		"type": "POST",
	    		"dataSrc": "",
	    		"data":{
	    			periode:$("#dropdown0 li a").attr("value"),
	    			idecue:$("#dropdown7 li a").attr("value"),
	    			idexam:$(this).attr("value"),
	    		}
	    			
	    		
	    	},
	    	"columns": [
	            {"data": "numero"},
	            {"data": "e_nom"},
	            {"data": "e_prenom"},
	            {"data": "e_sexe"},
	            {"data": "valeur"},
            ]
    	});

    	tableaux.ajax.reload();
	});

}

function addPeriodeListener(){
	$("#dropdown0 li a").on("click",function(){
		var sparc=$("#iparcour").val();
		var ssem=$("#isemestre").val();
		$("#dropdown0").attr("value",$(this).attr("value"));
		var site_url=$("#site").val();
		var url=site_url+"/resultat/loadResultNoteOnlyJSON/"+sparc+"/"+ssem+"/";
		$("a[data-activates='dropdown0']").html($(this).html());
		//modifier tableaux

		var tableaux=$("#tnote").DataTable({
			destroy: true,
	    	"searching":false,
			"paging":   false,
	    	"ajax":{
	    		"url": url,
	    		"type": "POST",
	    		"dataSrc": "",
	    		"data":{
	    			periode:$(this).attr("value"),
	    			idecue:$("#dropdown7 li a").attr("value"),
	    			idexam:$("#dropdown9 li a").attr("value"),
	    		}
	    			
	    		
	    	},
	    	"columns": [
	            {"data": "numero"},
	            {"data": "e_nom"},
	            {"data": "e_prenom"},
	            {"data": "e_sexe"},
	            {"data": "valeur"},
            ]
    	});

    	tableaux.ajax.reload();
	});
}


function addUESelectListener(){
	$("#dropdown8 li a").on("click",function(){
		$("#dropdown8").attr("value",$(this).attr("value"));
		loadECUESelectList($("#dropdown7"),$(this).attr("value"));
		$("a[data-activates='dropdown8']").html($(this).html());

		$("a[data-activates='dropdown7']").html("ECUE");
		$("#dropdown7").attr("value",0);
	});

}

function loadECUESelectList(elt,idUE){
	elt.html("");
	showLoader();

	var site_url=$("#site").val(),
		url=site_url+"/resultat/getECUEJSON/";

	$.ajax({
		type:"POST",
		url:url,
		data:{
			idue:idUE
		},
		success:function(response){
			var result="",
				rep=$.parseJSON(response);
			for(var i=0;i<rep.length;i++){
				result+="<li><a class=\"mini-font  blue-text text-darken-2\" href=\"#!\" value=\""+rep[i].id+"\">"+rep[i].nom+"</a></li>";
			}
			elt.html(result);
			hideLoader();
		},
		error:function(error){
			hideLoader();
			Materialize.toast('Erreur de chargement de la liste', 4000);
		}
	});
}

function addECUEListener(){
		$("#dropdown7").delegate("li a","click",function(){
			$("#dropdown7").attr("value",$(this).attr("value"));
			$("a[data-activates='dropdown7']").html($(this).html());
			//modifier tableaux
			var site_url=$("#site").val();
			var sparc=$("#iparcour").val();
			var ssem=$("#isemestre").val();
			var url=site_url+"/resultat/loadResultNoteOnlyJSON/"+sparc+"/"+ssem+"/";
			var tableaux=$("#tnote").DataTable({
			destroy: true,
	    	"searching":false,
			"paging":   false,
	    	"ajax":{
	    		"url": url,
	    		"type": "POST",
	    		"dataSrc": "",
	    		"data":{
	    			periode:$("#dropdown0 li a").attr('value'),
	    			idecue:$(this).attr("value"),
	    			idexam:$("#dropdown9 li a").attr("value"),
	    		}			
	    		
	    	},
	    	"columns": [
	            {"data": "numero"},
	            {"data": "e_nom"},
	            {"data": "e_prenom"},
	            {"data": "e_sexe"},
	            {"data": "valeur"},
            ]
    	});

    	tableaux.ajax.reload();
	});
}