// --------------------------------------------------------------------------
// Initialization of the firebase app
// --------------------------------------------------------------------------

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCNq8XUh2XHKnyxy5s0CvzZzKBmuE4MF5Y",
  authDomain: "betaksys.firebaseapp.com",
  databaseURL: "https://betaksys.firebaseio.com",
  projectId: "betaksys",
  storageBucket: "betaksys.appspot.com",
  messagingSenderId: "562309849225"
};
firebase.initializeApp(config);
// --------------------------------------------------------------------------

const database = firebase.database();

var contact_form = document.getElementById('contact-form');
var contact_btn = document.getElementById('contact-btn');
var contact_name = document.getElementById('contact-name');
var contact_email = document.getElementById('contact-email');
var contact_subject = document.getElementById('contact-subject');
var contact_message = document.getElementById('contact-message');
var contact_loading = document.getElementById('contact-loading');
var contact_error = document.getElementById('errormessage');
var contact_success = document.getElementById('sendmessage');

contact_btn.addEventListener('click',function(e){
    e.preventDefault();

    var nom = contact_name.value.trim();
    var mailtel = contact_email.value.trim();
    var sujet = contact_subject.value.trim();
    var message = contact_message.value.trim();
    

    if(nom != '' && mailtel != '' && message != ''){
        if(nom.length < 3){
          failureContact('Erreur, veuillez verifier votre nom');
        }
        else if(message.length < 8){
          failureContact('Erreur, veuillez verifier votre message'); 
        }
        else if(validateEmail(mailtel) || validatePhoneNumber(mailtel)){
          contact_loading.style.display = 'block';
          contact_form.style.display = 'none';
          sendContact(nom,mailtel,sujet,message);          
        }
        else{
          failureContact('Erreur, veuillez verifier votre email ou votre numéro de téléphone');
        }
    }
    else {
        failureContact('Erreur, veuillez remplir les champs requis');
    }

});

// send contact function
function sendContact(nom, mailtel, sujet, message) {
  database.ref('demande_contact').push().set({
    nom: nom,
    mailtel: mailtel,
    sujet: sujet,
    message: message
  }).then(successContact, failureContact);
}

function successContact(){

  contact_success.style.display = 'block';
  contact_error.style.display = 'none';
  contact_form.style.display  ='block';
  contact_loading.style.display = 'none';
  contact_name.value = '';
  contact_email.value = '';
  contact_message.value = '';
  contact_subject.value = '';
}

function failureContact(message = "Une erreur est survenue, veuillez réessayer ultérieurement"){
  contact_success.style.display = 'none';
  contact_loading.style.display = 'none';
  contact_form.style.display  ='block';
  contact_error.innerHTML = message;
  contact_error.style.display = "block";

}

function validateEmail(email) {
  var regexmail = /(\w+)\@(\w+)\.[a-zA-Z]/g;
  return regexmail.test(email);
}

function validatePhoneNumber(number){
  number = number.replace(/\s/g, '').replace(/\-/g,'');
  var regextelephone = /^(\+261\d{9}|033\d{7}|032\d{7}|034\d{7}|039\d{7}|020\d{7})$/;
  return regextelephone.test(number);
}



