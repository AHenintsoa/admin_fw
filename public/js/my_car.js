var pos = {
	accueil:0,
	produits:582,
	team:1738,
	impact:2200,
	historique:4620,
	galerie:13976,
	contact:15391
};

var prevYScroll = 0;
var pinY = 0;
var fromMenu = false;
var anchor_dest = "";

function setFromMenu(anchor){
	console.log("setFromMenu");
	fromMenu = true;
	anchor_dest = "#"+anchor;
}


$(function(){
	var k=0;
	var my_car = $("#my-car");
	var default_w_height = 654;
	var middleCar = (parseInt(my_car.css("height").replace("px",""))/2);

	function moveOnProduits(e){
		var pos = e.originalEvent;

		// console.log(pos); 

		var m_y=pos.clientY - middleCar ;
		my_car.css("opacity",1);

		my_car.css("left","unset");
		my_car.css("top",m_y);

	}



	$(window).scroll(function(e){

		var w_width = $(window).width();
		var w_height = $(window).height();

		var y = window.scrollY;

		var down = (y - prevYScroll) > 0;

		prevYScroll = y;

		if(y%3==0){
			my_car.css("width",55);
		}else{
			my_car.css("width",58);
		}

		if(pinY==0 && !fromMenu){
			if(my_car.collision("#road-af").length != 0){
				console.log(e);
				if(down){

					pinY = y;

					function noscroll(){
						window.scrollTo( 0,pinY);
					}

					window.addEventListener('scroll',noscroll);

					my_car.css("transform","rotate(90deg)");
					my_car.css("top",$("#road-af")[0].getBoundingClientRect().top);

					setTimeout(function(){
						var w_width = $(window).width();

						my_car.css("left","unset");
						my_car.css("right",w_width-70);
						my_car.css("opacity",1);

						setTimeout(function(){
							my_car.css("transform","rotate(0deg)");
							setTimeout(function(){
								my_car.css("top",$("#road-af")[0].getBoundingClientRect().top+100);
								setTimeout(function(){
									window.removeEventListener('scroll', noscroll);
									pinY = 0;
								},500);
							},500);
							
						},500);

					},500);

				}else{
					pinY = y;

					function noscroll(){
						window.scrollTo( 0,pinY);
					}

					window.addEventListener('scroll',noscroll);

					my_car.css("transform","rotate(90deg)");
					my_car.css("top",$("#road-af")[0].getBoundingClientRect().top);

					setTimeout(function(){
						var w_width = $(window).width();

						my_car.css("left","unset");
						my_car.css("right",17);
						my_car.css("opacity",1);

						setTimeout(function(){
							my_car.css("transform","rotate(0deg)");
							setTimeout(function(){
								my_car.css("top",$("#road-af")[0].getBoundingClientRect().top-100);
								setTimeout(function(){
									window.removeEventListener('scroll', noscroll);
									pinY = 0;
								},500);
								
							},500);
							
						},500);

					},500);
				}

			}else if(my_car.collision("#features").length != 0){

				my_car.css("left","unset");
				my_car.css("right",17);
				my_car.css("top",e.originalEvent.clientY - middleCar);
				my_car.css("opacity",1);
				my_car.css("transform","rotate(0deg)");

				$("#features").on("mousemove",moveOnProduits);

			}else if(my_car.collision("#after-road-af").length != 0 ){

				my_car.css("transform","rotate(0deg)");


			}else if(my_car.collision("#about").length != 0){

				my_car.css("left","unset");
				my_car.css("right",17);
				my_car.css("opacity",1);
				my_car.css("transform","rotate(0deg)");

			}else if(my_car.collision("#pilote").length != 0){

				var w_width = $(window).width();

				my_car.css("left","unset");
				my_car.css("right",w_width-70);
				$("#pilote").on("mousemove",moveOnProduits);

			}else if(my_car.collision("#faa").length != 0){

				var w_width = $(window).width();

				my_car.css("left","unset");
				my_car.css("right",w_width-70);

				$("#faa").on("mousemove",moveOnProduits);


			}else{
				my_car.css("transform","rotate(0deg)");
			}
		}else if(fromMenu){
			if(my_car.collision(anchor_dest).length!=0){
				fromMenu=false;
			}			
		}
	});
});