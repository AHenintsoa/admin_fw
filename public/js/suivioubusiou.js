$(document).ready(function(){

	var url = BASE_URL + "/xakapingloo/suivioubusioujsoniou";
	var today = new Date();
	/* DATEPICKER BEGIN */
	$("#date_bus").datepicker({
    	format: 'yyyy-mm-dd',
    	endDate:today			// future date is not available
	});

	$('.datepair-c .time').timepicker({
        'showDuration': false,
        'timeFormat': 'H:i',
        'maxTime':'20:00:00',
    	'minTime':'04:00:00',
    	'interval': 60
	});

	$(".datepair-c").datepair();
	/* DATEPICKER END */

	$("#suivioubusiou-button").on("click",function(e){
		
		e.preventDefault();
		var date = $("#date_bus").val();

		if(date == ""){
			return;
		}

		var heure_debut = $("#heure_debut_bus").val();
		var heure_fin = $("#heure_fin_bus").val();
		$.ajax({
  			url: url,
  			method: 'POST',
  			data:{
  				"date":date,
  				"heure_debut":heure_debut,
  				"heure_fin":heure_fin
  			}
		})
		.done(function( data ) {
			console.log(data)
			$("#tbody_bus").html("");
    		$.each(data,function(index, value){
    			var html = "<tr><th>"+ value.immatriculation+"</th><th>"+value.itag+"</th><th>"+value.date_derniere_apparition+"</th><th>"+value.heure_derniere_apparition+"</th></tr>";
    			$("#tbody_bus").append(html);
    		});
  		})
  		.fail(function( data) {
  			alert("error");
  			return;
  		});

	});



});