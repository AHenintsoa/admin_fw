

$(window).on('load',function(){
  $('#table_donation').dataTable({

    "bStateSave": true, 

    "lengthMenu": [
        [5, 10, 15, 20, -1],
        [5, 10, 15, 20, "Tout"] 
    ],
    
    // set the initial value
    "pageLength": 10,
    "language": {
        "lengthMenu": "Afficher _MENU_ éléments par page",
        "zeroRecords": "Aucun élément à afficher",
        "info":"",  // "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        "search": "Rechercher",
        "paginate":{
          "previous":"Précédent",
          "next": "Suivant"
        }
    },
//            "columnDefs": [{  // set default column settings
//                'orderable': false,
//                'targets': [5]
//            }, {
//                "searchable": false,
//                "targets": [5]
//            }],
    "order": [
        [1, "asc"]
    ] // set first column as a default sort by asc
  });
  $('#table_vente').dataTable({

    "bStateSave": true, 

    "lengthMenu": [
        [5, 15, 20, -1],
        [5, 15, 20, "Tout"] 
    ],
    
    // set the initial value
    "pageLength": -1,
    "language": {
        "lengthMenu": "Afficher _MENU_ éléments par page",
        "emptyTable": "Aucun élément à afficher",
        "info":"",  // "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        "search": "Rechercher",
        "paginate":{
          "previous":"Précédent",
          "next": "Suivant"
        }
    },
    "order": [
        [1, "asc"]
    ] // set first column as a default sort by asc
  });

  $('#table_position').dataTable({

    "bStateSave": true, 

    "lengthMenu": [
        [5, 15, 20, -1],
        [5, 15, 20, "Tout"] 
    ],
    
    // set the initial value
    "pageLength": -1,
    "language": {
        "lengthMenu": "Afficher _MENU_ éléments par page",
        "emptyTable": "Aucun élément à afficher",
        "info":"",  // "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        "search": "Rechercher",
        "paginate":{
          "previous":"Précédent",
          "next": "Suivant"
        }
    },
    "order": [
        [1, "asc"]
    ] // set first column as a default sort by asc
  });

  $('#table_bus_historique').dataTable({

    "bStateSave": true, 

    "lengthMenu": [
        [5, 15, 20, -1],
        [5, 15, 20, "Tout"] 
    ],
    
    // set the initial value
    "pageLength": -1,
    "language": {
        "lengthMenu": "Afficher _MENU_ éléments par page",
        "emptyTable": "Aucun élément à afficher",
        "info":"",  // "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        "search": "Rechercher",
        "paginate":{
          "previous":"Précédent",
          "next": "Suivant"
        }
    },
    "order": [
        [1, "asc"]
    ] // set first column as a default sort by asc
  });

    $("#content-info").css("height","");
    $("#content-info").css("opacity",1);    
    $("#loader").hide();
});