$('select').material_select();

function showLoader(){
	$('.loader').show('linear');
}

function hideLoader(){
	$('.loader').hide();
}

$('.collapsible').collapsible({
  accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
});

$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 50 // Creates a dropdown of 15 years to control year
  });

function loadRespList(elt,type){
	elt.html("");
	showLoader();
	var site_url=$("#site").val(),
		url=site_url+"/responsable/getJSON/"+type;
	$.ajax({
		type:'POST',
		url:url,
		data:{},
		success:function(response){
			var result="";
			response=$.parseJSON(response);
			console.table(response);
			for(var i=0;i<response.length;i++)
				result+="<tr><td>"+response[i].imatricule+"</td><td>"+response[i].r_nom+"</td><td>"+response[i].prenom+"</td><td>"+response[i].t_nom+"</td><td>"+response[i].adresse+"</td><td><a href=\"#\" value=\""+response[i].telephone+"\">Détail</a></td></tr>";
			elt.html(result);
			hideLoader();
		},
		error:function(error){
			hideLoader();
			Materialize.toast('Erreur de chargement de la liste', 4000); 
		}
	})
}

function loadSelectList(elt,cate,idP){
	elt.html("");
	showLoader();
	var site_url=$("#site").val(),
		url=site_url+"/etablissement/getJSON/"+cate;
	if(cate!="semestre") url+="/"+idP;
	$.ajax({
		type:"POST",
		url:url,
		data:{},
		success:function(response){
			var result="",
				rep=$.parseJSON(response);
			for(var i=0;i<rep.length;i++){
				result+="<li><a class=\"mini-font  blue-text text-darken-2\" href=\"#!\" value=\""+rep[i].id+"\">"+rep[i].nom+"</a></li>";
			}
			elt.html(result);
			hideLoader();
		},
		error:function(error){
			hideLoader();
			Materialize.toast('Erreur de chargement de la liste', 4000);
		}
	});
}

function loadStudentList(){
	var fac=$("#dropdown1").attr("value"),
		dom=$("#dropdown2").attr("value"),
		men=$("#dropdown3").attr("value"),
		parc=$("#dropdown4").attr("value"),
		sem=$("#dropdown5").attr("value"),
		periode=$("#dropdown6").attr("value"),
		site_url=$("#site").val(),
		url=site_url+"/etudiant/getJSON";

	$.ajax({
		type:"POST",
		url:url,
		data:{
			fac:fac,
			dom:dom,
			men:men,
			parc:parc,
			sem:sem,
			periode:periode
		},
		success:function(response){
			var result="";
			response=$.parseJSON(response);
			console.table(response);
			for(var i=0;i<response.length;i++)
				result+="<tr><td>"+response[i].numero+"</td><td>"+response[i].e_nom+"</td><td>"+response[i].prenom+"</td><td>"+response[i].p_nom+"</td><td>"+response[i].mail+"</td><td><a href=\"#\" value=\""+response[i].idinscription+"\">Détail</a></td></tr>";
	        $("#slist").html(result);
		},
		error:function(error){

		}
	});
}

function printStudentList(){
	var fac=$("#dropdown1").attr("value"),
		dom=$("#dropdown2").attr("value"),
		men=$("#dropdown3").attr("value"),
		parc=$("#dropdown4").attr("value"),
		sem=$("#dropdown5").attr("value"),
		periode=$("#dropdown6").attr("value"),
		site_url=$("#site").val(),
		url=site_url+"/etudiant/imprimerListe/"+fac+"/"+dom+"/"+men+"/"+parc+"/"+sem+"/"+periode;
	document.location.href=url;
}

function loadStudentDetail(id){
	var site_url=$("#site").val(),
		url=site_url+"/etudiant/getDetailJSON";
	$.ajax({
		type:"POST",
		url:url,
		data:{
			id:id
		},
		success:function(response){
			response=$.parseJSON(response);
			var rep=response[0];
			$("#d_title").html(rep.e_nom+" "+rep.prenom);
			$("#d_sexe").html(rep.sexe);
			$("#d_mail").html(rep.mail);
			$("#d_cin").html(rep.idCard);
			$("#d_date-del-cin").html(rep.date_delivrance_card);
			$("#d_lieu-del-cin").html(rep.localite);
			$("#d_date-duplicata").html(rep.date_duplicata);
			$("#d_date-naissance").html(rep.date_de_naissance);
			$("#d_lieu-naissance").html(rep.lieu_de_naissance);
			$("#d_pere").html(rep.nom_pere);
			$("#d_p-pere").html(rep.proffession_pere);
			$("#d_mere").html(rep.nom_mere);
			$("#d_p-mere").html(rep.proffession_mere);
			$("#d_tel").html(rep.tel);
			$("#d_adresse").html(rep.adresse);
			$("#d_nationalite").html(rep.n_nom);
			$("#d_autre").html(rep.autre_info);
			$("#d_nom_contact").html(rep.nom_contact);
			$("#d_tel_contact").html(rep.tel_contact);
			$("#student-detail").openModal();
		},
		error:function(response){

		}
	})
}
$(document).ready(function() {
    $('.listeEtud').DataTable({
    	"searching":false,
    	"filter": false

    });
    $('select').material_select();
} );


