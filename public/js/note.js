$('select').material_select();

function listenerForSelectInFiltre(allselect,allselectparent,allchamp,allmodel){
    var i=0;
    addSelectXlistener(allselect,i,allselectparent,allchamp,allmodel);
}

var allselect=['select_faculte','select_domaine','select_mention','select_parcour','select_semestre']
var allselectparent=['s_domaine','s_mention','s_parcour','s_semestre'];
var allchamp=['idDomaine,nom','idMention,nom','idParcour,nom','idsemestre,nom'];
var allmodel=['faculte','domaine','mention','parcour','semestre'];
listenerForSelectInFiltre(allselect,allselectparent,allchamp,allmodel);


if($('#select_ecue').length!=0){
    var allselect=['select_ue','select_ecue']
    var allselectparent=['s_ecue'];
    var allchamp=['idecue,nom'];
    var allmodel=['ue','ecue'];
    listenerForSelectInFiltre(allselect,allselectparent,allchamp,allmodel)
}

function addSelectXlistener(x,i,parentsnext,champs,model){
      var idxi=x[i];
      $("#"+idxi).on('change', function() {
        if (i<x.length-1) {
             var next=x[i+1];
            if($("#"+next).length==0){
                var classparent=parentsnext[i];
                $('.'+classparent).append (
                  '<select name="'+next+'" id="'+next+'"></select>');
                addSelectXlistener(x,i+1,parentsnext,champs,model);
            }
          var select_next=$("#"+next);
          var site_url=$("#site").val();
        $.ajax({
                type: 'GET',
                url: site_url+"/note/ajaxGetXinY/"+model[i+1]+"/"+model[i]+"/",
                data: {
                 id : this.value,
                 champ : champs[i]
                }
              ,
              success: function(response){

                  //alert(response);
                  select_next.html(response);
                  $('select').material_select();
                  select_next.trigger('change');
                  select_next.append('<br/>');

              },
              error: function(e){
                  ErrorMessage.show('Une erreur est survenue');
                }
              });          
        };    
    });

}
