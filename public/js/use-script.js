hideLoader();

$('#show_profil').on('click',function(){
	$("#mon_profil").openModal();
});

$("#drop-resp li a").on('click',function(){
	$("#drop-resp").attr("value",$(this).attr("value"));
	$("a[data-activates='drop-resp']").html($(this).html());
	loadRespList($("#rlist"),$(this).attr("value"));
});

loadSelectList($("#dropdown5"),"semestre",1);

$("#dropdown1 li a").on("click",function(){
	$("#dropdown1").attr("value",$(this).attr("value"));
	loadSelectList($("#dropdown2"),"domaine",$(this).attr("value"));
	$("a[data-activates='dropdown1']").html($(this).html());
	$("a[data-activates='dropdown2']").html("Domaine");
	$("a[data-activates='dropdown3']").html("Mention");	
	$("a[data-activates='dropdown4']").html("Parcours");

	$("#dropdown2").attr("value",0);
	$("#dropdown3").attr("value",0);
	$("#dropdown4").attr("value",0);
});

$("#dropdown2").delegate("li a","click",function(){
	$("#dropdown2").attr("value",$(this).attr("value"));
	loadSelectList($("#dropdown3"),"mention",$(this).attr("value"));
	$("a[data-activates='dropdown2']").html($(this).html());
	$("a[data-activates='dropdown3']").html("Mention");	
	$("a[data-activates='dropdown4']").html("Parcours");

	$("#dropdown3").attr("value",0);
	$("#dropdown4").attr("value",0);
});

$("#dropdown3").delegate("li a","click",function(){
	$("#dropdown3").attr("value",$(this).attr("value"));
	loadSelectList($("#dropdown4"),"parcours",$(this).attr("value"));
	$("a[data-activates='dropdown3']").html($(this).html());
	$("a[data-activates='dropdown4']").html("Parcours");

	$("#dropdown4").attr("value",0);
});

$("#dropdown4").delegate("li a","click",function(){
	$("#dropdown4").attr("value",$(this).attr("value"));
	$("a[data-activates='dropdown4']").html($(this).html());
});

$("#dropdown5").delegate("li a","click",function(){
	$("#dropdown5").attr("value",$(this).attr("value"));
	$("a[data-activates='dropdown5']").html($(this).html());
});

$("#dropdown6").delegate("li a","click",function(){
	$("#dropdown6").attr("value",$(this).attr("value"));
	$("a[data-activates='dropdown6']").html($(this).html());
});

$(".showStudentSorted").on("click",function(){
	loadStudentList();
});

$(".printStudentList").on("click",function(){
	printStudentList();
});

$("#slist").delegate("td a","click",function(){
	loadStudentDetail($(this).attr("value"));
});


