$(function () {

  'use strict'
	repasParMois();
	
})

function repasParMois(){
	
	var annee=$('#id_annee_mois').val();
	console.log(annee);
	// repas_distribue_par_enfant
	var canv = document.getElementById('id_chart1');
	while (canv.firstChild) {
		canv.removeChild(canv.firstChild);
	}
	var can_vao  = document.createElement('canvas') ;
	can_vao.id="repas_distribue";
	can_vao.height="180";
	can_vao.style="height: 180px;";
	canv.appendChild(can_vao) ; 
	
	var canvx = document.getElementById('id_chart2');
	while (canvx.firstChild) {
		canvx.removeChild(canvx.firstChild);
	}
	var can_vaox  = document.createElement('canvas') ;
	can_vaox.id="repas_distribue_par_enfant";
	can_vaox.height="180";
	can_vaox.style="height: 180px;";
	canvx.appendChild(can_vaox) ; 
	
	var moisChartCanvas = $('#repas_distribue').get(0).getContext('2d');
  var mois_enfantChartCanvas = $('#repas_distribue_par_enfant').get(0).getContext('2d');
  
	var mois_Chart;
	var mois_enfant_Chart;
		var xMLHttprequest = new XMLHttpRequest() ;
		xMLHttprequest.open('GET','repas_distribue_par_mois_general?annee='+annee)	;
		xMLHttprequest.addEventListener('readystatechange',function(){
			if(xMLHttprequest.readyState==XMLHttpRequest.DONE && xMLHttprequest.status==200) {
				var resp = JSON.parse(xMLHttprequest.responseText) ; 
				 mois_Chart       = new Chart(moisChartCanvas);
				 mois_enfant_Chart       = new Chart(mois_enfantChartCanvas);
				var data_mois={
				label               : 'Digital Goods',
				fillColor           : 'rgba(0, 123, 255, 0.9)',
				strokeColor         : 'rgba(0, 123, 255, 1)',
				pointColor          : '#3b8bba',
				pointStrokeColor    : 'rgba(0, 123, 255, 1)',
				pointHighlightFill  : '#fff',
				pointHighlightStroke: 'rgba(0, 123, 255, 1)',
				data                : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			  };
			  var data_mois_enfant={
				label               : 'Digital Goods',
				fillColor           : 'rgba(0, 123, 255, 0.9)',
				strokeColor         : 'rgba(0, 123, 255, 1)',
				pointColor          : '#3b8bba',
				pointStrokeColor    : 'rgba(0, 123, 255, 1)',
				pointHighlightFill  : '#fff',
				pointHighlightStroke: 'rgba(0, 123, 255, 1)',
				data                : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			  };
			  console.log(resp);
			  	for(var i= 0 ; i<12 ; i++){
					data_mois.data[i] = resp['repas_distribue_mois'][i];
					data_mois_enfant.data[i] = resp['repas_distribue_mois_enfant'][i];
				}
				console.log(data_mois.data);
				console.log(data_mois_enfant.data);
				var mois_ChartData = {
					labels  : ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet','Août','Septembre','Octobre','Novembre','Decembre'],
					datasets: [ data_mois
						]
				}
				var mois_enfant_ChartData = {
					labels  : ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet','Août','Septembre','Octobre','Novembre','Decembre'],
					datasets: [ data_mois_enfant
						]
				}
				
				$( "div" ).remove( ".loader" );
				$( "div" ).remove( ".loader2" );
				var mois_salesChartOptions = {
					//Boolean - If we should show the scale at all
					showScale               : true,
					//Boolean - Whether grid lines are shown across the chart
					scaleShowGridLines      : false,
					//String - Colour of the grid lines
					scaleGridLineColor      : 'rgba(0,0,0,.05)',
					//Number - Width of the grid lines
					scaleGridLineWidth      : 1,
					//Boolean - Whether to show horizontal lines (except X axis)
					scaleShowHorizontalLines: true,
					//Boolean - Whether to show vertical lines (except Y axis)
					scaleShowVerticalLines  : true,
					//Boolean - Whether the line is curved between points
					bezierCurve             : true,
					//Number - Tension of the bezier curve between points
					bezierCurveTension      : 0.3,
					//Boolean - Whether to show a dot for each point
					pointDot                : false,
					//Number - Radius of each point dot in pixels
					pointDotRadius          : 4,
					//Number - Pixel width of point dot stroke
					pointDotStrokeWidth     : 1,
					//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
					pointHitDetectionRadius : 20,
					//Boolean - Whether to show a stroke for datasets
					datasetStroke           : true,
					//Number - Pixel width of dataset stroke
					datasetStrokeWidth      : 2,
					//Boolean - Whether to fill the dataset with a color
					datasetFill             : true,
					//String - A legend template
					legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%=datasets[i].label%></li><%}%></ul>',
					//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
					maintainAspectRatio     : false,
					//Boolean - whether to make the chart responsive to window resizing
					responsive              : true
				  }
				  var mois__enfant_salesChartOptions = {
					//Boolean - If we should show the scale at all
					showScale               : true,
					//Boolean - Whether grid lines are shown across the chart
					scaleShowGridLines      : false,
					//String - Colour of the grid lines
					scaleGridLineColor      : 'rgba(0,0,0,.05)',
					//Number - Width of the grid lines
					scaleGridLineWidth      : 1,
					//Boolean - Whether to show horizontal lines (except X axis)
					scaleShowHorizontalLines: true,
					//Boolean - Whether to show vertical lines (except Y axis)
					scaleShowVerticalLines  : true,
					//Boolean - Whether the line is curved between points
					bezierCurve             : true,
					//Number - Tension of the bezier curve between points
					bezierCurveTension      : 0.3,
					//Boolean - Whether to show a dot for each point
					pointDot                : false,
					//Number - Radius of each point dot in pixels
					pointDotRadius          : 4,
					//Number - Pixel width of point dot stroke
					pointDotStrokeWidth     : 1,
					//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
					pointHitDetectionRadius : 20,
					//Boolean - Whether to show a stroke for datasets
					datasetStroke           : true,
					//Number - Pixel width of dataset stroke
					datasetStrokeWidth      : 2,
					//Boolean - Whether to fill the dataset with a color
					datasetFill             : true,
					//String - A legend template
					legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].lineColor%>"></span><%=datasets[i].label%></li><%}%></ul>',
					//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
					maintainAspectRatio     : false,
					//Boolean - whether to make the chart responsive to window resizing
					responsive              : true
				  }

				  //Create the line chart
				  mois_Chart.Line(mois_ChartData, mois_salesChartOptions)
					mois_enfant_Chart.Line(mois_enfant_ChartData, mois__enfant_salesChartOptions)

				  //---------------------------
				  //- END MONTHLY SALES CHART -
				
			}
		})
		xMLHttprequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xMLHttprequest.send() ;
		

  //---------------------------
}
