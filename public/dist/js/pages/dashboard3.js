$(document).ready(function(){
  'use strict'

	repasParMois();
});


function repasParMois(){
	var ticksStyle = {
    fontColor: '#495057',
    fontStyle: 'bold'
  }

  var mode      = 'index'
  var intersect = true
  
	// avoir l année selectionnée
	var annee=$('#id_annee_mois').val();
	
	$("#id_loader1").attr("class" , "loader");
	$("#id_loader2").attr("class" , "loader");

	
	
	//remove old canvas
	var div_canv = document.getElementById('id_chart1');
	while (div_canv.firstChild) {
		div_canv.removeChild(div_canv.firstChild);
	}
	var can_vao  = document.createElement('canvas') ;
	can_vao.id="id_repas_mois";
	can_vao.height="200";
	div_canv.appendChild(can_vao) ; 
	
	
	var div_canv2 = document.getElementById('id_chart2');
	while (div_canv2.firstChild) {
		div_canv2.removeChild(div_canv2.firstChild);
	}
	var can_vaox  = document.createElement('canvas') ;
	can_vaox.id="id_repas_mois_enfant";
	can_vaox.height="200";
	div_canv2.appendChild(can_vaox) ;
	
	
	var $repas_mois_Chart = $('#id_repas_mois')
   $.ajax({
       url : 'http://localhost/admin_fw/public/repas_distribue_par_mois_general?annee='+annee,
       type : 'GET',
       dataType : 'JSON',
       success : function(response, statut){ // success est toujours en place, bien sûr !
           console.log(response.repas_distribue_mois);
		   var repas_mois_Chart  = new Chart($repas_mois_Chart, {
		data   : {
		  labels  : ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet' , 'Août' , 'Septembre' , 'Octobre' , 'Novembre' , 'Décembre'],
		  datasets: [{
			type                : 'line',
			data                : response.repas_distribue_mois,
			backgroundColor     : 'transparent',
			borderColor         : '#007bff',
			pointBorderColor    : '#007bff',
			pointBackgroundColor: '#007bff',
			fill                : false
			// pointHoverBackgroundColor: '#007bff',
			// pointHoverBorderColor    : '#007bff'
		  }]
		},
		options: {
		  maintainAspectRatio: false,
		  tooltips           : {
			mode     : mode,
			intersect: intersect
		  },
		  hover              : {
			mode     : mode,
			intersect: intersect
		  },
		  legend             : {
			display: false
		  },
		  scales             : {
			yAxes: [{
			  // display: false,
			  gridLines: {
				display      : true,
				lineWidth    : '4px',
				color        : 'rgba(0, 0, 0, .2)',
				zeroLineColor: 'transparent'
			  },
			  ticks    : $.extend({
				beginAtZero : true,
				suggestedMax: 200
			  }, ticksStyle)
			}],
			xAxes: [{
			  display  : true,
			  gridLines: {
				display: false
			  },
			  ticks    : ticksStyle
			}]
		  }
		}
	  })
		$("#id_loader1").attr("class" , "");		
	  
	  var $id_repas_mois_enfant = $('#id_repas_mois_enfant')
	  var repas_mois_enfant_Chart  = new Chart($id_repas_mois_enfant, {
		data   : {
		  labels  : ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet' , 'Août' , 'Septembre' , 'Octobre' , 'Novembre' , 'Décembre'],
		  datasets: [{
			type                : 'line',
			data                : response.repas_distribue_mois_enfant,
			backgroundColor     : 'transparent',
			borderColor         : '#007bff',
			pointBorderColor    : '#007bff',
			pointBackgroundColor: '#007bff',
			fill                : false
			// pointHoverBackgroundColor: '#007bff',
			// pointHoverBorderColor    : '#007bff'
		  }]
		},
		options: {
		  maintainAspectRatio: false,
		  tooltips           : {
			mode     : mode,
			intersect: intersect
		  },
		  hover              : {
			mode     : mode,
			intersect: intersect
		  },
		  legend             : {
			display: false
		  },
		  scales             : {
			yAxes: [{
			  // display: false,
			  gridLines: {
				display      : true,
				lineWidth    : '4px',
				color        : 'rgba(0, 0, 0, .2)',
				zeroLineColor: 'transparent'
			  },
			  ticks    : $.extend({
				beginAtZero : true,
				suggestedMax: 200
			  }, ticksStyle)
			}],
			xAxes: [{
			  display  : true,
			  gridLines: {
				display: false
			  },
			  ticks    : ticksStyle
			}]
		  }
		}
	  })
	  $("#id_loader2").attr("class" , "");	
       },
		
       error : function(resultat, statut, erreur){
			console.log('tsy tafiditra');
       }
    });
}