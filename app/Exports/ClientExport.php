<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB ;
use App\Models\Utilisateur;

class ClientExport implements FromCollection, WithHeadings
{
    use Exportable;

    public function collection()
    {
		$utilisateur = Utilisateur::where('active' , '1')->get();
		
		for($i = 0 ; $i< count($utilisateur) ; $i++){
			$variable = $utilisateur[$i]->consommateur()->where('nom' , '!=' ,'null')->first();
			if($variable)
				$consommateurs[] = $variable;
		}
		// Achats (MGA) total par clients
		for($i = 0 ; $i < count($consommateurs ) ; $i++ ) {
			$achat[] = DB::table('produit_commande')
				->join('commande' , 'commande.id_commande', 'produit_commande.commande_id')
				->select( DB::raw('sum( produit_commande.prix_achat_unite*produit_commande.qte) as montant_total') )
				->where('commande.consommateur_id' , $consommateurs[$i]->id_consommateur)
				->groupBy('commande.consommateur_id')
				->first();
			// prix moyen choisi par produit
			$moyenne_prix_par_produit[] = DB::table('produit_commande')
				->join('commande' , 'commande.id_commande', 'produit_commande.commande_id')
				->join('donation' , 'donation.id_donation', 'produit_commande.donation_id')
				->join('produit' , 'produit.id_produit', 'donation.produit_id_produit')
				->select( 'produit.nom_produit' ,DB::raw('round(avg( produit_commande.prix_achat_unite),0) as prix_moyen') )
				->where('commande.consommateur_id' , $consommateurs[$i]->id_consommateur)
				->groupBy('donation.produit_id_produit')
				->get();
			// moyenne remise par produit	
			$moyenne_reduction[] = DB::table('consommateur')
					->join (DB::raw('(select c.consommateur_id  , p.nom_produit , round(avg(100*(p.prix-pc.prix_achat_unite)/p.prix),1) as moyenne_remise from produit_commande pc
									join donation d on d.id_donation=pc.donation_id
									join commande c on c.id_commande=pc.commande_id
									join produit p on p.id_produit=d.produit_id_produit
									where c.consommateur_id='.$consommateurs[$i]->id_consommateur.'
									group by p.nom_produit) as moyenne_reduction') ,'moyenne_reduction.consommateur_id','consommateur.id_consommateur' )
					->select('moyenne_reduction.nom_produit' , 'moyenne_reduction.moyenne_remise' )
					->get();
			$best_reduction[] = DB::table('consommateur')
					->join (DB::raw('(select  tab2.reduction as reduc, max(tab2.nombre) , tab2.consommateur_id   from
									(select count(tab1.reduction) as nombre, tab1.reduction as reduction, tab1.consommateur_id as consommateur_id from 
									(select pc.* , c.* , round((100*(p.prix-pc.prix_achat_unite)/p.prix),1) as reduction from produit_commande pc
									join donation d on d.id_donation=pc.donation_id
									join commande c on c.id_commande=pc.commande_id
									join produit p on p.id_produit=d.produit_id_produit) as tab1  group by tab1.reduction , tab1.consommateur_id order by nombre desc) as tab2 group by tab2.consommateur_id ) as best_reduction') ,'best_reduction.consommateur_id','consommateur.id_consommateur' )
					->select('consommateur.nom' , 'consommateur.prenom' , 'best_reduction.reduc')
					->where('consommateur.id_consommateur' , $consommateurs[$i]->id_consommateur)
					->first();	
		}	
		for($i = 0 ; $i< count( $consommateurs ) ; $i++){
			$data [] = array (
							'Nom complet du client' => $consommateurs[$i]->nom.' '.$consommateurs[$i]->prenom  ,
							'Numero de téléphone'=> $consommateurs[$i]->numero_telephone , 
							'Mail'=> $consommateurs[$i]->mail , 
							'Point de livraison'=> $consommateurs[$i]->adresse_de_livraison , 
							'Mobile banking'=> $consommateurs[$i]-> 	mobile_banking , 
							'Total d\'achat effectué'=> $achat[$i]->montant_total,
							'Réduction préférée' => $best_reduction[$i]->reduc, 
							'Moyenne du prix par produit' => ' ',
							'Moyenne de la remise par produit' => ' ' ,
					
			);
		}
		for($i = 0 ; $i< count( $consommateurs ) ; $i++){
			$data[$i]['Moyenne du prix par produit']  = $moyenne_prix_par_produit[$i][0]->nom_produit.' : Ar '.$moyenne_prix_par_produit[$i][0]->prix_moyen ;
				for($j= 1 ; $j< count( $moyenne_prix_par_produit[$i]  ) ; $j++ ){
					$data[$i]['Moyenne du prix par produit']  = $data[$i]['Moyenne du prix par produit'].' ; '.$moyenne_prix_par_produit[$i][$j]->nom_produit.' : Ar '.$moyenne_prix_par_produit[$i][$j]->prix_moyen ;
				}
		}
		for($i = 0 ; $i< count( $consommateurs ) ; $i++){
			$data[$i]['Moyenne de la remise par produit']  = $moyenne_reduction[$i][0]->nom_produit.' : '.$moyenne_reduction[$i][0]->moyenne_remise.' % ' ;
				for($j= 1 ; $j< count( $moyenne_reduction[$i]  ) ; $j++ ){
					$data[$i]['Moyenne de la remise par produit']  = $data[$i]['Moyenne de la remise par produit'].' ; '.$moyenne_reduction[$i][$j]->nom_produit.' : '.$moyenne_reduction[$i][$j]->moyenne_remise.' % ' ;
				}
		}
        return collect( $data );
    }

    public function headings(): array
    {
		$head = array('Nom complet du client' , 'Numero de téléphone' ,'Mail' , 'Point de livraison' ,'Mobile banking' , 'Total d\'achat effectué' , 'Réduction préférée' , 'Moyenne du prix par produit ' ,'Moyenne de la remise par produit'  ) ;
        return $head ;
    }

}