<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB ;
use App\Models\Utilisateur;

class EntrepriseExport implements FromCollection, WithHeadings
{
    use Exportable;

    public function collection()
    {
		$utilisateur = Utilisateur::where('active' , '1')->get();
		
		$entreprises = [] ;
		for($i = 0 ; $i <count($utilisateur) ; $i++){
			$entreprise = $utilisateur[$i]->entreprise()->first();
			if($entreprise['nom_entreprise'])
				$entreprises[] = $utilisateur[$i]->entreprise()->first();
		}
		$entreprise_type = [] ;
		for($i =0 ; $i< count($entreprises) ; $i++){
			$entreprise_type []= $entreprises[$i]->type_entreprise()->get();
		}
		$type_aliment = [];
		for($i =0 ; $i< count($entreprises) ; $i++){
			$type_aliment []= $entreprises[$i]->type_aliment()->get();
		}
		$adresse = [] ;
		for($i = 0 ; $i < count($entreprises ) ; $i++){
			$adresse [] = $entreprises[$i]->adresse()->get();
		}
		$employes = [] ;
		for($i =0 ; $i< count($entreprises) ; $i++){
			$employes []= $entreprises[$i]->entreprise_employe()->where('entreprise_id' , '=' , $entreprises[$i]->id_entreprise  , 'and' ,'position' ,'=', '1' ,'and', 'position' ,'=', '2')
													->get();
		}
		
		$liste_employe_info_responsable = [] ;
		for($i = 0 ; $i < count($employes ) ; $i++){
			$employe_info_responsable = [] ; 
			for($j = 0 ; $j< count($employes [$i]) ; $j++){
				$employe_info_responsable [] = $employes[$i][$j]->employe()->first();
			}
			$liste_employe_info_responsable [] = $employe_info_responsable ;
		}
		
		
		$entreprise_comptes = [] ;
		for($i =0 ; $i< count($entreprises) ; $i++){
			$entreprise_comptes []= $entreprises[$i]->compte()->get();
		}
		$liste_numero_compte = [] ;
		for($i = 0 ; $i < count($entreprise_comptes ) ; $i++){
			$comptes_par_entreprise = [] ; 
			for($j = 0 ; $j< count($entreprise_comptes [$i]) ; $j++){
				$comptes_par_entreprise [] = $entreprise_comptes[$i][$j]->entreprise_compte()->first();
			}
			$liste_numero_compte [] = $comptes_par_entreprise ;
		}
		
		for($i = 0 ; $i< count( $entreprises ) ; $i++){
			$data [] = array (
							'Nom de l\'entreprise' => $entreprises[$i]->nom_entreprise  ,
							'NIF'=> ' '.$entreprises[$i]->nif_entreprise , 
							'STAT'=>' '. $entreprises[$i]->stat_entreprise , 
							'Type de l\'entreprise'=> ' ' , 
							'Type de produit'=> ' ' , 
							'Adresses'=> '',
							'Numero de compte' => ' ',
							
					
			);
		}
		//structure type entreprsie
		for($i = 0 ; $i< count( $entreprises ) ; $i++){
			if(count($entreprise_type[$i] ) >0){
			$data[$i]['Type de l\'entreprise']  = $entreprise_type[$i][0]->label ;
				for($j= 1 ; $j< count( $entreprise_type[$i]  ) ; $j++ ){
					$data[$i]['Type de l\'entreprise']  = $data[$i]['Type de l\'entreprise'].' ; '.$entreprise_type[$i][$j]->label ;
				}
			}
		}
		
		//structure type aliment
		for($i = 0 ; $i< count( $entreprises ) ; $i++){
			if(count($type_aliment[$i] ) >0){
			$data[$i]['Type de produit']  = $type_aliment[$i][0]->label ;
				for($j= 1 ; $j< count( $type_aliment[$i]  ) ; $j++ ){
					$data[$i]['Type de produit']  = $data[$i]['Type de produit'].' ; '.$type_aliment[$i][$j]->label ;
				}
			}
		}
		
		//structure adresse entreprsie
		for($i = 0 ; $i< count( $entreprises ) ; $i++){
			if(count($adresse[$i] ) >0){
				$data[$i]['Adresses']  = $adresse[$i][0]->label.' - '.$adresse[$i][0]->ville.' - '.$adresse[$i][0]->code_postal;
					for($j= 1 ; $j< count( $adresse[$i]  ) ; $j++ ){
						$data[$i]['Adresses']  = $data[$i]['Adresses'].' ; '.$adresse[$i][$j]->label.' - '.$adresse[$i][$j]->ville.' - '.$adresse[$i][$j]->code_postal ;
					}
			}
		}
		
		//structure numero de compte + label
		for($i = 0 ; $i< count( $entreprises ) ; $i++){
			if(count( $entreprise_comptes[$i] ) >0){
				$data[$i]['Numero de compte']  = $entreprise_comptes[$i][0]->label.' - '.$liste_numero_compte[$i][0]->numero_compte;
				for($j= 1 ; $j< count( $entreprise_comptes[$i]  ) ; $j++ ){
					$data[$i]['Numero de compte']  = $data[$i]['Numero de compte'].' ; '.$entreprise_comptes[$i][$j]->label.' - '.$liste_numero_compte[$i][$j]->numero_compte;
				}
			}
		}
		
        return collect( $data );
    }

    public function headings(): array
    {
		$head = array('Nom de l\'entreprise' , 'NIF' ,'STAT' , 'Type de l\'entreprise' ,'Type de produit' , 'Adresses' ,  'Numero de compte'  ) ;
        return $head ;
    }

}



