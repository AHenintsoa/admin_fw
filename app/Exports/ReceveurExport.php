<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Organisation;
use App\Models\Type_organisation;
use App\Models\Pays;
use App\Models\Poste;

class ReceveurExport implements FromCollection, WithHeadings
{
    use Exportable;

    public function collection()
    {
		$receveurs = Organisation::where('active' , '1')->get();
		$liste_responsable = [] ; // organisation_employe
		$info_responsable = [] ; // employe
		$adresse = [] ; // adresse organisation
		$type = [] ; // type organisation
		
		
		for($i = 0 ; $i< count($receveurs) ; $i++){
			$liste_responsable []= $receveurs[$i]->organisation_employe()->where('position' , '1')->first();
			$info_responsable[] = $liste_responsable[$i]->employe()->first();
			
			$adresse[] = $receveurs[$i]->adresse()->first();
			$type[] = $receveurs[$i]->type_organisation()->first();
		}
		$liste_type_receveur= Type_organisation::all();
		$liste_pays= Pays::all();
		$liste_poste= Poste::where('type' , '2')->get();
		//nom d ‘entité, type d’entité, nom complet personne de contact, mail, tel facebook, adresse
		// $data [] = array('Nom de l\'organisation' , 'Type' , 'Responsable de contact' , 'Mail' , 'Telephone' , 'adresse de l\'organisation' ) ;
		for($i = 0 ; $i< count( $receveurs ) ; $i++){
			$data [] = array (
							'Nom de l\'organisation' => $receveurs[$i]->nom_organisation ,
							'Type'=> $type[$i]->label , 
							'Responsable de contact' => $info_responsable[$i]->nom.' '.$info_responsable[$i]->prenom ,
							'Mail' => $info_responsable[$i]->email ,
							'Telephone' => ' '.$info_responsable[$i]->telephone .' ',
							'adresse de l\'organisation' => $adresse[$i]->label,
					
			);
		}
        return collect( $data );
    }

    public function headings(): array
    {
		$head = array('Nom de l\'organisation' , 'Type' , 'Responsable de contact' , 'Mail' , 'Telephone' , 'adresse de l\'organisation' ) ;
        return $head ;
    }

}