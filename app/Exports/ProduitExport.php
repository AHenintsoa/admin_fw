<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB ;
use App\Models\Produit;

class ProduitExport implements FromCollection, WithHeadings
{
    use Exportable;

    public function collection()
    {
		$produit = Produit::all();
		$donations = [] ;
		for($i = 0 ; $i< count($produit) ; $i++){
			$donations[] = $produit[$i]->donation;
		}
		
		for($i = 0 ; $i< count( $produit ) ; $i++){
			$data [] = array (
							'Nom du produit' => $produit[$i]->nom_produit  ,
							'Description'=>$produit[$i]->description  , 
							'Poids'=> $produit[$i]->poids , 
							'Volume'=> $produit[$i]->volume , 
							'Quantite'=> $donations[$i]->quantite , 
							'Prix'=> $produit[$i]->prix,
							'Type du produit' => $produit[$i]->type_produit->label, 
							'Categorie du produit' => $produit[$i]->type_aliment->label,
							'Information nutritionnelle' => ' ' ,
							'Raison de surplus' => ' ', 
							'Stockage' => ' ', 
							
							
					
			);
		}
		
		for($i = 0 ; $i< count( $produit ) ; $i++){
			$donation = $produit[$i]->donation;
			$liste_raison = null;
			if($donation!=null) $liste_raison = $donation->raison_surplus()->get();
			if($liste_raison !=null){
				if(count($liste_raison)>0) $data[$i]['Raison de surplus'] = $liste_raison[0]->label ; 
				for($j= 1 ; $j< count( $liste_raison) ; $j++){
					$data[$i]['Raison de surplus']	= $data[$i]['Raison de surplus'] .' ; '.$liste_raison[$j]->label ;
				}
			}
			// var_dump($data[$i]['Raison de surplus'] );
			// var_dump($liste_raison );
			
		}
		for($i = 0 ; $i< count( $produit ) ; $i++){
			$info = $produit[$i]->information_nutritionnelle()->get();
			if($info !=null){
				if(count($info)>0) $data[$i]['Information nutritionnelle'] = $info[0]->label ; 
				for($j= 1 ; $j< count( $info) ; $j++){
					$data[$i]['Information nutritionnelle']	= $data[$i]['Information nutritionnelle'] .' ; '.$info[$j]->label ;
				}
			}
			// var_dump($data[$i]['Information nutritionnelle'] );
			// var_dump($liste_raison );
			
		}
		for($i = 0 ; $i< count( $produit ) ; $i++){
			$produit_equipement = $produit[$i]->produit_equipement()->where('etat' , '1')->get();
			if($produit_equipement !=null){
				if(count($produit_equipement)>0) $data[$i]['Stockage'] = $produit_equipement[0]->equipement->label ; 
				for($j= 1 ; $j< count( $produit_equipement) ; $j++){
					$data[$i]['Stockage']	= $data[$i]['Stockage'] .' ; '.$produit_equipement[$j]->equipement->label ;
				}
			}
			// var_dump($data[$i]['Stockage'] );
			// var_dump($liste_raison );
			
		}
		
		// die();
        return collect( $data );
    }

    public function headings(): array
    {
		$head = array('Nom du produit' , 'Description' ,'Poids' , 'Volume' ,'Quantite' , 'Prix' , 'Type du produit' , 'Categorie du produit', 'Information nutritionnelle' ,'Raison de surplus' , 'Stockage'   ) ;
        return $head ;
    }

}