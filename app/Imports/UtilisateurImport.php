<?php

namespace App\Imports;

use App\models\Utilisateur;
use Maatwebsite\Excel\Concerns\ToModel;

class UtilisateurImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
       return new Utilisateur([
           'pseudo'     => $row[0],
		   'password'    => $row[1], 
           'type_utilisateur_id'    => $row[2], 
        ]);
    }
}
