<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
	protected $table = 'commande'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'consommateur_id','date','etat'
    ];
	protected $primaryKey='id_commande';
	public $timestamps = false;
	
	
}