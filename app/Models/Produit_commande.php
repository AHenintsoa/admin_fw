<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produit_commande extends Model
{
	protected $table = 'produit_commande'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'commande_id','qte','prix_achat_unite','dnation_id'
    ];
	protected $primaryKey='id_produit_commande';
	public $timestamps = false;
	
	
}