<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Information_nutritionnelle extends Model
{
	protected $table = 'information_nutritionnelle'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label' , 'proprietaire' 
    ];
	protected $primaryKey='id_information_nutritionnelle';
	public $timestamps = false;
	
	
}