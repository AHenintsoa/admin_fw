<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pays extends Model
{
	protected $table = 'pays'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label','prefixe_tel'
    ];
	protected $primaryKey='id_pays';
	public $timestamps = false;
	
	public function membre_fw()
    {
        return $this->hasOne('App\Membre_fw' , 'pays_id');
    }
	
}