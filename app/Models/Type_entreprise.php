<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type_entreprise extends Model
{
	protected $table = 'type_entreprise'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label' , 'proprietaire' 
    ];
	protected $primaryKey='id_type_entreprise';
	public $timestamps = false;
	
	public function entreprise_type() { 
		
		return $this->hasMany('App\Models\Entreprise_type' , 'type_entreprise_id'); 
	}
	public function entreprise(){
		return $this->hasMany('App\Models\Entreprise' ,'entreprise_type' ,'type_entreprise_id' , 'entreprise_id');
	}
}

