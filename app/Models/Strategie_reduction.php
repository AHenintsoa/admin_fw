<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Strategie_reduction extends Model
{
	protected $table = 'strategie_reduction'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'periode_reduction_id' ,'pourcentage_reduction' , 'donation_id' 
    ];
	protected $primaryKey='id_strategie_reduction';
	public $timestamps = false;
	
	public function periode_reduction() { 
		return $this->belongsTo('App\Models\Periode_reduction' , 'periode_reduction_id'); 
	}
	
}