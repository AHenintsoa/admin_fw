<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employe extends Model
{
	protected $table = 'employe'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'nom' , 'prenom' , 'email' , 'telephone' , 'sexe' , 'poste_id' 
    ];
	protected $primaryKey='id_employe';
	public $timestamps = false;
	
	public function entreprise_employe() { 
		
		return $this->hasOne('App\Models\Entreprise_employe' , 'employe_id'); 
	}
	public function poste() { 
		
		return $this->belongsTo('App\Models\Poste' , 'poste_id'); 
	}
	
}