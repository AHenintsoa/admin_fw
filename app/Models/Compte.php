<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Compte extends Model
{
	protected $table = 'compte'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label' , 'pays_id' 
    ];
	protected $primaryKey='id_compte';
	public $timestamps = false;
	
	public function entreprise_compte()
    {
        return $this->hasOne('App\Models\Entreprise_compte' ,  'compte_id');
    }
	
}