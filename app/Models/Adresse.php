<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adresse extends Model
{
	protected $table = 'adresse'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label' , 'latitude', 'longitude', 'code_postal', 'ville', 'pays_id' 
    ];
	protected $primaryKey='id_adresse';
	public $timestamps = false;
	
	public function pays() { 
		return $this->belongsTo('App\Models\Pays' , 'pays_id'); 
	}
	
}