<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Utilisateur_type extends Model
{
	protected $table = 'utilisateur_type'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label'
    ];
	protected $primaryKey='id_utilisateur_type';
	public $timestamps = false;
	
	public function utilisateur()
    {
        return $this->hasOne('App\Models\Utilisateur' ,  'utilisateur_type_id');
    }
}