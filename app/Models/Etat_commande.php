<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etat_commande extends Model
{
	protected $table = 'etat_commande'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label'
    ];
	protected $primaryKey='id_etat_commande';
	public $timestamps = false;
	
	
}