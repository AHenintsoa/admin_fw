<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entreprise_type extends Model
{
	protected $table = 'entreprise_type'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'entreprise_id' , 'type_entreprise_id' 
    ];
	protected $primaryKey='id_entreprise_type';
	public $timestamps = false;
	
	public function entreprise()
    {
        return $this->hasManyThrough('App\Models\Type_entreprise', 'App\Models\Entreprise');

    }
	
}