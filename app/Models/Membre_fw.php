<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Membre_fw extends Model
{
	protected $table = 'membre_fw'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'nom','email','tel','logo','poste_id','utilisateur_id','pays_id'
    ];
	protected $primaryKey='id_membre_fw';
	public $timestamps = false;
	
	public function pays() { 
		
		return $this->belongsTo('App\Models\Pays' , 'pays_id'); 
	} 
	public function utilisateur() { 
		
		return $this->belongsTo('App\Models\Utilisateur' , 'utilisateur_id'); 
	} 
	public function poste() { 
		
		return $this->belongsTo('App\Models\Poste' , 'poste_id'); 
	} 
}