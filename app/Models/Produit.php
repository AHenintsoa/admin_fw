<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
	protected $table = 'produit'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'nom_produit','type_produit_id','description','poids','volume','type_aliment_id','prix' , 'groupement'
    ];
	protected $primaryKey='id_produit';
	public $timestamps = false;
	
	public function information_nutritionnelle()
    {
        return $this->belongsToMany('App\Models\Information_nutritionnelle'  ,'produit_information_nutritionnelle' , 'produit_id' , 'information_nutritionnelle_id');
    }
	public function photo_produit()
    {
        return $this->hasMany('App\Models\Photo_produit' , 'produit_id');
    }
	public function produit_equipement()
    {
        return $this->hasMany('App\Models\Produit_equipement' , 'produit_id');
    }
	public function type_produit() { 
		
		return $this->belongsTo('App\Models\Type_produit' , 'type_produit_id'); 
	}
	public function type_aliment() { 
		
		return $this->belongsTo('App\Models\Type_aliment' , 'type_aliment_id'); 
	}
	public function donation() { 
		
		return $this->hasOne('App\Models\Donation' , 'produit_id_produit'); 
	}	
	public function equipement()
    {
        return $this->belongsToMany('App\Models\Equipement'  ,'produit_equipement' , 'produit_id' , 'equipement_id');
    }
}