<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nombre_enfant extends Model
{
	protected $table = 'nombre_enfant'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'organisation_id' , 'tranche_age' , 'nombre' 
    ];
	protected $primaryKey='id_nombre_enfant';
	public $timestamps = false;
	
	
}