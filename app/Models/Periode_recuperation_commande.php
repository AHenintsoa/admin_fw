<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Periode_recuperation_commande extends Model
{
	protected $table = 'periode_recuperation_commande'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'jour','horaire_debut','horaire_fin','donation_id'
    ];
	protected $primaryKey='id_periode_recuperation_commande';
	public $timestamps = false;
	
	
}