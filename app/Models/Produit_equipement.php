<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produit_equipement extends Model
{
	protected $table = 'produit_equipement'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'produit_id' , 'equipement_id' , 'etat' 
    ];
	protected $primaryKey='id_produit_equipement';
	public $timestamps = false;
	
	public function equipement() { 
		
		return $this->belongsTo('App\Models\Equipement' , 'equipement_id'); 
	} 
}