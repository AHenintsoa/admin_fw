<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
	protected $table = 'donation'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'entreprise_id' , 'organisation_id' , 'produit_id_produit ' ,'quantite' , 'moment ' ,'quantite' , 'date_peremption_produit' ,'etat_donation_id' , 'action' , 'type_date' ,'minimum_qte_vente' , 'adresse_ramassage_id' ,'pourcentage_reduction' 
    ];
	protected $primaryKey='id_donation';
	public $timestamps = false;
	
	public function entreprise() { 
		
		return $this->belongsTo('App\Models\Entreprise' , 'entreprise_id'); 
	}
	public function organisation() { 
		
		return $this->belongsTo('App\Models\Organisation' , 'organisation_id'); 
	}
	public function etat_donation() { 
		
		return $this->belongsTo('App\Models\Etat_donation' , 'etat_donation_id'); 
	}
	public function produit() { 
		
		return $this->belongsTo('App\Models\Produit' , 'produit_id_produit'); 
	}
	public function raison_surplus()
    {
        return $this->belongsToMany('App\Models\Raison_surplus'  ,'raison_donation' , 'donation_id' , 'raison_surplus_id');
    }
}