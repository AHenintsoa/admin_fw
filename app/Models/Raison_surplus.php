<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Raison_surplus extends Model
{
	protected $table = 'raison_surplus'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label' , 'proprietaire' 
    ];
	protected $primaryKey='id_raison_surplus';
	public $timestamps = false;
	
	
}