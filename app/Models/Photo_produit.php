<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Photo_produit extends Model
{
	protected $table = 'photo_produit'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'produit_id','url','proprietaire'
    ];
	protected $primaryKey='id_photo_produit';
	public $timestamps = false;
	
	
}