<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Adresse_entreprise extends Model
{
	protected $table = 'adresse_entreprise'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'entreprise_id' , 'adresse_id' 
    ];
	protected $primaryKey='id_adresse_entreprise';
	public $timestamps = false;
	
	
}