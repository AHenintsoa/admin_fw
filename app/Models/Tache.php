<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tache extends Model
{
	protected $table = 'tache'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label','objectif','description','utilisateur_id','date_assignation','date_echeance'
    ];
	protected $primaryKey='id_tache';
	public $timestamps = false;

	public function utilisateur() { 
		
		return $this->belongsTo('App\Models\Utilisateur' , 'utilisateur_id'); 
	} 
	
}