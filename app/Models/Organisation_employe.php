<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organisation_employe extends Model
{
	protected $table = 'organisation_employe'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'employe_id' , 'organisation_id', 'position' 
    ];
	protected $primaryKey='id_organisation_employe';
	public $timestamps = false;
	
	public function employe() { 
		
		return $this->belongsTo('App\Models\Employe' , 'employe_id'); 
	} 
}