<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type_produit extends Model
{
	protected $table = 'type_produit'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label' , 'proprietaire' 
    ];
	protected $primaryKey='id_type_produit';
	public $timestamps = false;
	
	
}