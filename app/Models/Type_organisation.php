<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type_organisation extends Model
{
	protected $table = 'type_organisation'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label' , 'proprietaire' 
    ];
	protected $primaryKey='id_type_organisation';
	public $timestamps = false;
	
	
}