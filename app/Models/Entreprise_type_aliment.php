<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entreprise_type_aliment extends Model
{
	protected $table = 'entreprise_type_aliment'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'entreprise_id' , 'type_aliment_id' 
    ];
	protected $primaryKey='id_entreprise_type_aliment';
	public $timestamps = false;
	
	
	
}