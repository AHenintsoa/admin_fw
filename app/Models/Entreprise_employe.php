<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entreprise_employe extends Model
{
	protected $table = 'entreprise_employe'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'entreprise_id' , 'employe_id' , 'position' 
    ];
	protected $primaryKey='id_entreprise_employe';
	public $timestamps = false;
	
	public function employe() { 
		
		return $this->belongsTo('App\Models\Employe' , 'employe_id'); 
	} 
}