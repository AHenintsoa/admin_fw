<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type_aliment extends Model
{
	protected $table = 'type_aliment'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label' , 'proprietaire' 
    ];
	protected $primaryKey='id_type_aliment';
	public $timestamps = false;
	
	public function entreprise()
    {
		// return $this->belongsToMany('App\Question', 'tblquestionagecategory', 'qac_aca_id', 'qac_que_id');
        return $this->belongsToMany('App\Models\Entreprise'  ,'entreprise_type_aliment' , 'type_aliment_id' , 'entreprise_id');
    }
}