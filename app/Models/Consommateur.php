<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consommateur extends Model
{
	protected $table = 'consommateur'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'utilisateur_id','nom','prenom' ,'mail','mobile_banking','adresse_de_livraison','numero_telephone'
    ];
	protected $primaryKey='id_consommateur';
	public $timestamps = false;
	
	
}