<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model
{
	protected $table = 'utilisateur'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'pseudo','password','utilisateur_type_id'
    ];
	protected $primaryKey='id_utilisateur';
	public $timestamps = false;
	
	public function utilisateur_type() { 
		
		return $this->belongsTo('App\Models\Utilisateur_type' , 'utilisateur_type_id'); 
	} 
	
	public function membre_fw()
    {
        return $this->hasOne('App\Models\Membre_fw', 'utilisateur_type_id');
    }
	
	public function tache()
    {
        return $this->hasOne('App\Models\Tache', 'utilisateur_type_id');
    }
	public function entreprise()
    {
        return $this->hasOne('App\Models\Entreprise', 'utilisateur_id');
    }
	public function consommateur()
    {
        return $this->hasOne('App\Models\Consommateur', 'utilisateur_id');
    }
}