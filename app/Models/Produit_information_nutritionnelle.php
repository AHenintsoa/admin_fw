<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produit_information_nutritionnelle extends Model
{
	protected $table = 'produit_information_nutritionnelle'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'produit_id' , 'information_nutritionnelle_id' 
    ];
	protected $primaryKey='id_produit_information_nutritionnelle';
	public $timestamps = false;
	
	
}