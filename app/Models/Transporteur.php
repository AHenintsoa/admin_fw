<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transporteur extends Model
{
	protected $table = 'transporteur'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label' , 'logo' 
    ];
	protected $primaryKey='id_transporteur';
	public $timestamps = false;
	
	
}