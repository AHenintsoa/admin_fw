<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entreprise extends Model
{
	protected $table = 'entreprise'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'nom_entreprise' , 'ca_entreprise' , 'qte_kg ' ,'qte_mga' , 'logo ' ,'utilisateur_id' , 'nif_entreprise' ,'stat_entreprise' 
    ];
	protected $primaryKey='id_entreprise';
	public $timestamps = false;
	
	public function utilisateur() { 
		
		return $this->belongsTo('App\Models\Utilisateur' , 'utilisateur_id'); 
	} 
	
	public function entreprise_type(){
		
		return $this->hasMany('App\Models\Entreprise_type' , 'entreprise_id');
	}
	public function type_entreprise()
    {
        return $this->belongsToMany('App\Models\Type_entreprise'  ,'entreprise_type' , 'entreprise_id' , 'type_entreprise_id');
    }
	public function type_aliment()
    {
        return $this->belongsToMany('App\Models\Type_aliment'  ,'entreprise_type_aliment' , 'entreprise_id' , 'type_aliment_id');
    }
	public function adresse()
    {
        return $this->belongsToMany('App\Models\Adresse'  ,'adresse_entreprise' , 'entreprise_id' , 'adresse_id');
    }
	public function entreprise_compte()
    {
        return $this->hasMany('App\Models\Entreprise_compte' , 'entreprise_id');
    }
	public function compte()
    {
        return $this->belongsToMany('App\Models\Compte'  ,'entreprise_compte' , 'entreprise_id' , 'compte_id');
    }
	public function entreprise_employe()
    {
        return $this->hasMany('App\Models\Entreprise_employe' , 'entreprise_id');
    }
}