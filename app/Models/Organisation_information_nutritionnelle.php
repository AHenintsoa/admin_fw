<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organisation_information_nutritionnelle extends Model
{
	protected $table = 'organisation_information_nutritionnelle'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'information_nutritionnelle_id' , 'organisation_id' 
    ];
	protected $primaryKey='id_organisation_information_nutritionnelle';
	public $timestamps = false;
	
	
}