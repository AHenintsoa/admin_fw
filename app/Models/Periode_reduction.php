<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Periode_reduction extends Model
{
	protected $table = 'periode_reduction'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label','nombre_jour'
    ];
	protected $primaryKey='id_periode_reduction';
	public $timestamps = false;
	
	public function strategie_reduction() { 
		return $this->hasOne('App\Models\Strategie_reduction' , 'periode_reduction_id'); 
	}
}