<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Poste extends Model
{
	protected $table = 'poste'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label','type','proprietaire'
    ];
	protected $primaryKey='id_poste';
	public $timestamps = false;
	
	public function membre_fw()
    {
        return $this->hasOne('App\Models\Membre_fw' , 'poste_id');
    }
	
}