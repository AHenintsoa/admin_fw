<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organisation_beneficiaire_organisationPrimaire extends Model
{
	protected $table = 'organisation_beneficiaire_organisationPrimaire'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'beneficiaire_organisation_id' , 'organisation' 
    ];
	protected $primaryKey='id_organisation_beneficiaire_organisationPrimaire';
	public $timestamps = false;
	
	
}