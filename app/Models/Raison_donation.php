<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Raison_donation extends Model
{
	protected $table = 'raison_donation'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'raison_surplus_id' , 'donation_id' 
    ];
	protected $primaryKey='id_raison_donation';
	public $timestamps = false;
	
	
}