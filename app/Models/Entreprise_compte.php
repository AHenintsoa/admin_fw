<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entreprise_compte extends Model
{
	protected $table = 'entreprise_compte'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'id_entreprise_compte' , 'numero_compte' , 'compte_id' , 'entreprise_id' 
    ];
	protected $primaryKey='id_entreprise_compte';
	public $timestamps = false;
	
	// public function compte() { 
		
		// return $this->belongsTo('App\Models\Compte' , 'compte_id'); 
	// } 
	
}