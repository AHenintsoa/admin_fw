<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Equipement extends Model
{
	protected $table = 'equipement'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label','etat'
    ];
	protected $primaryKey='id_equipement';
	public $timestamps = false;
	
	public function produit_equipement() { 
		
		return $this->hasMany('App\Models\Produit_equipement' , 'equipement_id'); 
	}
	
}