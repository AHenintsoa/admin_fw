<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etat_donation extends Model
{
	protected $table = 'etat_donation'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'label' 
    ];
	protected $primaryKey='id_etat_donation';
	public $timestamps = false;
	
	
}