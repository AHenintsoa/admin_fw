<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
	protected $table = 'organisation'; // Nom de la table concernée par cette classe
	protected $fillable = [
        'nom_organisation' , 'adresse_id' , 'type_organisation_id ' ,'numero' 
    ];
	protected $primaryKey='id_organisation';
	public $timestamps = false;
	
	public function type_organisation() { 
		return $this->belongsTo('App\Models\Type_organisation' , 'type_organisation_id'); 
	}
	public function adresse() { 
		return $this->belongsTo('App\Models\Adresse' , 'adresse_id'); 
	}
	
	public function organisation_employe() { 
		return $this->hasMany('App\Models\Organisation_employe' , 'organisation_id'); 
	}
	
}