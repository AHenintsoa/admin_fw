<?php

namespace App\Http\Controllers;

use App\Models\Entreprise;
use App\Models\Entreprise_type;
use App\Models\Type_entreprise;
use App\Models\Employe;
use App\Models\Poste;
use App\Models\Type_aliment;
use App\Models\Utilisateur;
use App\Models\Organisation;
use App\Models\Organisation_employe;
use App\Models\Type_organisation;
use App\Models\Adresse;
use App\Models\Pays;
use App\Models\Adresse_entreprise;
use App\Models\Entreprise_type_aliment;
use App\Models\Donation;
use App\Models\Entreprise_employe;
use App\Models\Consommateur;
use App\Models\Produit;
use App\Models\Photo_produit;
use App\Models\Type_produit;
use App\Models\Strategie_reduction;
use App\Models\Equipement;
use App\Models\Periode_recuperation_commande;
use App\Models\Produit_equipement;
use App\Models\Information_nutritionnelle;
use App\Models\Produit_information_nutritionnelle;
use App\Models\Raison_donation;
use App\Models\Raison_surplus;
use App\Models\Periode_reduction;
use Illuminate\Http\Request;
Use Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Exports\ReceveurExport;
use App\Exports\ClientExport;
use App\Exports\EntrepriseExport;
use App\Exports\ProduitExport;

// use Maatwebsite\Excel\Facades\Excel;
// use DB;
use Excel ;

use Swift_TransportException ;


class ContacterController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {	
    	parent::__construct();
        return $this->render("map","");	
    }
	
	/** recherche */
	
	/****  Autres fonctionnalites   *****/
	
	
	
	public function page_contacterEntreprise(Request $request){
		 
		$entreprise = Entreprise::find($request->get('id_entreprise') );
		$liste_responsable = $entreprise->entreprise_employe()->where('position' , '=', '1' ,'or' , 'position' , '=' , '2')->where('entreprise_id' , $request->get('id_entreprise'))->get();

		// var_dump($liste_responsable[0]) ; die();
		$type="entreprise";
		$params= array(
			"entreprise" => $entreprise ,
			"liste_responsable" => $liste_responsable ,
			"type" => $type ,
			
		);
		return $this->render("page_contacter","","")->with($params);	
		 
	}
	public function page_contacterClient(Request $request){
		 
		$client = Consommateur::find($request->get('id_consommateur') );
		$type="client";
		$params= array(
			"client" => $client ,
			"type" => $type ,
			
		);
		return $this->render("page_contacter","")->with($params);	
		 
	}
	public function page_contacterReceveur(Request $request){
		 
		$organisation = Organisation::find($request->get('id_organisation') );
		$email = $organisation->organisation_employe()->where('position', '1')->first()->employe->email;
		
		$type="organisation";
		$params= array(
			"organisation" => $organisation ,
			"type" => $type ,
			"email" => $email ,
			
		);
		return $this->render("page_contacter","")->with($params);	
		 
	}
	public function contacter(Request $request){
			$email_destinataire= $request->get('email_destinataire');
			$sujet= $request->get('sujet');
			$contenumessage= $request->get('message');

		   $data = array (
				"contenumessage" => $contenumessage,
			) ;
		   try {
		   		$resultat = Mail::send('emails.contacter', $data, function ($message) use( $email_destinataire , $sujet  )  {
				$message->subject($sujet);
				$message->to($email_destinataire);
				
			});
		   } catch (Swift_TransportException $e) {
		   		return "erreur";
		   }
			return "succes";
	}


}
