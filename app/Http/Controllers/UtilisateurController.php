<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Utilisateur;
use Illuminate\Support\Facades\Hash;
use Validator;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class UtilisateurController extends BaseController
{
    //protected $action = "Accueil";

    public function __construct(){
        $this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
		
		$liste_utilisateur = Utilisateur::all();
        //var_dump($liste_utilisateur);
		return view("liste_utilisateur", compact('liste_utilisateur') );
    }
	
	public function page_login() {
		return $this->render("auth\login");
   }
   
   
   
	
}