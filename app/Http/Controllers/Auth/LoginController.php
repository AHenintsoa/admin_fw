<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Utilisateur;
use Validator;
use App\Models\Utilisateur_type;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
	
	public function authenticate(Request $request)
    {
        $credentials = $request->only('pseudo', 'password');
		$validator = Validator::make($request->all(), [
            'pseudo' => 'bail|required',
            'password' => 'bail|required',
            
        ]);
		 if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        if (Auth::attempt($credentials)) {
            // Authentication passed...
			
			$utilisateur_type_id= Auth::user()->utilisateur_type_id;
			$id_utilisateur= Auth::user()->id_utilisateur;
			$utilisateur=Utilisateur::find($id_utilisateur);
			if($utilisateur->active=='0'){
				Session::flash('login_erreur', "vous n'êtes plus autorisés");
				//Auth::logout();
				return redirect('logout');
			}
			$utilisateur_type=Utilisateur_type::where('id_utilisateur_type', $utilisateur_type_id)->first();
			
			$request->session()->put('id_utilisateur', $id_utilisateur);
			if($utilisateur_type!=null){
				$request->session()->put('id_utilisateur_type', $utilisateur_type->id_utilisateur_type);
				$request->session()->put('id_utilisateur', $id_utilisateur);
			}
            return redirect()->intended('/');
        
        }else {
			Session::flash('message', "un des champs est incorrect");
			return back()->withInput(Input::all());;
		}
	}
	
}
