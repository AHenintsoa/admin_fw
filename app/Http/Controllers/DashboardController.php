<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Utilisateur;
use App\Models\Membre_fw;
use App\Models\Pays;
use App\Models\Poste;
use App\Models\Tache;
use App\Models\Entreprise;
use App\Models\Etat_donation;
use App\Models\Consommateur;
use App\Models\Organisation;
use App\Models\Utilisateur_type;
use App\Models\Donation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\Brouillon;
use Validator;
use DateTime;
class DashboardController extends BaseController
{
	

    public function __construct(){
    	parent::__construct();
		$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	 /////// encours
	 
	 public function getDetailVenteEncours(Request $request){
		
		
		  $donation_encours = DB::table('donation')
            ->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
           // ->leftJoin('produit_information_nutritionnelle', 'produit_information_nutritionnelle.produit_id', '=', 'produit.id_produit')
            // ->leftJoin('information_nutritionnelle', 'information_nutritionnelle.id_information_nutritionnelle', '=', 'produit_information_nutritionnelle.information_nutritionnelle_id')
            ->join('type_aliment', 'type_aliment.id_type_aliment', '=', 'produit.type_aliment_id')
            ->join('type_produit', 'type_produit.id_type_produit', '=', 'produit.type_produit_id')
			->join('produit_commande', 'produit_commande.donation_id', '=', 'donation.id_donation')
            ->join('commande', 'commande.id_commande', '=', 'produit_commande.commande_id')
             // ->leftJoin('raison_donation', 'raison_donation.donation_id', '=', 'donation.id_donation')
             // ->leftJoin('raison_surplus', 'raison_surplus.id_raison_surplus', '=', 'raison_donation.raison_surplus_id')
            ->select( 'produit.nom_produit' ,'produit.description' , 'produit_commande.qte' , DB::raw('produit.volume*produit_commande.qte as volume') , DB::raw('produit_commande.prix_achat_unite*produit_commande.qte as montant') ,'produit_commande.prix_achat_unite as prix_unitaire', 'type_produit.label as type_produit', 'type_aliment.label as type_aliment' , 'produit.description')
			->where('commande.id_commande' , '=' , $request->get('id_commande'))
			->get();
			
			// var_dump($donation_encours);
			// die();
			return response($donation_encours);
	  }
	 public function getDetailDonEncours(Request $request){
		  
		  $donation_encours = DB::table('donation')
            ->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
            ->leftJoin('produit_information_nutritionnelle', 'produit_information_nutritionnelle.produit_id', '=', 'produit.id_produit')
            ->leftJoin('information_nutritionnelle', 'information_nutritionnelle.id_information_nutritionnelle', '=', 'produit_information_nutritionnelle.information_nutritionnelle_id')
            ->join('type_aliment', 'type_aliment.id_type_aliment', '=', 'produit.type_aliment_id')
            ->join('type_produit', 'type_produit.id_type_produit', '=', 'produit.type_produit_id')
             ->leftJoin('raison_donation', 'raison_donation.donation_id', '=', 'donation.id_donation')
             ->leftJoin('raison_surplus', 'raison_surplus.id_raison_surplus', '=', 'raison_donation.raison_surplus_id')
            ->select( 'donation.id_donation','produit.description' , 'raison_surplus.label as raison_surplus' , DB::raw('produit.volume*donation.quantite as volume') , 'type_produit.label as type_produit', 'type_aliment.label as type_aliment')
			->where('donation.id_donation' , '=' , $request->get('id_donation'))
			->get();
			
			
			return response($donation_encours);
	  }
	 
	 public function mail(Request $request){
		 if($request->get('email')==null){ //pour les brouillons
			$liste_info= $this->getInformationBrouillon($request->get('id_donation'));
			$defaut_brouillon = $request->get('defaut'); // 0 si info manquant sinon 1 si non valide 
		   $data = array (
				"nom_entreprise" => $liste_info['nom_entreprise'],
				"liste_info" => $liste_info['information_incomplet'],
				'link' => url('dashboard_page'),
				'defaut' => $defaut_brouillon ,
			) ;
		   $email_destinataire = $liste_info['email'] ;
		   $resultat = Mail::send('emails.brouillon', $data, function ($message) use( $email_destinataire , $defaut_brouillon)  {
				if($defaut_brouillon==0) $message->subject("Vous avez manqué de remplir votre brouillon");
				else $message->subject("Vous avez manqué de valider votre brouillon");
				$message->to($email_destinataire);
			});
			if(!$resultat){
				Session::flash('succes' , 1);
			}else{
				Session::flash('succes' , -1);
			}
			return back();
		 }else{ // por info profil
		 	

		 	$entreprise = Entreprise::find( $request->get('entreprise') );
			 $data = array (
					"nom_entreprise" => $entreprise->nom_entreprise,
					'link' => 'http://localhost/admin_fw/public/dashboard_page',
				) ;
			 $email_destinataire = $request->get('email');
		   $resultat = Mail::send('emails.brouillon', $data, function ($message) use( $email_destinataire )  {
				$message->subject("Vous avez manqué de compléter les informationssur vous");
				$message->to($email_destinataire);
			});
			if(!$resultat){
				// return response(true);
				return "ok";
			}else{
				return "oka";
				// return response(false);
			}
		 }
	}
	  public function getDetailBrouillon(Request $request){
		  
		  $donation_brouillon = DB::table('donation')
			->join('entreprise' , 'entreprise.id_entreprise', 'donation.entreprise_id')
			->select('entreprise.nom_entreprise' , 'donation.*')
			->where('donation.id_donation', $request->get('id_donation'))
			->first();
			$information_incomplet = [] ;
			if($donation_brouillon->produit_id_produit ==null) $information_incomplet[] = "Produit";
			if($donation_brouillon->quantite==null) $information_incomplet[] = "Quantité du produit";
			if($donation_brouillon->adresse_ramassage_id==null && $donation_brouillon->action==2) $information_incomplet[] = "Adresse du ramassage";	
			if($donation_brouillon->organisation_id==null && $donation_brouillon->action==1) $information_incomplet[] = "Receveur"; 	
			
			return response($information_incomplet);
	  }
	  public function getInformationBrouillon($id_donation){
		  
		  $donation_brouillon = DB::table('donation')
			->join('entreprise' , 'entreprise.id_entreprise', 'donation.entreprise_id')
			->join('entreprise_employe' , 'entreprise_employe.entreprise_id' , 'entreprise.id_entreprise')
			->join('employe' , 'employe.id_employe' , 'entreprise_employe.employe_id')
			->select('entreprise.nom_entreprise', 'employe.email' , 'donation.*')
			->where('donation.id_donation', $id_donation)
			->first();
			$information_incomplet = [] ;
			if($donation_brouillon->produit_id_produit ==null) $information_incomplet[] = "Produit";
			if($donation_brouillon->quantite==null) $information_incomplet[] = "Quantité du produit";
			if($donation_brouillon->adresse_ramassage_id==null && $donation_brouillon->action==2) $information_incomplet[] = "Adresse du ramassage";	
			if($donation_brouillon->organisation_id==null && $donation_brouillon->action==1) $information_incomplet[] = "Receveur"; 	
			
			$resultat = array (
				"nom_entreprise" => $donation_brouillon->nom_entreprise,
				"information_incomplet" => $information_incomplet,
				"email" => $donation_brouillon->email,
				);
			return $resultat;
	  }

	 
	 public function getDashboard_encours(){
		 	// Voir les donations et ventes en cours (comme au back end entreprises : etat, receveur, kg, quoi, etc.) 
		 $donation_encours = DB::table('donation')
            ->join('organisation', 'organisation.id_organisation', '=', 'donation.organisation_id')
            ->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
            ->join('entreprise', 'entreprise.id_entreprise', '=', 'donation.entreprise_id')
            ->join('etat_donation', 'etat_donation.id_etat_donation', '=', 'donation.etat_donation_id')
            ->select( 'donation.id_donation' , 'organisation.nom_organisation' , DB::raw('produit.poids*donation.quantite as poids') , 'produit.nom_produit' ,'donation.quantite' ,'donation.etat_donation_id' ,  'entreprise.nom_entreprise' , 'etat_donation.label', DB::raw('DATE_FORMAT(donation.moment ,"%d-%M-%Y") as date'))
			->where('donation.etat_donation_id' , '!=' , '5')
			->where('donation.etat_donation_id' , '!=' , '6')
			->where('donation.action' , '=' , '1')
			->get();
			setlocale(LC_TIME, "fr_FR");
		for($i = 0 ; $i < count($donation_encours) ; $i++){
			setlocale(LC_TIME, "fr_FR");
			$date=new DateTime($donation_encours[$i]->date);
			 $date = $date->format('d-m-Y');
			$donation_encours[$i]->date = strftime("%d %B %G", strtotime($date));
		}
		//liste etat donation
		$liste_etat_donation = Etat_donation::where('id_etat_donation', '!=' , '5' ,'and' ,'id_etat_donation', '!=' , '6' ,'and' ,'id_etat_donation', '!=' , '7' ,'and' ,'id_etat_donation', '!=' , '8' )
								->get();
		 $vente_encours = DB::table('donation')
			->join('produit_commande' , 'produit_commande.donation_id' , 'donation.id_donation')
			->join('commande' , 'commande.id_commande' , 'produit_commande.commande_id')
            ->join('etat_commande' , 'etat_commande.id_etat_commande' , 'commande.etat')
            ->join('consommateur' , 'consommateur.id_consommateur' , 'commande.consommateur_id')
            ->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
            ->join('entreprise', 'entreprise.id_entreprise', '=', 'donation.entreprise_id')
            // ->join('etat_donation', 'etat_donation.id_etat_donation', '=', 'donation.etat_donation_id')
            ->select('commande.id_commande' ,'consommateur.nom' ,'consommateur.prenom' , DB::raw('sum(produit.poids*produit_commande.qte) as poids')  ,  DB::raw('sum(produit_commande.qte) as quantite')  , DB::raw('sum(produit_commande.prix_achat_unite*produit_commande.qte) as montant') , 'etat_commande.label' , DB::raw('DATE_FORMAT(commande.date ,"%d-%M-%Y") as date'))
			// ->where('donation.etat_donation_id' , '!=' , '5')
			->where('commande.etat' , '=' , '1')
			->where('donation.action' , '=' , '2')
			->groupBy('commande.id_commande')
			->orderBy('commande.id_commande' , 'asc')
			->get();
			setlocale(LC_TIME, "fr_FR");
		for($i = 0 ; $i < count($vente_encours) ; $i++){
			setlocale(LC_TIME, "fr_FR");
			$date=new DateTime($vente_encours[$i]->date);
			 $date = $date->format('d-m-Y');
			$vente_encours[$i]->date = strftime("%d %B %G", strtotime($date));
		}
		// var_dump($vente_encours);
		// die();
		 // Voir les strategies marketings des entreprises
		 $strategies_entreprise_encours = DB::table('tache')
			->join('entreprise' , 'entreprise.id_entreprise' , 'tache.entreprise_concernee')
			->select('entreprise.nom_entreprise'  , 'tache.objectif' , 'tache.montant_objectif' , DB::raw('DATE_FORMAT(tache.date_echeance ,"%d-%M-%Y") as date_echeance'))
			->where('tache.date_echeance' , '>=' , DB::raw('now()'))
			->get();
			setlocale(LC_TIME, "fr_FR");
		for($i = 0 ; $i < count($strategies_entreprise_encours) ; $i++){
			setlocale(LC_TIME, "fr_FR");
			$date=new DateTime($strategies_entreprise_encours[$i]->date_echeance);
			 $date = $date->format('d-m-Y');
			$strategies_entreprise_encours[$i]->date_echeance = strftime("%d %B %G", strtotime($date));
		}	
		 // 	Voir s’il y a des info incomplets (a toute stade) afin de pouvoir réagir
		 $donation_brouillon = DB::table('donation')
			->join('entreprise' , 'entreprise.id_entreprise', 'donation.entreprise_id')
			->select('entreprise.nom_entreprise' , 'donation.*')
			->where('donation.etat_donation_id', 5)
			->get();
			// 	Voir s’il y a des info incomplets dans le profil
				// DB::enableQueryLog(); //before query

		 $profil_incomplet = DB::table('entreprise')
			->leftJoin('entreprise_compte' , 'entreprise.id_entreprise', 'entreprise_compte.entreprise_id')
			->leftJoin(DB::raw('(select distinct(entreprise_id) as entreprise_id , entreprise_type.id_entreprise_type from entreprise_type group by entreprise_id) as  type') , 'entreprise.id_entreprise', 'type.entreprise_id')
			 // ->select( DB::raw('distinct(entreprise.nom_entreprise)') ,'entreprise.*' , 'entreprise_compte.*' , 'entreprise_type.id_entreprise_type'  )
			->leftJoin('entreprise_employe' , 'entreprise_employe.entreprise_id' , 'entreprise.id_entreprise')
			->leftJoin('employe' , 'employe.id_employe' , 'entreprise_employe.employe_id')
			->whereNull('entreprise.ca_entreprise' )
			->orWhereNull('entreprise.logo' )
			->orWhereNull('nif_entreprise' )
			->orWhereNull('stat_entreprise')
			->orWhereNull('id_entreprise_compte' )
			->orWhereNull('numero_compte' )
			->orWhereNull('id_entreprise_type' )
			->get();
		// var_dump(DB::getQueryLog());  //  visualiser la requete
// var_dump($profil_incomplet);  //  visualiser la requete
		// die();		
		$champs_manquant = array(array());
		for($i = 0 ; $i < count($profil_incomplet) ; $i++){
			if($profil_incomplet[$i]->ca_entreprise ==null){
				$champs_manquant[$i][] = 'Chiffre d\'affaire';
			}
			if($profil_incomplet[$i]->logo ==null){
				$champs_manquant[$i][] = 'Photo du profil';
			}
			if($profil_incomplet[$i]->nif_entreprise ==null){
				$champs_manquant[$i][] = 'NIF';
			}
			if($profil_incomplet[$i]->stat_entreprise ==null){
				$champs_manquant[$i][] = 'STAT';
			}
			if($profil_incomplet[$i]->numero_compte ==null){
				$champs_manquant[$i][] = 'Numero de compte';
			}
			if($profil_incomplet[$i]->id_entreprise_type ==null){
				$champs_manquant[$i][] = 'Type de l\'entreprise';
			}			
		}
		$liste_profil = array (
								"profil_incomplet" => $profil_incomplet ,
								"champs_manquant" => $champs_manquant ,
		);
			
		
		// var_dump(json_encode($liste_profil['champs_manquant']));  //  visualiser la requete
		// var_dump($profil_incomplet);  //  visualiser la requete
		// die();
				
		$type_probleme_brouillon = [];
		for($i = 0 ; $i< count($donation_brouillon) ; $i++ ){
			
			if( $donation_brouillon[$i]->produit_id_produit == null || $donation_brouillon[$i]->quantite == null || ($donation_brouillon[$i]->adresse_ramassage_id == null  && $donation_brouillon[$i]->action == 2) || ($donation_brouillon[$i]->organisation_id == null && $donation_brouillon[$i]->action == 1 )   ){
				$type_probleme_brouillon[] = "information(s) manquante(s)" ;
			}else{
				$type_probleme_brouillon[] = "don/vente non validé(e)" ;
			}
		}
		
		$params = array(
			"donation_encours" => $donation_encours ,
			"vente_encours" => $vente_encours ,
			"strategies_entreprise_encours" => $strategies_entreprise_encours ,
			"donation_brouillon" => $donation_brouillon ,
			"type_probleme_brouillon"=> $type_probleme_brouillon ,
			"liste_etat_donation"=> $liste_etat_donation ,
			"liste_profil" => $liste_profil,
			);
			
		
		return $this->render("dashboard_encours","dashboard", "dashboard_encours")->with($params);	
	 }
	 
	 /* fin encours    */
	 
	  public function getRepas_mois(Request $request){
		 
		//liste organisation
		$liste_organisation = Organisation::all();
		$annee = $request->get('annee');
		//nombre organisation	
		$effectif_organisation = $liste_organisation->count() ;	
		// get repas par mois par organisation
		$array_liste_repas_distribue_mois=array();
		for($i = 0 ; $i < $effectif_organisation ; $i++) {
			$array_liste_repas_distribue_mois[]= DB::table('organisation')
			->leftJoin(DB::raw('(select donation.organisation_id , AVG(produit.poids*donation.quantite/0.25) as repas , month(donation.moment) as mois
						from donation
						join produit on produit.id_produit=donation.produit_id_produit
						where donation.etat_donation_id!=5 and donation.action = 1 and  year(donation.moment) = '.$annee.' group by donation.organisation_id , mois ) as tab1') , 'tab1.organisation_id' , 'organisation.id_organisation' )
			->where('organisation.id_organisation', $liste_organisation[$i]->id_organisation )
			->orderBy('tab1.mois' , 'asc')
			->get();
		}
		/*teste*/
		// for($i = 0 ; $i< count($array_liste_repas_distribue_mois) ; $i++){
			// var_dump($array_liste_repas_distribue_mois[$i]);
		// }
		// die();
		/*****fin tste */
		// structure en mois complet
		for($indice = 0 ; $indice < $effectif_organisation  ; $indice++){
			 // var_dump($array_liste_repas_distribue_mois[$indice]) ; 
			$length = count($array_liste_repas_distribue_mois[$indice]);		
			$repas_distribue_mois=[];
			$j=0;
			for ( $i =0 ; $i< $length ; $i++) {
				if($length == 1 &&  $array_liste_repas_distribue_mois[$indice][$i]->mois == null){
					for( $k=0  ; $k < 12 ; $k++){
						$repas_distribue_mois[] = array( 
													"repas" => 0,
													"nom_organisation" => $array_liste_repas_distribue_mois[$indice][$i]->nom_organisation,
													);
					}
					break;
				}

				
				if($i+1 != $array_liste_repas_distribue_mois[$indice][$i]->mois ){
					for( $j  ; $j < $array_liste_repas_distribue_mois[$indice][$i]->mois-1 ; $j++){
						$repas_distribue_mois[] = array( 
													"repas" => 0,
													"nom_organisation" => $array_liste_repas_distribue_mois[$indice][$i]->nom_organisation,
													);
					}
					$repas_distribue_mois[] =   array( 
													"repas" => $array_liste_repas_distribue_mois[$indice][$i]->repas ,
													"nom_organisation" => $array_liste_repas_distribue_mois[$indice][$i]->nom_organisation,
													);
					$j =  $array_liste_repas_distribue_mois[$indice][$i]->mois;
				} 
				if($i+1 == $array_liste_repas_distribue_mois[$indice][$i]->mois){
					$repas_distribue_mois[] =   array( 
													"repas" => $array_liste_repas_distribue_mois[$indice][$i]->repas ,
													"nom_organisation" => $array_liste_repas_distribue_mois[$indice][$i]->nom_organisation,
													);
					$j++;
				}
				if($array_liste_repas_distribue_mois[$indice][$i]->mois <12 && $length == $i+1) {
					for( $k = $array_liste_repas_distribue_mois[$indice][$i]->mois  ; $k < 12 ; $k++){
						$repas_distribue_mois[] = array( 
													"repas" => 0,
													"nom_organisation" => $array_liste_repas_distribue_mois[$indice][$i]->nom_organisation,
													);
					}
				}
			}
			
			 $array_liste_repas_distribue_mois[$indice]=$repas_distribue_mois;
			// var_dump($array_liste_repas_distribue_mois[$indice]);
		}
		return response($array_liste_repas_distribue_mois);
				
		/*---------------  */
	 }
	 
	 public function getRepas_mois_enfant(Request $request){
		
		$annee = $request->get('annee');		
		//liste organisation
		$liste_organisation = Organisation::all();
		//nombre organisation	
		$effectif_organisation = $liste_organisation->count() ;	
		// get repas par mois par enfant par organisation
		// get repas par mois par enfant par organisation
		$array_liste_repas_distribue_mois_enfant=array();
		for($i = 0 ; $i < $effectif_organisation ; $i++) {
		
			$array_liste_repas_distribue_mois_enfant[]= DB::table('organisation')
			->leftjoin(DB::raw('(select donation.organisation_id, avg(produit.poids*donation.quantite/(0.25*effectif_enfant.effectif)) as repas , avg(produit.poids*donation.quantite/0.25) as poids , month(donation.moment) as mois
																from donation 
																join produit 
																on produit.id_produit=donation.produit_id_produit
																join (select DISTINCT organisation_id , sum(nombre) as effectif from nombre_enfant where tranche_age!=5 and tranche_age!=4 group by organisation_id ) as effectif_enfant
																on donation.organisation_id = effectif_enfant.organisation_id
																where donation.etat_donation_id!=5 and year(donation.moment) = '.$annee.'
																group by donation.organisation_id , mois ) as tab1') , 'tab1.organisation_id', '=', 'organisation.id_organisation')
			->where('organisation.id_organisation', $liste_organisation[$i]->id_organisation )
			->orderBy('tab1.mois' , 'asc')
			->get();
			
		}
		 
		// structure en mois complet
		for($indice = 0 ; $indice < $effectif_organisation  ; $indice++){
			$length = count($array_liste_repas_distribue_mois_enfant[$indice]);		
			$repas_distribue_mois=[];
			$j=0;
			for ( $i =0 ; $i< $length ; $i++) {
				if($length == 1 &&  $array_liste_repas_distribue_mois_enfant[$indice][$i]->mois == null){ // orqanisation qui n a pas recu de don en un an
					for( $k=0  ; $k < 12 ; $k++){
						$repas_distribue_mois[] = array( 
													"repas" => 0,
													"nom_organisation" => $array_liste_repas_distribue_mois_enfant[$indice][$i]->nom_organisation,
													);
					}
					break;
				}
				if($i+1 != $array_liste_repas_distribue_mois_enfant[$indice][$i]->mois ){
					for( $j  ; $j < $array_liste_repas_distribue_mois_enfant[$indice][$i]->mois-1 ; $j++){
						$repas_distribue_mois[] = array( 
													"repas" => 0,
													"nom_organisation" => $array_liste_repas_distribue_mois_enfant[$indice][$i]->nom_organisation,
													);
					}
					$repas_distribue_mois[] =  array( 
													"repas" => $array_liste_repas_distribue_mois_enfant[$indice][$i]->repas ,
													"nom_organisation" => $array_liste_repas_distribue_mois_enfant[$indice][$i]->nom_organisation,
													);
					$j =  $array_liste_repas_distribue_mois_enfant[$indice][$i]->mois;
				} else{
					$repas_distribue_mois[] =  array(
													"repas"=>$array_liste_repas_distribue_mois_enfant[$indice][$i]->repas,
													"nom_organisation" => $array_liste_repas_distribue_mois_enfant[$indice][$i]->nom_organisation, 
													);
					$j++;
				}
				if($array_liste_repas_distribue_mois_enfant[$indice][$i]->mois <12 && $length == $i+1) {
					for( $k = $array_liste_repas_distribue_mois_enfant[$indice][$i]->mois  ; $k < 12 ; $k++){
						$repas_distribue_mois[] = array( 
													"repas" => 0,
													"nom_organisation" => $array_liste_repas_distribue_mois_enfant[$indice][$i]->nom_organisation,
													);
					}
				}
			}
			
			 $array_liste_repas_distribue_mois_enfant[$indice]=$repas_distribue_mois;
			// var_dump($array_liste_repas_distribue_mois_enfant[$indice]);
		}
		return response($array_liste_repas_distribue_mois_enfant);
				
		/*---------------  */
	 }
	 /////// commande totale
	 public function getDashboard_commande(){
		 $commande_total = DB::table('commande')
				->join('produit_commande' , 'produit_commande.commande_id' , 'commande.id_commande')
				->join('donation' , 'donation.id_donation' , 'produit_commande.donation_id')
				->join('entreprise' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->join('produit' , 'donation.produit_id_produit' , 'produit.id_produit')
				->join('etat_commande' , 'etat_commande.id_etat_commande' , 'commande.etat')
				->join('consommateur' , 'consommateur.id_consommateur' , 'commande.consommateur_id')
				->select('entreprise.nom_entreprise', 'etat_commande.label as etat' ,'commande.id_commande','consommateur.nom', 'consommateur.prenom' , 'produit.nom_produit' , 'produit_commande.qte' ,'produit_commande.prix_achat_unite as pu', DB::raw('produit_commande.prix_achat_unite* produit_commande.qte as montant' ) ,  DB::raw('date(commande.date) as date'), DB::raw('time(commande.date) as time' ) )
				//->whereBetween('commande.date',[$request->get('date_debut') , $request->get('date_fin')])
				->orderBy('commande.date' , 'asc')
				->orderBy('commande.id_commande' , 'asc')
				->get();
				
		setlocale(LC_TIME, "fr_FR");
		for($i = 0 ; $i < count($commande_total) ; $i++){
			setlocale(LC_TIME, "fr_FR");
			$date=new DateTime($commande_total[$i]->date);
			 $date = $date->format('d-m-Y');
			$commande_total[$i]->date = strftime("%d %B %G", strtotime($date));
		}			
		return $this->render("dashboard_commande_total","dashboard", "dashboard_commande")->with('commande_total' , $commande_total);			
	 }
	 
	 /////// transports
	 public function getDashboard_transport(){
		 
	 }
	 
	  /////// clients
	 public function getDashboard_client(Request $request){
		 // 	Total clients consommateur
		 $nombre_consommateur = Consommateur::all()->count();
		 // Achats (MGA) total par clients
		  $nombre_consommateur_ce_mois = DB::table('consommateur')
			->join('utilisateur' , 'utilisateur.id_utilisateur' , 'consommateur.utilisateur_id' )
			->select(DB::raw('count(*)'))
			->where(DB::raw('month(utilisateur.moment_inscription)') , '=' , DB::raw('month(now())'))
			->first();
		// Produits de préférence globale
		$produits_preference_globaux= DB::table('produit')
			->join('wish_list' , 'wish_list.produit_id' , 'produit.id_produit')
			->join('consommateur' , 'consommateur.id_consommateur' , 'wish_list.consommateur_id')
			->select('produit.nom_produit' , DB::raw('count(*) as nombre') )
			->groupBy('produit.nom_produit')
			->get();
		// Produits de préférence par client
		$produits_preference_par_client= DB::table('produit')
			->join('wish_list' , 'wish_list.produit_id' , 'produit.id_produit')
			->join('consommateur' , 'consommateur.id_consommateur' , 'wish_list.consommateur_id')
			->select('consommateur.nom','consommateur.prenom','produit.nom_produit'  )
			->groupBy('produit.nom_produit')
			->groupBy('consommateur.nom')
			->orderBy('consommateur.id_consommateur', 'asc')
			->get();
		if($request->get('date_debut')!=null || $request->get('date_fin')!=null ){
			$validator = Validator::make($request->all(), [
			'date_debut'=>'required',
			'date_fin'=>'required',
			
		  ], ['date_debut.required'=>'Champs obligatoire',
			'date_fin.required'=>'Champs obligatoire',]);
		  if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}	
			// Achats (MGA) total par clients
			$achat_par_client = DB::table('produit_commande')
				->join('commande' , 'commande.id_commande', 'produit_commande.commande_id')
				->join('consommateur' , 'commande.consommateur_id', 'consommateur.id_consommateur')
				->select('consommateur.nom' , 'consommateur.prenom' , DB::raw('sum( produit_commande.prix_achat_unite*produit_commande.qte) as montant_total') )
				->whereBetween('commande.date',[$request->get('date_debut') , $request->get('date_fin')])
				->groupBy('commande.consommateur_id')
				->get();
			// Réductions la plus choisi par client
			$best_reduction = DB::table('consommateur')
				->join (DB::raw('(select  tab2.reduction as reduc, max(tab2.nombre) , tab2.consommateur_id   from
								(select count(tab1.reduction) as nombre, tab1.reduction as reduction, tab1.consommateur_id as consommateur_id from 
								(select pc.* , c.* , (100*(p.prix-pc.prix_achat_unite)/p.prix) as reduction from produit_commande pc
								join donation d on d.id_donation=pc.donation_id
								join commande c on c.id_commande=pc.commande_id
								join produit p on p.id_produit=d.produit_id_produit where c.date between '.$request->get('date_debut').' and '.$request->get('date_fin').') as tab1  group by tab1.reduction , tab1.consommateur_id order by nombre desc) as tab2 group by tab2.consommateur_id ) as best_reduction') ,'best_reduction.consommateur_id','consommateur.id_consommateur' )
				->select('consommateur.nom' , 'consommateur.prenom' , 'best_reduction.reduc')
				->get();
			// heure et jours où les commandes ar les clietns sont les plus frequents	
			$moment_frequent = DB::table('commande')
				->select(DB::raw('count(*) as nombre') , DB::raw('hour(date) as heure') , DB::raw('DAYOFWEEK(date) as day') )
				->whereBetween('commande.date',[$request->get('date_debut') , $request->get('date_fin')])
				->groupBy('heure')
				->groupBy('day')
				->orderBy('nombre','desc')
				->limit(1)
				->first();
			$jour_frequent = DB::table('commande')
				->select( 'commande.date', DB::raw('dayofweek(date) as jour') , DB::raw('hour(date) as heure') ,  DB::raw('MINUTE(date) as minute') )
				->orderBy('commande.date' , 'asc')
				->whereBetween('commande.date',[$request->get('date_debut') , $request->get('date_fin')])
				->get();
			/*if($moment_frequent->day==1){
				$moment_frequent->day="Dimanche";
			}else if($moment_frequent->day==2){
				$moment_frequent->day="Lundi";
			
			}else if($moment_frequent->day=3){
				$moment_frequent->day="Mardi";
			
			}else if($moment_frequent->day==4){
				$moment_frequent->day="Mercredi";
			
			}else if($moment_frequent->day==5){
				$moment_frequent->day="Jeudi";
			
			}else if($moment_frequent->day==6){
				$moment_frequent->day="Vendredi";
			}else{
				$moment_frequent->day="Samedi";
			}*/
			
			// Produits plus vendus         /////////////////////////////////////////////////////////////////////
			$produit_plus_vendu = DB::table('produit')
				->join('donation' , 'donation.produit_id_produit' , 'produit.id_produit')
				->join('produit_commande' , 'produit_commande.donation_id' , 'donation.id_donation')
				->join('commande' , 'commande.id_commande' , 'produit_commande.commande_id')
				->select(DB::raw('sum(qte) as nombre') , 'produit.nom_produit')
				->whereBetween('commande.date',[$request->get('date_debut') , $request->get('date_fin')])
				->groupBy('produit.nom_produit')
				->orderBy('nombre' , 'desc')
				->limit(3)
				->get();
				
			
			// Satisfaction client (1 sur 5 etoiles) sur differets sujets :delivery time, food quality, price, choice, general service of the app
			$Satisfaction_client = DB::table('feedback')
				->leftjoin('label_feedback' , 'feedback.label_feedback_id' , 'label_feedback.id_label_feedback')
				->select('label_feedback.label' , DB::raw('FORMAT (avg(feedback.note),1) as note') )
				->whereBetween('feedback.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->groupBy('label_feedback.label')
				->get();
			$debut_periode = $request->get('date_debut');	
			$fin_periode = $request->get('date_fin');	
			Session::flash('date_client' , 1);
		}else {
			// Achats (MGA) total par clients
			$achat_par_client = DB::table('produit_commande')
				->join('commande' , 'commande.id_commande', 'produit_commande.commande_id')
				->join('consommateur' , 'commande.consommateur_id', 'consommateur.id_consommateur')
				->select('consommateur.nom' , 'consommateur.prenom' , DB::raw('sum( produit_commande.prix_achat_unite*produit_commande.qte) as montant_total') )
				->groupBy('commande.consommateur_id')
				->get();
			// Réductions la plus choisi par client
			$best_reduction = DB::table('consommateur')
				->join (DB::raw('(select  tab2.reduction as reduc, max(tab2.nombre) , tab2.consommateur_id   from
								(select count(tab1.reduction) as nombre, tab1.reduction as reduction, tab1.consommateur_id as consommateur_id from 
								(select pc.* , c.* , (100*(p.prix-pc.prix_achat_unite)/p.prix) as reduction from produit_commande pc
								join donation d on d.id_donation=pc.donation_id
								join commande c on c.id_commande=pc.commande_id
								join produit p on p.id_produit=d.produit_id_produit) as tab1  group by tab1.reduction , tab1.consommateur_id order by nombre desc) as tab2 group by tab2.consommateur_id ) as best_reduction') ,'best_reduction.consommateur_id','consommateur.id_consommateur' )
				->select('consommateur.nom' , 'consommateur.prenom' , 'best_reduction.reduc')
				->get();
			// heure et jours où les commandes ar les clietns sont les plus frequents	
			$moment_frequent = DB::table('commande')
				->select(DB::raw('count(*) as nombre') , DB::raw('hour(date) as heure') , DB::raw('DAYOFWEEK(date) as day') )
				->groupBy('heure')
				->groupBy('day')
				->orderBy('nombre','desc')
				->limit(1)
				->first();
			$jour_frequent = DB::table('commande')
				->select( 'commande.date', DB::raw('dayofweek(date) as jour') , DB::raw('hour(date) as heure') ,  DB::raw('MINUTE(date) as minute') )
				->orderBy('commande.date' , 'asc')
				->get();	
				
				/*
				if($moment_frequent->day==1){
				$moment_frequent->day="Dimanche";
			}else if($moment_frequent->day==2){
				$moment_frequent->day="Lundi";
			
			}else if($moment_frequent->day=3){
				$moment_frequent->day="Mardi";
			
			}else if($moment_frequent->day==4){
				$moment_frequent->day="Mercredi";
			
			}else if($moment_frequent->day==5){
				$moment_frequent->day="Jeudi";
			
			}else if($moment_frequent->day==6){
				$moment_frequent->day="Vendredi";
			}else{
				$moment_frequent->day="Samedi";
			}*/
			// Produits plus vendus
			$produit_plus_vendu = DB::table('produit')
				->join('donation' , 'donation.produit_id_produit' , 'produit.id_produit')
				->join('produit_commande' , 'produit_commande.donation_id' , 'donation.id_donation')
				->select(DB::raw('sum(qte) as nombre') , 'produit.nom_produit')
				->groupBy('produit.nom_produit')
				->orderBy('nombre' , 'desc')
				->limit(3)
				->get();
				
			
			// Satisfaction client (1 sur 5 etoiles) sur differets sujets :delivery time, food quality, price, choice, general service of the app
			$Satisfaction_client = DB::table('feedback')
				->leftjoin('label_feedback' , 'feedback.label_feedback_id' , 'label_feedback.id_label_feedback')
				->select('label_feedback.label' , DB::raw('FORMAT (avg(feedback.note),1) as note') )
				->groupBy('label_feedback.label')
				->get();
			$debut_periode = 0;	
			$fin_periode = 0;	
		}
		// get jour frequent et heure dans une intervalle de 30 min
		if(count( $jour_frequent ) != 0){
			$max= array(
					"jour"=>$jour_frequent[0]->jour ,
					"heure"=>$jour_frequent[0]->heure ,
					"minute"=>$jour_frequent[0]->minute ,
				);	
			$compteur = 0;
			$maximum= 1;
			$heure = $jour_frequent[0]->heure ;
			$minute = $jour_frequent[0]->minute  ; 
			$jour = $jour_frequent[0]->jour  ;
			for($i =0 ; $i< count($jour_frequent) ; $i++){
				
				// var_dump($jour_frequent[$i-1]->jour.'/'.$jour_frequent[$i-1]->heure."/". $jour_frequent[$i-1]->minute);
				// var_dump($jour_frequent[$i]->jour.'/'.$jour_frequent[$i]->heure."/". $jour_frequent[$i]->minute);
				
				
				if($i+1<count($jour_frequent)){
					//var_dump($jour_frequent[$i]->jour.'/'.$jour_frequent[$i]->heure.'**'.$jour_frequent[$i+1]->jour.'/'.$jour_frequent[$i+1]->heure);
					if($jour_frequent[$i]->jour==$jour_frequent[$i+1]->jour && $jour_frequent[$i]->heure==$jour_frequent[$i+1]->heure){
						//var_dump($jour_frequent[$i-1]->jour.'/'.$jour_frequent[$i-1]->heure."/". $jour_frequent[$i-1]->minute.'**'.$jour_frequent[$i]->jour.'/'.$jour_frequent[$i]->heure."/". $jour_frequent[$i]->minute);
						if($jour_frequent[$i]->minute >0 && $jour_frequent[$i]->minute <31 && $jour_frequent[$i+1]->minute>0 && $jour_frequent[$i+1]->minute <31){
							$compteur++;
							// var_dump($jour_frequent[$i-1]->jour.'/'.$jour_frequent[$i-1]->heure."/". $jour_frequent[$i-1]->minute.'**'.$jour_frequent[$i]->jour.'/'.$jour_frequent[$i]->heure."/". $jour_frequent[$i]->minute);
						}
						if ($jour_frequent[$i]->minute>60 && $jour_frequent[$i]->minute >30 && $jour_frequent[$i+1]->minute>60 && $jour_frequent[$i+1]->minute >30) {
							 //var_dump($jour_frequent[$i-1]->jour.'/'.$jour_frequent[$i-1]->heure."/". $jour_frequent[$i-1]->minute.'**'.$jour_frequent[$i]->jour.'/'.$jour_frequent[$i]->heure."/". $jour_frequent[$i]->minute);
							$compteur++;
						}
					}
				}
				// var_dump($maximum);
				if($compteur >=  $maximum){
					$maximum = $compteur;
					$heure = $jour_frequent[$i]->heure ;
					$minute = $jour_frequent[$i]->minute  ; 
					$jour = $jour_frequent[$i]->jour  ;
					$compteur = 1;	
				}
			}
			$jour_frequent = null;
			if($jour==1){
				$jour_frequent = "Dimanche";
			}else if($jour==2){
				$jour_frequent = "Lundi";
			
			}else if($jour==3){
				$jour_frequent = "Mardi";
			
			}else if($jour==4){
				$jour_frequent = "Mercredi";
			
			}else if($jour==5){
				$jour_frequent = "Jeudi";
			
			}else if($jour==6){
				$jour_frequent = "Vendredi";
			}else{
				$jour_frequent = "Samedi";
			}
				
			if($minute<60 && $minute>30){
				$heure_frequent = 'De '.$heure.': 30 à '.($heure+1);
			}
			if($minute>0 && $minute<=30){
				$heure_frequent = 'De '.$heure.' à '.$heure.': 30';
			}		
		}else{
			$jour_frequent= '-';
			$heure_frequent='-';
		}
		
		 $params = array(
			"nombre_consommateur" => $nombre_consommateur ,
			"nombre_consommateur_ce_mois" => $nombre_consommateur_ce_mois ,
			"achat_par_client" => $achat_par_client ,
			"best_reduction" => $best_reduction ,
			"jour_frequent" => $jour_frequent ,
			"heure_frequent" => $heure_frequent ,
			"produit_plus_vendu" => $produit_plus_vendu ,
			"produits_preference_globaux" => $produits_preference_globaux ,
			"produits_preference_par_client" => $produits_preference_par_client ,
			"Satisfaction_client" => $Satisfaction_client ,
			"debut_periode"=> $debut_periode,
			"fin_periode"=> $fin_periode ,
			);
			return $this->render("dashboard_client","dashboard", "dashboard_client")->with($params);
	 }
	 
	  /////// benevole
	 public function getDashboard_benevole(){
		 // Total bénévoles
		 $nombre_benevoles = DB::table('membre_fw')
            ->join('utilisateur', 'utilisateur.id_utilisateur', '=', 'membre_fw.utilisateur_id')
            ->join('utilisateur_type', 'utilisateur.utilisateur_type_id', '=', 'utilisateur_type.id_utilisateur_type')
            ->select(DB::raw('count(*) as nombre_total'))
			->where('active' , '1')
			->where('utilisateur_type.label' , 'Benevole')
			->first();
		
		// Nouveaux bénévoles ce mois ci	
		$nombre_benevoles_ce_mois= DB::table('membre_fw')
            ->join('utilisateur', 'utilisateur.id_utilisateur', '=', 'membre_fw.utilisateur_id')
			->join('utilisateur_type', 'utilisateur.utilisateur_type_id', '=', 'utilisateur_type.id_utilisateur_type')
			// ->select( DB::raw('count(*) as nombre_ce_mois') )
			->select('membre_fw.nom')
			->where('active' , '1')
			->where('utilisateur_type.label' , 'Benevole')
			->where(DB::raw('month(utilisateur.moment_inscription)') , '=' , DB::raw('month(now())'))
			->get();
		$params = array (
			"nombre_benevoles" => $nombre_benevoles ,
			"nombre_benevoles_ce_mois" => $nombre_benevoles_ce_mois ,
		);
		return $this->render("dashboard_benevole","dashboard", "dashboard_benevole")->with($params);	
	 }
	 
	 
	 /////// receveurs
	 public function getRepas_distribue_mois_receveur(){
		 
		//liste organisation
		$liste_organisation = Organisation::all();
		//nombre organisation	
		$effectif_organisation = $liste_organisation->count() ;	
		
		// get repas par mois par organisation
		$array_liste_repas_distribue_mois=array();
		for($i = 0 ; $i < $effectif_organisation ; $i++) {
			$array_liste_repas_distribue_mois[]= DB::table('organisation')
			->leftJoin(DB::raw('(select donation.organisation_id , AVG(produit.poids*donation.quantite/0.25) as repas , month(donation.moment) as mois
						from donation
						join produit on produit.id_produit=donation.produit_id_produit
						where donation.etat_donation_id!=5 and donation.action = 1 and  year(donation.moment) = year(now()) group by donation.organisation_id , mois ) as tab1') , 'tab1.organisation_id' , 'organisation.id_organisation' )
			->where('organisation.id_organisation', $liste_organisation[$i]->id_organisation )
			->orderBy('tab1.mois' , 'asc')
			->get();
		}
		/*teste*/
		// for($i = 0 ; $i< count($array_liste_repas_distribue_mois) ; $i++){
			// var_dump($array_liste_repas_distribue_mois[$i]);
		// }
		// die();
		/*****fin tste */
		// structure en mois complet
		for($indice = 0 ; $indice < $effectif_organisation  ; $indice++){
			 // var_dump($array_liste_repas_distribue_mois[$indice]) ; 
			$length = count($array_liste_repas_distribue_mois[$indice]);		
			$repas_distribue_mois=[];
			$j=0;
			for ( $i =0 ; $i< $length ; $i++) {
				if($length == 1 &&  $array_liste_repas_distribue_mois[$indice][$i]->mois == null){
					for( $k=0  ; $k < 12 ; $k++){
						$repas_distribue_mois[] = array( 
													"repas" => 0,
													"nom_organisation" => $array_liste_repas_distribue_mois[$indice][$i]->nom_organisation,
													);
					}
					break;
				}

				
				if($i+1 != $array_liste_repas_distribue_mois[$indice][$i]->mois ){
					for( $j  ; $j < $array_liste_repas_distribue_mois[$indice][$i]->mois-1 ; $j++){
						$repas_distribue_mois[] = array( 
													"repas" => 0,
													"nom_organisation" => $array_liste_repas_distribue_mois[$indice][$i]->nom_organisation,
													);
					}
					$repas_distribue_mois[] =   array( 
													"repas" => $array_liste_repas_distribue_mois[$indice][$i]->repas ,
													"nom_organisation" => $array_liste_repas_distribue_mois[$indice][$i]->nom_organisation,
													);
					$j =  $array_liste_repas_distribue_mois[$indice][$i]->mois;
				} 
				if($i+1 == $array_liste_repas_distribue_mois[$indice][$i]->mois){
					$repas_distribue_mois[] =   array( 
													"repas" => $array_liste_repas_distribue_mois[$indice][$i]->repas ,
													"nom_organisation" => $array_liste_repas_distribue_mois[$indice][$i]->nom_organisation,
													);
					$j++;
				}
				if($array_liste_repas_distribue_mois[$indice][$i]->mois <12 && $length == $i+1) {
					for( $k = $array_liste_repas_distribue_mois[$indice][$i]->mois  ; $k < 12 ; $k++){
						$repas_distribue_mois[] = array( 
													"repas" => 0,
													"nom_organisation" => $array_liste_repas_distribue_mois[$indice][$i]->nom_organisation,
													);
					}
				}
			}
			
			 $array_liste_repas_distribue_mois[$indice]=$repas_distribue_mois;
			// var_dump($array_liste_repas_distribue_mois[$indice]);
		}
		// die();
		
		// get repas par mois par enfant par organisation
		$array_liste_repas_distribue_mois_enfant=array();
		for($i = 0 ; $i < $effectif_organisation ; $i++) {
		
			$array_liste_repas_distribue_mois_enfant[]= DB::table('organisation')
			->leftjoin(DB::raw('(select donation.organisation_id, avg(produit.poids*donation.quantite/(0.25*effectif_enfant.effectif)) as repas , avg(produit.poids*donation.quantite/0.25) as poids , month(donation.moment) as mois
																from donation 
																join produit 
																on produit.id_produit=donation.produit_id_produit
																join (select DISTINCT organisation_id , sum(nombre) as effectif from nombre_enfant where tranche_age!=5 and tranche_age!=4 group by organisation_id ) as effectif_enfant
																on donation.organisation_id = effectif_enfant.organisation_id
																where donation.etat_donation_id!=5 and year(donation.moment) = year(now())
																group by donation.organisation_id , mois ) as tab1') , 'tab1.organisation_id', '=', 'organisation.id_organisation')
			->where('organisation.id_organisation', $liste_organisation[$i]->id_organisation )
			->orderBy('tab1.mois' , 'asc')
			->get();
			
		}
		 
		// structure en mois complet
		for($indice = 0 ; $indice < $effectif_organisation  ; $indice++){
			$length = count($array_liste_repas_distribue_mois_enfant[$indice]);		
			$repas_distribue_mois=[];
			$j=0;
			for ( $i =0 ; $i< $length ; $i++) {
				if($length == 1 &&  $array_liste_repas_distribue_mois_enfant[$indice][$i]->mois == null){ // orqanisation qui n a pas recu de don en un an
					for( $k=0  ; $k < 12 ; $k++){
						$repas_distribue_mois[] = array( 
													"repas" => 0,
													"nom_organisation" => $array_liste_repas_distribue_mois_enfant[$indice][$i]->nom_organisation,
													);
					}
					break;
				}
				if($i+1 != $array_liste_repas_distribue_mois_enfant[$indice][$i]->mois ){
					for( $j  ; $j < $array_liste_repas_distribue_mois_enfant[$indice][$i]->mois-1 ; $j++){
						$repas_distribue_mois[] = array( 
													"repas" => 0,
													"nom_organisation" => $array_liste_repas_distribue_mois_enfant[$indice][$i]->nom_organisation,
													);
					}
					$repas_distribue_mois[] =  array( 
													"repas" => $array_liste_repas_distribue_mois_enfant[$indice][$i]->repas ,
													"nom_organisation" => $array_liste_repas_distribue_mois_enfant[$indice][$i]->nom_organisation,
													);
					$j =  $array_liste_repas_distribue_mois_enfant[$indice][$i]->mois;
				} else{
					$repas_distribue_mois[] =  array(
													"repas"=>$array_liste_repas_distribue_mois_enfant[$indice][$i]->repas,
													"nom_organisation" => $array_liste_repas_distribue_mois_enfant[$indice][$i]->nom_organisation, 
													);
					$j++;
				}
				if($array_liste_repas_distribue_mois_enfant[$indice][$i]->mois <12 && $length == $i+1) {
					for( $k = $array_liste_repas_distribue_mois_enfant[$indice][$i]->mois  ; $k < 12 ; $k++){
						$repas_distribue_mois[] = array( 
													"repas" => 0,
													"nom_organisation" => $array_liste_repas_distribue_mois_enfant[$indice][$i]->nom_organisation,
													);
					}
				}
			}
			
			 $array_liste_repas_distribue_mois_enfant[$indice]=$repas_distribue_mois;
			// var_dump($array_liste_repas_distribue_mois_enfant[$indice]);
		}
		// die();
		// var_dump() ;
		// var_dump() ;
		// die();
		$resultat= array(
			"array_liste_repas_distribue_mois"=> $array_liste_repas_distribue_mois , 
			"array_liste_repas_distribue_mois_enfant" => $array_liste_repas_distribue_mois_enfant ,
			);
		return $resultat;
				
		/*---------------  */
	 }
	 
	  public function getDashboard_receveur(Request $request){
		  
		  // Total receveurs
		  $nombre_receveur = DB::table('organisation')
			->join('type_organisation', 'type_organisation.id_type_organisation' , 'organisation.type_organisation_id')
			->join('organisation_beneficiaire_organisation' ,'organisation.id_organisation' , 'organisation_beneficiaire_organisation.organisation_id')
			->join('beneficiaire_organisation' , 'beneficiaire_organisation.id_beneficiaire_organisation' , 'organisation_beneficiaire_organisation.beneficiaire_organisation_id')
			// ->select('organisation.id_organisation' , 'organisation.nom_organisation' , 'type_organisation.label as type' ,  'beneficiaire_organisation.label as beneficiaire' )
			->select(DB::raw('count(*) as nombre_receveur'))
			->first();
			// Total enfant beneficiaire
		  $total_beneficiaire = DB::table('nombre_enfant')
				->select(DB::raw('sum(nombre) as total'))
			->first();
			// total beneficiaires enfants
		$nombre_beneficiaire = DB::table('organisation')
			->join('nombre_enfant' , 'nombre_enfant.organisation_id' , 'organisation.id_organisation')
			->select('organisation.id_organisation' , 'organisation.nom_organisation' , DB::raw('sum(nombre_enfant.nombre) as nombre') )
			//->where('nombre_enfant.tranche_age' , '!=' , 4)
			//->where('nombre_enfant.tranche_age' , '!=' , 5)
			->groupBy('nombre_enfant.organisation_id')
			->get();
			// Repas distribués en moyenne par mois (KG total données / 0 .25kg)
		$repas=$this->getRepas_distribue_mois_receveur();
		$repas_en_moyenne_par_mois = $repas['array_liste_repas_distribue_mois'];
			// Repas distribués en moyenne par mois par enfant (KG total données / 0 .25kg)
		$repas_en_moyenne_par_mois_par_enfant = $repas['array_liste_repas_distribue_mois_enfant'];
		
			
			
		if($request->get('date_debut')!=null || $request->get('date_fin')!=null ){
			$validator = Validator::make($request->all(), [
			'date_debut'=>'required',
			'date_fin'=>'required',
			
		  ], ['date_debut.required'=>'Champs obligatoire',
			'date_fin.required'=>'Champs obligatoire',]);
		  if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}		
			// aliments recus par receveur en kg
			$aliments_recus_par_receveur_poids = DB::table('organisation')
				->join('donation' , 'donation.organisation_id' , 'organisation.id_organisation')
				->join('produit' , 'produit.id_produit' , 'donation.produit_id_produit')
				->select('organisation.id_organisation' ,'organisation.nom_organisation' , DB::raw('sum(donation.quantite*produit.poids) as poids') )
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->groupBy('donation.organisation_id')
				->groupBy('organisation.nom_organisation')
				->get();
				// Aliments recu par Receveurs (nombre)	
				$aliments_recus_par_receveur_nombre = DB::table('organisation')
				->join('donation' , 'donation.organisation_id' , 'organisation.id_organisation')
				->join('produit' , 'produit.id_produit' , 'donation.produit_id_produit')
				->select('organisation.id_organisation' ,'organisation.nom_organisation' , DB::raw('sum(donation.quantite) as nombre_aliments') )
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->groupBy('donation.organisation_id')
				->groupBy('organisation.nom_organisation')
				->get();
			// Repas recu par receveur (nombre)
			$repas_recus_par_receveur_poids = DB::table('organisation')
				->join('donation' , 'donation.organisation_id' , 'organisation.id_organisation')
				->join('produit' , 'produit.id_produit' , 'donation.produit_id_produit')
				->select('organisation.id_organisation' ,'organisation.nom_organisation' , DB::raw('sum(donation.quantite*produit.poids/0.25) as nombre_repas') )
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->groupBy('donation.organisation_id')
				->groupBy('organisation.nom_organisation')
				->get();	
			// Repas par enfants recu par receveur (nombre)
			$repas_recus_par_receveur_poids_par_enfant = DB::table('organisation')
				->join('donation' , 'donation.organisation_id' , 'organisation.id_organisation')
				->join('produit' , 'produit.id_produit' , 'donation.produit_id_produit')
				->join(DB::raw('(select nombre_enfant.organisation_id , sum(nombre) as nombre from nombre_enfant group by nombre_enfant.organisation_id) as nb_enfant') , 'nb_enfant.organisation_id' , 'organisation.id_organisation')
				->select('organisation.id_organisation' ,'organisation.nom_organisation' , DB::raw('sum(donation.quantite*produit.poids/0.25)/nb_enfant.nombre as nombre_repas') )
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->groupBy('donation.organisation_id')
				->groupBy('organisation.nom_organisation')
				->get();		
			$debut_periode = $request->get('date_debut');	
			$fin_periode = $request->get('date_fin');
			Session::flash('date_receveur' , 1);
		}else{
			$aliments_recus_par_receveur_poids = DB::table('organisation')
				->join('donation' , 'donation.organisation_id' , 'organisation.id_organisation')
				->join('produit' , 'produit.id_produit' , 'donation.produit_id_produit')
				->select('organisation.id_organisation' ,'organisation.nom_organisation' , DB::raw('sum(donation.quantite*produit.poids) as poids') )
				->groupBy('donation.organisation_id')
				->groupBy('organisation.nom_organisation')
				->get();
				// Aliments recu par Receveurs (nombre)	
				$aliments_recus_par_receveur_nombre = DB::table('organisation')
				->join('donation' , 'donation.organisation_id' , 'organisation.id_organisation')
				->join('produit' , 'produit.id_produit' , 'donation.produit_id_produit')
				->select('organisation.id_organisation' ,'organisation.nom_organisation' , DB::raw('sum(donation.quantite) as nombre_aliments') )
				->groupBy('donation.organisation_id')
				->groupBy('organisation.nom_organisation')
				->get();
			// Repas recu par receveur (nombre)
			$repas_recus_par_receveur_poids = DB::table('organisation')
				->join('donation' , 'donation.organisation_id' , 'organisation.id_organisation')
				->join('produit' , 'produit.id_produit' , 'donation.produit_id_produit')
				->select('organisation.id_organisation' ,'organisation.nom_organisation' , DB::raw('sum(donation.quantite*produit.poids/0.25) as nombre_repas') )
				->groupBy('donation.organisation_id')
				->groupBy('organisation.nom_organisation')
				->get();	
			// Repas par enfants recu par receveur (nombre)
			$repas_recus_par_receveur_poids_par_enfant = DB::table('organisation')
				->join('donation' , 'donation.organisation_id' , 'organisation.id_organisation')
				->join('produit' , 'produit.id_produit' , 'donation.produit_id_produit')
				->join(DB::raw('(select nombre_enfant.organisation_id , sum(nombre) as nombre from nombre_enfant group by nombre_enfant.organisation_id) as nb_enfant') , 'nb_enfant.organisation_id' , 'organisation.id_organisation')
				->select('organisation.id_organisation' ,'organisation.nom_organisation' , DB::raw('sum(donation.quantite*produit.poids/0.25)/nb_enfant.nombre as nombre_repas') )
				->groupBy('donation.organisation_id')
				->groupBy('organisation.nom_organisation')
				->get();
			$debut_periode = 0;	
			$fin_periode = 0;
		}
		$params = array (
				"nombre_receveur" => $nombre_receveur ,
				"total_beneficiaire" => $total_beneficiaire ,
				"nombre_beneficiaire" => $nombre_beneficiaire ,
				"aliments_recus_par_receveur_poids" => $aliments_recus_par_receveur_poids ,
				"repas_en_moyenne_par_mois" => $repas_en_moyenne_par_mois ,
				"repas_en_moyenne_par_mois_par_enfant" => $repas_en_moyenne_par_mois_par_enfant ,
				"aliments_recus_par_receveur_nombre" => $aliments_recus_par_receveur_nombre ,
				"repas_recus_par_receveur_poids" => $repas_recus_par_receveur_poids ,
				"repas_recus_par_receveur_poids_par_enfant" => $repas_recus_par_receveur_poids_par_enfant ,
				"debut_periode" => $debut_periode ,
				"fin_periode"=> $fin_periode ,
				);	
		// var_dump($repas_en_moyenne_par_mois);		
		// die();
		return $this->render("dashboard_receveur","dashboard", "dashboard_receveur")->with($params);	
		
			
	  }
	 
	 
	 ////// entreprise
	 // getDetailAlimentVendu
	 public function getDetailAlimentVendu(Request $request){
		 $liste_aliments_donnes_detail = DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->join('produit_commande' , 'donation.id_donation' , 'produit_commande.donation_id')
				->join('produit', 'donation.produit_id_produit', 'produit.id_produit')
				->select('entreprise.nom_entreprise' , DB::raw('sum(produit_commande.qte) as quantite' ) , 'produit.nom_produit'  )
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 2)
				->where('entreprise.id_entreprise' , '=' , $request->get('id_entreprise'))
				//->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				 ->groupBy('donation.entreprise_id')
				 ->groupBy('produit.nom_produit')
				 ->orderBy('quantite','desc')
				->get();	
		 
		 return response($liste_aliments_donnes_detail);
	 }
	 public function getDetailAlimentDon(Request $request){
		 $liste_aliments_donnes_detail = DB::table('entreprise') 
			->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
			->join('produit', 'donation.produit_id_produit', 'produit.id_produit')
			->select('entreprise.nom_entreprise' , DB::raw('sum(donation.quantite) as quantite' ) , 'produit.nom_produit'  )
			->where('donation.etat_donation_id' , '!=' , 5)
			->where('donation.action' , '=' , 1)
			->where('entreprise.id_entreprise' , '=' , $request->get('id_entreprise'))
			//->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
			 ->groupBy('donation.entreprise_id')
			 ->groupBy('produit.nom_produit')
			 ->orderBy('quantite','desc')
			->get();
		 
		 return response($liste_aliments_donnes_detail);
	 }
	 
	 
	 
	 public function getDashboard_entreprise(Request $request){
		 $liste_entreprise = Entreprise::all()->count();
		 
		 $liste_entreprise_ce_mois = DB::table('entreprise') 
			->join ('utilisateur' , 'utilisateur.id_utilisateur' , 'entreprise.utilisateur_id')
			->leftjoin(DB::raw('(select entreprise.id_entreprise as entreprise_id , sum(produit_commande.qte*produit_commande.prix_achat_unite) as montant from entreprise
							join donation on donation.entreprise_id = entreprise.id_entreprise
							join produit_commande on produit_commande.donation_id = donation.id_donation
							group by entreprise.id_entreprise) as ca') , 'ca.entreprise_id' , 'entreprise.id_entreprise')
			->leftJoin('tache', 'tache.entreprise_concernee' ,'entreprise.id_entreprise')
			->select( 'entreprise.nom_entreprise' , 'entreprise.nif_entreprise' , 'entreprise.stat_entreprise' , DB::raw('month(utilisateur.moment_inscription) as mois') , 'tache.montant_objectif' ,'tache.objectif' , 'ca.montant' , DB::raw('FORMAT ((ca.montant*100/tache.montant_objectif),2) as kpi'))
			->where(DB::raw('month(utilisateur.moment_inscription) ' ) , '=' , DB::raw('month(now())'))
			->orderBy('kpi' , 'desc')
			->get() ;
		if($request->get('date_debut')!=null || $request->get('date_fin')!=null ){
			$validator = Validator::make($request->all(), [
			'date_debut'=>'required',
			'date_fin'=>'required',
			
		  ], ['date_debut.required'=>'Champs obligatoire',
			'date_fin.required'=>'Champs obligatoire',]);
		  if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}	
			$liste_aliments_vendus_detail = DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->join('produit_commande', 'produit_commande.donation_id', 'donation.id_donation')
				->join('produit', 'donation.produit_id_produit', 'produit.id_produit')
				->select('entreprise.nom_entreprise' ,'produit.nom_produit' , DB::raw('sum(produit_commande.qte) as quantite' ) )
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 2)
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->groupBy('donation.produit_id_produit')
				->groupBy('produit.nom_produit')
				->groupBy('entreprise.nom_entreprise')
				->orderBy('quantite','desc')
				->get();
			$nombre_aliments_vendus= DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->join('produit_commande', 'produit_commande.donation_id', 'donation.id_donation')
				->join('produit', 'donation.produit_id_produit', 'produit.id_produit')
				->select('entreprise.nom_entreprise' , DB::raw('sum(produit_commande.qte) as quantite' ) )
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 2)
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				 ->groupBy('entreprise.nom_entreprise')
				 ->orderBy('quantite','desc')
				->get();	
			$nombre_aliments_donnes= DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->select('entreprise.nom_entreprise'  , DB::raw('sum(donation.quantite) as quantite' ) )
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 1)
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				 ->groupBy('entreprise.nom_entreprise')
				  ->orderBy('quantite','desc')
				->get();
			
			$liste_aliments_donnes_detail = DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->join('produit', 'donation.produit_id_produit', 'produit.id_produit')
				->select('entreprise.nom_entreprise' , DB::raw('sum(donation.quantite) as quantite' ) , 'produit.nom_produit'  )
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 1)
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				 ->groupBy('donation.entreprise_id')
				 ->groupBy('produit.nom_produit')
				 ->orderBy('quantite','desc')
				->get();	
			$ca_entreprise = DB::table('entreprise') 
				->leftjoin(DB::raw('(select entreprise.id_entreprise as entreprise_id , sum(produit_commande.qte*produit_commande.prix_achat_unite) as montant from entreprise
								join donation on donation.entreprise_id = entreprise.id_entreprise
								join produit_commande on produit_commande.donation_id = donation.id_donation where donation.moment between '.$request->get('date_debut').' and '.$request->get('date_fin').'
								group by entreprise.id_entreprise) as ca') , 'ca.entreprise_id' , 'entreprise.id_entreprise')
				->select( 'entreprise.nom_entreprise' , 'ca.montant')
				->orderBy('ca.montant' , 'desc')
				->get() ;
			$raison_surplus = DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->leftJoin('raison_donation' , 'raison_donation.donation_id' , 'donation.id_donation')
				->leftJoin('raison_surplus' , 'raison_surplus.id_raison_surplus' , 'raison_donation.raison_surplus_id')
				->select( 'entreprise.nom_entreprise' , 'raison_surplus.label' ,DB::raw('count(raison_surplus.label) as frequence'))
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 1)
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->groupBy('raison_surplus.label')
				->groupBy('entreprise.nom_entreprise')
				->orderBy('frequence' , 'desc' )
				->get();
			$nombre_aliments_vendus_dons= DB::table('entreprise')
				->join(DB::raw('(select entreprise.id_entreprise, entreprise.nom_entreprise, sum(donation.quantite) as quantite from entreprise join donation on donation.entreprise_id = entreprise.id_entreprise where donation.etat_donation_id != 5 and donation.action = 1 and donation.moment between '.$request->get('date_debut').' and '.$request->get('date_fin').'  group by entreprise.id_entreprise order by quantite desc) as tab_dons') , 'tab_dons.id_entreprise' , 'entreprise.id_entreprise')
				->leftJoin(DB::raw('( select entreprise.id_entreprise, entreprise.nom_entreprise, sum(produit_commande.qte) as quantite from entreprise  join donation on donation.entreprise_id = entreprise.id_entreprise  join produit_commande on produit_commande.donation_id = donation.id_donation join commande on commande.id_commande=produit_commande.commande_id  join produit on donation.produit_id_produit = produit.id_produit where commande.date between '.$request->get('date_debut').' and '.$request->get('date_fin').' and donation.etat_donation_id != 5 and donation.action = 2 group by entreprise.nom_entreprise order by quantite desc) as tabs_vente') , 'tabs_vente.id_entreprise', 'entreprise.id_entreprise')
				->select('entreprise.id_entreprise','entreprise.nom_entreprise' , DB::raw('tab_dons.quantite+tabs_vente.quantite as quantite' ) )
				 ->groupBy('entreprise.nom_entreprise')
				 ->orderBy('quantite','desc')
				->get();	
			$receveur = DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->join('organisation' , 'organisation.id_organisation' , 'donation.organisation_id')
				->join('produit' , 'produit.id_produit' , 'donation.produit_id_produit')
				->select( 'entreprise.nom_entreprise' , 'organisation.nom_organisation' ,'donation.quantite' , 'produit.nom_produit' , DB::raw( 'DATE_FORMAT(date(donation.moment), "%d %M %Y") as moment') )
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 1)
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->get();
				setlocale(LC_TIME, "fr_FR");
			for($i = 0 ; $i < count($receveur) ; $i++){
				setlocale(LC_TIME, "fr_FR");
				$date=new DateTime($receveur[$i]->moment);
				 $date = $date->format('d-m-Y');
				$receveur[$i]->moment = strftime("%d %B %G", strtotime($date));
			}	
			$debut_periode = $request->get('date_debut');	
			$fin_periode = $request->get('date_fin');
			Session::flash('date_entreprise' , 1);
		}else{
			$liste_aliments_vendus_detail = DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->join('produit_commande', 'produit_commande.donation_id', 'donation.id_donation')
				->join('produit', 'donation.produit_id_produit', 'produit.id_produit')
				->select('entreprise.nom_entreprise' ,'produit.nom_produit' , DB::raw('sum(produit_commande.qte) as quantite' ) )
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 2)
				->groupBy('donation.produit_id_produit')
				->groupBy('produit.nom_produit')
				->groupBy('entreprise.nom_entreprise')
				->orderBy('quantite','desc')
				->get();
			$nombre_aliments_vendus= DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->join('produit_commande', 'produit_commande.donation_id', 'donation.id_donation')
				->join('produit', 'donation.produit_id_produit', 'produit.id_produit')
				->select('entreprise.id_entreprise','entreprise.nom_entreprise' , DB::raw('sum(produit_commande.qte) as quantite' ) )
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 2)
				 ->groupBy('entreprise.nom_entreprise')
				 ->orderBy('quantite','desc')
				->get();	
				
			$nombre_aliments_donnes= DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->select('entreprise.id_entreprise', 'entreprise.nom_entreprise'  , DB::raw('sum(donation.quantite) as quantite' ) )
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 1)
				 ->groupBy('entreprise.nom_entreprise')
				  ->orderBy('quantite','desc')
				->get();
				
			$liste_aliments_donnes_detail = DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->join('produit', 'donation.produit_id_produit', 'produit.id_produit')
				->select('entreprise.nom_entreprise' , DB::raw('sum(donation.quantite) as quantite' ) , 'produit.nom_produit'  )
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 1)
				 ->groupBy('donation.entreprise_id')
				 ->groupBy('produit.nom_produit')
				 ->orderBy('quantite','desc')
				->get();	
			$nombre_aliments_vendus_dons= DB::table('entreprise')
				->join(DB::raw('(select entreprise.id_entreprise, entreprise.nom_entreprise, sum(donation.quantite) as quantite from entreprise join donation on donation.entreprise_id = entreprise.id_entreprise where donation.etat_donation_id != 5 and donation.action = 1 group by entreprise.id_entreprise order by quantite desc) as tab_dons') , 'tab_dons.id_entreprise' , 'entreprise.id_entreprise')
				->leftJoin(DB::raw('( select entreprise.id_entreprise, entreprise.nom_entreprise, sum(produit_commande.qte) as quantite from entreprise  join donation on donation.entreprise_id = entreprise.id_entreprise  join produit_commande on produit_commande.donation_id = donation.id_donation  join produit on donation.produit_id_produit = produit.id_produit where donation.etat_donation_id != 5 and donation.action = 2 group by entreprise.nom_entreprise order by quantite desc) as tabs_vente') , 'tabs_vente.id_entreprise', 'entreprise.id_entreprise')
				->select('entreprise.id_entreprise','entreprise.nom_entreprise' , DB::raw('tab_dons.quantite+tabs_vente.quantite as quantite' ) )
				 ->groupBy('entreprise.nom_entreprise')
				 ->orderBy('quantite','desc')
				->get();	
			
			$ca_entreprise = DB::table('entreprise') 
				->leftjoin(DB::raw('(select entreprise.id_entreprise as entreprise_id , sum(produit_commande.qte*produit_commande.prix_achat_unite) as montant from entreprise
								join donation on donation.entreprise_id = entreprise.id_entreprise
								join produit_commande on produit_commande.donation_id = donation.id_donation
								group by entreprise.id_entreprise) as ca') , 'ca.entreprise_id' , 'entreprise.id_entreprise')
				->select( 'entreprise.nom_entreprise' , 'ca.montant')
				->orderBy('ca.montant' , 'desc')
				->get() ;
			$raison_surplus = DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->leftJoin('raison_donation' , 'raison_donation.donation_id' , 'donation.id_donation')
				->leftJoin('raison_surplus' , 'raison_surplus.id_raison_surplus' , 'raison_donation.raison_surplus_id')
				->select( 'entreprise.nom_entreprise' , 'raison_surplus.label' ,DB::raw('count(raison_surplus.label) as frequence'))
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 1)
				->groupBy('raison_surplus.label')
				->groupBy('entreprise.nom_entreprise')
				->orderBy('frequence' , 'desc' )
				->get();
			$receveur = DB::table('entreprise') 
				->join('donation' , 'donation.entreprise_id' , 'entreprise.id_entreprise')
				->join('organisation' , 'organisation.id_organisation' , 'donation.organisation_id')
				->join('produit' , 'produit.id_produit' , 'donation.produit_id_produit')
				->select( 'entreprise.nom_entreprise' , 'organisation.nom_organisation' ,'donation.quantite' , 'produit.nom_produit' , DB::raw( 'DATE_FORMAT(date(donation.moment), "%d %M %Y") as moment') )
				->where('donation.etat_donation_id' , '!=' , 5)
				->where('donation.action' , '=' , 1)
				->get();
				setlocale(LC_TIME, "fr_FR");
				for($i = 0 ; $i < count($receveur) ; $i++){
					$date=new DateTime($receveur[$i]->moment);
					 $date = $date->format('y-m-d');
					 setlocale(LC_TIME, "fr_FR");
					$receveur[$i]->moment = strftime("%d %B %G", strtotime($date));
				}
			
			$debut_periode = 0;	
			$fin_periode = 0;
			
		}
		
		$params=array(
			"liste_entreprise" => $liste_entreprise ,
			"liste_entreprise_ce_mois" => $liste_entreprise_ce_mois,
			"liste_aliments_vendus_detail" => $liste_aliments_vendus_detail ,
			"nombre_aliments_vendus" => $nombre_aliments_vendus ,
			"nombre_aliments_donnes" => $nombre_aliments_donnes ,
			"nombre_aliments_vendus_dons" => $nombre_aliments_vendus_dons ,
			//"aliments_dons_vente" => $nombre_aliments_vendus + $nombre_aliments_donnes ,
			"liste_aliments_donnes_detail" => $liste_aliments_donnes_detail ,
			"ca_entreprise" => $ca_entreprise ,
			"raison_surplus" => $raison_surplus ,
			"receveur" => $receveur ,
			"debut_periode" => $debut_periode ,
			"fin_periode"=> $fin_periode ,
		);
		return $this->render("dashboard_entreprise","dashboard", "dashboard_entreprise")->with($params);
	 }
	 
	 
	 ////// general
	 public function getRepas_distribue_mois_general(Request $request){
		 if($request->get('annee')==null){ //cette annee
			$repas_distribue_mois_array = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->leftJoin('organisation', 'organisation.id_organisation', '=', 'donation.organisation_id')
				->select( DB::raw('AVG(produit.poids*donation.quantite/0.25) as repas') , DB::raw('month(donation.moment) as mois' ))
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','1')
				->where(DB::raw('year(donation.moment)'),DB::raw('year(now())'))
				->groupBy('mois')
				->get();	
		 }else{ //année differente
			$repas_distribue_mois_array = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->leftJoin('organisation', 'organisation.id_organisation', '=', 'donation.organisation_id')
				->select( DB::raw('AVG(produit.poids*donation.quantite/0.25) as repas') , DB::raw('month(donation.moment) as mois' ))
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','1')
				->where(DB::raw('year(donation.moment)'),$request->get('annee'))
				->groupBy('mois')
				->get();	


		 }
		/*structurer   array du mois   */ 
		$length = count($repas_distribue_mois_array);		
		$repas_distribue_mois=array();
		$j=0;
		for ( $i =0 ; $i< $length ; $i++) {
			
			if($i+1 != $repas_distribue_mois_array[$i]->mois ){
				for( $j  ; $j < $repas_distribue_mois_array[$i]->mois-1 ; $j++){
					$repas_distribue_mois[] = 0;
				}
				$repas_distribue_mois[] =  $repas_distribue_mois_array[$i]->repas;
				$j =  $repas_distribue_mois_array[$i]->mois;
			} else{
				$repas_distribue_mois[] =  $repas_distribue_mois_array[$i]->repas;
				$j++;
			}
			if($repas_distribue_mois_array[$i]->mois <12 && $length == $i+1) {
				for( $k = $repas_distribue_mois_array[$i]->mois  ; $k < 12 ; $k++){
					$repas_distribue_mois[] = 0;
				}
			}

		}
		if($request->get('annee')==null){ //cette annee
			$repas_distribue_mois_enfant_array = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->join(DB::raw('(select DISTINCT organisation_id , sum(nombre) as effectif from nombre_enfant where tranche_age!=5 and tranche_age!=4 group by organisation_id  ) as nb_e') , 'nb_e.organisation_id', '=', 'donation.organisation_id')
				 ->select( DB::raw('sum(produit.poids*donation.quantite/(nb_e.effectif)) as repas') , DB::raw('month(donation.moment) as mois' ))
				 ->where('donation.etat_donation_id', '!=' ,'5')
				 ->where(DB::raw('year(donation.moment)'),DB::raw('year(now())'))
				 ->where('donation.action','1')
				 ->groupBy('mois')
				->get();
		}else{
			$repas_distribue_mois_enfant_array = DB::table('donation')
            ->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
			->join(DB::raw('(select DISTINCT organisation_id , sum(nombre) as effectif from nombre_enfant where tranche_age!=5 and tranche_age!=4 group by organisation_id  ) as nb_e') , 'nb_e.organisation_id', '=', 'donation.organisation_id')
			 ->select( DB::raw('sum(produit.poids*donation.quantite/(nb_e.effectif)) as repas') , DB::raw('month(donation.moment) as mois' ))
			 ->where('donation.etat_donation_id', '!=' ,'5')
			 ->where('donation.action','1')
			->where(DB::raw('year(donation.moment)'),$request->get('annee'))
			 ->groupBy('mois')
			->get();
		}			
		/*structurer   array du mois   */ 
		$length2 = count($repas_distribue_mois_enfant_array);		
		$repas_distribue_mois_enfant=array();
		$j=0;
		for ( $i =0 ; $i< $length2 ; $i++) {
			
			if($i+1 != $repas_distribue_mois_enfant_array[$i]->mois ){
				
				for( $j  ; $j < $repas_distribue_mois_enfant_array[$i]->mois-1 ; $j++){
					$repas_distribue_mois_enfant[] = 0;
					
				}
				$repas_distribue_mois_enfant[] =  $repas_distribue_mois_enfant_array[$i]->repas;
				$j =  $repas_distribue_mois_enfant_array[$i]->mois;
			
			} else{
				$repas_distribue_mois_enfant[] =  $repas_distribue_mois_enfant_array[$i]->repas;
				$j++;
			}
			if($repas_distribue_mois_enfant_array[$i]->mois <12 && $length2 == $i+1) {
				for( $k = $repas_distribue_mois_enfant_array[$i]->mois  ; $k < 12 ; $k++){
					$repas_distribue_mois_enfant[] = 0;
				}
			}

		}
		$resultat = array(
			"repas_distribue_mois"=> $repas_distribue_mois ,
			"repas_distribue_mois_enfant"=> $repas_distribue_mois_enfant ,
			);
		return response($resultat);
		/*---------------  */
	 }
	 
	
    public function index(Request $request){
    	
		if($request->get('date_debut')!=null || $request->get('date_fin')!=null ){
			$validator = Validator::make($request->all(), [
			'date_debut'=>'required',
			'date_fin'=>'required',
			
		  ], ['date_debut.required'=>'Champs obligatoire',
			'date_fin.required'=>'Champs obligatoire',]);
			
		  if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}
			$valeur_sauvee_vente_objet = DB::table('produit_commande')
				->leftJoin('donation', 'produit_commande.donation_id', '=', 'donation.id_donation')
				->leftJoin('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->select(  DB::raw('SUM(produit.prix*produit_commande.qte) as valeur_sauvee'), DB::raw('SUM(produit_commande.qte) as nombre_produits') )
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','2')
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->first();
			$valeur_sauvee_donation_objet = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->select( DB::raw('SUM(produit.prix*donation.quantite) as valeur_sauvee') , DB::raw('SUM(donation.quantite) as nombre_produits'))
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','1')
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->first();	
			 $poids_aliment_sauve_vente_objet = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->join('produit_commande', 'produit_commande.donation_id', '=', 'donation.id_donation')
				->select( DB::raw('SUM(produit.poids*produit_commande.qte) as poids_sauve')  )
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','2')
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->first();
				
			$poids_aliment_sauve_donation_objet = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->select( DB::raw('SUM(produit.poids*donation.quantite) as poids_sauve') )
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','1')
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->first();
				
			$poids_aliment_sauve_donation_orphelinat_objet = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->leftJoin('organisation', 'organisation.id_organisation', '=', 'donation.organisation_id')
				->join('organisation_beneficiaire_organisation', 'organisation_beneficiaire_organisation.organisation_id', '=', 'organisation.id_organisation')
				->select( DB::raw('SUM(produit.poids*donation.quantite) as poids_sauve') )
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','1')
				->where('organisation_beneficiaire_organisation.beneficiaire_organisation_id','1')
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->first();
			$chiffre_affaire_objet = 	DB::table('produit_commande')
			->join('commande' , 'commande.id_commande' , 'produit_commande.commande_id')
			->select( DB::raw('SUM(produit_commande.qte*produit_commande.prix_achat_unite) as ca') )
			->whereBetween('commande.date',[$request->get('date_debut') , $request->get('date_fin')])
			->first();
			$valeur_monetaire_moyenne_par_produit_sauvee_donation = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->select( DB::raw('avg(produit.prix*donation.quantite) as valeur_monetaire_moyenne') , 'produit.nom_produit' )
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','1')
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->groupBy('produit.nom_produit')
				->get();
			$valeur_monetaire_moyenne_par_produit_sauvee_vente = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->join('produit_commande', 'produit_commande.donation_id', '=', 'donation.id_donation')
				->select( DB::raw('avg(produit.prix*produit_commande.qte) as valeur_monetaire_moyenne') , 'produit.nom_produit' )
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','2')
				->whereBetween('donation.moment',[$request->get('date_debut') , $request->get('date_fin')])
				->groupBy('produit.nom_produit')
				->get();
			$debut_periode = $request->get('date_debut');	
			$fin_periode = $request->get('date_fin');
		}else{
			
			$valeur_sauvee_vente_objet = DB::table('produit_commande')
				->leftJoin('donation', 'produit_commande.donation_id', '=', 'donation.id_donation')
				->leftJoin('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->select(  DB::raw('SUM(produit.prix*produit_commande.qte) as valeur_sauvee'), DB::raw('SUM(produit_commande.qte) as nombre_produits') )
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','2')
				->first();
				
				
			$valeur_sauvee_donation_objet = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->select( DB::raw('SUM(produit.prix*donation.quantite) as valeur_sauvee') , DB::raw('SUM(donation.quantite) as nombre_produits'))
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','1')
				->first();	
				
				
			 $poids_aliment_sauve_vente_objet = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->join('produit_commande', 'produit_commande.donation_id', '=', 'donation.id_donation')
				->select( DB::raw('SUM(produit.poids*produit_commande.qte) as poids_sauve')  )
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','2')
				->first();
				
				
			$poids_aliment_sauve_donation_objet = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->select( DB::raw('SUM(produit.poids*donation.quantite) as poids_sauve') )
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','1')
				->first();
			
			
			$poids_aliment_sauve_donation_orphelinat_objet = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->leftJoin('organisation', 'organisation.id_organisation', '=', 'donation.organisation_id')
				->join('organisation_beneficiaire_organisation', 'organisation_beneficiaire_organisation.organisation_id', '=', 'organisation.id_organisation')
				->select( DB::raw('SUM(produit.poids*donation.quantite) as poids_sauve') )
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','1')
				->where('organisation_beneficiaire_organisation.beneficiaire_organisation_id','1')
				->first();
				$chiffre_affaire_objet = 	DB::table('produit_commande')
				->select( DB::raw('SUM(produit_commande.qte*produit_commande.prix_achat_unite) as ca') )
				->first();
				
			$valeur_monetaire_moyenne_par_produit_sauvee_donation = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->select( DB::raw('avg(produit.prix*donation.quantite) as valeur_monetaire_moyenne') , 'produit.nom_produit' )
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','1')
				->groupBy('produit.nom_produit')
				->get();
			$valeur_monetaire_moyenne_par_produit_sauvee_vente = DB::table('donation')
				->join('produit', 'produit.id_produit', '=', 'donation.produit_id_produit')
				->join('produit_commande', 'produit_commande.donation_id', '=', 'donation.id_donation')
				->select( DB::raw('avg(produit.prix*produit_commande.qte) as valeur_monetaire_moyenne') , 'produit.nom_produit' )
				->where('donation.etat_donation_id', '!=' ,'5')
				->where('donation.action','2')
				->groupBy('produit.nom_produit')
				->get();
				
				
			$debut_periode = 0;	
			$fin_periode = 0;	
		}
		
		//DB::enableQueryLog(); //before query
		//var_dump(DB::getQueryLog());  //  visualiser la requete
		// die();
		
		
		
			
		$poids_aliment_sauve_vente	= $poids_aliment_sauve_vente_objet->poids_sauve;
		$poids_aliment_sauve_donation = $poids_aliment_sauve_donation_objet->poids_sauve;
		
		$valeur_monetaire_sauvee_vente	= $valeur_sauvee_vente_objet->valeur_sauvee;
		$valeur_monetaire_sauvee_donation = $valeur_sauvee_donation_objet->valeur_sauvee;
		$nombre_produits_concerne_vente = $valeur_sauvee_vente_objet->nombre_produits;
		$nombre_produits_concerne_donation = $valeur_sauvee_donation_objet->nombre_produits;
		
		$somme_valeur_monetaire = number_format( $valeur_monetaire_sauvee_vente+ $valeur_monetaire_sauvee_donation, 0 , "," , " ") ;
		$somme_produit_concerne = number_format( $nombre_produits_concerne_vente+ $nombre_produits_concerne_donation, 0 , "," , " ") ;
		
		$nombre_sauve_vente	= $valeur_sauvee_vente_objet->nombre_produits;
		$nombre_sauve_donation = $valeur_sauvee_donation_objet->nombre_produits;
		
		$nombre_repas_par_vente = $poids_aliment_sauve_vente/0.25 ;
		$nombre_repas_par_donation = $poids_aliment_sauve_donation/0.25;
		
		$nombre_co2_par_vente = $poids_aliment_sauve_vente/1.9 ;
		$nombre_co2_par_donation =  $poids_aliment_sauve_donation/1.9 ;
		
		$nombre_repas_orphelinat= ($poids_aliment_sauve_donation_orphelinat_objet->poids_sauve ) /0.25; 
		
		$totat_produit_sauve= $nombre_sauve_vente + $nombre_sauve_donation;
		$total_co2_sauve = number_format( $nombre_co2_par_vente +  $nombre_co2_par_donation, 1 , "," , " ") ;
		$total_aliments_sauves = number_format( $poids_aliment_sauve_vente+ $poids_aliment_sauve_donation, 1 , "," , " ") ;
		$total_repas_sauves = number_format( $nombre_repas_par_vente+ $nombre_repas_par_donation, 0 , "," , " ") ;
		
		$chiffre_affaire= $chiffre_affaire_objet->ca;
		
		 $params = array(
			"total_aliments_sauves" => $total_aliments_sauves ,
			"total_repas_sauves" => $total_repas_sauves ,
			"nombre_repas_par_vente" => number_format($nombre_repas_par_vente , 0 , "," , " "),
			"nombre_repas_par_donation" => $nombre_repas_par_donation ,
			"total_co2_sauve"=> $total_co2_sauve ,
			"valeur_monetaire_sauvee_vente"=> number_format( $valeur_monetaire_sauvee_vente , 0 , "," , " "),
			"nombre_produits_concerne_vente" => number_format( $nombre_produits_concerne_vente , 0 , "," , " "),
			"valeur_monetaire_sauvee_donation"=> number_format( $valeur_monetaire_sauvee_donation , 0 , "," , " "),
			"nombre_produits_concerne_donation" => number_format( $nombre_produits_concerne_donation , 0 , "," , " "),
			"somme_valeur_monetaire"=> $somme_valeur_monetaire ,
			"somme_produit_concerne"=> $somme_produit_concerne ,
			"valeur_monetaire_moyenne_par_produit_sauvee_donation"=> $valeur_monetaire_moyenne_par_produit_sauvee_donation ,
			"valeur_monetaire_moyenne_par_produit_sauvee_vente"=> $valeur_monetaire_moyenne_par_produit_sauvee_vente ,
			"totat_produit_sauve"=> number_format( $totat_produit_sauve , 0 , "," , " "),
			"nombre_repas_orphelinat"=> number_format( $nombre_repas_orphelinat , 0 , "," , " "),
			"chiffre_affaire"=> number_format( $chiffre_affaire , 0 , "," , " ") ,
			"debut_periode"=>$debut_periode,
			"fin_periode"=>$fin_periode,
		 );
		return $this->render("dashboard","dashboard" , "dashboard_general")->with($params);
    }
	
	public function page_dashboard(){
		return $this->render("dashboard","dashboard" , "dashboard_general");
	}
	
	
	
}
