<?php

namespace App\Http\Controllers;

use App\Models\Entreprise;
use App\Models\Entreprise_type;
use App\Models\Type_entreprise;
use App\Models\Employe;
use App\Models\Poste;
use App\Models\Type_aliment;
use App\Models\Utilisateur;
use App\Models\Organisation;
use App\Models\Organisation_employe;
use App\Models\Type_organisation;
use App\Models\Adresse;
use App\Models\Pays;
use App\Models\Adresse_entreprise;
use App\Models\Entreprise_type_aliment;
use App\Models\Donation;
use App\Models\Entreprise_employe;
use App\Models\Consommateur;
use App\Models\Produit;
use App\Models\Photo_produit;
use App\Models\Type_produit;
use App\Models\Strategie_reduction;
use App\Models\Equipement;
use App\Models\Periode_recuperation_commande;
use App\Models\Produit_equipement;
use App\Models\Information_nutritionnelle;
use App\Models\Produit_information_nutritionnelle;
use App\Models\Raison_donation;
use App\Models\Raison_surplus;
use App\Models\Periode_reduction;
use Illuminate\Http\Request;
Use Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

use App\Exports\ReceveurExport;
use App\Exports\ClientExport;
use App\Exports\EntrepriseExport;
use App\Exports\ProduitExport;

// use Maatwebsite\Excel\Facades\Excel;
use DB;
use Excel ;


class BddController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	parent::__construct();
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render("map","");	
    }
	
	/** recherche */
	public function rechercheGlobale(Request $request){
		$mot_cle = $request->get('mot');
		$produit = DB::Table('produit')->select('produit.id_produit' , 'produit.nom_produit' , DB::raw('"produit"'))->where('produit.nom_produit' , 'like' , "%".$mot_cle."%" );
		$entreprise = DB::Table('entreprise')->join('utilisateur', 'utilisateur.id_utilisateur' , 'entreprise.utilisateur_id')->select('entreprise.id_entreprise' , 'entreprise.nom_entreprise' , DB::raw('"entreprise"'))->where('entreprise.nom_entreprise' , 'like' , "%".$mot_cle."%" )->where('utilisateur.active' , '1');
		$receveur = DB::Table('organisation')->select('organisation.id_organisation' , 'organisation.nom_organisation' , DB::raw('"organisation"'))->where('organisation.nom_organisation' , 'like' , "%".$mot_cle."%" )->where('organisation.active' , '1');
		$client = DB::Table('consommateur')->join('utilisateur', 'utilisateur.id_utilisateur' , 'consommateur.utilisateur_id')->select('consommateur.id_consommateur' , 'consommateur.nom' , DB::raw('"client"'))->where('consommateur.nom' , 'like' , "%".$mot_cle."%" )->where('utilisateur.active' , '1')->union($produit)->union($entreprise)->union($receveur)->get();
		
		
		
		// $resultat = DB::Table("select id_produit , nom_produit , 'produit' from produit where nom_produit like '%".$mot_cle."%' union select id_consommateur , nom  , 'utilisateur' from consommateur where nom like '%".$mot_cle."%' or  prenom like '%".$mot_cle."%' ")->get();
		$params = array(
				'data'=> $client,
				'mot_cle' =>$mot_cle
		);
		// var_dump($client) ; die();
		
		
		return $this->render("resultat_recherche","","")->with($params);	
	}

	/****  BDD produits    *****/
	
	public function archiver(Request $request){
		$donation = Donation::where('produit_id_produit' , $request->get('id_produit'))->first();
		$donation-> etat_donation_id = 8 ;
		$donation ->save();
		return redirect('produit');
	}
	public function vendre(Request $request){
		$donation = Donation::where('produit_id_produit' , $request->get('id_produit'))->first();
		$donation-> etat_donation_id = 7 ;
		$donation ->save();
		return redirect('produit');
	}
	
	public function exportProduit(){
		
		
		return Excel::download(new ProduitExport(), 'export.xlsx');
	}
	public function updateProduit( Request $request ){
		$validator = Validator::make($request->all(), [
			'nom_produit'=>'required',
			'id_type_produit'=>'required',
			'poids'=>'numeric|required',
			'volume'=>'numeric|required',
			'id_type_aliment'=>'required',
			'prix'=>'numeric|required',
			'groupement'=>'required',
			'id_information_nutritionnelle'=>'required',
			'id_raison_surplus'=>'required',
		  ], [
			'nom_produit.required'=>'Champs obligatoire',
			'poids.required'=>'Champs obligatoire',
			'poids.numeric'=>'Format incorect',
			'volume.required'=>'Champs obligatoire',
			'volume.numeric'=>'Format incorect',
			'id_type_aliment.required'=>'Champs obligatoire',
			'groupement.required'=>'Champs obligatoire',
			'prix.required'=>'Champs obligatoire',
			'prix.numeric'=>'Format incorect',
			'id_raison_surplus.required'=>'Champs obligatoire',
			'id_information_nutritionnelle.required'=>'Champs obligatoire',
			'id_type_produit.required'=>'Champs obligatoire',
			]);
			
		  if ($validator->fails()) {
			  // var_dump(  Input::all()); die();
				return back()->withErrors($validator)->withInput();
			}
			// var_dump( 'tafa'); die();
		$liste_input = Input::all();
		
		// $this->uploadImageProduit($request);
		
		// nouveau produit	
		$id_produit = $request->get('id_produit');
		$produit = Produit::find($id_produit) ;
		$produit->nom_produit = $request->get('nom_produit');
		$produit->type_produit_id  = $request->get('id_type_produit');
		$produit->description  = $request->get('description');
		$produit->poids  = $request->get('poids');
		$produit->volume  = $request->get('volume');
		$produit->type_aliment_id  = $request->get('id_type_aliment');
		$produit->prix  = $request->get('prix');
		$produit->groupement  = $request->get('groupement');
		
		$x = $produit->save();
		
		
		//produit equipement
		// for($i=1 ; $i<=4 ;$i++){
			// if($i==3) continue ;
			
			// $produit_equipement = new Produit_equipement ;
			// $produit_equipement->produit_id = $id_produit;
			// $produit_equipement->equipement_id = $i;
			// if($request->get('equipement'.$i) == '1') $produit_equipement->etat = $request->get('equipement'.$i);
			// else $produit_equipement->etat = '0' ;
			// var_dump($produit_equipement);
			// $produit_equipement->save();
		// }
		// die();
		//info nutritionnelle produit
		
		$info = Produit_information_nutritionnelle::where('produit_id', $id_produit)->get();
		// var_dump($info); die();
		if($info!=null){
			for($i = 0 ; $i< count($info) ; $i++){
				$info[$i] ->delete();
			}
		}
		$liste_info_nut = $request->get('id_information_nutritionnelle');
		// var_dump($liste_info_nut); die();
		for($i = 0 ; $i< count($liste_info_nut) ; $i++){
			$info_nut = new Produit_information_nutritionnelle ;
			$info_nut->information_nutritionnelle_id = $liste_info_nut[$i];
			$info_nut->produit_id = $id_produit;
			$info_nut->save();
		}
		
		$donation = Donation::where('produit_id_produit' , $id_produit)->first();
		$raison_surplus = Raison_donation::where('donation_id' , $donation->id_donation)->get();
		for($i = 0 ; $i< count($raison_surplus) ; $i++){
			$raison_surplus[$i] ->delete();
		}	
		//raisonSurplus
		$liste_raison = $request->get('id_raison_surplus');
		for($i = 0 ; $i< count($liste_raison) ; $i++){
			$raison_donation = new Raison_donation ;
			$raison_donation->raison_surplus_id = $liste_raison[$i];
			$raison_donation->donation_id =  $donation->id_donation;
			$raison_donation->save();
			
		}		
	
		
		return redirect('produit');
	}	
	public function ajouterProduitDon( Request $request ){
		$liste_input = Input::all();
		
		$this->uploadImageProduit($request);
		
		// nouveau produit	
		
		$produit = new Produit ;
		$produit->nom_produit = $request->get('nom_produit');
		$produit->type_produit_id  = $request->get('id_type_produit');
		$produit->description  = $request->get('description');
		$produit->poids  = $request->get('poids');
		$produit->volume  = $request->get('volume');
		$produit->type_aliment_id  = $request->get('id_type_aliment');
		$produit->prix  = $request->get('prix');
		$produit->groupement  = $request->get('groupement');
		
		$x = $produit->save();
		$id_produit = $produit->id_produit ;
		
		//produit equipement
		for($i=1 ; $i<=4 ;$i++){
			if($i==3) continue ;
			
			$produit_equipement = new Produit_equipement ;
			$produit_equipement->produit_id = $id_produit;
			$produit_equipement->equipement_id = $i;
			if($request->get('equipement'.$i) == '1') $produit_equipement->etat = $request->get('equipement'.$i);
			else $produit_equipement->etat = '0' ;
			// var_dump($produit_equipement);
			$produit_equipement->save();
		}
		// die();
		//info nutritionnelle produit
		
		$liste_info_nut = $request->get('id_information_nutritionnelle');
		for($i = 0 ; $i< count($liste_info_nut) ; $i++){
			$info_nut = new Produit_information_nutritionnelle ;
			$info_nut->information_nutritionnelle_id = $liste_info_nut[$i];
			$info_nut->produit_id = $id_produit;
			$info_nut->save();
			
		}	
			
		//entreprise info
		$entreprise = Entreprise::find($request->get('id_entreprise'));	
			
		
		
		//donation 
		
		$donation = new Donation ;
		$donation->entreprise_id = $entreprise->id_entreprise;
		$donation->produit_id_produit  = $id_produit;
		$donation->quantite  = $request->get('quantite');
		$donation->date_peremption_produit  = $request->get('date_peremption');
		$donation->etat_donation_id  = 1; //mis en vente
		$donation->action  = 1 ;
		$donation->type_date  = $request->get('type_date');
		$donation->minimum_qte_vente  = $request->get('quantite_minimum');
		$donation->save();
		$id_donation = $donation->id_donation;
		
		//raisonSurplus
		$liste_raison = $request->get('id_raison_surplus');
		for($i = 0 ; $i< count($liste_raison) ; $i++){
			$raison_donation = new Raison_donation ;
			$raison_donation->raison_surplus_id = $liste_raison[$i];
			$raison_donation->donation_id = $id_donation;
			$raison_donation->save();
			
		}	
		
		
		return redirect('produit');
	}	
	public function ajouterProduitVente( Request $request ){
		$liste_input = Input::all();
	
	//photo_produit
		$this->uploadImageProduit($request);
		
		
		
		// nouveau produit	
		
		$produit = new Produit ;
		$produit->nom_produit = $request->get('nom_produit');
		$produit->type_produit_id  = $request->get('id_type_produit');
		$produit->description  = $request->get('description');
		$produit->poids  = $request->get('poids');
		$produit->volume  = $request->get('volume');
		$produit->type_aliment_id  = $request->get('id_type_aliment');
		$produit->prix  = $request->get('prix');
		$produit->groupement  = $request->get('groupement');
		
		$x = $produit->save();
		$id_produit = $produit->id_produit ;
		$liste_photo = $request->file('photo');
		for($i = 0 ; $i<count($liste_photo) ; $i++) {
			$photo = new Photo_produit;
			$photo->produit_id = $id_produit;
			$photo->url = "produits/".$liste_photo[$i]->getClientOriginalName();
			$photo->save();
		}
		//produit equipement
		for($i=1 ; $i<=4 ;$i++){
			if($i==3) continue ;
			
			$produit_equipement = new Produit_equipement ;
			$produit_equipement->produit_id = $id_produit;
			$produit_equipement->equipement_id = $i;
			if($request->get('equipement'.$i) == '1') $produit_equipement->etat = $request->get('equipement'.$i);
			else $produit_equipement->etat = '0' ;
			
			$produit_equipement->save();
		}
		
		//info nutritionnelle produit
		
		$liste_info_nut = $request->get('id_information_nutritionnelle');
		for($i = 0 ; $i< count($liste_info_nut) ; $i++){
			$info_nut = new Produit_information_nutritionnelle ;
			$info_nut->information_nutritionnelle_id = $liste_info_nut[$i];
			$info_nut->produit_id = $id_produit;
			$info_nut->save();
			
		}	
			
		//entreprise info
		$entreprise = Entreprise::find($request->get('id_entreprise'));	
			
		//get adresse de ramassage
		if($request->get('lieuRamassage') == '1'){
			$id_adresse_ramassage=$entreprise->adresse()->get()[0]->id_adresse;
		}else{
			// nouvell adresse
			$adresse = new Adresse ;
			$adresse->label = $request->get('label');
			$adresse->latitude = $request->get('latitude');
			$adresse->longitude = $request->get('longitude');
			$adresse->code_postal = $request->get('code_postal');
			$adresse->ville = $request->get('ville');
			$adresse->pays_id = $request->get('pays_id');
			$adresse->save();
			$id_adresse_ramassage = $adresse->id_adresse;
			
			//nouvelle adresse entreprise
			
			$adresse_entreprise = new Adresse_entreprise;
			$adresse_entreprise->entreprise_id = $entreprise->id_entreprise;
			$adresse_entreprise->adresse_id = $id_adresse_ramassage;
			$adresse_entreprise->save();
			
		}
		
		
		//donation 
		
		$donation = new Donation ;
		$donation->entreprise_id = $entreprise->id_entreprise;
		$donation->produit_id_produit  = $id_produit;
		$donation->quantite  = $request->get('quantite');
		$donation->date_peremption_produit  = $request->get('date_peremption');
		$donation->etat_donation_id  = 7; //mis en vente
		$donation->action  = 2 ;
		$donation->type_date  = $request->get('type_date');
		$donation->minimum_qte_vente  = $request->get('quantite_minimum');
		$donation->adresse_ramassage_id  = $id_adresse_ramassage ;
		$donation->pourcentage_reduction  = $request->get('reduction');
		$donation->save();
		$id_donation = $donation->id_donation;
		
		//raisonSurplus
		$liste_raison = $request->get('id_raison_surplus');
		for($i = 0 ; $i< count($liste_raison) ; $i++){
			$raison_donation = new Raison_donation ;
			$raison_donation->raison_surplus_id = $liste_raison[$i];
			$raison_donation->donation_id = $id_donation;
			$raison_donation->save();
			
		}	
		//strategie marketing 
		
		for($i = 1 ; $i <5 ; $i++){
			if( $request->get('reduction'.$i) != ''){
				$strategie = new Strategie_reduction ;
				$strategie->periode_reduction_id = $i;
				$strategie->pourcentage_reduction =$request->get('reduction'.$i) ;
				$strategie->donation_id = $id_donation;
				$strategie->save();
				
			}
		}
		
		//autre strategie 
		$liste_id_autre_periode = [] ;
		foreach ($liste_input as $key => $value) {
			if(strpos($key,"id_periode_reduction") !== false ){
				$nbs = str_replace('id_periode_reduction','',$key);
					$liste_id_autre_periode [] = $nbs ; 
			}
		}
		if( count( $liste_id_autre_periode   ) > 0){
			
			for($i = 0 ; $i < count( $liste_id_autre_periode ) ; $i++){
				$strategie_reduction = new Strategie_reduction ;
				$strategie_reduction->periode_reduction_id = $request->get('id_periode_reduction'.$liste_id_autre_periode[$i]);
				$strategie_reduction->pourcentage_reduction  = $request->get('autre_reduction'.$liste_id_autre_periode[$i]);
				$strategie_reduction->donation_id = $id_donation;
				$strategie_reduction->save();
				
			}
		}
		
		//periode de collecte
		$moment_collecte = new Periode_recuperation_commande ;
		$moment_collecte->jour = $request->get('jour');
		$moment_collecte->horaire_debut = $request->get('heure_debut');
		$moment_collecte->horaire_fin = $request->get('heure_fin');
		$moment_collecte->donation_id = $id_donation ;
		$moment_collecte->save();
		
		//autre periode de collecte
		$liste_id_autre_moment = [] ;
		foreach ($liste_input as $key => $value) {
			if(strpos($key,"autre_jour") !== false ){
				$nbs = str_replace('autre_jour','',$key);
					$liste_id_autre_moment [] = $nbs ; 
			}
		}
		if( count( $liste_id_autre_moment   ) > 0){
			
			for($i = 0 ; $i < count( $liste_id_autre_moment ) ; $i++){
				$moment_collecte = new Periode_recuperation_commande ;
				$moment_collecte->jour = $request->get('autre_jour'.$liste_id_autre_moment[$i]);
				$moment_collecte->horaire_debut = $request->get('heure_fin'.$liste_id_autre_moment[$i]);
				$moment_collecte->horaire_fin = $request->get('heure_fin'.$liste_id_autre_moment[$i]);
				$moment_collecte->donation_id = $id_donation ;
				$moment_collecte->save();
				var_dump($moment_collecte->jour);
			}
		}
		// die();
		return redirect('produit');
	}	
	public function page_ajoutProduit( Request $request ){
		$type_aliment = Type_aliment::all();
		$type_produit = Type_produit::all();
		$information_nutritionnelle = Information_nutritionnelle::all();
		$raison_surplus = Raison_surplus::all();
		$utilisateur_active= Utilisateur::where('active','1')->get();
		$entreprise = [] ;
		for($i=0 ; $i< count($utilisateur_active) ;$i++){
			$temp =$utilisateur_active[$i]->entreprise()->first();
			if($temp['nom_entreprise']) $entreprise [] = $temp ;
		} 
		
		$pays = Pays::all();
		$autre_periode = Periode_reduction::where('id_periode_reduction','>','4')->get();
		$params = array(
					"type_produit"=>$type_produit,
					"type_aliment"=>$type_aliment,
					"information_nutritionnelle"=>$information_nutritionnelle,
					"raison_surplus"=>$raison_surplus,
					"entreprise"=>$entreprise,
					"pays"=>$pays,
					"autre_periode"=>$autre_periode,
					
				);
	
		return $this->render("ajouter_produit","nos_donnees")->with($params);	
	}
	
	public function uploadImageProduit(Request $request) {
      $file = $request->file('photo');
      $destinationPath = public_path('img/produits');
	  for($i = 0 ; $i < count( $file) ;$i++){
		$file[$i]->move($destinationPath,$file[$i]->getClientOriginalName());
	  }
   }
   
   
	public function page_updateProduit(Request $request){
		$produit = Produit::find($request->get('id_produit'));
		$type_aliment = Type_aliment::all();
		$type_produit = Type_produit::all();
		$liste_photo = $produit->photo_produit()->get();
		$info_nut = $produit->information_nutritionnelle()->get();
		$raison = $produit->donation()->first()->raison_surplus()->get();
		$photo_value = '';
		$liste_raison_surplus = Raison_surplus::all();
		$liste_info_nut = Information_nutritionnelle::all();
		if(count($liste_photo)!=0 ){
			$photo_value = asset('img/'.$photo_value.$liste_photo[0]->url); 
		}
		for($i = 0 ; $i< count($liste_photo) ; $i++){
			$photo_value = $photo_value.';'.asset('img/'.$liste_photo[$i]->url);
		}
		// var_dump($photo_value);
		// die();
		$params = array(
						"produit"=>$produit,
						"type_aliment"=>$type_aliment,
						"type_produit"=>$type_produit,
						"photo_value"=>$photo_value,
						"liste_raison_surplus"=>$liste_raison_surplus,
						"raison_surplus"=>$raison,
						"informationNutritionnelle"=>$info_nut,
						"liste_info_nut"=>$liste_info_nut,
						
					);
		
		return $this->render("update_produit","nos_donnees","poduit")->with($params);	
	}
	
	public function getProduit(Request $request){
		$produit = Produit::find($request->get('id_produit'));
		$type_produit = $produit->type_produit()->first();
		$type_aliment = $produit->type_aliment()->first();
		$donation = $produit->donation()->first();
		$entreprise_proprietaire = $donation->entreprise()->first();
		$photos = $produit->photo_produit()->get();
		 $information_nutritionnelles = $produit->information_nutritionnelle()->get();
		$stockages = [] ;
		$produit_equipement = $produit->produit_equipement()->where('etat' , '1')->get();
		for($i = 0 ; $i<count($produit_equipement) ;$i++){
			$stockage = $produit_equipement[$i]->equipement->label;
			if($stockage=='Frigo') $stockages[] = "Le produit nécessite d'être réfrigéré ";
			if($stockage=='Réfrigérateur') $stockages[] = "Le produit nécessite d'être congelé";
		}

		$params = array(
						"produit"=>$produit,
						"type_produit"=>$type_produit,
						"type_aliment"=>$type_aliment,
						"photos"=>$photos,
						"information_nutritionnelles"=>$information_nutritionnelles,
						"entreprise_proprietaire"=>$entreprise_proprietaire,
						"stockages"=>$stockages,
						
					);
		
		return $this->render("fiche_produit","nos_donnees","produit")->with($params);	
	}
	
	public function getListeProduit(Request $request){
		$produits = Produit::all();
		
		$produits_a_vendre = [] ;
		$donation_vente = 	Donation::where('etat_donation_id', 7)->get();
		$produits_archive = [] ;
		$donation_archive = 	Donation::where('etat_donation_id', 8)->get();
		for($i = 0 ; $i< count($donation_vente) ; $i++){
			$produits_a_vendre [] = $donation_vente[$i]->produit()->first();
		}
		for($i = 0 ; $i< count($donation_archive) ; $i++){
			$produits_archive [] = $donation_archive[$i]->produit()->first();
		}
		
		$params = array(
						"produits"=>$produits,
						"produits_archive"=>$produits_archive,
						"produits_a_vendre"=>$produits_a_vendre,
						
					);
		
		return $this->render("produit","nos_donnees","produit")->with($params);	
	}
	
	/****  BDD consommateur    *****/
	
	public function exportClient(){
		
		
		return Excel::download(new ClientExport(), 'export.xlsx');
	}
	
	public function ajouterClient( Request $request ){
		
		$validator = Validator::make( $request->all() , [
			'nom'=>'required',
			'prenom'=>'required',
			'mail'=>'email|required',
			'adresse_de_livraison'=>'required',
			'numero_telephone'=>'numeric|required',
			'mobile_banking'=>'numeric|required',
			'pseudo' => 'bail|required|unique:utilisateur',
			'password' => 'required|string|min:6|confirmed',
		  ], ['nom.required'=>'Champs obligatoire',
			'prenom.required'=>'Champs obligatoire',
			'mail.required'=>'Champs obligatoire',
			'mail.email'=>'Format incorrect',
			'adresse_de_livraison.required'=>'Champs obligatoire',
			'numero_telephone.required'=>'Champs obligatoire',
			'numero_telephone.numeric'=>'Format incorrect',
			'mobile_banking.numeric'=>'Format incorrect',
			'pseudo.required'=>'Champs obligatoire',
			'pseudo.unique'=>'Choisissez un autre',
			'password.required'=>'Champs obligatoire',
			'password.min'=>'Entrez au moins 6 caractères',
			'password.confirmed'=>'Mots de passe différents',
			] );
			
		  if ($validator->fails()) {
				Session::flash('erreur_ajout' , '1');
				return back()->withErrors($validator)->withInput();
			}	
		
		$utilisateur = new Utilisateur ;
		$utilisateur->pseudo = $request->get('pseudo');
		$utilisateur->password = Hash::make( $request->get('password') );
		$x = $utilisateur->save();
		
		
		$consommateur = new Consommateur ;
		$consommateur->nom = $request->get('nom');
		$consommateur->prenom =  $request->get('prenom');
		$consommateur->utilisateur_id = $utilisateur->id_utilisateur ;
		$consommateur->mail =  $request->get('mail');
		$consommateur->mobile_banking =  $request->get('mobile_banking');
		$consommateur->numero_telephone =  $request->get('numero_telephone');
		$consommateur->adresse_de_livraison =  $request->get('adresse_de_livraison');
		$consommateur->save();
		
		
		return redirect('client');
	}	
	
	public function updateClient( Request $request ){
		
		$validator = Validator::make( $request->all() , [
			'nom'=>'required',
			'prenom'=>'required',
			'mail'=>'email|required',
			'adresse_de_livraison'=>'required',
			'numero_telephone'=>'numeric|required',
			'mobile_banking'=>'numeric|required',
			
		  ], ['nom.required'=>'Champs obligatoire',
			'prenom.required'=>'Champs obligatoire',
			'mail.required'=>'Champs obligatoire',
			'mail.email'=>'Format incorrect',
			'adresse_de_livraison.required'=>'Champs obligatoire',
			'numero_telephone.required'=>'Champs obligatoire',
			'mobile_banking.required'=>'Champs obligatoire',
			'numero_telephone.numeric'=>'Format incorrect',
			'mobile_banking.numeric'=>'Format incorrect',
			] );
			
		  if ($validator->fails()) {
				Session::flash('erreur_update' , '1');
				return back()->withErrors($validator)->withInput();
			}	
		$id_consommateur = $request->get('id_consommateur');
		
		$consommateur = Consommateur::find($id_consommateur);
		$consommateur->nom = $request->get('nom');
		$consommateur->prenom =  $request->get('prenom');
		$consommateur->mail =  $request->get('mail');
		$consommateur->mobile_banking =  $request->get('mobile_banking');
		$consommateur->numero_telephone =  $request->get('numero_telephone');
		$consommateur->adresse_de_livraison =  $request->get('adresse_de_livraison');
		$consommateur->save();
		
		
		return redirect('client');
	}
	
	public function page_updateClient(Request $request){
		$id_consommateur = $request->get('id_consommateur');
		$consommateur = Consommateur::find( $id_consommateur);
		
		$params = array(
						"consommateur"=>$consommateur,
						
					);
		
		return $this->render("update_client","nos_donnees","client")->with($params);	
	}
	
	public function dropClient( Request $request){
		$utilisateur=Utilisateur::find($request->get('utilisateur_id'));
		
		$utilisateur->active= 0;
		$utilisateur->save();
		// $ajout=1;
		// Session::flash('choix' , $ajout);
		//if ($request->get('return')==null) return back();
		//else 
		return redirect('client');
	}
	
	
	public function getDetailConsommateur(Request $request){
		// prix moyenne choisi par produit, porucentage moyenne chosiit par produits 
		// Achats (MGA) total par clients
		$achat = DB::table('produit_commande')
			->join('commande' , 'commande.id_commande', 'produit_commande.commande_id')
			->select( DB::raw('sum( produit_commande.prix_achat_unite*produit_commande.qte) as montant_total') )
			->where('commande.consommateur_id' , $request->get('id_consommateur'))
			->groupBy('commande.consommateur_id')
			->get();
		// prix moyen choisi par produit
		$moyenne_prix_par_produit = DB::table('produit_commande')
			->join('commande' , 'commande.id_commande', 'produit_commande.commande_id')
			->join('donation' , 'donation.id_donation', 'produit_commande.donation_id')
			->join('produit' , 'produit.id_produit', 'donation.produit_id_produit')
			->select( 'produit.nom_produit' ,DB::raw('round(avg( produit_commande.prix_achat_unite),0) as prix_moyen') )
			->where('commande.consommateur_id' , $request->get('id_consommateur'))
			->groupBy('donation.produit_id_produit')
			->get();
		// moyenne remise par produit	
		$moyenne_reduction = DB::table('consommateur')
				->join (DB::raw('(select c.consommateur_id  , p.nom_produit , round(avg(100*(p.prix-pc.prix_achat_unite)/p.prix),1) as moyenne_remise from produit_commande pc
								join donation d on d.id_donation=pc.donation_id
								join commande c on c.id_commande=pc.commande_id
								join produit p on p.id_produit=d.produit_id_produit
                                where c.consommateur_id='.$request->get('id_consommateur').'
                                group by p.nom_produit) as moyenne_reduction') ,'moyenne_reduction.consommateur_id','consommateur.id_consommateur' )
				->select('moyenne_reduction.nom_produit' , 'moyenne_reduction.moyenne_remise' )
				->get();
		$best_reduction = DB::table('consommateur')
				->join (DB::raw('(select  tab2.reduction as reduc, max(tab2.nombre) , tab2.consommateur_id   from
								(select count(tab1.reduction) as nombre, tab1.reduction as reduction, tab1.consommateur_id as consommateur_id from 
								(select pc.* , c.* , round((100*(p.prix-pc.prix_achat_unite)/p.prix),1) as reduction from produit_commande pc
								join donation d on d.id_donation=pc.donation_id
								join commande c on c.id_commande=pc.commande_id
								join produit p on p.id_produit=d.produit_id_produit) as tab1  group by tab1.reduction , tab1.consommateur_id order by nombre desc) as tab2 group by tab2.consommateur_id ) as best_reduction') ,'best_reduction.consommateur_id','consommateur.id_consommateur' )
				->select('consommateur.nom' , 'consommateur.prenom' , 'best_reduction.reduc')
				->where('consommateur.id_consommateur' , $request->get('id_consommateur'))
				->get();	
		
		$data = array (
				"achat" => $achat ,
				"moyenne_prix_par_produit" => $moyenne_prix_par_produit ,
				"moyenne_reduction" => $moyenne_reduction ,
				"best_reduction" => $best_reduction ,
				
		) ;
			
		return response($data);
	}
	public function getFicheConsommateur(Request $request){
		$consommateur = Consommateur::find($request->get('id_consommateur'));
		// prix moyenne choisi par produit, porucentage moyenne chosiit par produits 
		// Achats (MGA) total par clients
		$achat = DB::table('produit_commande')
			->join('commande' , 'commande.id_commande', 'produit_commande.commande_id')
			->select( DB::raw('sum( produit_commande.prix_achat_unite*produit_commande.qte) as montant_total') )
			->where('commande.consommateur_id' , $request->get('id_consommateur'))
			->groupBy('commande.consommateur_id')
			->first();
		// prix moyen choisi par produit
		$moyenne_prix_par_produit = DB::table('produit_commande')
			->join('commande' , 'commande.id_commande', 'produit_commande.commande_id')
			->join('donation' , 'donation.id_donation', 'produit_commande.donation_id')
			->join('produit' , 'produit.id_produit', 'donation.produit_id_produit')
			->select( 'produit.nom_produit' ,DB::raw('round(avg( produit_commande.prix_achat_unite),0) as prix_moyen') )
			->where('commande.consommateur_id' , $request->get('id_consommateur'))
			->groupBy('donation.produit_id_produit')
			->get();
		// moyenne remise par produit	
		$moyenne_reduction = DB::table('consommateur')
				->join (DB::raw('(select c.consommateur_id  , p.nom_produit , round(avg(100*(p.prix-pc.prix_achat_unite)/p.prix),1) as moyenne_remise from produit_commande pc
								join donation d on d.id_donation=pc.donation_id
								join commande c on c.id_commande=pc.commande_id
								join produit p on p.id_produit=d.produit_id_produit
                                where c.consommateur_id='.$request->get('id_consommateur').'
                                group by p.nom_produit) as moyenne_reduction') ,'moyenne_reduction.consommateur_id','consommateur.id_consommateur' )
				->select('moyenne_reduction.nom_produit' , 'moyenne_reduction.moyenne_remise' )
				->get();
		$best_reduction = DB::table('consommateur')
				->join (DB::raw('(select  tab2.reduction as reduc, max(tab2.nombre) , tab2.consommateur_id   from
								(select count(tab1.reduction) as nombre, tab1.reduction as reduction, tab1.consommateur_id as consommateur_id from 
								(select pc.* , c.* , round((100*(p.prix-pc.prix_achat_unite)/p.prix),1) as reduction from produit_commande pc
								join donation d on d.id_donation=pc.donation_id
								join commande c on c.id_commande=pc.commande_id
								join produit p on p.id_produit=d.produit_id_produit) as tab1  group by tab1.reduction , tab1.consommateur_id order by nombre desc) as tab2 group by tab2.consommateur_id ) as best_reduction') ,'best_reduction.consommateur_id','consommateur.id_consommateur' )
				->select('consommateur.nom' , 'consommateur.prenom' , 'best_reduction.reduc')
				->where('consommateur.id_consommateur' , $request->get('id_consommateur'))
				->first();	
		
		$data = array (
				"achat" => $achat ,
				"moyenne_prix_par_produit" => $moyenne_prix_par_produit ,
				"moyenne_reduction" => $moyenne_reduction ,
				"best_reduction" => $best_reduction ,
				"consommateur" => $consommateur ,
				
		) ;
		// var_dump($best_reduction); die();
			
		return  $this->render("fiche_consommateur","nos_donnees","client")->with($data);	
	}
	
	public function getListeConsommateur(){
		$utilisateur = Utilisateur::where('active' , '1')->get();
		
		for($i = 0 ; $i< count($utilisateur) ; $i++){
			$variable = $utilisateur[$i]->consommateur()->where('nom' , '!=' ,'null')->first();
			if($variable)
				$consommateurs[] = $variable;
		}
		
		$params = array ( 'consommateurs'=> $consommateurs ) ;
		
		return $this->render("client","nos_donnees","client")->with($params);	
	}
	
	
	
	/****  BDD receveur    *****/
	
	public function exportReceveur(){
	
		return Excel::download(new ReceveurExport(), 'export.xlsx');
	}
	
	
	public function ajouterReceveur( Request $request ){
		$validator = Validator::make( $request->all() , [
			'nom_organisation'=>'required',
			'numero'=>'required',
			'id_type_organisation'=>'required',
			'label_adresse'=>'required',
			'latitude_adresse'=>'numeric|required',
			'longitude_adresse'=>'numeric|required',
			'code_postal_adresse'=>'numeric|required',
			'id_pays'=>'required',
			'ville_adresse'=>'required',
			'nom_responsable'=>'required',
			'prenom_responsable'=>'required',
			'email_responsable'=>'email|required',
			'telephone_responsable'=>'numeric|required',
			
		  ], ['nom_organisation.required'=>'Champs obligatoire',
			'numero.required'=>'Champs obligatoire',
			'id_type_organisation.required'=>'Champs obligatoire',
			'label_adresse.required'=>'Champs obligatoire',
			'label_adresse.required'=>'Champs obligatoire',
			'latitude_adresse.required'=>'Champs obligatoire',
			'latitude_adresse.numeric'=>'Format incorrect',
			'longitude_adresse.required'=>'Champs obligatoire',
			'longitude_adresse.numeric'=>'Format incorrect',
			'code_postal_adresse.numeric'=>'Format incorrect',
			'id_pays.required'=>'Champs obligatoire',
			'ville_adresse.required'=>'Champs obligatoire',
			'nom_responsable.required'=>'Champs obligatoire',
			'prenom_responsable.required'=>'Champs obligatoire',
			'email_responsable.required'=>'Champs obligatoire',
			'email_responsable.email'=>'Format incorrect',
			'telephone_responsable.numeric'=>'Format incorrect',
			'telephone_responsable.required'=>'Champs obligatoire',] );
			
		  if ($validator->fails()) {
				Session::flash('erreur_update' , '1');
				return back()->withErrors($validator)->withInput();
			}
		
		$adresse = new Adresse ;
		$adresse->label =  $request->get('label_adresse');
		$adresse->latitude =  $request->get('latitude_adresse');
		$adresse->longitude =  $request->get('longitude_adresse');
		$adresse->code_postal =  $request->get('code_postal_adresse');
		$adresse->pays_id =  $request->get('id_pays');
		$adresse->ville =  $request->get('ville_adresse');
		$adresse->save();
		$id_adresse = $adresse->id_adresse ;
		
		$receveur = new Organisation;
		$receveur->nom_organisation = $request->get('nom_organisation');
		$receveur->numero =  $request->get('numero');
		$receveur->type_organisation_id =  $request->get('id_type_organisation');
		$receveur->adresse_id = $id_adresse ;
		$receveur->save();
		$id_organisation = $receveur->id_organisation;
		
		$employe = new Employe;
		$employe->nom =  $request->get('nom_responsable');
		$employe->prenom =  $request->get('prenom_responsable');
		$employe->email =  $request->get('email_responsable');
		$employe->telephone =  $request->get('telephone_responsable');
		$employe->poste_id =  $request->get('id_poste');
		$employe->save();
		$id_employe = $employe->id_employe;
		
		$employe_organisation = new Organisation_employe ;
		$employe_organisation->organisation_id = $id_organisation ;
		$employe_organisation->employe_id = $id_employe;
		$employe_organisation->position = 1;
		$employe_organisation->save();
		
		$adresse = new Adresse ;
		$adresse->label =  $request->get('label_adresse');
		$adresse->latitude =  $request->get('latitude_adresse');
		$adresse->longitude =  $request->get('longitude_adresse');
		$adresse->code_postal =  $request->get('code_postal_adresse');
		$adresse->pays_id =  $request->get('id_pays');
		$adresse->ville =  $request->get('ville_adresse');
		$adresse->save();
		
		
		
		return redirect('receveur');
	}
	
	public function updateReceveur( Request $request ){
		$validator = Validator::make($request->all(), [
			'nom_organisation'=>'required',
			'numero'=>'numeric|required',
			'id_type_organisation'=>'required',
			'label_adresse'=>'required',
			'latitude_adresse'=>'numeric|required',
			'longitude_adresse'=>'numeric|required',
			'code_postal_adresse'=>'numeric|required',
			'id_pays'=>'required',
			'ville_adresse'=>'required',
			'nom_responsable'=>'required',
			'prenom_responsable'=>'required',
			'email_responsable'=>'email|required',
			'telephone_responsable'=>'numeric|required',
			
		  ], ['nom_organisation.required'=>'Champs obligatoire',
			'numero.required'=>'Champs obligatoire',
			'numero.numeric'=>'Format incorect',
			'id_type_organisation.required'=>'Champs obligatoire',
			'label_adresse.required'=>'Champs obligatoire',
			'latitude_adresse.required'=>'Champs obligatoire',
			'latitude_adresse.numeric'=>'Format incorect',
			'longitude_adresse.numeric'=>'Format incorect',
			'longitude_adresse.required'=>'Champs obligatoire',
			'code_postal_adresse.required'=>'Champs obligatoire',
			'id_pays.required'=>'Champs obligatoire',
			'ville_adresse.required'=>'Champs obligatoire',
			'nom_responsable.required'=>'Champs obligatoire',
			'prenom_responsable.required'=>'Champs obligatoire',
			'email_responsable.required'=>'Champs obligatoire',
			'email_responsable.email'=>'Format incorrect',
			'telephone_responsable.required'=>'Champs obligatoire',
			'telephone_responsable.numeric'=>'Format incorect',]);
			
		  if ($validator->fails()) {
				return back()->withErrors($validator)->withInput();
			}
		$id_organisation = $request->get('id_organisation');
		$id_employe = $request->get('id_employe');
		$id_adresse = $request->get('id_adresse');
		
		$receveur = Organisation::find($id_organisation);
		$receveur->nom_organisation = $request->get('nom_organisation');
		$receveur->numero =  $request->get('numero');
		$receveur->type_organisation_id =  $request->get('id_type_organisation');
		$receveur->save();
		
		$adresse = Adresse::find($id_adresse);
		$adresse->label =  $request->get('label_adresse');
		$adresse->latitude =  $request->get('latitude_adresse');
		$adresse->longitude =  $request->get('longitude_adresse');
		$adresse->code_postal =  $request->get('code_postal_adresse');
		$adresse->pays_id =  $request->get('id_pays');
		$adresse->ville =  $request->get('ville_adresse');
		$adresse->save();
		
		$employe = Employe::find($id_employe);
		$employe->nom =  $request->get('nom_responsable');
		$employe->prenom =  $request->get('prenom_responsable');
		$employe->email =  $request->get('email_responsable');
		$employe->telephone =  $request->get('telephone_responsable');
		$employe->save();
		
		return redirect('receveur');
	}
	
	public function getDetailReceveur(Request $request){
		$id_organisation = $request->get('id_organisation');
		$receveur = Organisation::find( $id_organisation);
		$receveur_employe = $receveur->organisation_employe()->where('position', '1')->first()
															->employe()->first();
		$receveur_type = $receveur->type_organisation()->first();
		$adresse = $receveur->adresse()->first();
		$pays = $adresse->pays()->first();
		$liste_type_receveur= Type_organisation::all();
		$liste_pays= Pays::all();
		
		
		$params = array(
						"receveur"=>$receveur,
						"receveur_employe"=>$receveur_employe,
						"receveur_type"=>$receveur_type,
						"adresse"=>$adresse,
						"pays"=>$pays,
						"liste_pays"=>$liste_pays,
						"liste_type_receveur"=>$liste_type_receveur,
					);
		
		return response($params);	
	}
	public function getFicheReceveur(Request $request){
		$id_organisation = $request->get('id_organisation');
		$receveur = Organisation::find( $id_organisation);
		$receveur_employe = $receveur->organisation_employe()->where('position', '1')->first()
															->employe()->first();
		$receveur_type = $receveur->type_organisation()->first();
		$adresse = $receveur->adresse()->first();
		$pays = $adresse->pays()->first();
		$liste_type_receveur= Type_organisation::all();
		$liste_pays= Pays::all();
		
		
		$params = array(
						"receveur"=>$receveur,
						"receveur_employe"=>$receveur_employe,
						"receveur_type"=>$receveur_type,
						"adresse"=>$adresse,
						"pays"=>$pays,
						"liste_pays"=>$liste_pays,
						"liste_type_receveur"=>$liste_type_receveur,
					);
		// var_dump($receveur->adresse->longitude) ; die();
		return  $this->render("fiche_organistaion","nos_donnees","receveur")->with($params);	
	}
	
	public function page_updateReceveur(Request $request){
		$id_organisation = $request->get('id_organisation');
		$receveur = Organisation::find( $id_organisation);
		$receveur_employe = $receveur->organisation_employe()->where('position', '1')->first()
															->employe()->first();
		$receveur_type = $receveur->type_organisation()->first();
		$adresse = $receveur->adresse()->first();
		$pays = $adresse->pays()->first();
		$liste_type_receveur= Type_organisation::all();
		$liste_pays= Pays::all();
		$liste_poste= Poste::where('type', '2')->get();
		
		
		$params = array(
						"receveur"=>$receveur,
						"receveur_employe"=>$receveur_employe,
						"receveur_type"=>$receveur_type,
						"adresse"=>$adresse,
						"pays"=>$pays,
						"liste_pays"=>$liste_pays,
						"liste_poste"=>$liste_poste,
						
						"liste_type_receveur"=>$liste_type_receveur,
					);
		
		return $this->render("update_receveur","nos_donnees","receveur")->with($params);	
	}
	
	public function dropReceveur( Request $request){
		$receveur=Organisation::find($request->get('id_organisation'));
		$receveur->active= 0;
		$receveur->save();
		// $ajout=1;
		// Session::flash('choix' , $ajout);
		//if ($request->get('return')==null) return back();
		//else 
		return redirect('receveur');
	}
	
	public function getListeReceveur(Request $request){
		$receveurs = Organisation::where('active' , '1')->get();
		$liste_responsable = [] ; // organisation_employe
		$info_responsable = [] ; // employe
		$adresse = [] ; // adresse organisation
		$type = [] ; // type organisation
		
		
		for($i = 0 ; $i< count($receveurs) ; $i++){
			$liste_responsable []= $receveurs[$i]->organisation_employe()->where('position' , '1')->first();
			$info_responsable[] = $liste_responsable[$i]->employe()->first();
			
			$adresse[] = $receveurs[$i]->adresse()->first();
			$type[] = $receveurs[$i]->type_organisation()->first();
		}
		$liste_type_receveur= Type_organisation::all();
		$liste_pays= Pays::all();
		$liste_poste= Poste::where('type' , '2')->get();
		// var_dump( count( $numero_compte_entreprise) );
		// var_dump( $type[1] );
		// die();
		
		$params = array(
						"receveurs"=>$receveurs,
						"info_responsable"=>$info_responsable,
						"adresse"=>$adresse,
						"type"=>$type,
						"liste_pays"=>$liste_pays,
						"liste_poste"=>$liste_poste,
						"liste_type_receveur"=>$liste_type_receveur,
					);
		return $this->render("receveur","nos_donnees","receveur")->with($params);	
	}
	
	
	
	
	/****  BDD entreprise    *****/
	
	public function exportEntreprise(){		
		return Excel::download(new EntrepriseExport(), 'export.xlsx');
	}
	
	public function ajouterEntreprise(Request $request){
		$liste_input= Input::all();
		 
		$validator = Validator::make( $request->all() , [
			'pseudo' => 'bail|required|unique:utilisateur',
			'password' => 'required|string|min:6|confirmed',
				
		 ], ['pseudo.required'=>'Champs obligatoire',
			'password.required'=>'Champs obligatoire',
			
			] );

		/******** info perso**************/
		$utilisateur= new Utilisateur;
		$utilisateur->pseudo = $request->get('pseudo');
		$utilisateur->password = Hash::make( $request->get('password') );
		$x = $utilisateur->save();
		$id_utilisateur =  $utilisateur->id_utilisateur;
		
		$entreprise = new Entreprise ;
		$entreprise->nom_entreprise = $request->get('nom_entreprise') ;
		$entreprise->utilisateur_id = $id_utilisateur ;
		$entreprise->nif_entreprise = $request->get('nif_entreprise') ;
		$entreprise->stat_entreprise = $request->get('stat_entreprise') ;
		$entreprise->ca_entreprise = $request->get('ca_entreprise') ;
		$entreprise->logo = $request->get('logo') ;
		$entreprise->save();
		$id_entreprise = $entreprise->id_entreprise ;
		
		
		
		
		//ajout type
		
		$liste_input_id_entreprise_type = $request->get('id_type_entreprise');
		if($liste_input_id_entreprise_type!= null){
			for ($i = 0 ; $i< count($liste_input_id_entreprise_type) ; $i++){
				$type_entreprise = new Entreprise_type ;
				$type_entreprise->entreprise_id = $id_entreprise;
				$type_entreprise->type_entreprise_id = $liste_input_id_entreprise_type[$i];
				$type_entreprise->save();
			}
		}
		
		/******** Responsable numéro 1 **************/
		$responsable1 = new Employe;
		$responsable1->nom = $request->get('nom_responsable1') ;
		$responsable1->prenom = $request->get('prenom_responsable1') ;
		$responsable1->email = $request->get('email_responsable1') ;
		$responsable1->telephone = $request->get('telephone_responsable1') ;
		$responsable1->sexe = $request->get('sexe_responsable1') ;
		$responsable1->poste_id = $request->get('id_poste1') ;
		$responsable1->save();
		
		$entreprise_employe1 = new Entreprise_employe ;
			$entreprise_employe1->entreprise_id  = $id_entreprise ;
			$entreprise_employe1->employe_id  = $responsable1->id_employe ;
			$entreprise_employe1->position  = 1 ;
			$entreprise_employe1->save();
		
		/******** Ajout Responsable numéro 2 **************/
		if($request->get('id_nouvel_employe_responsable2')== 'misy'){
			
			$responsable2 = new Employe;
			$responsable2->nom = $request->get('nom_responsable2') ;
			$responsable2->prenom = $request->get('prenom_responsable2') ;
			$responsable2->email = $request->get('email_responsable2') ;
			$responsable2->telephone = $request->get('telephone_responsable2') ;
			$responsable2->sexe = $request->get('sexe_responsable2') ;
			$responsable2->poste_id = $request->get('id_poste2') ;
			$responsable2->save();

			$entreprise_employe = new Entreprise_employe ;
			$entreprise_employe->entreprise_id  = $id_entreprise ;
			$entreprise_employe->employe_id  = $responsable2->id_employe ;
			$entreprise_employe->position  = 2 ;
			$entreprise_employe->save();
			
		}		
			
			
		
		/******** adresse**************/
		// premier adresse
		
		$adresse = new Adresse ;
		$adresse->label = $request->get('adresse_label');
		$adresse->latitude = $request->get('adresse_latitude');
		$adresse->longitude = $request->get('adresse_longitude');
		$adresse->code_postal = $request->get('adresse_code_postal');
		$adresse->ville = $request->get('adresse_ville');
		$adresse->pays_id = $request->get('adresse_pays_id');
		$adresse->save();
		$id_adresse = $adresse->id_adresse ;
		
		//nouvelle adresse entreprise
		
		$adresse_entreprise = new Adresse_entreprise ;
		$adresse_entreprise->entreprise_id = $id_entreprise ;
		$adresse_entreprise->adresse_id = $id_adresse ;
		$adresse_entreprise->save();
		
		
		$liste_id_nouvelle_adresse = [] ;
		foreach ($liste_input as $key => $value) {
			if(strpos($key,"id_nouvelle_adresse_") !== false ){
				$nbs = str_replace('id_nouvelle_adresse_','',$key);
					$liste_id_nouvelle_adresse [] = $nbs ;
			}
		}
		
		
		
		
		if( count( $liste_id_nouvelle_adresse   ) > 0){
			
			for($i = 0 ; $i < count( $liste_id_nouvelle_adresse ) ; $i++){
				$adresse = new Adresse ;
				$adresse->label = $request->get('nouvelle_adresse_label_'.$liste_id_nouvelle_adresse[$i]);
				$adresse->latitude = $request->get('nouvelle_adresse_latitude_'.$liste_id_nouvelle_adresse[$i]);
				$adresse->longitude = $request->get('nouvelle_adresse_longitude_'.$liste_id_nouvelle_adresse[$i]);
				$adresse->code_postal = $request->get('nouvelle_adresse_code_postal_'.$liste_id_nouvelle_adresse[$i]);
				$adresse->ville = $request->get('nouvelle_adresse_ville_'.$liste_id_nouvelle_adresse[$i]);
				$adresse->pays_id = $request->get('nouvelle_adresse_pays_id_'.$liste_id_nouvelle_adresse[$i]);
				$adresse->save();
				$id_adresse = $adresse->id_adresse ;
				
				//nouvelle adresse entreprise
				
				$adresse_entreprise = new Adresse_entreprise ;
				$adresse_entreprise->entreprise_id = $id_entreprise ;
				$adresse_entreprise->adresse_id = $id_adresse ;
				$adresse_entreprise->save();
				
				
			}
		}
		
		
		
		/******** type aliment **************/
		
		//ajout type
		$liste_input_id_type_aliment = $request->get('id_type_aliment');
		if($liste_input_id_type_aliment!= null){
			for ($i = 0 ; $i< count($liste_input_id_type_aliment) ; $i++){
				$type_aliment = new Entreprise_type_aliment ;
				$type_aliment->entreprise_id = $id_entreprise;
				$type_aliment->type_aliment_id = $liste_input_id_type_aliment[$i];
				$type_aliment->save();
			}
		}
		
		
		
		 // var_dump( $liste_id_adresse_recue );
		 // var_dump( $liste_id_nouvelle_adresse );
		 // var_dump("nety");
		return redirect('entreprise');
	}
	
	public function updateEntreprise(Request $request){
		$liste_input= Input::all();
		 
		$id_entreprise = $request->get('id_entreprise') ;

		/******** info perso**************/
		$entreprise = Entreprise::find( $id_entreprise ) ;
		$entreprise->nom_entreprise = $request->get('nom_entreprise') ;
		$entreprise->nif_entreprise = $request->get('nif_entreprise') ;
		$entreprise->stat_entreprise = $request->get('stat_entreprise') ;
		$entreprise->ca_entreprise = $request->get('ca_entreprise') ;
		$entreprise->logo = $request->get('logo') ;
		$entreprise->save();
		
		/******** info type **************/
		$types_entreprise = Entreprise_type::where('entreprise_id' , $id_entreprise)->get();
		//supression des types 
		
		for($i = 0 ; $i<count($types_entreprise) ; $i++){
			$types_entreprise[$i]->delete();
		}
		
		//ajout type
		$liste_input_id_entreprise_type = $request->get('id_type_entreprise');
		if($liste_input_id_entreprise_type!= null){
			
			for ($i = 0 ; $i< count($liste_input_id_entreprise_type) ; $i++){
				$type_entreprise = new Entreprise_type ;
				$type_entreprise->entreprise_id = $id_entreprise;
				$type_entreprise->type_entreprise_id = $liste_input_id_entreprise_type[$i];
				$type_entreprise->save();
			}
		}
		
		/******** Responsable numéro 1 **************/
		$responsable1 = Employe::find( $request->get('id_employe_responsable1') );
		$responsable1->nom = $request->get('nom_responsable1') ;
		$responsable1->prenom = $request->get('prenom_responsable1') ;
		$responsable1->email = $request->get('email_responsable1') ;
		$responsable1->telephone = $request->get('telephone_responsable1') ;
		$responsable1->sexe = $request->get('sexe_responsable1') ;
		$responsable1->poste_id = $request->get('id_poste1') ;
		$responsable1->save();
		
		/******** Responsable numéro 2 **************/
		if($request->get('id_employe_responsable2') && $request->get('teste_responsable_existe')=='1'){
			$responsable2 = Employe::find( $request->get('id_employe_responsable2') );
			$responsable2->nom = $request->get('nom_responsable2') ;
			$responsable2->prenom = $request->get('prenom_responsable2') ;
			$responsable2->email = $request->get('email_responsable2') ;
			$responsable2->telephone = $request->get('telephone_responsable2') ;
			$responsable2->sexe = $request->get('sexe_responsable2') ;
			$responsable2->poste_id = $request->get('id_poste2') ;
			$responsable2->save();
			
		}
		if($request->get('id_employe_responsable2') && $request->get('teste_responsable_existe')=='0'){  //suppression
			$entreprise_employe = Entreprise_employe::where('employe_id', $request->get('id_employe_responsable2'))->first();
			$entreprise_employe->delete();
			$responsable2 = Employe::where( 'id_employe' ,$request->get('id_employe_responsable2') )->first();
			$responsable2->delete();
		}
		
		/******** Ajout Responsable numéro 2 **************/
		
		if($request->get('nouvel_employe_responsable')!=null){
			
			$responsable2 = new Employe;
			$responsable2->nom = $request->get('nom_responsable2') ;
			$responsable2->prenom = $request->get('prenom_responsable2') ;
			$responsable2->email = $request->get('email_responsable2') ;
			$responsable2->telephone = $request->get('telephone_responsable2') ;
			$responsable2->sexe = $request->get('sexe_responsable2') ;
			$responsable2->poste_id = $request->get('id_poste2') ;
			$responsable2->save();
			
			$entreprise_employe = new Entreprise_employe ;
			$entreprise_employe->entreprise_id  = $id_entreprise ;
			$entreprise_employe->employe_id  = $responsable2->id_employe ;
			$entreprise_employe->position  = 2 ;
			$entreprise_employe->save();
			
		}		
				
		
		/******** adresse**************/
		
		$liste_id_adresse = Adresse_entreprise::where('entreprise_id' , $request->get('id_entreprise') )->get();
		
		$liste_id_nouvelle_adresse = [] ;
		$liste_id_adresse_recue = [] ;
		foreach ($liste_input as $key => $value) {
			if(strpos($key,"id_adresse_") !== false ){
				$nb = str_replace('id_adresse_','',$key);
					$liste_id_adresse_recue [] = $nb ;
			}
			if(strpos($key,"id_nouvelle_adresse_") !== false ){
				$nbs = str_replace('id_nouvelle_adresse_','',$key);
					$liste_id_nouvelle_adresse [] = $nbs ;
			}
		}
		
		if( count($liste_id_adresse) != count($liste_id_adresse_recue ) ){
			
			$teste = 0;
			for($i = 0 ; $i < count($liste_id_adresse) ; $i++){
				for($j =0 ; $j < count($liste_id_adresse_recue) ; $j++ ) {
					
					if( (int)$liste_id_adresse_recue[$j] == $liste_id_adresse[$i]->adresse_id){
						break ; 
					}
					
					if($j+1==count($liste_id_adresse_recue) ){
						
						if( (int)$liste_id_adresse_recue[$j] != $liste_id_adresse[$i]->adresse_id )   {
							$teste=1; 
						}
					}
				}
				if($teste==1){
					//teste si l'adresse est utilisé dans l'adresse ramassage dans la table donation
					$liste_id_donation = Donation::where('adresse_ramassage_id' , $liste_id_adresse[$i]->adresse_id)->get();
					
					if(count($liste_id_donation)!=0){
						for($k = 0 ; $k < count($liste_id_donation) ; $k++){
							//mis a jour de l adresse  de ramassage par la premiere adresse de l'entreprise
							$liste_id_donation[$k]->adresse_ramassage_id = $liste_id_adresse[0]->adresse_id ; 
							$liste_id_donation[$k]->save();
						}
					}
					//supression de l'adresse_entreprise
					$adresse_entreprise = Adresse_entreprise::where('adresse_id' , $liste_id_adresse[$i]->adresse_id)->first();
					$adresse_entreprise->delete();
					
					//suppression de l'adresse 
					$adresse = Adresse::find( $liste_id_adresse[$i]->adresse_id  );
					$adresse->delete();
					$teste=0;
				}
			}
		}
		
		//update adresse
		for($i = 0 ; $i < count( $liste_id_adresse_recue ) ; $i++){
			$adresse = Adresse::find($liste_id_adresse_recue[$i]) ;
				// var_dump( $adresse  );
				// var_dump( $liste_id_adresse_recue[$i]  );
					// die();
			$adresse->label = $request->get('adresse_label'.$liste_id_adresse_recue[$i]);
			$adresse->latitude = $request->get('adresse_latitude'.$liste_id_adresse_recue[$i]);
			$adresse->longitude = $request->get('adresse_longitude'.$liste_id_adresse_recue[$i]);
			$adresse->code_postal = $request->get('adresse_code_postal'.$liste_id_adresse_recue[$i]);
			$adresse->ville = $request->get('adresse_ville'.$liste_id_adresse_recue[$i]);
			$adresse->pays_id = $request->get('adresse_pays_id'.$liste_id_adresse_recue[$i]);
			$adresse->save();
			
		}
		
		if( count( $liste_id_nouvelle_adresse   ) > 0){
			
			for($i = 0 ; $i < count( $liste_id_nouvelle_adresse ) ; $i++){
				$adresse = new Adresse ;
				$adresse->label = $request->get('nouvelle_adresse_label_'.$liste_id_nouvelle_adresse[$i]);
				$adresse->latitude = $request->get('nouvelle_adresse_latitude_'.$liste_id_nouvelle_adresse[$i]);
				$adresse->longitude = $request->get('nouvelle_adresse_longitude_'.$liste_id_nouvelle_adresse[$i]);
				$adresse->code_postal = $request->get('nouvelle_adresse_code_postal_'.$liste_id_nouvelle_adresse[$i]);
				$adresse->ville = $request->get('nouvelle_adresse_ville_'.$liste_id_nouvelle_adresse[$i]);
				$adresse->pays_id = $request->get('nouvelle_adresse_pays_id_'.$liste_id_nouvelle_adresse[$i]);
				$adresse->save();
				$id_adresse = $adresse->id_adresse ;
				
				//nouvelle adresse entreprise
				
				$adresse_entreprise = new Adresse_entreprise ;
				$adresse_entreprise->entreprise_id = $request->get('id_entreprise');
				$adresse_entreprise->adresse_id = $id_adresse ;
				$adresse_entreprise->save();
				
				
			}
		}
		
		
		/******** type aliment **************/
		$types_aliment = Entreprise_type_aliment::where('entreprise_id' , $id_entreprise)->get();
		//supression des types 
		
		for($i = 0 ; $i<count($types_aliment) ; $i++){
			$types_aliment[$i]->delete();
		}
		
		//ajout type
		$liste_input_id_type_aliment = $request->get('id_type_aliment');
		if($liste_input_id_type_aliment!= null){
			for ($i = 0 ; $i< count($liste_input_id_type_aliment) ; $i++){
				$type_aliment = new Entreprise_type_aliment ;
				$type_aliment->entreprise_id = $id_entreprise;
				$type_aliment->type_aliment_id = $liste_input_id_type_aliment[$i];
				$type_aliment->save();
			}
		}

		
		
		
		 // var_dump( $liste_id_adresse_recue );
		 // var_dump( $liste_id_nouvelle_adresse );
		 // var_dump("nety");
		return redirect('information_entreprise?id_entreprise='.$id_entreprise);
	}
	
	public function getListeEntreprise(Request $request){
		$utilisateur = Utilisateur::where('active' , '1')->get();
		
		$entreprises = [] ;
		for($i = 0 ; $i <count($utilisateur) ; $i++){
			$entreprise = $utilisateur[$i]->entreprise()->first();
			if($entreprise['nom_entreprise'])
				$entreprises[] = $utilisateur[$i]->entreprise()->first();
		}
		
		// $entreprises = Entreprise::all();
		for($i =0 ; $i< count($entreprises) ; $i++){
			$entreprise_type []= $entreprises[$i]->type_entreprise()->get();
		}
		
		$params = array(
						"entreprises"=>$entreprises,
						"entreprise_type"=>$entreprise_type,
					);
		
		return $this->render("entreprise","nos_donnees","entreprise")->with($params);	
	}
	
	public function getEntreprise(Request $request){
		$id_entreprise = $request->get('id_entreprise');
		$entreprise = Entreprise::find( $id_entreprise);
		$entreprise_compte = $entreprise->compte()->get();
		$entreprise_type = $entreprise->type_entreprise()->get();
		$type_aliment = $entreprise->type_aliment()->get();
		$employe = $entreprise->entreprise_employe()->where('entreprise_id' , '=' , $id_entreprise  , 'and' ,'position' ,'=', '1' ,'and', 'position' ,'=', '2')
													->get();
												
		$employe_info_responsable = [] ;
		for($i = 0 ; $i < count($employe ) ; $i++){
			$employe_info_responsable [] = $employe[$i]->employe()->first();
		}
		
		$poste_employe_responsable = [] ;
		for($i = 0 ; $i < count($employe_info_responsable ) ; $i++){
			$poste_employe_responsable [] = $employe_info_responsable[$i]->poste()->first();
		}
		$adresse_entreprise = $entreprise->adresse()->get();
		
		$compte_entreprise = $entreprise->compte()->get();
		$numero_compte_entreprise = [];
		for($i = 0 ; $i < count($compte_entreprise ) ; $i++){
			$numero_compte_entreprise [] = $compte_entreprise[$i]->entreprise_compte()->first();
		}
		$pays = [] ;
		for($i = 0 ; $i < count($adresse_entreprise ) ; $i++){
			$pays [] = $adresse_entreprise[$i]->pays()->first();
		}
		$params = array(
						"entreprise"=>$entreprise,
						"type_aliment"=>$type_aliment,
						"entreprise_type"=>$entreprise_type,
						"entreprise_compte"=>$entreprise_compte,
						"poste_employe_responsable"=>$poste_employe_responsable,
						"adresse_entreprise"=>$adresse_entreprise,
						"compte_entreprise"=>$compte_entreprise,
						"numero_compte_entreprise"=>$numero_compte_entreprise,
						"employe_info_responsable"=>$employe_info_responsable,
						"pays"=>$pays,
					
					
					);
		// var_dump( count( $numero_compte_entreprise) );
		// var_dump( $poste_employe_responsable[0] );
		// die();
		return $this->render("profil_entreprise","nos_donnees","entreprise")->with($params);	
	}
	
	public function page_updateEntreprise(Request $request){
		$id_entreprise = $request->get('id_entreprise');
		$entreprise = Entreprise::find( $id_entreprise);
		$entreprise_compte = $entreprise->compte()->get();
		$entreprise_type = $entreprise->type_entreprise()->get();
		$type_aliment = $entreprise->type_aliment()->get();
		$employe = $entreprise->entreprise_employe()->where('entreprise_id' , '=' , $id_entreprise  , 'and' ,'position' ,'=', '1' ,'and', 'position' ,'=', '2')
													->get();
		$employe_info_responsable = [] ;
		for($i = 0 ; $i < count($employe ) ; $i++){
			$employe_info_responsable [] = $employe[$i]->employe()->first();
		}
		
		$poste_employe_responsable = [] ;
		for($i = 0 ; $i < count($employe_info_responsable ) ; $i++){
			$poste_employe_responsable [] = $employe_info_responsable[$i]->poste()->first();
		}
		$adresse_entreprise = $entreprise->adresse()->get();
		
		$compte_entreprise = $entreprise->compte()->get();
		$numero_compte_entreprise = [];
		for($i = 0 ; $i < count($compte_entreprise ) ; $i++){
			$numero_compte_entreprise [] = $compte_entreprise[$i]->entreprise_compte()->first();
		}
		
		$liste_type_entreprise = Type_entreprise::all();
		$liste_poste = Poste::where('type' , '1')->get();
		$liste_type_aliment = Type_aliment::all();
		$liste_pays = Pays::all();
		$params = array(
						"entreprise"=>$entreprise,
						"type_aliment"=>$type_aliment,
						"entreprise_type"=>$entreprise_type,
						"entreprise_compte"=>$entreprise_compte,
						"poste_employe_responsable"=>$poste_employe_responsable,
						"adresse_entreprise"=>$adresse_entreprise,
						"compte_entreprise"=>$compte_entreprise,
						"numero_compte_entreprise"=>$numero_compte_entreprise,
						"liste_type_entreprise"=>$liste_type_entreprise,
						"employe_info_responsable"=>$employe_info_responsable,
						"liste_poste"=>$liste_poste,
						"liste_type_aliment"=>$liste_type_aliment,
						"liste_pays"=>$liste_pays,
						
					
					);
		// var_dump( count( $numero_compte_entreprise) );
		// var_dump( $poste_employe_responsable[0] );
		// die();
		return $this->render("update_entreprise","nos_donnees","entreprise")->with($params);	
	}
	
	public function page_ajoutEntreprise(Request $request){
		
		
		
		$liste_type_entreprise = Type_entreprise::all();
		$liste_poste = Poste::where('type' , '1')->get();
		$liste_type_aliment = Type_aliment::all();
		$liste_pays = Pays::all();
		$params = array(
						
						"liste_type_entreprise"=>$liste_type_entreprise,
						"liste_poste"=>$liste_poste,
						"liste_type_aliment"=>$liste_type_aliment,
						"liste_pays"=>$liste_pays,
						
					
					);
		return $this->render("ajouter_entreprise","nos_donnees","entreprise")->with($params);	
	}
	
	public function dropEntreprise( Request $request){
		$entreprise=Entreprise::find($request->get('id_entreprise'));
		$utilisateur_entreprise=Utilisateur::find($request->get('utilisateur_id'));
		$utilisateur_entreprise->active=0;
		$utilisateur_entreprise->save();
		// $ajout=1;
		// Session::flash('choix' , $ajout);
		if ($request->get('return')==null) return back();
		else  return redirect('entreprise');
	}
	
	/**** ajout utilitaire ***/
	public function ajouterTypeAliment(Request $request ){
		$type_aliment = new Type_aliment ;
		$type_aliment->label = $request->get('label') ;
		$type_aliment->proprietaire =0;
		$type_aliment->save();
		return response($type_aliment);
		
	}
	
	public function ajouterTypeProduit(Request $request ){
		$type_produit = new Type_produit ;
		$type_produit->label = $request->get('label') ;
		$type_produit->proprietaire =0;
		$type_produit->save();
		return response($type_produit);
		
	}
	
	public function ajouterInformationNutritionnelle(Request $request ){
		$informationNutritionnelle = new Information_nutritionnelle ;
		$informationNutritionnelle->label = $request->get('label') ;
		$informationNutritionnelle->proprietaire =0;
		$informationNutritionnelle->save();
		return response($informationNutritionnelle);
		
	}
	
	public function ajouterRaisonSurplus(Request $request ){
		$raisonSurplus = new Information_nutritionnelle ;
		$raisonSurplus->label = $request->get('label') ;
		$raisonSurplus->proprietaire =0;
		$raisonSurplus->save();
		return response($raisonSurplus);
		
	}
	
	public function ajouterTypeEntreprise(Request $request ){
		$type_entreprise = new Type_entreprise ;
		$type_entreprise->label = $request->input('label') ;
		$type_entreprise->proprietaire =0;
		$type_entreprise->save();
		return response($type_entreprise);
		
	}
	
	public function ajouterPoste(Request $request ){
		$poste = new Poste ;
		$poste->label = $request->input('label') ;
		$poste->proprietaire =0;
		$poste->type =1;
		$poste->save();
		return response($poste);
		
	}
	public function ajouterPosteOrganisation(Request $request ){
		$poste = new Poste ;
		$poste->label = $request->input('label') ;
		$poste->proprietaire =0;
		$poste->type =2;
		$poste->save();
		return response($poste);
		
	}
	public function ajouterPeriodeReduction(Request $request ){
		$periode = new Periode_reduction ;
		$periode->label = $request->input('label') ;
		$periode->nombre_jour = $request->input('nombre_jour') ;
		
		$periode->save();
		return response($periode);
		
	}

	public function testePseudo(Request $request ){
		$utilisateur = Utilisateur::where('pseudo' , $request->input('pseudo'))->first() ;
		if($utilisateur!=null)
			return response($utilisateur);
		else
			return response(null);
	}
	
}
