<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    protected $list_menu = array();

    public function __construct(){
    	$this->middleware('auth');
    	

		$this->list_menu = array(
			array(
				"menu"=>array(
					"name"=>"Dashboard",
					"link"=>url("#"),
					"action"=>"dashboard",
					"icon"=>"fa-dashboard",
					"sous_menu" => array(
						array(
							"name"=>"Général",
							"link"=> url("dashboard_page"),
							"sous_action"=>"dashboard_general",
							"icon"=>"fa-align-center",
						),
						array(
							"name"=>"Entreprise",
							"link"=> url("dashboard_entreprise"),
							"sous_action"=>"dashboard_entreprise",
							"icon"=>"fa-align-center",
						),
						array(
							"name"=>"Organisation",
							"link"=> url("dashboard_receveur"),
							"sous_action"=>"dashboard_receveur",
							"icon"=>"fa-align-center",
						),
						array(
							"name"=>"Bénévole",
							"link"=> url("dashboard_benevole"),
							"sous_action"=>"dashboard_benevole",
							"icon"=>"fa-align-center",
						),
						array(
							"name"=>"Consommateur",
							"link"=> url("dashboard_client"),
							"sous_action"=>"dashboard_client",
							"icon"=>"fa-align-center",
						),
						array(
							"name"=>"Commande",
							"link"=> url("dashboard_commande"),
							"sous_action"=>"dashboard_commande",
							"icon"=>"fa-align-center",
						),
						array(
							"name"=>"Activités en cours",
							"link"=> url("dashboard_encours"),
							"sous_action"=>"dashboard_encours",
							"icon"=>"fa-align-center",
						),
					),
				)
			),
			array(
				"menu"=>array(
					"name"=>"Nos Données",
					"link"=>url("#"),
					"action"=>"nos_donnees",
					"icon"=>"fa-database",
					"sous_menu" => array(
						array(
							"name"=>"Entreprise",
							"link"=> url("entreprise"),
							"sous_action"=>"entreprise",
							"icon"=>"fa-align-center",
						),
						array(
							"name"=>"Receveur",
							"link"=> url("receveur"),
							"sous_action"=>"receveur",
							"icon"=>"fa-align-center",
						),
						array(
							"name"=>"Clients",
							"link"=> url("client"),
							"sous_action"=>"client",
							"icon"=>"fa-align-center",
						),
						array(
							"name"=>"Produits",
							"link"=> url("produit"),
							"sous_action"=>"produit",
							"icon"=>"fa-align-center",
						)
						
					),
				)
			),
			
		);
    }

    protected function render($view,$action="" , $sous_action=""){
    	
    	return view($view)->with('list_menu',$this->list_menu)->with("action",$action)->with("sous_action",$sous_action);
    }
    
}
