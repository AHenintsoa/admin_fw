<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AccueilController extends BaseController
{
    protected $action = "Accueil";

    public function __construct(){
		$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
		// return $this->render("accueil",$this->action);
		return redirect('/dashboard_page');
    }
}
