<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Utilisateur;
use App\Models\Membre_fw;
use App\Models\Pays;
use App\Models\Poste;
use App\Models\Tache;
use App\Models\Utilisateur_type;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use App\Exports\UtilisateurExport;
use App\Imports\UtilisateurImport;
use Maatwebsite\Excel\Facades\Excel;
//use Illuminate\Support\Facades\Excel;
//use DB;


class MonEspaceController extends BaseController
{
	

    public function __construct(){
    	parent::__construct();
		$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 public function export() 
    {
		
        return Excel::download(new UtilisateurExport, 'utilisateur.xlsx');
    }
	
	public function importFile(Request $request){
		 Excel::import(new UtilisateurImport,request()->file('donnee'));
        return back();
    } 

  
	public function page_update_tache(Request $request) {
		// var_dump($request->all());
		$tache= Input::all();
		$liste_membre_fw = Membre_fw::all();
		$params = array(
			"tache"=> $tache ,
			"liste_membre_fw" => $liste_membre_fw,
		 );
		// var_dump($params);
 		return $this->render('update_utilisateur',"")->with($params);;
		
   } 
  
  public function page_password() {
		return $this->render('password_page',"");
		
   } 
   
   public function page_update_utilisateur(Request $request) {
		$profile= Input::all();
		$membre_detail = Membre_fw::where('id_membre_fw' , $request->get('id_membre_fw'))-> first();
		$poste = Poste::where('type','3')->get(); // only fw
		 $pays=Pays::all();
		 $utilisateur = $membre_detail->utilisateur;
		 $utilisateur_type = Utilisateur_type::all();
		$params = array(
			"profile"=> $profile ,
			"utilisateur_type"=> $utilisateur_type ,
			"poste" => $poste,
			"pays"=> $pays ,
		 );
		// var_dump($params);
 		return $this->render('update_utilisateur',"")->with($params);
		
   } 
	 
	 
    public function index(Request $request){

		$id_utilisateur = $request->session()->get('id_utilisateur');
		 
		 $membre_detail = Membre_fw::where('utilisateur_id' , $id_utilisateur)-> first();
		 if($membre_detail==null){
			 return redirect('logout');
		 }
		 $poste=$membre_detail->poste;
		 $pays=$membre_detail->pays;
		 $utilisateur = $membre_detail->utilisateur;
		 $utilisateur_type = $utilisateur->utilisateur_type;
		 
		 $liste_utilisateur_type = Utilisateur_type::all();
		  $liste_pays = Pays::all();
		 $liste_poste = Poste::where('type','3')->get(); // only fw
		// $liste_membre = Membre_fw::all();
		 $liste_membre = DB::table('membre_fw')
            ->join('utilisateur', 'utilisateur.id_utilisateur', '=', 'membre_fw.utilisateur_id')
            ->join('utilisateur_type', 'utilisateur.utilisateur_type_id', '=', 'utilisateur_type.id_utilisateur_type')
			->leftJoin('poste', 'membre_fw.poste_id', '=', 'poste.id_poste')
			->leftJoin('pays', 'membre_fw.pays_id', '=', 'pays.id_pays')
            ->select('membre_fw.id_membre_fw','membre_fw.nom' , 'membre_fw.email' ,'membre_fw.tel' , 'membre_fw.logo' ,'membre_fw.utilisateur_id' , 'utilisateur.utilisateur_type_id as role_id', 'membre_fw.poste_id', 'membre_fw.pays_id', 'utilisateur_type.label as role' , 'pays.label as pays', 'poste.label as poste','pays.id_pays as pays_id', 'poste.id_poste as poste_id' )
			->where('active' , '1')
			->get();
		$liste_tache = DB::table('tache')
            ->leftJoin('membre_fw', 'tache.utilisateur_id', '=', 'membre_fw.utilisateur_id')
            ->select('tache.id_tache' ,'tache.label' ,'tache.description' ,'tache.objectif' , DB::raw( 'DATE_FORMAT(tache.date_assignation ,  "%d-%b-%Y") as date_assignation' ), DB::raw( 'DATE_FORMAT(tache.date_echeance ,  "%d-%b-%Y") as date_echeance' ), 'membre_fw.nom' )
			->get();	
		$choix=0;
		 $params = array(
			"membre_detail"=> $membre_detail ,
			"utilisateur_type" => $utilisateur_type,
			"pays" => $pays,
			"poste" => $poste,
			"liste_utilisateur_type" => $liste_utilisateur_type ,
			"liste_poste"=>  $liste_poste ,
			"liste_pays"=>  $liste_pays ,
			"liste_membre"=>  $liste_membre ,
			"liste_tache"=>  $liste_tache ,
		 );
		// Session::flash('choix' , 0);
		// var_dump($liste_tache_non_assigne);
		return $this->render("mon_espace","")->with($params);
    }
	
	public function resetPassword(Request $request){
		
		$validator = Validator::make($request->all(), [
			'password' => 'required|string|min:6|confirmed',
        ]);
		 if ($validator->fails()) {
			 Session::flash('password' , 'mots');
            return back()->withErrors($validator)->withInput();
        }
		$id_utilisateur= $request->session()->get('id_utilisateur');
		$utilisateur = Utilisateur::find($id_utilisateur);
		$utilisateur->password = Hash::make($request->get('password'));
		$resultat = $utilisateur->save();
		
		Session::flash('password_succes', "Mot de passe changé");
		return back();
	}
	
	public function ajouterTache(Request $request){
		$ajout=1;
		$validator = Validator::make($request->all(), [
            'objectif' => 'bail|required',
			 'objectif' => 'bail|required',
			'description' => 'bail|required',
			'date_assignation' => 'bail|required',
            'date_echeance' => 'bail|required',
            
        ]);
		 if ($validator->fails()) {
			 Session::flash('choix' , $ajout);
            return back()->withErrors($validator)->withInput();
        }
		$tache = new Tache;
		$tache->label = $request->get('label');
		$tache->description = $request->get('description');
		$tache->objectif = $request->get('objectif');
		$tache->utilisateur_id = $request->get('utilisateur_id');
		$tache->date_assignation = $request->get('date_assignation');
		$tache->date_echeance = $request->get('date_echeance');
		$resultat = $tache->save();
		
		Session::flash('choix' , $ajout);
		return back();
	}
	
	public function uploadImage(Request $request) {
      $file = $request->file('logo');
      $destinationPath = public_path('img/upload_profil');
      $file->move($destinationPath,$file->getClientOriginalName());
   }

	public function ajouterUtilisateur(Request $request){
		$ajout=2;
		$validator = Validator::make($request->all(), [
            'pseudo' => 'bail|required|unique:utilisateur',
			'nom' => 'bail|required',
			'telephone' => 'bail|numeric|min:7|nullable',
            'password' => 'required|string|min:6|confirmed',
            
        ]);
		 if ($validator->fails()) {
			 Session::flash('erreur_ajouter_profile' , 'err');
			 Session::flash('choix' , $ajout);
            return back()->withErrors($validator)->withInput();
        }
		
		$utilisateur = new Utilisateur;
		$password=Hash::make( $request->get('password') );
		$utilisateur->pseudo = $request->get('pseudo');
		$utilisateur->password = $password;
		
		$utilisateur->utilisateur_type_id = $request->get('utilisateur_type_id');
		$utilisateur->save();
		$id_utilisateur = $utilisateur->id_utilisateur;
		$membre = new Membre_fw;
		$membre->nom = $request->get('nom');
		$membre->logo = 'upload_profil/profil.png';
		$membre->email = $request->get('email');
		$membre->poste_id = $request->get('poste_id');
		$membre->pays_id = $request->get('pays_id');
		$membre->utilisateur_id = $id_utilisateur;
		$membre->poste_id = $request->get('utilisateur_type_id');
		$membre->save();
		
		Session::flash('choix' , $ajout);
		return back();
		//return back()->with("choix",$ajout);
		//return redirect('/show_profile');
	}
	
	public function updateProfile( Request $request){
		
		$validator = Validator::make($request->all(), [
            'nom' => 'bail|required',
			'poste_id' => 'bail|required',
			'tel' => 'bail|numeric|min:7',
        ]);
		 if ($validator->fails()) {
			 
			Session::flash('erreur_update_profile' , 'err');
            return back()->withErrors($validator)->withInput();
        }
		
	  //var_dump($request->all());
	  // var_dump($request->file('logo'));
	  // die();
		$profile=Membre_fw::find($request->get('id_membre_fw'));
		$profile->nom = $request->get('nom');
		if( $request->file('logo')){
			
			$this->uploadImage($request);
			$file_name= $request->file('logo')->getClientOriginalName();
			$profile->logo = 'upload_profil/'.$file_name;
		}
		
		$profile->email = $request->get('email');
		$profile->tel = $request->get('tel');
		$profile->poste_id = $request->get('poste_id');
		$profile->pays_id = $request->get('pays_id');
		
		$resultat= $profile->save();
		//var_dump($resultat);
		//$ajout=2;
		//Session::flash('choix' , $ajout);
		return back();
		//return redirect('/show_profile');
	}
	
	public function updateUtilisateur( Request $request){
		$validator = Validator::make($request->all(), [
        'nom'=>'required',
		'tel'=>'numeric|min:7|nullable',
      ]);
	  if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
	  
		$profile=Membre_fw::find($request->get('id_membre_fw'));
		$profile->nom = $request->get('nom');
		$profile->email = $request->get('email');
		$profile->tel = $request->get('tel');
		$profile->poste_id = $request->get('poste_id');
		$profile->pays_id = $request->get('pays_id');
		
		$resultat= $profile->save();
		//var_dump($resultat);
		$ajout=2;
		Session::flash('choix' , $ajout);
		return redirect('show_profile');
		//return redirect('/show_profile');
	}
	public function updateTache( Request $request){
		$validator = Validator::make($request->all(), [
        'label'=>'required',
		'description'=>'required',
		'objectif'=>'required',
		'date_assignation'=>'required',
		'date_echeance'=>'required',
      ]);
	  if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
		$tache=Tache::find($request->get('id_tache'));
		$tache->label = $request->get('label');
		$tache->description = $request->get('description');
		$tache->objectif = $request->get('objectif');
		$tache->utilisateur_id = $request->get('utilisateur_id');
		$tache->date_assignation = $request->get('date_assignation');
		$tache->date_echeance = $request->get('date_echeance');
		
		$resultat= $tache->save();
		
		$ajout=1;
		Session::flash('choix' , $ajout);
		//return back();
		return redirect('show_profile');
	}
	
	public function dropUtilisateur( Request $request){
		$utilisateur=Utilisateur::find($request->get('utilisateur_id'));
		$utilisateur->active =  0;
		$utilisateur->save();
		$ajout=2;
		Session::flash('choix' , $ajout);
		return back();
	}
	public function dropTache( Request $request){
		$tache=Tache::find($request->get('id_tache'));
		$tache->delete();
		$ajout=1;
		Session::flash('choix' , $ajout);
		return back();
	}
	public function getTache( Request $request){
		$tache = DB::table('tache')
            ->leftJoin('membre_fw', 'tache.utilisateur_id', '=', 'membre_fw.utilisateur_id')
            ->select('tache.id_tache' ,'tache.label' ,'tache.description' ,'tache.objectif' ,'tache.date_assignation' , 'tache.date_echeance' , 'membre_fw.nom' , 'tache.utilisateur_id')
			->where('tache.id_tache', $request->get('id_tache'))
			->first();
			
			//var_dump($tache);
			//die();
		return response((array)$tache);
		
		//return redirect('/show_profile');
	}
}
