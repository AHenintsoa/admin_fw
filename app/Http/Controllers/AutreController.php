<?php

namespace App\Http\Controllers;

use App\Models\Entreprise;
use App\Models\Entreprise_type;
use App\Models\Type_entreprise;
use App\Models\Employe;
use App\Models\Poste;
use App\Models\Type_aliment;
use App\Models\Utilisateur;
use App\Models\Organisation;
use App\Models\Organisation_employe;
use App\Models\Type_organisation;
use App\Models\Adresse;
use App\Models\Pays;
use App\Models\Adresse_entreprise;
use App\Models\Entreprise_type_aliment;
use App\Models\Donation;
use App\Models\Entreprise_employe;
use App\Models\Consommateur;
use App\Models\Produit;
use App\Models\Photo_produit;
use App\Models\Type_produit;
use App\Models\Strategie_reduction;
use App\Models\Equipement;
use App\Models\Periode_recuperation_commande;
use App\Models\Produit_equipement;
use App\Models\Information_nutritionnelle;
use App\Models\Produit_information_nutritionnelle;
use App\Models\Raison_donation;
use App\Models\Raison_surplus;
use App\Models\Periode_reduction;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
Use Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\Exports\ReceveurExport;
use App\Exports\ClientExport;
use App\Exports\EntrepriseExport;
use App\Exports\ProduitExport;

// use Maatwebsite\Excel\Facades\Excel;
// use DB;
use Excel ;
use Swift_TransportException ;
/*use Exception ;
*/

class AutreController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->render("map","");	
    }
	
	/** recherche */
	
	/****  Autres fonctionnalites   *****/

	public function updateEtatDonation(Request $request){
		$donation = Donation::find($request->get('id_donation'));
		//$donation = Donation::find(6);
		$donation->etat_donation_id = $request->get('id_etat_donation') ;
		//$donation->etat_donation_id = 1 ;
		$donation ->save();
		
		//notifier entreprise
		$entreprise = $donation->entreprise; 
		$entreprise_email = $entreprise->entreprise_employe()->where('position' , '1')->first()->employe->email ;
		$sujet= 'Etat de votre donation';
		$etat_notification1 = 1; 
		$data = array (
				"entreprise" => $entreprise,
				"etat_donation_id" => $request->get('id_etat_donation') ,
				
			) ;
		try {
			$resultat = Mail::send('emails.notifier_entreprise_etat_donation', $data, function ($message) use( $entreprise_email , $sujet  )  {
					 $message->subject($sujet);
					$message->to($entreprise_email);
					
				});
		} catch (Swift_TransportException $e) {
		 	$etat_notification1 = 0; 
		 }	
		//notifier organisation
		$organisation = $donation->organisation; 
		$organisation_email = $organisation->organisation_employe()->where('position' , '1')->first()->employe->email ;
		$sujet= 'Etat de votre donation';
		$etat_notification2 = 1; 
		$data = array (
				"organisation" => $organisation,
				"etat_donation_id" => $request->get('id_etat_donation') ,
				
			) ;

		 try {
		 	$resultat = Mail::send('emails.notifier_organisation_etat_donation', $data, function ($message) use( $organisation_email , $sujet  )  {
				 $message->subject($sujet);
				//$message->to($organisation_email);
				$message->to($organisation_email);
				
			});
		 } catch (Swift_TransportException $e) {
		 	$etat_notification2 = 0; 
		 }
		if($etat_notification1==1 & $etat_notification2==1){
			$donation ->save();
		}
		$data = array(
			"etat_donation_label"=> $donation->etat_donation->label  ,
			"etat_notification_entreprise"=> $etat_notification1 , // 0 echec
			"etat_notification_organisation"=> $etat_notification2 , // 0 echec

			);
		
		
		return response($data);
	}
	
	
}
